# **SoPE** #

## Introduction ##
***
SoPE is a framework for estimating software performance. In a nutshell, it does so by analyzing the code-of-interest and looking up any benchmark codes that have similar characteristics. If such benchmarks are found, their performance metrics are used to estimate the code-of-interest performance. For more details, please refer to the paper and the code in this repository.

Please note that this repository is changing constantly as new features are added and issues are resolved. The status of each of the substems is shown in the following table.

| Subsystem             | SLOC    | Status   |
| ----------------------|---------|----------|
| Performance Estimator | ~10800  | Underway |
| Knowledge Base        | ~2300   | Underway |
| Benchmarker           | ~2100   | Done     |
| Clang Interfacing     | ~2700   | Done     |

## Pre-requisites ##
***
* Clang 3.7.1: Also tested for 3.8.0. Later versions may not work because some of the Clang interfaces are not guaranteed to stay the same. You should compile clang with exceptions enabled, which in turn needs RTTI to be enabled (LLVM_ENABLE_EH:BOOL=ON and LLVM_ENABLE_RTTI:BOOL=ON).
* Unix OS.
* GCC 4.8.4: or any other version that supports c++11. You will need this to compile the code-of-interest and link with capture libraries. Later on, this requirement will be removed as we move all the parts of the framework to use Clang.
* Capture and Injector libraries: The Benchmarking tool runs the injector pass to instrument the code (inserts capture hooks into the code's IR). At that point, the code's IR contains calls to function declarations that have no bodies. In order for that code to be assembled and run, the definition for those functions have to be linked. These definition are in the Metric Capture library. You will need to specify the locations of both the injector and metric capture libs in the [config.json](config.json) file
    * You can find both libraries [HERE](https://github.com/eelaaraj/metric-capture-injector). This is a CMake project and therefore the necessary changes to the compiler paths will be needed.

## Setup ##
***
SoPE is implemented as a Clang tool, and therefore can be used to preprocess your code. If any compile errors exist, the tool will run, report the errors and exit. If no errors exist, the tool will continue and go through the various steps of analysis. For now, SoPE only supports basic c++ codes such as the demo examples in numerical analysis. More language features will be added later but are not currently on the roadmap.

To run SoPE, you need to edit the following:

1. makefile: the makefile in this repository contains everything you need to compile SoPE. You will need to change the Clang source and binary paths as well as include paths.
2. compile_commands.json: Clang uses this file to identify the files to run. You will need to change the paths to the test files as well as the clang paths.
3. SoPE can be ran as follows:
```
#!bash

sope -p . test/someTestFile.cpp test/folder/anotherTestFile.cpp -functions="Function1ToAnalyze, Function2ToAnalyze"
```

### Notes: ###
* We recommend running the demo examples as is since they have been curated to showcase the capabilities of the framework. These also adhere to the list of supported language features.
* The Benchmarker/injector tool is not fully integrated in this repository due to the fact that it uses RTTI which is disabled in the makefile. In the demos, we do not perform automatic injection, but rather manually insert the capture hooks. This still uses the capture libraries but not in a fully automatic way. This has been implemented for the benchmarker and will be integrated soon.
* A lot of the code is still being tested and some new features added (such as logging). In these cases, some files might contain commented out code. These will be removed shortly.

### Contact ###
For any questions, please contact E ElAaraj at syr dot edu
