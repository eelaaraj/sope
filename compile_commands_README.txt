This readme explains the compilation database json object entries.
Example compile_cmmands.json

[
{
  "directory": "/tmp",
  "command": "gcc div0.c",
  "file": "/tmp/div0.c"
},
{
  "directory": "/tmp",
  "command": "gcc -DFOO div0.c",
  "file": "/tmp/div0.c"
}
]

It's just a list of entries, each of which consists of these fields:

    File: the file to which the compilation applies. This filename is just a key into the compilation database. So it doesn't even
            have to be a valid thing (I think). Though in my file, I keep it valid with a valid relative path too.
    Command: the exact compilation command used
    Directory: the directory from which the compilation is executed [3]. In the pevious example, its shows the compilation is running
            in the temp directory. I tried doing the same as done here and do it in my compile_commands file and it worked for analysis.
            It failed though on Replacements tool. It kept complaining there was no test3.cpp file. Im assuming that the directory had
            to be the working directory which is the build directory. So what i did, is i fixed all direcotry entries in the compile_commands
            file to be the build directory which is the top projct directory ClangSimilarityFinder, and then I specified the file and command
            as relative from that path. This worked for both analysis and replacements.

As you can see above, there may be multiple entries for the same file. This is not a mistake - it's entirely
plausible that the same file gets compiled multiple times inside a project, each time with different options.

REFERENCE:
=========
http://eli.thegreenplace.net/2014/05/21/compilation-databases-for-clang-based-tools
