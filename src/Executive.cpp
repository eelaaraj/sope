/*
 * Executive.cpp
 *
 * Description: FndNamedClassVisitor is a recursive visitor used to navigate the clang AST (abstract syntax tree) and
 *				extract the relevant information to be used in creating patterns and sequences. The main approach is that
 *				the user, OR an automatic tool, will select a certain function to analyze. The visitor will exclude all
 *				other functions from the traversal. In that function we will look for loops, binary operations, and all associated
 * 				variables. The difficulty in usign the clang AST with the recursive visitor is that there is not way to
 * 				track scopes besides using the AST and checking recursively (using a parentMap) if the current node is a
 * 				child of a certain other node. Thus we need to have a lot of state variables to keep track of the current
 * 				operation and the previous operation as well as previous major operations(these being loops, and assignment
 * 				operators, and the function body which happens to be a compound statement). An exampel of that is: assume
 * 				we have two nested loops. When we use recursive visitor, we only use the Visit* functions like VisitForStmt
 * 				to visit each and every FORSTMT in the AST. Each of these ForStmt is treated as a single statement without any
 * 				information about its scope, parent,etc... Thus we have to take care of that. The same happens in an even more
 * 				complex way when we have nested binary operations. It gets complicated to keep track of what is the major parent
 * 				(an assignment operator in this case most of the time).
 * 				To do all this we have kept three important stacks, one to keep track of the major parents of the operation. Another
 * 				is a wrapper stack which wraps Clang objects into Operation objects, this is mainly used to create the patterns. Any
 * 				time an operation is pushed, we check if its in the same scope as the previous operation (or a child scope of the
 * 				previous operaion) by checking if they have the same major parent or not. In the case its not, we start processing
 * 				all the operations on the stack until we reach an operation which has the same parent as the new operation being
 * 				pushed on the stack.
 * 				As for popping operations, this is triggered when pushing variables on the stack. If the previous operation is
 * 				a binary operation,then we wait for its two operands and pop and process the operation with its operands and
 * 				push it back on the stack (which now can become an operand to a parent binary operation). This stops when the
 * 				operation has been processed and the new operation being pushed is not part of the previous operation (such as a new
 * 				assignment operator or loop or even when the new oepration is part of a higher scope).
 * 				So to sum it up, pushOperation checks if the operation is in the same scope or not and if its in a higher scope
 * 				will trigger stack pops and processing of the popped operations until we hit an operation with the same scope as
 * 				this new operation being pushed on the stack. And on every pushVariable, we trigger processign of binary oeprations
 * 				until the current major binary operation is processed. This major binary operations is then probably a loop
 * 				statement. This does not get popped until the whole scope of that loop gets popped when a new operation belonging
 * 				to a higher scope is being pushed on the stack. Ex: for { for { a = b+c } d = e + f; }. In this case we push for
 * 				then for again (because its a child scope). Then we push =, then a, then +, then b, then c, then pop b and c and process +,
 * 				then push (b+c), then we pop a, and pop b+c because both operands to = have been processed. a is a simple variable and thus
 * 				processed naturally, and we just processed b+c. So we process = and then push it again as (a=b+c). Now we come to push =
 * 				but the scope of this is higher than the scope of the previous operation (a=b+c). Thus we keep popping until we reach
 * 				the second for loop which has the same scope as d=e+f. By popping a=b+c, we add it as a statement to the second for
 * 				loop object (if the for loop had more statements, we do the same thing to those by adding them to the statements
 * 				data structrue in forloop). So we do the same to process the binary operation d=e+f and then push the = operation as
 * 				(d=e+f). Now no more operations exist and thus we finish processing. Becuase there is not way to say if the processing
 * 				ended, we trigger the last piece of processing of what is left on the stack in the class destructor. We pop each scope
 * 				and add the statemets to its corresponding parent until the stack is empty. Thus we pop (d=e+f) and the second for loop
 * 				and add them as statements of the first for loop. Then we pop the first for loop. At this point we have one point of entry
 * 				to any sort of processing we want to do. If we want to traverse the extracted tree we just traverse this last for loop
 * 				(or basically the last statement(s) that we pop form the stack). I do this as an example by traversing in-order and
 * 				traversing post-order. I also traverse the extracted tree post-order and use a function pointer as an argument to the
 * 				traversal function. This helps me do anthing while traversing each node. I can enunciate, or even create a pattern
 * 				while i'm walking a tree.
 * 				Some notes: Keep in mind the the tree structrue we get is similar to the clang AST. the only reason we needed to walk
 * 				the AST was to wrap operations around clang objects so that we can use our KMP pattern comparison classes. AND the main
 * 				reason is because we didnt need all the bloat in the clang AST like variable declarations and other function calls. So
 * 				we extract what we need and leave the rest.
 *
 *  Created on: May 4, 2015
 *      Author: heisenberg
 */
#include <string>
#include <vector>
#include <iostream>
#include "clang/Tooling/Tooling.h"
#include "clang/Tooling/Refactoring.h"
#include "clang/Tooling/CommonOptionsParser.h"
#include "clang/Rewrite/Frontend/Rewriters.h"
#include "clang/Rewrite/Core/Rewriter.h"
#include "ClangOperationWrappers.h"
#include "utils/OperationHelpers.h"
#include "Repository.h"
#include "./proc/ClangASTProcessor.h"
//#include "./clang_pass1/Pass1Visitor.h"
#include "./clang_pass2/Pass2Visitor.h"
#include "utils/Utils.h"
#include <stack>
#include "PatternTreeConsumer.h"
#include "knowledge_base/MockUtils.h"
#include "SimilaritySearch.h"
#include "SopeAction.h"
#include "LookupProcessor.h"
#include <spdlog/spdlog.h>

using namespace clang;
using namespace clang::tooling;
using namespace pattern::matching::wrapper;
using namespace pattern::processing;

static llvm::cl::OptionCategory MyToolCategory("Visitor Sample");
static llvm::cl::opt<std::string> functionNamesCommaSeperated("functions", llvm::cl::cat(MyToolCategory));
static llvm::cl::opt<bool> injectOption("inject", llvm::cl::cat(MyToolCategory));


/* Benchmark a code and stores it in the knowledge base. Usually
 * the process goes as follows: 1- Use clang to parse the code into AST
 * 2- Use our ASTprocessors to traverse the AST and create the operations
 * and the PatternTree. 3- Instrument the code in the right places right
 * infront of the loops. 4- Run the instrumented executable which will capture
 * the necessary variables. 5- Create a record for the benchmarked code in the DB/KB
 * which contains its serialized AST, pattern tree, hashtree and the captured
 * data
 */
void sopeBenchmark(const std::string& filename, const std::string& fcnName) {

}

/* Tries to match the COI to a code in the Knowledge base. The process goes as
 * follows: 1- Use clang to parse the code into AST. 2- Use our ASTprocessors
 * to traverse the AST and create the operations and the PatternTree.
 * 3- Query the KB using our pattern tree, and hashtree. 4- If a match exists,
 * the KB will return a Queryatch object which contains the benchmark that matches
 * with the COI. It also contains the node at which the two codes match. 5- Use the
 * matching benchmark ID to query the metrics of that benchmark. 6- Once the metrics
 * are returned, the estimator might interpolate the missing metrics in order to
 * generate a final performance estimate.
 */
void sopeLookup(const std::string& filename, const std::string& fcnName) {

}

int main(int argc, const char **argv) {
    auto logger = spdlog::stdout_logger_st("sopelogger", true);
    // This way we can run our tool on an arbitrary string code for quick tests.
    /*if (argc > 1) {
        clang::tooling::runToolOnCode(new FindNamedClassAction, argv[1]);
    }*/
    logger->set_level(spdlog::level::debug);
    logger->set_pattern("[%n] [%l] %v");
    logger->info("info\nanother line");
    logger->error("error");
    logger->critical("critical");
    logger->warn("warning");
    logger->debug("debug");

    for(int i=0; i<argc; i++) {
        //std::cout << "Arg[" << i << "] = " << argv[i] << std::endl;
        spdlog::get("sopelogger")->debug("Arg[{}] = {}", i, argv[i]);
    }
    /* The tool will accept the cl::opt (clang options) defined at the beginning of this file.
     * -p :         specifies the path to directory where the clangTool should look for the source files
     *              (which are specified after the path): ex: -p ./ file1.cpp file2.cpp
     *
     * -functions : specifies the names of the functions that we are going to search for and
     *              analyze. This is a comma seperated string that we will need to parse and
     *              extract individual function names from.
     *
     * -inject :    specifies whether we just want to parse and create pattern trees and sequences
     *              out of this file OR Do all of the above + inject metric capture code.
    */
    std::string fileName;
    std::vector<std::string> funcNames;
    bool inject;


    CommonOptionsParser OptionsParser(argc, argv, MyToolCategory);
    // Using refactoringTool rather than ClangTool because refactoringtool allows us
    // to getreplacements and make changes and then save those changes into the file or
    // a new file. This is needed when we want to inject source code.
    RefactoringTool Tool(OptionsParser.getCompilations(),
                         OptionsParser.getSourcePathList());

    inject = injectOption.getValue();
    StringRef commaSepStr(functionNamesCommaSeperated.getValue());
    SmallVector<StringRef, 3> sepStrings;
    commaSepStr.split(sepStrings, StringRef(","));
    //std::cout << sepStrings.size() << std::endl;
    spdlog::get("sopelogger")->debug("Passing {} function(s)", sepStrings.size());
    for(auto s: sepStrings) {
        funcNames.push_back(s.str());
        //std::cout << s.str() << std::endl;
        spdlog::get("sopelogger")->debug("Function = {}", s.str());
    }

    int returnVal;
    {
        std::shared_ptr<Repository<operation>> spRepo(new Repository<operation>());
        //std::cout << "Repository use count = " << spRepo.use_count() << std::endl;
        spdlog::get("sopelogger")->debug("Repository use count = {}", spRepo.use_count());
        //std::cout << "Repository uniqueness = " << spRepo.unique() << std::endl;
        spdlog::get("sopelogger")->debug("Repository uniqueness = {}", spRepo.unique());

        // Pass 1 is ran first to collect all the function calls within a particular set of
        // function declarations. Once we collect all those funtion calls recursively,
        // we add them to the list of functions to be analyzed in the third pass
        /*
        std::cout << std::endl << "===========================================================================================" << std::endl;
        std::cout << "Running FindCallsPass: Collect all called function from within the user-requested functions" << std::endl;
        std::cout << "===========================================================================================" << std::endl;
        ClangTool pass1Tool(OptionsParser.getCompilations(), OptionsParser.getSourcePathList());
        std::shared_ptr<FindCallsActionFactory<operation>> callsActionFactory =
                std::shared_ptr<FindCallsActionFactory<operation>>(new FindCallsActionFactory<operation>(fileName, funcNames, spRepo, inject));
        pass1Tool.run(callsActionFactory.get());

        for(auto funcName: funcNames) {
            std::cout << funcName << std::endl;
        }*/

        // The 2nd pass will parse the functions and create a Def-Use chain from the
        // variables in that function
        /*
        std::cout << std::endl <<  "============================================================================" << std::endl;
        std::cout << "Running DefUsePass: to analyze data-dependencies and generate def-use chains" << std::endl;
        std::cout << "============================================================================" << std::endl;
        DefUse* DU = nullptr;
        //std::cout << "DU before init = " << DU << std::endl;
        ClangTool pass2Tool(OptionsParser.getCompilations(), OptionsParser.getSourcePathList());
        std::shared_ptr<BuildDefUseActionFactory<operation>> defUseActionFactory =
                std::shared_ptr<BuildDefUseActionFactory<operation>>(new BuildDefUseActionFactory<operation>(&DU, fileName, funcNames, spRepo, inject));
        pass2Tool.run(defUseActionFactory.get());
        // We can't pass DU to the next pass because the references that it holds to declrefExp and varDecls have
        // been destroyed by now when the tool pass2Tool is destroyed. Thus we will need to buildDefChains in
        // every pass that requires it.
        //std::cout << "DU address = " << DU << std::endl;
        if(DU!=nullptr)
            delete DU;
        else
            std::cout << "Defuse is null" << std::endl;
        */

        // The 3rd pass will parse the collected functions, build the patterns,
        // and the pattern-trees
        //std::cout << std::endl <<  "===========================================================================================" << std::endl;
        //std::cout << "Running PatternTreeBuilderPass: to generate a miniature AST and a corresponding PatternTree" << std::endl;
        //std::cout << "===========================================================================================" << std::endl;
        spdlog::get("sopelogger")->debug("===========================================================================================");
        spdlog::get("sopelogger")->debug("Running PatternTreeBuilderPass: to generate a miniature AST and a corresponding PatternTree");
        spdlog::get("sopelogger")->debug("===========================================================================================");
        std::shared_ptr<MyASTActionFactory<operation,LookupProcessor>> actionFactory = std::shared_ptr<MyASTActionFactory<operation,LookupProcessor>>(new MyASTActionFactory<operation,LookupProcessor>(fileName, funcNames, spRepo, &Tool.getReplacements(), inject));
        //std::cout << "factory references = " << actionFactory.use_count() << "\n";
        spdlog::get("sopelogger")->debug("factory references = {}", actionFactory.use_count());
        returnVal = Tool.runAndSave(actionFactory.get());
        //std::cout << "factory references = " << actionFactory.use_count() << "\n";
        spdlog::get("sopelogger")->debug("factory references = {}", actionFactory.use_count());

        //std::cout << "Repository use count = " << spRepo.use_count() << std::endl;
        //std::cout << "Repository uniqueness = " << spRepo.unique() << std::endl;
        spdlog::get("sopelogger")->debug("Repository use count = {}", spRepo.use_count());
        spdlog::get("sopelogger")->debug("Repository uniqueness = {}", spRepo.unique());

        Repository<operation>::PatternStore pStore = spRepo->getPatterns();
        Repository<operation>::PatternTreeStore pTreeStore = spRepo->getPatternTrees();
        //std::cout << "Repo contains "<< pStore.size() << " patterns" << std::endl;
        spdlog::get("sopelogger")->debug("Repo contains {} patterns", pStore.size() );
        for(auto item : pStore) {
            //std::cout << "Pattern size: " << item.size() << std::endl;
            spdlog::get("sopelogger")->debug("Pattern size: {}", item.size());
        }

        // Added an AST Store to the repository. Testing that functionality here. Can be removed once tested.
        /*
        Repository<operation>::ASTStore astStore = spRepo->getASTs();
        std::function<bool(std::shared_ptr<operation>)> predicateAlwaysTrue = [] (std::shared_ptr<operation> toCheck) { return true; };
        for(auto item : astStore) {
            item->traverseInOrder(printOperation, printFormatter, predicateAlwaysTrue, item);
        }
        */
        // End of testing AST Store

        /* Process to match the code of interest:
         * 1- Run the parser action which transforms code into our version of
         *    an AST (a tree of operations) and a pattern tree.
         * 2- Run the matcher on the AST and/or PatternTree and return the result.
         *    The result will contain which tree(s) in the Kowledge base are similar,
         *    particularly which subtrees in them match to subtees in the query tree.
         *    The subtrees are basically a Stmt or a expression. These stmts are part
         *    of a certain block in the CFG of the code. The blocks of these statements
         *    have to be known if we are to do dynamic analysis to capture loop trip counts
         *    and other important variables.
         * 3- Run the Def-Use analysis to figure out which variables are used by the blocks
         *    of those matches retrieved from step 2. These variables will be the variables
         *    that have to be captured.
         * 4- Run the injection pass. This pass will inject the capture functions in the
         *    right places.
         */
        //std::cout << "\nComparing patterns with each other..." << std::endl;
        spdlog::get("sopelogger")->debug("Comparing patterns with each other...");
        for(size_t i=0; i< pStore.size(); i++) {
            KMP<operation> kmp(pStore[i]);
            for(size_t j=0; j<pStore.size(); j++) {
                //std::cout << "-- Comparing patterns " << i << " and " << j << std::endl;
                spdlog::get("sopelogger")->debug("-- Comparing patterns {} and {}", i, j);
                SearchOutput searchResult = kmp.search(pStore[j]); // Argument to search is a sequence. The passed in parameter is a pattern. This should call the promotion cstr of sequence implicitly
                const int* partialMatches = kmp.getPartialMatches();
                //std::cout << searchResult << std::endl;
                std::ostringstream oss;
                oss << searchResult;
                spdlog::get("sopelogger")->info("{}", oss.str()); // had to write SearchOutput to stream. debug() didn't compile with searchOutput usin operator<< as the documentaiton states in spdlog
                std::ostringstream partialMatchesStream;
                for (int k = 0; k < pStore[j].size(); k++) {
                    //std::cout << partialMatches[k] << ", ";
                    partialMatchesStream << partialMatches[k] << ", ";
                }
                spdlog::get("sopelogger")->debug("{}", partialMatchesStream.str());
                //std::cout << std::endl;
                spdlog::get("sopelogger")->debug("\n");
            }
        }
        //std::cout << "\nFinished comparing patterns with each other." << std::endl;
        spdlog::get("sopelogger")->debug("Finished comparing patterns with each other.");

        //std::cout << "\n\nComparing trees with each other..." << std::endl;
        spdlog::get("sopelogger")->debug("Comparing trees with each other...");

        //std::cout << "We have " << pTreeStore.size() << " trees" << std::endl;
        spdlog::get("sopelogger")->debug("We have {} trees", pTreeStore.size());
        for(size_t i=0; i< pTreeStore.size(); i++) {
            //std::cout << "-- Hashing Tree " << i << std::endl;
            spdlog::get("sopelogger")->debug("-- Hashing Tree {}", i);
            // Note we use index i rather than j because for now the fingerprintSearchFunctor
            // only hashes a trees BUT does not do searches. So we are only hashing and not comparing two trees
            FingerprintSearchFunctor<operation> hashSearch(pTreeStore[i], nullptr);
            hashSearch();
            for(size_t j=i+1; j<pTreeStore.size(); j++) {
                //std::cout << "-- Comparing Tree " << i << " and " << j << std::endl;
                spdlog::get("sopelogger")->debug("-- Comparing Tree {} and {}", i, j);
                SearchFunctor<operation> searchFunc(pTreeStore[j]); // Argument to search is a sequence. The passed in parameter is a pattern. This should call the promotion cstr of sequence implicitly
                //SearchFunctor<operation> searchFunc(*(pTreeStore[j].root()->getPattern().get())); // Argument to search is a sequence. The passed in parameter is a pattern. This should call the promotion cstr of sequence implicitly
                std::function<void(std::shared_ptr<IPatternNode>&,
                                   std::function<void(std::shared_ptr<operation>)>)> operatorParenthesis = searchFunc;
                pTreeStore[i]->traverseTreeDFSL2RB2T<operation,void>(operatorParenthesis);
                //std::cout << std::endl;
                spdlog::get("sopelogger")->debug("\n");
            }
        }
        //std::cout << "\nFinished comparing trees with each other." << std::endl;
        spdlog::get("sopelogger")->debug("Finished comparing trees with each other.");

        //std::cout << "Replacements collected by the tool:\n";
        spdlog::get("sopelogger")->debug("Replacements collected by the tool:\n");
        for (auto &r : Tool.getReplacements()) {
            //std::cout << r.toString() << "\n";
            spdlog::get("sopelogger")->debug("{}", r.toString());
        }

        //  std::cout << "Applying all replacements...\n";
        //  Tool.applyAllReplacements();
        //  std::cout << "Applied.\n";

        //std::cout << "Repository use count = " << spRepo.use_count() << std::endl;
        //std::cout << "Repository uniqueness = " << spRepo.unique() << std::endl;
        spdlog::get("sopelogger")->debug("Repository use count = {}", spRepo.use_count());
        spdlog::get("sopelogger")->debug("Repository uniqueness = {}", spRepo.unique());
    }
    //std::cout << "Exiting" << std::endl;
    spdlog::get("sopelogger")->debug("Exiting");
    return returnVal;
}
