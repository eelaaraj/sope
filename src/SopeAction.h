#ifndef SOPEACTION_H
#define SOPEACTION_H

#include "clang/AST/ASTConsumer.h"
#include "clang/AST/RecursiveASTVisitor.h"
#include "clang/Frontend/CompilerInstance.h"
#include "clang/Frontend/FrontendAction.h"
#include "./proc/ClangASTProcessor.h"

using clang::ASTConsumer;
using clang::ASTFrontendAction;

template<typename OperationTypename, typename PostProcessor>
class FindNamedClassVisitor
        : public RecursiveASTVisitor<FindNamedClassVisitor<OperationTypename,PostProcessor>> {
public:
    explicit FindNamedClassVisitor(ASTContext *Context, std::shared_ptr<Repository<OperationTypename>> repository, const std::string& fName, const std::vector<std::string>& funcNames, Replacements* replace, bool injection)
    {
        parseState = std::shared_ptr<ParseState>(new ParseState());
        parseState->Context = Context;
        parseState->repo = repository;
        parseState->fileName = fName;
        parseState->functionNames = funcNames;
        parseState->replacements = replace;
        parseState->inject = injection;
        parseState->function = nullptr;
        parseState->loop = nullptr;
        parseState->currentAssignmentNode = nullptr;
        parseState->assignmentLHS = nullptr;
        parseState->assignmentRHS = nullptr;
        parseState->assignmentOperChildren = nullptr;
        parseState->functionParentMap = nullptr;
        parseState->currentParent = nullptr;
        std::shared_ptr<ClangASTProcessor<PostProcessor>> clASTProcessor = std::shared_ptr<ClangASTProcessor<PostProcessor>>(new ClangASTProcessor<PostProcessor>(parseState, astProcessor));

        astProcessor = clASTProcessor;
        // We need to set the self pointer here. It does not work the way we do it in the constructor unless we pass
        // as reference to the astProcessor shared_ptr. If we just pass the shared_ptr by value, then the weak_ptr
        // in clangASTProcessor will be set to the initial address value of astProcessor shared_ptr which is nullptr.
        clASTProcessor->setSelfSharedPointer(astProcessor);

        //std::cout << "Repository use count visitor = " << repository.use_count() << "\n";
        spdlog::get("sopelogger")->debug("Repository use count visitor = {}", repository.use_count());
    }

    ~FindNamedClassVisitor() {

        //std::cout << "Destroying named class visitor" <<"\n";
        spdlog::get("sopelogger")->debug("Destroying named class visitor");
    }

    /*
     * Completes the information needed to identify operations. for example
     * on creation of a subscript operation, we have no idea if its a memory
     * load or memory store. But after we process it an add it to lhs of
     * an assignment operator, its clear now that its a memory store. If
     * it shows of the rhs, then its memory load.
     */
    void completeOperation();

    bool VisitCXXRecordDecl(CXXRecordDecl *Declaration) {
        //if (Declaration->getQualifiedNameAsString() == "n::m::C")
        if(parseState->Context->getSourceManager().isInMainFile(Declaration->getLocation()))
        {
            FullSourceLoc FullLocation = parseState->Context->getFullLoc(Declaration->getLocStart());
            if (FullLocation.isValid()) {
                //std::cout << "Found declaration at "
                //          << FullLocation.printToString(parseState->Context->getSourceManager()) << "\n ";
                spdlog::get("sopelogger")->debug("Found declaration at {}", FullLocation.printToString(parseState->Context->getSourceManager()));
                //<< FullLocation.getSpellingLineNumber() << ":"
                //<< FullLocation.getSpellingColumnNumber() << "\n";
            }
        }
        return true;
    }

    bool VisitCallExpr(CallExpr* callExpr) {
        return astProcessor->processCallExpr(callExpr);
    }

    /*bool VisitCXXMemberCallExpr(CXXMemberCallExpr* cxxMemberCallExpr) {
        std::cout << "CXXMemberCallExpr" <<std::endl;
        cxxMemberCallExpr->dumpColor();
        std::cout << "CXXMemberCallExpr callee" <<std::endl;
        cxxMemberCallExpr->getCallee()->dumpColor();
        std::cout << "CXXMemberCallExpr direct callee" <<std::endl;
        cxxMemberCallExpr->getDirectCallee()->dumpColor();
        std::cout << "CXXMemberCallExpr method decl" <<std::endl;
        cxxMemberCallExpr->getMethodDecl()->dumpColor();
        std::cout << "CXXMemberCallExpr callee decl" <<std::endl;
        cxxMemberCallExpr->getCalleeDecl()->dumpColor();
        return true;
    }*/

    bool VisitFunctionDecl(FunctionDecl* functionDecl) {
        return astProcessor->processFunctionDecl(functionDecl);
    }

    bool VisitForStmt(ForStmt *forStmt) {
        return astProcessor->processForStmt(forStmt);
    }

    bool VisitIfStmt(IfStmt *ifStmt) {
        return astProcessor->processIfStmt(ifStmt);
    }

    bool VisitCompoundStmt(CompoundStmt *compoundStmt) {
        return astProcessor->processCompoundStmt(compoundStmt);
    }

    bool VisitBinaryOperator(BinaryOperator* binOper) {
        return astProcessor->processBinaryOperator(binOper);
    }

    bool VisitUnaryOperator(UnaryOperator* unaryOper) {
        return astProcessor->processUnaryOperator(unaryOper);
    }

    bool VisitArraySubscriptExpr(ArraySubscriptExpr* subsExpr) {
        return astProcessor->processArraySubscriptExpr(subsExpr);
    }

    bool VisitDeclRefExpr(DeclRefExpr* declRefExpr) {
        return astProcessor->processDeclRefExpr(declRefExpr);
    }

    bool VisitVarDecl(VarDecl* varDecl) {
        return astProcessor->processVariableDecl(varDecl);
    }

    bool VisitIntegerLiteral(IntegerLiteral* intLiteral) {
        return astProcessor->processIntegerLiteral(intLiteral);
    }

    bool VisitFloatingLiteral(FloatingLiteral* floatLiteral) {
        return astProcessor->processFloatingLiteral(floatLiteral);
    }

private:
    std::shared_ptr<ParseState> parseState;
    std::shared_ptr<ClangASTProcessor<PostProcessor>> astProcessor;
};

template<typename OperationTypename, typename PostProcessor>
class FindNamedClassConsumer : public clang::ASTConsumer {
public:
    explicit FindNamedClassConsumer(ASTContext *Context, std::shared_ptr<Repository<OperationTypename>> repo, const std::string& fileName, std::vector<std::string> functionNames, Replacements* replace, bool inject)
        : Visitor(Context, repo, fileName, functionNames, replace, inject)
    { //std::cout << "Repository use count consumer= " << repo.use_count() << "\n";
        spdlog::get("sopelogger")->debug("Repository use count consumer= {}", repo.use_count());
    }

    virtual void HandleTranslationUnit(clang::ASTContext &Context) {
        Visitor.TraverseDecl(Context.getTranslationUnitDecl());
    }
private:
    FindNamedClassVisitor<OperationTypename,PostProcessor> Visitor;
};

template<typename OperationTypename, typename PostProcessor>
class FindNamedClassAction : public clang::ASTFrontendAction {
public:
    FindNamedClassAction(const std::string& fName, const std::vector<std::string> funcNames, std::shared_ptr<Repository<OperationTypename>> repo, Replacements* replace, bool injection) : fileName(fName), functionNames(funcNames), repository(repo), replacements(replace), inject(injection)
    { //std::cout << "Repository use count action= " << repository.use_count() << "\n";
        spdlog::get("sopelogger")->debug("Repository use count action= {}", repository.use_count());
    }
    ~FindNamedClassAction() {//std::cout << "destroying FindNamedClassAction\n";
        spdlog::get("sopelogger")->debug("destroying FindNamedClassAction");
    }

    virtual std::unique_ptr<clang::ASTConsumer> CreateASTConsumer(
            clang::CompilerInstance &Compiler, llvm::StringRef InFile) {
        return std::unique_ptr<clang::ASTConsumer>(
                    new FindNamedClassConsumer<OperationTypename, PostProcessor>(&Compiler.getASTContext(), repository, fileName, functionNames, replacements, inject));
    }
private:
    std::string fileName;
    std::vector<std::string> functionNames;
    std::shared_ptr<Repository<OperationTypename>> repository;
    Replacements* replacements;
    bool inject;
};

template<typename OperationTypename, typename PostProcessor>
class MyASTActionFactory: public FrontendActionFactory {
public:
    MyASTActionFactory(std::string fName, std::vector<std::string> funcNames, std::shared_ptr<Repository<OperationTypename>> repo, Replacements* replace, bool injection) : fileName(fName), functionNames(funcNames), repository(repo), replacements(replace), inject(injection)
    { //std::cout << "Repository use count factory = " << repository.use_count() << "\n";
        spdlog::get("sopelogger")->debug("Repository use count factory = {}", repository.use_count());
    }

    clang::FrontendAction* create() {
        //std::cout << "Repository use count factory::create()= " << repository.use_count() << "\n";
        spdlog::get("sopelogger")->debug("Repository use count factory::create()= {}", repository.use_count());
        return new FindNamedClassAction<OperationTypename,PostProcessor>(fileName, functionNames, repository, replacements, inject);
    }
public:
    std::string fileName;
    std::vector<std::string> functionNames;
    std::shared_ptr<Repository<OperationTypename>> repository;
    Replacements* replacements;
    bool inject;
};

#endif // SOPEACTION_H
