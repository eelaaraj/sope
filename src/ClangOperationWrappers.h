/*
 * ClangOperationWrappers.h
 *
 *  Created on: Jul 23, 2015
 *      Author: heisenberg
 */

#ifndef CLANGOPERATIONWRAPPERS_H_
#define CLANGOPERATIONWRAPPERS_H_

#include "llvm/Support/Casting.h"
#include "Operations.h"
#include "clang/AST/AST.h"
#include <spdlog/spdlog.h>
using namespace clang;
using namespace pattern::matching;

namespace pattern {
namespace matching {
namespace wrapper {

template<typename CompPolicy>
class ClangForLoopWrapper : public LoopOperation<CompPolicy> {
public:
    ClangForLoopWrapper(int numIter, const std::string& iterLiteral,
                        const std::string& iterStart, const std::string& iterEnd,
                        const ParallelType& par, int numOper, OperandType* operTypes)
        : LoopOperation<CompPolicy>(Operation<CompPolicy>::OK_ClangForLoopWrapper , numIter, iterLiteral,iterStart, iterEnd, par, numOper, operTypes), _forStmt(nullptr) {}

    ClangForLoopWrapper(ForStmt* forStmt, int loopId) : LoopOperation<CompPolicy>(Operation<CompPolicy>::OK_ClangForLoopWrapper, loopId, -1, /*forStmt->getInit()->casttoDeclref->getDeclName*/ "", "", "", ParallelType::none, -1, nullptr), _forStmt(forStmt) { //std::cout << "creating clLoop operation at " << this << "\n";
                                                                                                                                                                                                                                              spdlog::get("sopelogger")->debug("creating clLoop operation at {}", (void*) this);}

    virtual ~ClangForLoopWrapper() { //std::cout << "destroying clloop operation at " << this << "\n";
                                     spdlog::get("sopelogger")->debug("destroying clloop operation at {}", (void*) this);
                                   }
    static bool classof(const Operation<CompPolicy>* baseOperation) {
        return baseOperation->getKind()
                == Operation<CompPolicy>::OK_ClangForLoopWrapper;
    }

    Stmt* getUnderlyingPtr() { return _forStmt;}
private:
    ForStmt* _forStmt;
};

template<typename CompPolicy>
class ClangCallWrapper : public CallOperation<CompPolicy> {
public:
    ClangCallWrapper(const std::string& calleeName,
                        const ParallelType& par, int numOper, OperandType* operTypes)
        : CallOperation<CompPolicy>(Operation<CompPolicy>::OK_ClangCallOperation , OperationType::call, calleeName, par, numOper, operTypes), _callExpr(nullptr) {}
    ClangCallWrapper(CallExpr* callExpr, const OperationType& opType) : CallOperation<CompPolicy>(Operation<CompPolicy>::OK_ClangCallOperation, opType, callExpr->getDirectCallee()->getNameAsString(),ParallelType::none, callExpr->getNumArgs(), nullptr), _callExpr(callExpr) { //std::cout << "creatng clCall operation at "<<this<< "\n";
                                                                                                                                                                                                                                                                                   spdlog::get("sopelogger")->debug("creatng clCall operation at {}", (void*) this);
                                                                                                                                                                                                                                                                                 }
    virtual ~ClangCallWrapper() { //std::cout << "destroying clCall operation at " << this << "\n";
                                  spdlog::get("sopelogger")->debug("destroying clCall operation at {}", (void*) this);
                                }
    static bool classof(const Operation<CompPolicy>* baseOperation) {
        return baseOperation->getKind()
                == Operation<CompPolicy>::OK_ClangCallOperation;
    }

    Stmt* getUnderlyingPtr() { return _callExpr;}
private:
    CallExpr* _callExpr;
};

template<typename CompPolicy>
class ClangFunctionWrapper : public FunctionOperation<CompPolicy> {
public:
    ClangFunctionWrapper(const std::string& functionName,
                        const ParallelType& par, int numOper, OperandType* operTypes)
        : FunctionOperation<CompPolicy>(Operation<CompPolicy>::OK_ClangFunctionWrapper , functionName, par, numOper, operTypes), _funcDecl(nullptr) {}

    ClangFunctionWrapper(FunctionDecl* funcDecl) : FunctionOperation<CompPolicy>(Operation<CompPolicy>::OK_ClangFunctionWrapper, funcDecl->getNameAsString()  , ParallelType::none, -1, nullptr), _funcDecl(funcDecl) {}

    static bool classof(const Operation<CompPolicy>* baseOperation) {
        return baseOperation->getKind()
                == Operation<CompPolicy>::OK_ClangFunctionWrapper;
    }
    FunctionDecl* getDecl() { return _funcDecl; }

    Stmt* getUnderlyingPtr() { return _funcDecl->getBody();}
private:
    FunctionDecl* _funcDecl;
};

template<typename CompPolicy>
class ClangIfWrapper : public IfOperation<CompPolicy> {
public:
    ClangIfWrapper(const std::string& functionName,
                        const ParallelType& par, int numOper, OperandType* operTypes)
        : IfOperation<CompPolicy>(Operation<CompPolicy>::OK_ClangIfWrapper, par, numOper, operTypes), _ifStmt(nullptr) {}

    ClangIfWrapper(IfStmt* ifStmt) : IfOperation<CompPolicy>(Operation<CompPolicy>::OK_ClangIfWrapper, ParallelType::none, -1, nullptr), _ifStmt(ifStmt) {}

    static bool classof(const Operation<CompPolicy>* baseOperation) {
        return baseOperation->getKind()
                == Operation<CompPolicy>::OK_ClangIfWrapper;
    }
    Stmt* getUnderlyingPtr() { return _ifStmt;}
private:
    IfStmt* _ifStmt;
};


template<typename CompPolicy>
class ClangSubscriptWrapper : public MemoryOperation<CompPolicy> {
public:
    // Just fill in the arguments of the Memory operation constructor with trash. Later we can assign them to their proper values
    ClangSubscriptWrapper(ArraySubscriptExpr* subsExpr, const OperationType& opType) : MemoryOperation<CompPolicy>(Operation<CompPolicy>::OK_ClangSubscriptWrapper, AccessType::contiguous, "", "", opType, ParallelType::none, 0, nullptr), _subsExpr(subsExpr) {}
    static bool classof(const Operation<CompPolicy>* baseOperation) {
        return baseOperation->getKind()
                == Operation<CompPolicy>::OK_ClangSubscriptWrapper;
    }
    Stmt* getUnderlyingPtr() { return _subsExpr;}
private:
    ArraySubscriptExpr* _subsExpr;
};

template<typename CompPolicy>
class ClangBinaryOperationWrapper : public BinaryOperation<CompPolicy> {
public:
    // Just fill in the arguments of the binary operation constructor with trash. Later we can assign them to their proper values
    //ClangBinaryOperationWrapper(BinaryOperator* binOper) : BinaryOperation<CompPolicy>(Operation<CompPolicy>::OK_ClangBinaryOperatorWrapper, "A", "B", "C", ParallelType::none, 0, nullptr), _binOper(binOper) {}

    ClangBinaryOperationWrapper(BinaryOperator* binOper, const OperationType& opType, const std::string& opCode) : BinaryOperation<CompPolicy>(Operation<CompPolicy>::OK_ClangBinaryOperatorWrapper, opType, opCode, "B", "C", ParallelType::none, 0, nullptr), _binOper(binOper) {}

    static bool classof(const Operation<CompPolicy>* baseOperation) {
        return baseOperation->getKind()
                == Operation<CompPolicy>::OK_ClangBinaryOperatorWrapper;
    }
    Stmt* getUnderlyingPtr() { return _binOper;}
private:
    BinaryOperator* _binOper;
};

template<typename CompPolicy>
class ClangUnaryOperationWrapper : public UnaryOperation<CompPolicy> {
public:
    // Just fill in the arguments of the binary operation constructor with trash. Later we can assign them to their proper values
    //ClangBinaryOperationWrapper(BinaryOperator* binOper) : BinaryOperation<CompPolicy>(Operation<CompPolicy>::OK_ClangBinaryOperatorWrapper, "A", "B", "C", ParallelType::none, 0, nullptr), _binOper(binOper) {}

    ClangUnaryOperationWrapper(UnaryOperator* unaryOper, const OperationType& opType, const std::string& opCode) : UnaryOperation<CompPolicy>(Operation<CompPolicy>::OK_ClangUnaryOperatorWrapper, opType, opCode, "A", ParallelType::none, 0, nullptr), _unaryOper(unaryOper) {}

    static bool classof(const Operation<CompPolicy>* baseOperation) {
        return baseOperation->getKind()
                == Operation<CompPolicy>::OK_ClangUnaryOperatorWrapper;
    }
    Stmt* getUnderlyingPtr() { return _unaryOper;}
private:
    UnaryOperator* _unaryOper;
};

template<typename CompPolicy>
class ClangVariableDeclRefWrapper : public VariableReferenceOperation<CompPolicy> {
public:
    // Just fill in the arguments of the operation constructor with trash. Later we can assign them to their proper values
    //ClangVariableDeclRefWrapper(DeclRefExpr* declRefExpr) : Operation<CompPolicy>(Operation<CompPolicy>::OK_ClangVariableDeclWrapper, OperationType::add, ParallelType::none, 0, nullptr), _declRefExpr(declRefExpr) {}
    ClangVariableDeclRefWrapper(DeclRefExpr* declRefExpr, const std::string& variableLiteral) : VariableReferenceOperation<CompPolicy>(Operation<CompPolicy>::OK_ClangVariableRefWrapper, variableLiteral, OperationType::none, ParallelType::none, 0, nullptr), _declRefExpr(declRefExpr) {}

    ~ClangVariableDeclRefWrapper() { //std::cout << "Destroyed ClangVariableDeclRefWrapper at : " << this << std::endl;
                                     spdlog::get("sopelogger")->debug("Destroyed ClangVariableDeclRefWrapper at : {}", (void*) this);
                                   }
    static bool classof(const Operation<CompPolicy>* baseOperation) {
        return baseOperation->getKind()
                == Operation<CompPolicy>::OK_ClangVariableRefWrapper;
    }
    Stmt* getUnderlyingPtr() { return _declRefExpr;}
private:
    DeclRefExpr* _declRefExpr;
};

template<typename CompPolicy>
class ClangCompoundStatementWrapper : public CompoundStatementOperation<CompPolicy> {
public:
    // Just fill in the arguments of the operation constructor with trash. Later we can assign them to their proper values
    ClangCompoundStatementWrapper(CompoundStmt* compStmt) : CompoundStatementOperation<CompPolicy>(Operation<CompPolicy>::OK_ClangCompoundStatementWrapper, ParallelType::none, 0, nullptr), _compStmt(compStmt) {}

    static bool classof(const Operation<CompPolicy>* baseOperation) {
        return baseOperation->getKind()
                == Operation<CompPolicy>::OK_ClangCompoundStatementWrapper;
    }
    Stmt* getUnderlyingPtr() { return _compStmt;}
private:
    CompoundStmt* _compStmt;
};

template<typename CompPolicy>
class ClangVarDeclWrapper : public VariableDeclOperation<CompPolicy> {
public:
    ClangVarDeclWrapper(VarDecl* varDecl, DeclStmt* varDeclStmt) : VariableDeclOperation<CompPolicy>(Operation<CompPolicy>::OK_ClangVariableDeclaration, varDecl->getNameAsString(), OperationType::none, ParallelType::none, 0, nullptr), _varDecl(varDecl), _varDeclStmt(varDeclStmt) {}

    static bool classof(const Operation<CompPolicy>* baseOperation) {
        return baseOperation->getKind()
                == Operation<CompPolicy>::OK_ClangVariableDeclaration;
    }
    // Here we are forced to return DeclStmt rather than VarDecl address. This is because Stmt and Decl and non-related. Clang does not have
    // one hierarchy tree. Instead there is Decl tree and Stmt tree. To allow Statements to have declaration, they have adapter classes like
    // DeclStmt which is a statement that might contains declarations. This is used in compound statements and ForStmts for example. The
    // ForStmt class has a getConditionVariableDeclStmt which returns the condition variable declation as a statement. If you just want the varDecl
    // then you can get it also using getConditionVariable.
    DeclStmt* getUnderlyingPtr() { return _varDeclStmt;}

    //std::shared_ptr<Operation<CompPolicy>>& getInitExpression() { return _initExpression; }
    //std::string& unaryOperandString() { return _initExpressionString; }

    //std::string getMainStringLiteral() { return stringLiteral + " = " + _initExpressionString; }
private:
    VarDecl*  _varDecl;
    DeclStmt* _varDeclStmt;
    //std::string stringLiteral;
    //std::string _initExpressionString;
    //std::shared_ptr<Operation<CompPolicy>> _initExpression;
};

template<typename CompPolicy>
class ClangIntegerLiteralWrapper: public NumericLiteral<CompPolicy> {
public:
    ClangIntegerLiteralWrapper(IntegerLiteral* intLiteral) : NumericLiteral<CompPolicy>(Operation<CompPolicy>::OK_ClangIntegerLiteral, intLiteral->getValue().toString(10, true), ParallelType::none, 0, nullptr), _intLiteral(intLiteral) {}
    static bool classof(const Operation<CompPolicy>* baseOperation) {
        return baseOperation->getKind()
                == Operation<CompPolicy>::OK_ClangIntegerLiteral;
    }
    ~ClangIntegerLiteralWrapper() {}
    // Here we are forced to return DeclStmt rather than VarDecl address. This is because Stmt and Decl and non-related. Clang does not have
    // one hierarchy tree. Instead there is Decl tree and Stmt tree. To allow Statements to have declaration, they have adapter classes like
    // DeclStmt which is a statement that might contains declarations. This is used in compound statements and ForStmts for example. The
    // ForStmt class has a getConditionVariableDeclStmt which returns the condition variable declation as a statement. If you just want the varDecl
    // then you can get it also using getConditionVariable.
    IntegerLiteral* getUnderlyingPtr() { return _intLiteral;}

    std::string getMainStringLiteral() {
        SmallVector<char,3> str;
        //StringRef strRef;
        _intLiteral->getValue().toStringSigned(str, 10);
        StringRef stref(str.data(), str.size());
        return stref.str();
    }
private:
    IntegerLiteral* _intLiteral;
};

template<typename CompPolicy>
class ClangFloatLiteralWrapper: public NumericLiteral<CompPolicy> {
public:
    ClangFloatLiteralWrapper(FloatingLiteral* floatLiteral) : NumericLiteral<CompPolicy>(Operation<CompPolicy>::OK_ClangFloatLiteral, "", ParallelType::none, 0, nullptr), _floatLiteral(floatLiteral) {}
    static bool classof(const Operation<CompPolicy>* baseOperation) {
        return baseOperation->getKind()
                == Operation<CompPolicy>::OK_ClangFloatLiteral;
    }
    ~ClangFloatLiteralWrapper() {}
    // Here we are forced to return DeclStmt rather than VarDecl address. This is because Stmt and Decl and non-related. Clang does not have
    // one hierarchy tree. Instead there is Decl tree and Stmt tree. To allow Statements to have declaration, they have adapter classes like
    // DeclStmt which is a statement that might contains declarations. This is used in compound statements and ForStmts for example. The
    // ForStmt class has a getConditionVariableDeclStmt which returns the condition variable declation as a statement. If you just want the varDecl
    // then you can get it also using getConditionVariable.
    FloatingLiteral* getUnderlyingPtr() { return _floatLiteral;}

    std::string getMainStringLiteral() {
        SmallVector<char,3> str;
        //StringRef strRef;
        _floatLiteral->getValue().toString(str, 10);
        StringRef stref(str.data(), str.size());
        return stref.str();
    }
private:
    FloatingLiteral* _floatLiteral;
};
}
}
}
#endif /* CLANGOPERATIONWRAPPERS_H_ */
