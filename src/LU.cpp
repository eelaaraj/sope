/*
 * LU.cpp
 *
 *  Created on: May 5, 2015
 *      Author: heisenberg
 */

// Philip Wallstedt 2007-2008
// See explanation at http://www.sci.utah.edu/~wallstedt

#include <iostream>
#include <iomanip>
#include <cmath>
#include <random>
#include <chrono>
#include <vector>
#include <map>
#include <algorithm>
#include <string>

using namespace std;

std::map<std::string, std::map<int, std::vector<std::chrono::duration<double>>>> timedUnitInnerTimes;
std::map<int, std::vector<std::chrono::duration<double>>> randomAccessInnerTimes;
std::map<int, std::vector<std::chrono::duration<double>>> croutInnerTimes;
std::map<int, std::vector<std::chrono::duration<double>>> dolittleInnerTimes;

// These functions print matrices and vectors in a nice format
void coutMatrix(int d,double*m){
   cout<<'\n';
   for(int i=0;i<d;++i) {
      for(int j=0;j<d;++j)
    	  cout<<setw(14)<<m[i*d+j];
      cout<<'\n';
   }
}

void coutVector(int d,double*v){
   cout<<'\n';
   for(int j=0;j<d;++j)cout<<setw(14)<<v[j];
   cout<<'\n';
}

struct Sum {
	Sum() { sum = std::chrono::duration<double>::zero(); }
	void operator()(std::chrono::duration<double> elapsed) { sum+=elapsed;}
	std::chrono::duration<double> sum;
};

// The following compact LU factorization schemes are described
// in Dahlquist, Bjorck, Anderson 1974 "Numerical Methods".
//
// S and D are d by d matrices.  However, they are stored in
// memory as 1D arrays of length d*d.  Array indices are in
// the C style such that the first element of array A is A[0]
// and the last element is A[d*d-1].
//
// These routines are written with separate source S and
// destination D matrices so the source matrix can be retained
// if desired.  However, the compact schemes were designed to
// perform in-place computations to save memory.  In
// other words, S and D can be the SAME matrix.  Just
// modify the code like this example:
//
//    void Crout(int d,double*S){
//       for(int k=0;k<d;++k){
//          for(int i=k;i<d;++i){
//             double sum=0.;
//             for(int p=0;p<k;++p)sum+=S[i*d+p]*S[p*d+k];
//             S[i*d+k]=S[i*d+k]-sum; // not dividing by diagonals
//          }
//          for(int j=k+1;j<d;++j){
//             double sum=0.;
//             for(int p=0;p<k;++p)sum+=S[k*d+p]*S[p*d+j];
//             S[k*d+j]=(S[k*d+j]-sum)/S[k*d+k];
//          }
//       }
//    }

// Crout uses unit diagonals for the upper triangle
void Crout_original(int d,double*S,double*D){
   for(int k=0;k<d;++k){
      for(int i=k;i<d;++i){
         double sum=0.;
         for(int p=0;p<k;++p)sum+=D[i*d+p]*D[p*d+k];
         D[i*d+k]=S[i*d+k]-sum; // not dividing by diagonals
      }
      for(int j=k+1;j<d;++j){
         double sum=0.;
         for(int p=0;p<k;++p)sum+=D[k*d+p]*D[p*d+j];
         D[k*d+j]=(S[k*d+j]-sum)/D[k*d+k];
      }
   }
}

// Doolittle uses unit diagonals for the lower triangle
void Doolittle_original(int d,double*S,double*D){
   for(int k=0;k<d;++k){
      for(int j=k;j<d;++j){
         double sum=0.;
         for(int p=0;p<k;++p)sum+=D[k*d+p]*D[p*d+j];
         D[k*d+j]=(S[k*d+j]-sum); // not dividing by diagonals
      }
      for(int i=k+1;i<d;++i){
         double sum=0.;
         for(int p=0;p<k;++p)sum+=D[i*d+p]*D[p*d+k];
         D[i*d+k]=(S[i*d+k]-sum)/D[k*d+k];
      }
   }
}

// takes an array containingthe random inices. These were generated beforehand to avoid adding delay to the fucntion
// randomIndices should have same size as S or D
void completeRandomAccessDemo(int d,double*S,double*D, const int*randomIndices){
    for(int k=0;k<d;++k){
		// Beginning of injected code section: start timer
		std::chrono::time_point<std::chrono::system_clock> start1, end1, start2,end2;
		start1 = std::chrono::system_clock::now();
		// end of injected code section
      for(int j=k;j<d;++j){
         double sum=0.;
         for(int p=0;p<k;++p)sum+=D[k*d+p]*D[p*d+j];
         D[k*d+j]=(S[k*d+j]-sum); // not dividing by diagonals
      }
		// Beginning of injected code section : to stop timer and save elapsed and start new timer for next loop
		end1 = std::chrono::system_clock::now();
		std::chrono::duration<double> elapsed1 = end1 - start1;
		timedUnitInnerTimes["completeRandom"][1].push_back(elapsed1);
		start2 = std::chrono::system_clock::now();
		// end of injected code section
      for(int i=k+1;i<d;++i){
         double sum=0.;
         for(int p=0;p<k;++p)
        	 sum+=D[randomIndices[i*d+p]]*D[randomIndices[p*d+k]];
         D[randomIndices[i*d+k]]=(S[randomIndices[i*d+k]]-sum)/D[k*d+k];
      }
		// Beginning of injected code section : to stop timer and save elapsed
		end2 = std::chrono::system_clock::now();
		std::chrono::duration<double> elapsed2 = end2 - start2;
		timedUnitInnerTimes["completeRandom"][2].push_back(elapsed2);
		//end of injected code section
   }
}

// takes an array containingthe random inices. These were generated beforehand to avoid adding delay to the fucntion
// randomIndices should have same size as S or D
void randomAccessDemo(int d,double*S,double*D, int* randomIndices){
	//cout << randomIndices << endl;
	//cout << "checking randomIndices[0] during beginnning randomaccess= " << randomIndices[0] << endl << randomIndices[10] << endl << randomIndices[300] << endl;
	//int initialValue = randomIndices[0];
	//randomAccessInnerTimes.insert(std::pair<int, std::vector<std::chrono::duration<double>>>(1, std::vector<std::chrono::duration<double>>()));
	//randomAccessInnerTimes.insert(std::pair<int, std::vector<std::chrono::duration<double>>>(2, std::vector<std::chrono::duration<double>>()));
    for(int k=0;k<d;++k){
		// Beginning of injected code section: start timer
		std::chrono::time_point<std::chrono::system_clock> start1, end1, start2,end2;
		start1 = std::chrono::system_clock::now();
		// end of injected code section
      for(int j=k;j<d;++j){
         double sum=0.;
         for(int p=0;p<k;++p)sum+=D[k*d+p]*D[p*d+j];
         D[k*d+j]=(S[k*d+j]-sum); // not dividing by diagonals
      }
		// Beginning of injected code section : to stop timer and save elapsed and start new timer for next loop
		end1 = std::chrono::system_clock::now();
		std::chrono::duration<double> elapsed1 = end1 - start1;
		//randomAccessInnerTimes[1] = std::vector<std::chrono::duration<double>>();
		/*for(auto apair : randomAccessInnerTimes) {
			cout << apair.first << " | vec size: " << apair.second.size() << endl ;
		}*/
		randomAccessInnerTimes[1].push_back(elapsed1);
		start2 = std::chrono::system_clock::now();
		// end of injected code section
      for(int i=k+1;i<d;++i){
         double sum=0.;
         int constantIndex = randomIndices[i*d+k];
         for(int p=0;p<k;++p) {
        	 int contiguousIndex = randomIndices[i*d+p];
        	 sum+=D[contiguousIndex]*D[p*d+k];
         }
         D[constantIndex]=(S[constantIndex]-sum)/D[k*d+k];
      }
		// Beginning of injected code section : to stop timer and save elapsed
		end2 = std::chrono::system_clock::now();
		std::chrono::duration<double> elapsed2 = end2 - start2;
		randomAccessInnerTimes[2].push_back(elapsed2);
		//end of injected code section
   }
    //cout << randomIndices << endl;
    //cout << "checking randomIndices[0] during end randomaccess= " << randomIndices[0] << endl;
}

// Crout uses unit diagonals for the upper triangle
void Crout(int d,double*S,double*D){
   for(int k=0;k<d;++k){
	   // Beginning of injected code section: start timer
	  std::chrono::time_point<std::chrono::system_clock> start1, end1, start2, end2;
	  start1 = std::chrono::system_clock::now();
	  // end of injected code section
      for(int i=k;i<d;++i){
         double sum=0.;
         for(int p=0;p<k;++p)sum+=D[i*d+p]*D[p*d+k];
         D[i*d+k]=S[i*d+k]-sum; // not dividing by diagonals
      }
      // Beginning of injected code section : to stop timer and save elapsed and start new timer for next loop
      end1 = std::chrono::system_clock::now();
      std::chrono::duration<double> elapsed1 = end1-start1;
      croutInnerTimes[1].push_back(elapsed1);
      start2 = std::chrono::system_clock::now();
      // end of injected code section
      for(int j=k+1;j<d;++j){
         double sum=0.;
         for(int p=0;p<k;++p)sum+=D[k*d+p]*D[p*d+j];
         //if(D[k*d+k] < 0.01 && D[k*d+k] > -0.01) cout << " crout problem: " << D[k*d+k] << endl;
         D[k*d+j]=(S[k*d+j]-sum)/D[k*d+k];
      }
      // Beginning of injected code section : to stop timer and save elapsed
      end2 = std::chrono::system_clock::now();
      std::chrono::duration<double> elapsed2 = end2-start2;
      croutInnerTimes[2].push_back(elapsed2);
      //end of injected code section
   }
}
void solveCrout(int d,double*LU,double*b,double*x){
   double y[d];
   for(int i=0;i<d;++i){
      double sum=0.;
      for(int k=0;k<i;++k)sum+=LU[i*d+k]*y[k];
      y[i]=(b[i]-sum)/LU[i*d+i];
   }
   for(int i=d-1;i>=0;--i){
      double sum=0.;
      for(int k=i+1;k<d;++k)sum+=LU[i*d+k]*x[k];
      x[i]=(y[i]-sum); // not dividing by diagonals
   }
}



// Doolittle uses unit diagonals for the lower triangle
void Doolittle(int d,double*S,double*D){
   for(int k=0;k<d;++k){
	  std::chrono::time_point<std::chrono::system_clock> start1, end1, start2, end2;
	  start1 = std::chrono::system_clock::now();
      for(int j=k;j<d;++j){
         double sum=0.;
         for(int p=0;p<k;++p)sum+=D[k*d+p]*D[p*d+j];
         D[k*d+j]=(S[k*d+j]-sum); // not dividing by diagonals
      }
      end1 = std::chrono::system_clock::now();
      std::chrono::duration<double> elapsed1 = end1-start1;
      dolittleInnerTimes[1].push_back(elapsed1);
      start2 = std::chrono::system_clock::now();
      for(int i=k+1;i<d;++i){
         double sum=0.;
         for(int p=0;p<k;++p)sum+=D[i*d+p]*D[p*d+k];
         D[i*d+k]=(S[i*d+k]-sum)/D[k*d+k];
      }
      end2 = std::chrono::system_clock::now();
      std::chrono::duration<double> elapsed2 = end2-start2;
      dolittleInnerTimes[2].push_back(elapsed2);
   }
}
void solveDoolittle(int d,double*LU,double*b,double*x){
   double y[d];
   for(int i=0;i<d;++i){
      double sum=0.;
      for(int k=0;k<i;++k)sum+=LU[i*d+k]*y[k];
      y[i]=(b[i]-sum); // not dividing by diagonals
   }
   for(int i=d-1;i>=0;--i){
      double sum=0.;
      for(int k=i+1;k<d;++k)sum+=LU[i*d+k]*x[k];
      x[i]=(y[i]-sum)/LU[i*d+i];
   }
}



// Cholesky requires the matrix to be symmetric positive-definite
void Cholesky(int d,double*S,double*D){
   for(int k=0;k<d;++k){
      double sum=0.;
      for(int p=0;p<k;++p)sum+=D[k*d+p]*D[k*d+p];
      D[k*d+k]=sqrt(S[k*d+k]-sum);
      for(int i=k+1;i<d;++i){
         double sum=0.;
         for(int p=0;p<k;++p)sum+=D[i*d+p]*D[k*d+p];
         D[i*d+k]=(S[i*d+k]-sum)/D[k*d+k];
      }
   }
}
// This version could be more efficient on some architectures
// Use solveCholesky for both Cholesky decompositions
void CholeskyRow(int d,double*S,double*D){
   for(int k=0;k<d;++k){
      for(int j=0;j<d;++j){
         double sum=0.;
         for(int p=0;p<j;++p)sum+=D[k*d+p]*D[j*d+p];
         D[k*d+j]=(S[k*d+j]-sum)/D[j*d+j];
      }
      double sum=0.;
      for(int p=0;p<k;++p)sum+=D[k*d+p]*D[k*d+p];
      D[k*d+k]=sqrt(S[k*d+k]-sum);
   }
}
void solveCholesky(int d,double*LU,double*b,double*x){
   double y[d];
   for(int i=0;i<d;++i){
      double sum=0.;
      for(int k=0;k<i;++k)sum+=LU[i*d+k]*y[k];
      y[i]=(b[i]-sum)/LU[i*d+i];
   }
   for(int i=d-1;i>=0;--i){
      double sum=0.;
      for(int k=i+1;k<d;++k)sum+=LU[k*d+i]*x[k];
      x[i]=(y[i]-sum)/LU[i*d+i];
   }
}



int main(){
   // the following checks are published at:
   // http://math.fullerton.edu/mathews/n2003/CholeskyMod.html

   // We want to solve the system Ax=b

  /* double A[25]={2.,1.,1.,3.,2.,
                 1.,2.,2.,1.,1.,
                 1.,2.,9.,1.,5.,
                 3.,1.,1.,7.,1.,
                 2.,1.,5.,1.,8.};*/

   int iterations = 50;
   int aSize = 9000000;
   int bSize = 3000;
   /*double A[aSize]; int randomIndices[aSize];
   double LU[aSize];
   double b[bSize];*/
while(bSize <= 10000) {

   double* A = new double[aSize];
   int* randomIndices = new int[aSize];
   double* LU = new double[aSize];
   double* b= new double[bSize];
   //double x[bSize];
   int& d = bSize;

   int numMeasurements = 2; // one for each first level inner loop

   std::chrono::time_point<std::chrono::system_clock> now = std::chrono::system_clock::now();
   std::time_t currentTime = std::chrono::system_clock::to_time_t(now);
   cout << "-------------------------------------------------------------------" << endl;
   cout << bSize << " x " << bSize << " = " << aSize << " started at " <<  std::ctime(&currentTime);// << endl;
   cout << "-------------------------------------------------------------------" << endl;
   /*cout << "RAND_MAX : " << RAND_MAX << endl;
   cout << "INT_MIN : " << std::numeric_limits<int>::min() << endl;
   cout << "INT_MAX : " << std::numeric_limits<int>::max() << endl;*/
   //return 0;

   std::map<std::string,std::vector<std::chrono::duration<double>>> timedUnitTotal;
   std::map<std::string,std::map<int, std::vector<double>>> timedSectionTotal;
   std::map<std::string,std::map<int, std::vector<std::chrono::duration<double>>>> timedSectionMinTotal, timedSectionMaxTotal;
   std::map<int, std::vector<double>> croutSectionTotal, dolittleSectionTotal, randomAccessSectionTotal;
   std::map<int, std::vector<std::chrono::duration<double>>> croutSectionMinTotal, croutSectionMaxTotal;
   std::map<int, std::vector<std::chrono::duration<double>>> dolittleSectionMinTotal, dolittleSectionMaxTotal;
   std::map<int, std::vector<std::chrono::duration<double>>> randomAccessSectionMinTotal, randomAccessSectionMaxTotal;
   std::vector<std::chrono::duration<double>> croutTotal, dolittleTotal, randomAccessTotal;

   for(int i=1;i<=numMeasurements; i++) {
	   croutSectionTotal[i] = vector<double>();
	   dolittleSectionTotal[i] = vector<double>();
	   croutSectionMinTotal[i] = vector<std::chrono::duration<double>>();
	   croutSectionMaxTotal[i] = vector<std::chrono::duration<double>>();
	   dolittleSectionMinTotal[i] = vector<std::chrono::duration<double>>();
	   dolittleSectionMaxTotal[i] = vector<std::chrono::duration<double>>();
	   randomAccessSectionMinTotal[i] = vector<std::chrono::duration<double>>();
	   randomAccessSectionMaxTotal[i] = vector<std::chrono::duration<double>>();
   	   }

//while()
   for(int iter=0; iter <iterations; iter++) {
	   //cout << "------- Iteration : " << iter << endl;

	   //cout << "------- clearing maps "<< endl;
	   randomAccessInnerTimes.clear();
	   croutInnerTimes.clear();
	   dolittleInnerTimes.clear();
	   timedUnitInnerTimes.clear();
/*	   cout << "------- creating arrays "<< endl;

	   double* A = new double[aSize];
	   int* randomIndices = new int[aSize];
	   double* LU = new double[aSize];
	   double* b= new double[bSize];*/
/*int max_el = 0;
int max_index = 0;*/
	   for(int i=0;i<aSize;i++) {
		   A[i] = rand() % 10;
		   randomIndices[i] = rand() % aSize; // Generate random numbers from 0->aSize
		   /*if(randomIndices[i] > max_el) {
		   		max_el = A[i];
		   		max_index = i;
		   }*/
		   /*if(i==0)
			   cout<< "randomIndices[0] = " << randomIndices[0] << endl;
		   if(A[i]<0 || randomIndices[i] < 0) {
			   cout << "random gen overflow" << endl;
			   return 3;
		   }*/
	   }
	   //cout << "max_random = " << max_el << " | max index = " << max_index << endl;

	   for(int i=0;i<bSize;i++)
		   b[i] = rand() % 10;

	   // Set-up the map depending on the number of measurements we will take per iteration
	   for(int i=1;i<=numMeasurements; i++) {
		   croutInnerTimes[i] = vector<std::chrono::duration<double>>();
		   dolittleInnerTimes[i] = vector<std::chrono::duration<double>>();
		   randomAccessInnerTimes[i] = vector<std::chrono::duration<double>>();
	   }
	   //cout << "Calling Crout" << endl;
	   std::chrono::time_point<std::chrono::system_clock> start, end;
	   start = std::chrono::system_clock::now();
	   Crout(d,A,LU);
	   end = std::chrono::system_clock::now();
	   std::chrono::duration<double> elapsed = end-start;
	   croutTotal.push_back(elapsed);
	   //coutMatrix(d, A);
	   //cout << "Run-time : " << elapsed.count() << endl;

	   std::vector<Sum> croutSum = std::vector<Sum>(numMeasurements);
	   std::vector<std::chrono::duration<double>>::iterator max, min;
	   for(int i=1;i<=numMeasurements; i++) {
			croutSum[i-1] = for_each(croutInnerTimes[i].begin(), croutInnerTimes[i].end(), Sum());
			max = max_element(croutInnerTimes[i].begin(), croutInnerTimes[i].end());
			min = min_element(croutInnerTimes[i].begin(), croutInnerTimes[i].end());
			double av = croutSum[i-1].sum.count()/croutInnerTimes[i].size();
			croutSectionTotal[i].push_back(av);
			croutSectionMaxTotal[i].push_back(*max);
			croutSectionMinTotal[i].push_back(*min);
//			cout << "Crout loop"<< i << " average : " << croutSum[i-1].sum.count()/croutInnerTimes[i].size() << endl;
//			cout << "Crout loop"<< i << " max : " << max->count() << endl;
//			cout << "Crout loop"<< i << " min : " << min->count() << endl;
	   }
	   //cout<<"Crout";
	   //coutMatrix(d,LU);
	   //solveCrout(d,LU,b,x);
	   //cout<<"solveCrout";
	   //coutVector(d,x);
/*
	   //cout << "Calling Dolittle" << endl;
	   start = std::chrono::system_clock::now();
	   Doolittle(d,A,LU);
	   end = std::chrono::system_clock::now();
	   elapsed = end-start;
	   dolittleTotal.push_back(elapsed);
	   //cout << endl << "Run-time : " << elapsed.count() << endl;

	   std::vector<Sum> dolittleSum = std::vector<Sum>(numMeasurements);
		  //std::vector<std::chrono::duration<double>>::iterator max, min;
	   for(int i=1;i<=numMeasurements; i++) {
			dolittleSum[i-1] = for_each(dolittleInnerTimes[i].begin(), dolittleInnerTimes[i].end(), Sum());
			max = max_element(dolittleInnerTimes[i].begin(), dolittleInnerTimes[i].end());
			min = min_element(dolittleInnerTimes[i].begin(), dolittleInnerTimes[i].end());
			double av = dolittleSum[i-1].sum.count()/dolittleInnerTimes[i].size();
			dolittleSectionTotal[i].push_back(av);
			dolittleSectionMaxTotal[i].push_back(*max);
			dolittleSectionMinTotal[i].push_back(*min);
//			cout << "DoLittle loop"<< i << " average : " << dolittleSum[i-1].sum.count()/dolittleInnerTimes[i].size() << endl;
//			cout << "DoLittle loop"<< i << " max : " << max->count() << endl;
//			cout << "DoLittle loop"<< i << " min : " << min->count() << endl;
	   }

	   //cout<<"Doolittle";
	   //coutMatrix(d,LU);
	   //solveDoolittle(d,LU,b,x);
	   //cout<<"solveDoolittle";
	   //coutVector(d,x);

	   //cout << "Calling Random" << endl;

	   //cout << "checking randomIndices[0] before randomaccess= " << randomIndices[0] << endl;
	   start = std::chrono::system_clock::now();
		randomAccessDemo(d, A, LU, randomIndices);
		end = std::chrono::system_clock::now();
		//cout << "checking randomIndices[0] after randomaccess= " << randomIndices[0] << endl;
		elapsed = end - start;
		randomAccessTotal.push_back(elapsed);
		//cout << endl << "Run-time : " << elapsed.count() << endl;

		std::vector < Sum > randomAccessSum = std::vector < Sum > (numMeasurements);
		//std::vector<std::chrono::duration<double>>::iterator max, min;
		for (int i = 1; i <= numMeasurements; i++) {
			randomAccessSum[i - 1] = for_each(randomAccessInnerTimes[i].begin(), randomAccessInnerTimes[i].end(), Sum());
			max = max_element(randomAccessInnerTimes[i].begin(), randomAccessInnerTimes[i].end());
			min = min_element(randomAccessInnerTimes[i].begin(), randomAccessInnerTimes[i].end());
			double av = randomAccessSum[i - 1].sum.count() / randomAccessInnerTimes[i].size();
			randomAccessSectionTotal[i].push_back(av);
			randomAccessSectionMaxTotal[i].push_back(*max);
			randomAccessSectionMinTotal[i].push_back(*min);
		}
try{
		//cout << "Calling Completely Random" << endl;
		//cout << "checking randomIndices[0] before = " << randomIndices[0] << endl;
		start = std::chrono::system_clock::now();
		completeRandomAccessDemo(d, A, LU, randomIndices);
		end = std::chrono::system_clock::now();
		//cout << "checking randomIndices[0] after = " << randomIndices[0] << endl;
		elapsed = end - start;
		timedUnitTotal["completeRandom"].push_back(elapsed);
		//cout << endl << "Run-time : " << elapsed.count() << endl;

		std::vector < Sum > timedUnitSectionSum = std::vector <Sum> (numMeasurements);

		for (int i = 1; i <= numMeasurements; i++) {
			timedUnitSectionSum[i - 1] = for_each(timedUnitInnerTimes["completeRandom"][i].begin(),timedUnitInnerTimes["completeRandom"][i].end(), Sum());
			max = max_element(timedUnitInnerTimes["completeRandom"][i].begin(),timedUnitInnerTimes["completeRandom"][i].end());
			min = min_element(timedUnitInnerTimes["completeRandom"][i].begin(),timedUnitInnerTimes["completeRandom"][i].end());
			double av = timedUnitSectionSum[i - 1].sum.count()/ timedUnitInnerTimes["completeRandom"][i].size();
			timedSectionTotal["completeRandom"][i].push_back(av);
			timedSectionMaxTotal["completeRandom"][i].push_back(*max);
			timedSectionMinTotal["completeRandom"][i].push_back(*min);
		}
}catch (exception &e) { cout << e.what(); }
*/
	/*
	   Cholesky(d,A,LU);
	   //cout<<"Cholesky";
	   //coutMatrix(d,LU);
	   solveCholesky(d,LU,b,x);
	   cout<<"solveCholesky";
	   coutVector(d,x);
	   */

   }
   std::map<std::string, std::vector<double>> timedSectionAverage;
   std::map<std::string, std::vector<double>> timedSectionMaxAverage,  timedSectionMinAverage;
   std::vector<double> croutSectionAverage, dolittleSectionAverage, randomAccessSectionAverage;
   // vector to store averages for each loop/section in the algorithm
   std::vector<double> croutSectionMaxAverage, croutSectionMinAverage, dolittleSectionMaxAverage, dolittleSectionMinAverage
   , randomAccessSectionMaxAverage, randomAccessSectionMinAverage;

   Sum croutSum = for_each(croutTotal.begin(), croutTotal.end(), Sum());
   Sum dolittleSum = for_each(dolittleTotal.begin(), dolittleTotal.end(), Sum());
   Sum randomAccessSum = for_each(randomAccessTotal.begin(), randomAccessTotal.end(), Sum());
   double croutRunTimeAverage = croutSum.sum.count()/iterations;
   double dolittleRuntimeAverage = dolittleSum.sum.count()/iterations;
   double randomAccessRuntimeAverage = randomAccessSum.sum.count()/iterations;
   cout << "Crout run-time: " << croutRunTimeAverage << endl;
	cout << "Dolittle run-time: " << dolittleRuntimeAverage << endl;
	cout << "Rndm Acs run-time: " << randomAccessRuntimeAverage << endl;

	for(auto aTimedUnitTotal : timedUnitTotal) {
		Sum timedUnitSum = for_each(aTimedUnitTotal.second.begin(), aTimedUnitTotal.second.end(), Sum());
		double timedUnitRunTimeAverage = timedUnitSum.sum.count() / iterations;
		cout << "timedUnit[" << aTimedUnitTotal.first << "] run-time: " << timedUnitRunTimeAverage << endl;
		//Sum timedUnitSum = for_each(timedUnitTotal["completeRandom"].begin(), timedUnitTotal["completeRandom"].end(), Sum());
		//double timedUnitRunTimeAverage = timedUnitSum.sum.count() / iterations;
		//cout << "timedUnit[" << "completeRanom" << "] run-time: " << timedUnitRunTimeAverage << endl;
	}


   for(int i=1; i<=numMeasurements; i++) {
	   double sum = 0;
	   double average = 0;

	   for(auto aTimedSectionTotal : timedSectionTotal) {
		  for_each((aTimedSectionTotal.second)[i].begin(), (aTimedSectionTotal.second)[i].end(), [&sum](double n) {sum+=n;});
		  average = sum/iterations;
		  timedSectionAverage[aTimedSectionTotal.first].push_back(average);
	   }
	   for(auto aTimedSectionMaxTotal : timedSectionMaxTotal) {
		   Sum timedSectionMaxTotals = for_each((aTimedSectionMaxTotal.second)[i].begin(), (aTimedSectionMaxTotal.second)[i].end(), Sum());
		   timedSectionMaxAverage[(aTimedSectionMaxTotal.first)].push_back(timedSectionMaxTotals.sum.count()/iterations);
	   }

	   for(auto aTimedSectionMinTotal : timedSectionMinTotal) {
		   Sum timedSectionMinTotals = for_each((aTimedSectionMinTotal.second)[i].begin(), (aTimedSectionMinTotal.second)[i].end(), Sum());
		   timedSectionMinAverage[(aTimedSectionMinTotal.first)].push_back(timedSectionMinTotals.sum.count()/iterations);
	   }


	   sum = 0;
	   for_each(croutSectionTotal[i].begin(), croutSectionTotal[i].end(), [&sum](double n) {sum+=n;});
	   average = sum/iterations;
	   croutSectionAverage.push_back(average);

	   sum = 0;
	   for_each(dolittleSectionTotal[i].begin(), dolittleSectionTotal[i].end(), [&sum](double n) {sum+=n;});
	   average = sum/iterations;
	   dolittleSectionAverage.push_back(average);

	   sum = 0;
	   for_each(randomAccessSectionTotal[i].begin(), randomAccessSectionTotal[i].end(), [&sum](double n) {sum+=n;});
	   average = sum/iterations;
	   randomAccessSectionAverage.push_back(average);

	   Sum croutSectionMaxTotals = for_each(croutSectionMaxTotal[i].begin(), croutSectionMaxTotal[i].end(), Sum());
	   Sum croutSectionMinTotals = for_each(croutSectionMinTotal[i].begin(), croutSectionMinTotal[i].end(), Sum());
	   Sum dolittleSectionMaxTotals = for_each(dolittleSectionMaxTotal[i].begin(), dolittleSectionMaxTotal[i].end(), Sum());
	   Sum dolittleSectionMinTotals = for_each(dolittleSectionMinTotal[i].begin(), dolittleSectionMinTotal[i].end(), Sum());
	   Sum randomAccessSectionMaxTotals = for_each(randomAccessSectionMaxTotal[i].begin(), randomAccessSectionMaxTotal[i].end(), Sum());
	   Sum randomAccessSectionMinTotals = for_each(randomAccessSectionMinTotal[i].begin(), randomAccessSectionMinTotal[i].end(), Sum());

	   croutSectionMaxAverage.push_back(croutSectionMaxTotals.sum.count()/iterations);
	   croutSectionMinAverage.push_back(croutSectionMinTotals.sum.count()/iterations);
	   dolittleSectionMaxAverage.push_back(dolittleSectionMaxTotals.sum.count()/iterations);
	   dolittleSectionMinAverage.push_back(dolittleSectionMinTotals.sum.count()/iterations);
	   randomAccessSectionMaxAverage.push_back(randomAccessSectionMaxTotals.sum.count()/iterations);
	   randomAccessSectionMinAverage.push_back(randomAccessSectionMinTotals.sum.count()/iterations);
   }

   for(int i=1;i<=numMeasurements; i++) {
	   cout << "Crout loop" << i << " av: " << croutSectionAverage[i-1] << endl;
	   cout << "Doltl loop" << i << " av: " << dolittleSectionAverage[i-1] << endl;
	   cout << "rndm loop" << i << " av: " << randomAccessSectionAverage[i-1] << endl;
	   for(auto aTimedSectionAverage : timedSectionAverage) {
		   cout << (aTimedSectionAverage.first) << " loop" << i << " av: " << (aTimedSectionAverage.second)[i-1] << endl;
	   }
	   /*cout << "Crout loop" << i << " av max : " << croutSectionMaxAverage[i-1] << endl;
	   cout << "Crout loop" << i << " av min : " << croutSectionMinAverage[i-1] << endl;
		cout << "DoLtl loop" << i << " av max : " << dolittleSectionMaxAverage[i-1] << endl;
		cout << "DoLtl loop" << i << " av min : " << dolittleSectionMinAverage[i-1] << endl;*/
	}

   delete[] A;
   delete[] b;
   delete[] LU;
   delete[] randomIndices;

   bSize+=100;
   aSize = bSize*bSize;
}
}





