/*
 * FunctionFinder.cpp
 *
 *  Created on: Jul 7, 2015
 *      Author: heisenberg
 */

#include "clang/ASTMatchers/ASTMatchers.h"
#include "clang/ASTMatchers/ASTMatchFinder.h"
#include "clang/AST/ASTContext.h"
#include "clang/Tooling/Tooling.h"
#include "clang/Tooling/CommonOptionsParser.h"
#include <string>
#include <iostream>

using namespace clang;
using namespace clang::ast_matchers;
using namespace clang::tooling;

static llvm::cl::OptionCategory MyToolCategory("Matcher Sample");
/*
static bool areSameVariable(const ValueDecl *First, const ValueDecl *Second) {
  return First && Second &&
         First->getCanonicalDecl() == Second->getCanonicalDecl();
}

static bool areSameExpr(ASTContext *Context, const Expr *First,
                        const Expr *Second) {
  if (!First || !Second)
    return false;
  llvm::FoldingSetNodeID FirstID, SecondID;
  First->Profile(FirstID, *Context, true);
  Second->Profile(SecondID, *Context, true);
  return FirstID == SecondID;
}
*/
DeclarationMatcher functionMatcher = functionDecl(isDefinition(),
		unless(constructorDecl()), unless(destructorDecl()), unless(isImplicit())).bind("funcDecl");

/*
StatementMatcher LoopMatcher =
    forStmt(hasLoopInit(declStmt(
                hasSingleDecl(varDecl(hasInitializer(integerLiteral(equals(0))))
                                  .bind("initVarName")))),
            hasIncrement(unaryOperator(
                hasOperatorName("++"),
                hasUnaryOperand(declRefExpr(
                    to(varDecl(hasType(isInteger())).bind("incVarName")))))),
            hasCondition(binaryOperator(
                hasOperatorName("<"),
                hasLHS(ignoringParenImpCasts(declRefExpr(
                    to(varDecl(hasType(isInteger())).bind("condVarName"))))),
                hasRHS(expr(hasType(isInteger())))))).bind("forLoop");
*/
class FunctionHandler : public MatchFinder::MatchCallback {
public :
  virtual void run(const MatchFinder::MatchResult &Result) {
    ASTContext *Context = Result.Context;
    const FunctionDecl *FD = Result.Nodes.getDeclAs<FunctionDecl>("funcDecl");
    // We do not want to convert header files!
    if (!FD || !Context->getSourceManager().isInMainFile(FD->getLocation()))
      return;
    //const VarDecl *IncVar = Result.Nodes.getNodeAs<VarDecl>("incVarName");
    //const VarDecl *CondVar = Result.Nodes.getNodeAs<VarDecl>("condVarName");
    //const VarDecl *InitVar = Result.Nodes.getNodeAs<VarDecl>("initVarName");

    //if (!areSameVariable(IncVar, CondVar) || !areSameVariable(IncVar, InitVar))
    //  return;
    llvm::outs() << "Potential function discovered. "<< FD->getDeclName() << " " << FD->getLocation().printToString(*(Result.SourceManager)) << "\n";
  }
};

class LoopHandler : public MatchFinder::MatchCallback {
public :
  virtual void run(const MatchFinder::MatchResult &Result) {
    ASTContext *Context = Result.Context;

    const ForStmt *FS = Result.Nodes.getStmtAs<ForStmt>("forLoop");
    const WhileStmt *WS = Result.Nodes.getStmtAs<WhileStmt>("whileLoop");

    // We do not want to convert header files!
        if ((!FS || !Context->getSourceManager().isInMainFile(FS->getForLoc())) && (!WS || !Context->getSourceManager().isInMainFile(WS->getWhileLoc())))
          return;

    if(FS) {
    	//Expr *exp = FS->getCond();
    	Stmt *body = const_cast<Stmt*>(FS->getBody()); // getBody() was not using the non-const version for some reason
    	//const StmtIterator si2 = FS->children();
    	//ConstStmtIterator si(si2);
    	 //CompoundStmt *cStmt = dyn_cast<CompoundStmt>(body);
    	//StmtIterator si(&body);//begin();
    	{
    		if(CompoundStmt *cStmt = dyn_cast<CompoundStmt>(body)) {
    			//cStmt->dump();
    			 llvm::outs() << "Potential CompoundStatement discovered. " << cStmt->getLBracLoc().printToString(*(Result.SourceManager)) << "\n";
    			 //std::cout << "stmts = " << cStmt->size() << std::endl;
    			 for(CompoundStmt::body_iterator bi = cStmt->body_begin(); bi != cStmt->body_end(); bi++) {
    				 //(*bi)->dump();
    				 //std::cout << "HERE" << std::endl;
    				 if(CallExpr* ce = dyn_cast<CallExpr>(*bi))
    					 llvm::outs() << "Call Expr found " << ce->getDirectCallee()->getDeclName()<< "\n";
    				 else if(ForStmt* fStmt = dyn_cast<ForStmt>(*bi))
    				    llvm::outs() << "FOR discovered. " << fStmt->getForLoc().printToString(*(Result.SourceManager)) << "\n";
    			 }
    		}
    	}
//FS->dump();
       	llvm::outs() << "Potential FOR discovered. " << FS->getForLoc().printToString(*(Result.SourceManager)) << "\n";
    }
    if(WS)
       	llvm::outs() << "Potential WHILE discovered. " << WS->getWhileLoc().printToString(*(Result.SourceManager)) << "\n";


    //const VarDecl *IncVar = Result.Nodes.getNodeAs<VarDecl>("incVarName");
    //const VarDecl *CondVar = Result.Nodes.getNodeAs<VarDecl>("condVarName");
    //const VarDecl *InitVar = Result.Nodes.getNodeAs<VarDecl>("initVarName");

    //if (!areSameVariable(IncVar, CondVar) || !areSameVariable(IncVar, InitVar))
    //  return;
    /*if(FS)
    	llvm::outs() << "Potential FOR discovered. " << FS->getForLoc().printToString(*(Result.SourceManager)) << "\n";
    if(WS)
    	llvm::outs() << "Potential WHILE discovered. " << WS->getWhileLoc().printToString(*(Result.SourceManager)) << "\n";*/
  }
};

class CallExprHandler : public MatchFinder::MatchCallback {
public :
  virtual void run(const MatchFinder::MatchResult &Result) {
    ASTContext *Context = Result.Context;

    const CallExpr *cE = Result.Nodes.getStmtAs<CallExpr>("callExpr");


    // We do not want to convert header files!
    if (!cE || !Context->getSourceManager().isInMainFile(cE->getExprLoc()))
          return;

    llvm::outs() << "Found callExpression " << cE->getDirectCallee()->getDeclName() << " with "<< cE->getNumArgs() << " args " << cE->getExprLoc().printToString(*(Result.SourceManager)) << "\n" ;
    llvm::outs() << " - direct callee canonnical name : " << cE->getDirectCallee()->getCanonicalDecl()->getDeclName()<< "\n";
    llvm::outs() << " - direct callee qualified name : " << cE->getDirectCallee()->getQualifiedNameAsString() << "\n";
    llvm::outs() << " - callee type : " << cE->getCallee()->getType().getAsString()<< "\n";
    llvm::outs() << " - type : " << cE->getType().getAsString() << "\n";
    llvm::outs() << " - return type: "<< cE->getCallReturnType(*(Result.Context)).getAsString() << "\n" ;

    const Expr * const *args = cE->getArgs();
    for(const Expr* arg : cE->arguments()) {
    	llvm::outs() << " ---- arg type : " << arg->getType().getAsString() << "\n";
    }
  }
};


class BinaryOperHandler : public MatchFinder::MatchCallback {
public :
  virtual void run(const MatchFinder::MatchResult &Result) {
    ASTContext *Context = Result.Context;

    const BinaryOperator *bO = Result.Nodes.getStmtAs<BinaryOperator>("binOper");

    // We do not want to convert header files!
    if (!bO || !Context->getSourceManager().isInMainFile(bO->getExprLoc()))
          return;

    ArrayRef<ast_type_traits::DynTypedNode> parents = Result.Context->getParents(*bO);
    //llvm::outs() << "Binary operator has parents : " << parents.size() << "\n";
    for(ast_type_traits::DynTypedNode parent : parents) {
    	if(const ForStmt *pFor = parent.get<ForStmt>()) {
    		//llvm::outs() << "\n ---- Parent of operator is for loop" << "\n";
    		// get the loop condition expression to check if its the same address as the binary operator
    		// this will help us prune those because we dont need them
    		const Expr *eCondition = pFor->getCond();
    		if(eCondition == bO){
    			llvm::outs() << "\n ---- binary operator found but is part of the loop condition" << "\n";
    			return;
    		}
    	}
    }

    llvm::outs() << "\nFound binary operator " << "\n";
    llvm::outs() << " - opCode : " << bO->getOpcodeStr().str() << "\n";
    llvm::outs() << " - canonical type :" << bO->getType().getCanonicalType().getAsString() << "\n"; //getCanonicalTypeInternal().getAsString() << "\n";
    llvm::outs() << " - de-sugared type :" << bO->getType().getDesugaredType(*(Result.Context)).getAsString() << "\n";
    llvm::outs() << " - unqualified de-sugared type :" << bO->getType()->getUnqualifiedDesugaredType()->getTypeClassName() << "\n";
    llvm::outs() << " - type class name :" << bO->getType()->getTypeClassName() << "\n";
    llvm::outs() << " - unqualified type :" << bO->getType().getUnqualifiedType().getAsString() << "\n";

    Expr* lhs=  bO->getLHS();
	Expr* rhs = bO->getRHS();
	// LHS can be declRefExpr or subscriptOperator or cxxOperator. From what I've seen
	// RHS is the same but can't be subscriptOperator Unless it's ImplicitCast first and then subscriptOperator performed.
	// This is logical since the LHS can not be an RValue. Example, x[k] = y[i]+z[j], y and z have to be
	// casted to RValue first, but in the case of x we can't. Its an lValue. Keep in mind that any subscriptOperator will
	// eventually implicitly cast to an rvalue because it transforms the type being subscripted into a pointer sort of.
	// Thus it transforms double x[k] into double* x and then subscripts into k.
	if(ArraySubscriptExpr *subLHS = dyn_cast<ArraySubscriptExpr>(lhs)) {
		// Every array subscript expr has an implicit cast expr child then a declRefExpr
		if(DeclRefExpr* declVar = dyn_cast<DeclRefExpr>(dyn_cast<ImplicitCastExpr>(subLHS->getLHS())->getSubExpr())) {
			llvm::outs() << "\nDecl found-name : " << declVar->getFoundDecl()->getNameAsString() << "\n";
			llvm::outs() << "Decl found-name version 2: " << declVar->getFoundDecl()->getDeclName().getAsString()<< "\n";
			llvm::outs() << "Decl name : " << declVar->getDecl()->getNameAsString() << "\n";
			llvm::outs() << "Decl name version 2 : " << declVar->getDecl()->getDeclName().getAsString() << "\n";
		}
		if(BinaryOperator* operRHS = dyn_cast<BinaryOperator>(subLHS->getRHS())) {
			llvm::outs() << "Array Subscript operator indeed has a binary operator as its second child" << "\n";
		}
	} else if(ImplicitCastExpr *castLHS = dyn_cast<ImplicitCastExpr>(lhs)) {
		llvm::outs() << "LHS is Implicit Cast Expr. Probably LHS of a RHS expression" << "\n";
	}
  }
};

int main(int argc, const char **argv) {
  CommonOptionsParser OptionsParser(argc, argv, MyToolCategory);
  ClangTool Tool(OptionsParser.getCompilations(),
                 OptionsParser.getSourcePathList());

  std::string fName = argv[2];
  std::string fName2 = argv[3];
  std::cout << fName << std::endl;
  /*StatementMatcher loopMatcher = anyOf(forStmt(
		  hasAncestor(
		    		functionDecl(isDefinition(),
		    		unless(constructorDecl()), unless(destructorDecl()), unless(isImplicit()), hasName(fName))
		    		)).bind("forLoop"), whileStmt().bind("whileLoop"));
  */
  StatementMatcher loopMatcher = stmt(anyOf(forStmt().bind("forLoop"),whileStmt().bind("whileLoop"))
		  //,anyOf(hasAncestor(forStmt()),hasAncestor(whileStmt()))
		  ,hasAncestor(functionDecl(anyOf(hasName(fName), hasName(fName2))))
		  );

  StatementMatcher callMatcher = callExpr(
  		  anyOf(hasAncestor(forStmt()),hasAncestor(whileStmt()))
		  //,!hasParent(anyOf(forStmt(), whileStmt())) // we don't want it to match loop checks
  		  ,hasAncestor(functionDecl(anyOf(hasName(fName), hasName(fName2))))
  		  ).bind("callExpr");

  StatementMatcher operatorMatcher = binaryOperator(
   		  anyOf(hasAncestor(forStmt()),hasAncestor(whileStmt()))
 		  //,!hasParent(anyOf(forStmt(), whileStmt())) // we don't want it to match loop checks
   		  ,hasAncestor(functionDecl(anyOf(hasName(fName), hasName(fName2))))
   		  ).bind("binOper");

  LoopHandler printer;
  CallExprHandler callExprPrinter;
  BinaryOperHandler binOperPrinter;
  MatchFinder finder;
  //finder.addMatcher(loopMatcher, &printer);
  //finder.addMatcher(callMatcher, &callExprPrinter);
  finder.addMatcher(operatorMatcher, &binOperPrinter);

  return Tool.run(newFrontendActionFactory(&finder).get());
}


