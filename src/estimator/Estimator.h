#ifndef ESTIMATOR_HEADER
#define ESTIMATOR_HEADER

#include "../knowledge_base/IKnowledgeBase.h"
#include "../knowledge_base/QueryGenerator.h"
#include "dlfcn.h"
#include "../utils/FileUtils.h"
#include "../ParseState.h"
#include "../utils/ProcessingUtils.h"

class IPerformanceEstimator {
public:
    virtual ~IPerformanceEstimator() {}
    virtual void estimate() = 0;
};

//class Matches; // Come from the Knowledge-base
class ScopeRelationships; // Come from Static-Analyzer
//class RuntimeData; // Come from Dynamic-Analysis
//class Metrics; // Come from Metric-Knowledge-base

#include "../json.hpp"
using Matches = ASTTreeQueryResult::MatchSet;
using Metrics = IMetricCollection;
using RuntimeData = nlohmann::json;

// Does naive addition of metrics. More complicated estimators can
// do interpolations, extrapolations, data-fitting into polynomials...
class NaiveEstimator : public IPerformanceEstimator {
public:
    //NaiveEstimator(Matches* matches, Metrics* metrics);
    NaiveEstimator(IKnowledgeBase* db, Matches* matches, Metrics* metrics, RuntimeData captures, const std::vector<std::shared_ptr<LoopInfoStruct>>& functionLoops);
    //NaiveEstimator(Matches matches, ScopeRelationships rels, RuntimeData run, Metrics metrics);
    void estimate();
private:
    void getDataPoints();
    //IMetricCollection* prepareMetricsQuery(long dbId, long runId, IDataPointCollection *dps);
private:
    IKnowledgeBase* _db;
    Metrics* _metrics;
    Matches* _matches;
    RuntimeData _captures;
    std::vector<std::shared_ptr<LoopInfoStruct>> _functionLoops;
};

class LinearInterpolationEstimator : public IPerformanceEstimator {
public:
    LinearInterpolationEstimator(Matches matches, Metrics metrics);
    //LinearInterpolationEstimator(Matches matches, ScopeRelationships rels, RuntimeData run, Metrics metrics);
    void estimate();
};

class PolynomialFittingEstimator : public IPerformanceEstimator {
public:
    PolynomialFittingEstimator(Matches matches, Metrics metrics);
    //PolynomialFittingEstimator(Matches matches, ScopeRelationships rels, RuntimeData run, Metrics metrics);
    void estimate();
};

NaiveEstimator::NaiveEstimator(IKnowledgeBase* db, Matches* matches, Metrics* metrics, RuntimeData captures, const std::vector<std::shared_ptr<LoopInfoStruct>>& functionLoops)
    : _db(db), _metrics(metrics), _matches(matches), _captures(captures), _functionLoops(functionLoops) {

}

/* (x1,y1) and (x2,y2) are the known datapoints. x is the current datapoint and
 * y is the y component we are trying to interpolate. Linear interpolation
 * and extrapolation is the same. We get the slope and y-intercept and then
 * deduce the y-component.
*/
float linear_polate(float x1, float y1 , float x, float x2, float y2) {
    float m = (y2-y1)/(x2-x1);
    float b = y2 - m*x2;
    float y = m*x + b;
    return y;
}

void NaiveEstimator::getDataPoints() {
    RuntimeData capture;
    std::map<std::string, int> intVars;
    std::map<std::string, float> floatVars;
    std::map<std::string, std::string> stringVars;
    if(capture.is_number_integer()) {

    }
    else if(capture.is_number_float()) {

    }
    else if(capture.is_string()) {

    }
    //doGetDatapoints(intVars, floatVars, stringVars);
}

//void traverseLoopAndSerialize(std::shared_ptr<LoopInfoStruct> loop, Matches* matches) {
void traverseLoopAndSerialize(std::shared_ptr<LoopInfoStruct> loop, ForStmt* matchedForStmt, std::shared_ptr<IPatternNode> matchingNode, std::ostream& oss) {
    /*ForStmt* matchedForStmt = nullptr;
    QueryMatch firstMatch = matches->front();
    clforOperation* forLoop = dyn_cast<clforOperation>(firstMatch.coiNode);
    if(forLoop) {
        ForStmt* forStmt = dyn_cast<ForStmt>(forLoop->getUnderlyingPtr());
        if(forStmt) {
            // we get LoopInfoStruct of this forStmt
            matchedForStmt = forStmt;
        }
    }*/
    //std::ostringstream oss;
    if(matchedForStmt != loop->current) { // if its not the matched loop, serialize it
        oss << loop->modifiedText;
        oss << "{\n";
        for(auto child: loop->innerLoops) {
            traverseLoopAndSerialize(child, matchedForStmt, matchingNode, oss);
        }
        oss << "\n}";
    }
    else { // if its the matchedloop, don't serialize. Instead inject a query Call to the KB for the metrics of that loop
        std::shared_ptr<operation> loopOperation = matchingNode->getPattern()->at(0);
        int matchingLoopId = -1;
        if(clforOperation* casted = dyn_cast<clforOperation>(loopOperation.get())) {
            matchingLoopId = casted->getLoopId();
        }
        else {
            spdlog::get("sopelogger")->error("Error in Estimator. Matching operation should be a LoopOperation but it is not.");
        }
        //oss << "// query KB here;";
        oss << "IDataPoint* dp = new DataPoint<";
        oss << "int" << ">";
        //oss << "({{\"k\",112}}, {1});" << "\n";
        //oss << "({{\"k\"," << loop->tripCount << "}}, {1});" << "\n";
        oss << "({";
        oss << "{\"_sope_loop_start_\"," << loop->start << "},";
        oss << "{\"_sope_loop_end_\"," << loop->end << "},";
        oss << "{\"_sope_loop_trip_count_\"," << loop->tripCount << "}";
        oss << "}, {";
        oss << "2";
        //oss << std::to_string(matchingLoopId);
        oss << "});" << "\n";
        oss << "dpv->add(dp);" << "\n";
        //oss << "queryKB();";
    }
}

void generateQueryerFile(long runId, std::string queryFunctionBody) {
    std::string space = " ";
    std::string newLine = "\n";
    std::string className = "queryer"+std::to_string(runId);
    std::string includes = "#include \"../src/knowledge_base/QueryGenerator.h\"";
    std::string inheritedClass = ": public QueryGenerator {";
    std::string funcReturn = "IDataPointCollection*";
    std::string funcSignature = "queryDatapoints(std::map<std::string, int>& intVars, std::map<std::string, float>& floatVars, std::map<std::string, std::string>& stringVars)";
    std::string funcDecl = funcReturn + space + funcSignature + ";";
    std::string classBody = "public:" + newLine+ funcDecl + newLine + "};";
    std::string classDecl ="class" +space + className + inheritedClass + newLine + classBody;
    //std::string funcImpl = "std::vector<float> returns = {1.2f, 34.03f, 96.51f }; return returns;";
    std::string metricVector = "DataPointVector* dpv = new DataPointVector();";
    std::string returnStmt = "return dpv;";
    std::string funcImpl = "std::vector<float> returns = {1.2f, 34.03f, 96.51f };" + newLine + metricVector + newLine + queryFunctionBody + newLine + returnStmt;
    std::string funcDef = funcReturn + space + className + "::" + funcSignature + "{\n"+ funcImpl + "\n}";
    std::string globalFactoryFunc = "QueryGenerator* create_queryer() { return new " + className + ";}";
    std::string fileContent = includes + newLine + classDecl + newLine + funcDef + newLine + globalFactoryFunc;

    std::string filePath = "./generated/"+className+".cpp";
    createCXXFile(filePath, fileContent);
}

/* The estimator does the following:
 * 1- creates (textual representation) a function that mimics the functions in the COI we are analyzing. (it's basically a shell of the function and the loops)
 * 2- creates a file and embeds this function. The file has a predetermined format, includes, etc..
 * 3- compiles the file into a .o file, then into a .so shared object.
 */
int buildDynamicQueryer(long runId) {
    std::string fname = "queryer"+std::to_string(runId);
    //std::string headerDir  = "/home/elie/Qt-workspace/astcomparator/src/knowledge_base/";
    std::string headerDir  = "./generated/";
    std::string headerPath = headerDir + fname + ".cpp";
    std::string soDir  = "./lib/";
    std::string soPath = soDir + fname + ".so";
    std::cout << "Header: " << headerPath << std::endl;
    std::string compilePIC = "g++ -std=c++11 -shared -fPIC -o " + soPath + " " + headerPath;
    int r = std::system(compilePIC.c_str());
    return r;
}

/* The estimator does the following:
 * 1- loads the .so files, imports all the symbols, and runs the function we just built.
 * 2- the function will basically query the Knowledge base for the appropriate metrics (since it mimics the original COI function)
 * 2- uses the result of that function to estimate the performance of the COI.
 */
IDataPointCollection* loadDynamicQueryer(long runId, std::map<std::string, int>& iVars, std::map<std::string, float>& fVars, std::map<std::string, std::string>& sVars) {
    std::streambuf* oldBuffer = std::cout.rdbuf();
    /* on Linux, use "./myclass.so" */
    //std::string soFilepath = "/home/elie/Qt-workspace/astcomparator/lib/queryer"+std::to_string(runId)+".so";
    std::string soFilepath = "./lib/queryer"+std::to_string(runId)+".so";
    std::cout << "SharedLib: " << soFilepath << std::endl;
    void* handle = dlopen(soFilepath.c_str(), RTLD_NOW);

    if(!handle) {
        spdlog::get("sopelogger")->error("{}", dlerror());
    }
    QueryGenerator* (*create)(); // function ptrs
    void (*destroy)(QueryGenerator*);

    create = (QueryGenerator* (*)()) dlsym(handle, "create_queryer");
    //destroy = (void (*)(QueryGenerator*))dlsym(handle, "destroy_queryer");

    QueryGenerator* queryer = (QueryGenerator*)create();
    if(!queryer) {
        spdlog::get("sopelogger")->error("{}", dlerror());
    }
    IDataPointCollection* result = queryer->queryDatapoints(iVars,fVars,sVars);
    std::cerr << result->size() << std::endl;
    // destroy all the objects created
    delete queryer;
    //destroy( queryer);
    // close the shared object handle
    //dlclose(handle);
    std::cout.rdbuf(oldBuffer);
    //std::cerr << "Results size = " << result->size() << std::endl;
    return result;
}

void loadCaptures(RuntimeData captures, std::map<std::string, int>& iVars, std::map<std::string, float>& fVars, std::map<std::string, std::string>& sVars) {
    if(!captures.is_null()) {
        json capturedVars = captures["captured_vars"];
        for (json::iterator it = capturedVars.begin(); it != capturedVars.end(); ++it) {
            std::string name = (*it)["name"];
            json val = (*it)["value"];
            if(val.is_number_integer()) {
                iVars[name] = (*it)["value"];
            }
            else if(val.is_number_float()) {
                fVars[name] = (*it)["value"];
            }
            else if(val.is_string()) {
                sVars[name] = (*it)["value"];
            }
        }
    }
}

/*IMetricCollection* NaiveEstimator::prepareMetricsQuery(long dbId, long runId, IDataPointCollection* dps) {
    // prepares the metric query and uses the DBUtils functions to query the metrics.
    IMetricQuery* q = new ASTMetricQuery(dbId, runId, dps);
    IMetricQueryResult* mqr = _db->queryCodeMetrics(q, treeQueryResult);
    return mqr->getMetrics();
}*/

void NaiveEstimator::estimate() {
    // Get the forStmt(s) at which matches occured
    ForStmt* matchedForStmt = nullptr;
    matchedForStmt = _functionLoops[0]->innerLoops[0]->innerLoops[0]->current;
    /*QueryMatch firstMatch = _matches->front(); // TODO: Assumes one match only
    clforOperation* forLoop = dyn_cast<clforOperation>(firstMatch.coiNode->getPattern()->at(0).get());
    if(forLoop) {
        ForStmt* forStmt = dyn_cast<ForStmt>(forLoop->getUnderlyingPtr());
        if(forStmt) {
            matchedForStmt = forStmt;
        }
    }*/
    // Serialize the loops in the COI function to generated the queryer call to the Knowledge base
    std::ostringstream oss;
    for(auto loop: _functionLoops) {
        traverseLoopAndSerialize(loop,matchedForStmt, _matches->front().benchNode, oss);
        std::string queryFunctionBody = oss.str();
        generateQueryerFile(123, queryFunctionBody);
    }
    // Build and run the generated file.
    buildDynamicQueryer(123);
    std::map<std::string, int> intVars;
    std::map<std::string, float> floatVars;
    std::map<std::string, std::string> stringVars;
    loadCaptures(_captures, intVars, floatVars, stringVars);
    auto result = loadDynamicQueryer(123, intVars, floatVars, stringVars);
    //std::cout << "HERE" << std::endl;
    //std::cout << result << std::endl;
    std::cerr << "Datapoints queried = " << result->size() << std::endl;
    DataPointVector* dpv = dyn_cast<DataPointVector>(result);
    /*if(dpv) {
        std::cout << "CASTED" << std::endl;
        int count = 0;
        auto dps = dpv->dataPoints();
        for(IDataPoint* dp : dps) {
            //if(count%100==0)
            {
                std::cout << dp->toString() << std::endl;
            }
            count++;
        }
    }*/

    //std::cout << "Matching benchmark = " << firstMatch.matchingBench->Id() << std::endl;
    //IMetricCollection* resultMetrics = prepareMetricsQuery(12, -1, result);

    QueryMatch firstMatch = _matches->front(); // TODO: Assumes one match only
    auto matchingBench = firstMatch.matchingBench;
    auto benchId = matchingBench->Id();
    std::cout << "Estimator retrieving metrics for benchmark with benchId = " << benchId << std::endl;
    // queryMetrics fucntion needs an ITreeQueryResult and we create one using
    // matches we passed into this Estimator.
    HashTreeQueryResult htrq;
    htrq.matchSet = *_matches;
    IMetricQuery* metricQuery = prepareMetricQuery(result, benchId, -1);
    IMetricQueryResult* metricResult = queryMetrics(_db, metricQuery, &htrq);
    IMetricCollection* resultMetrics = metricResult->getMetrics();

    std::cout << "# of returned metrics = " << resultMetrics->size() << std::endl;
    //prepareMetricsQuery(firstMatch.matchingBench->Id(), -1, result);
    std::cout << "AFTER" << std::endl;
    //getDataPoints();
    /* TODO: Here for simplicity we get the first two datapoints with values, and use those
     * two points to interpolate all other points. This is quite wrong, and will estimate
     * performance linearly based on these two points. We want something that is more
     * piece-wise linear. Thus we should interpolate based on the closest two valid points and
     * not the first valid two points.
    */
    _metrics = resultMetrics;
    std::cout << "_metrics size = " << _metrics->size() << std::endl;
    MetricVectorIterator start = dyn_cast<DataPointMetricVector>(_metrics)->begin();
    MetricVectorIterator from = start;
    MetricVectorIterator until = dyn_cast<DataPointMetricVector>(_metrics)->end();
    IDataPointMetric* first = nullptr;
    IDataPointMetric* second = nullptr; // the first and second points of interpolation
    size_t size = _metrics->size();
    int i=0;
    while(from!=until) {
        float y = (*from)->getMetric();
        if(y > 0.0f && i < size) { // pick the first two datapoints that have values and use those for interpolation/extrapolation
            if(!first)
                first = *from;
            else if(!second)
                second = *from;
        }
        ++i;
        ++from;
    }

    float estimate = 0.;
    int interpolatedCount = 0;
    std::string units = " nsec";
    if(first && second) { // if we have two points to interpolate from
        MetricVectorIterator start = dyn_cast<DataPointMetricVector>(_metrics)->begin();
        MetricVectorIterator from = start;
        MetricVectorIterator until = dyn_cast<DataPointMetricVector>(_metrics)->end();
        while(from!=until) {
            //std::cout << (*from)->toString() << std::endl;
            float x = (*from)->datapoint()->point();
            float y = (*from)->getMetric();
            if(y < 0.0f){
                y = linear_polate(first->datapoint()->point(), first->getMetric(), x, second->datapoint()->point(), second->getMetric());
                (*from)->setMetric(y);
                std::cout << "Interpolated (" << x << "," << "-1.0f) to " << "(" << x << "," << y << ")" << std::endl;
                interpolatedCount++;
            }
            estimate+=y;
            ++from;
        }
    }
    if(estimate!=0.) {
        std::cout << "\n============ Summary ============" << std::endl;
        std::cout << "Trip count = " << size << std::endl;
        std::cout << "Previously observed = " << size - interpolatedCount << std::endl;
        std::cout << "Fitted = " << interpolatedCount << std::endl;
        std::cout << "Performace estimate = " << estimate << units << std::endl;
        std::cout << "===================================\n" << std::endl;

    }
    delete resultMetrics; // TODO: implement destructor in IMetricCollection hierarchy to delete Datapoints in the vectors
    delete metricResult;
    delete metricQuery;
}

LinearInterpolationEstimator::LinearInterpolationEstimator(Matches matches, Metrics metrics){

}

void LinearInterpolationEstimator::estimate(){

}

PolynomialFittingEstimator::PolynomialFittingEstimator(Matches matches, Metrics metrics){

}

void PolynomialFittingEstimator::estimate(){

}

#endif // ESTIMATOR_HEADER
