#ifndef OPTYPACSTYPCOMPARATOR_H
#define OPTYPACSTYPCOMPARATOR_H

#include "Operations.h"

namespace pattern {
namespace matching {


class OperationTypeCompare { //: public ComparePolicy {
public:
    OperationTypeCompare() {
    }
    ;
    virtual ~OperationTypeCompare() {
    }
    template<typename CompPolicy>
    bool isEqual( Operation<CompPolicy>& op1,
                  Operation<CompPolicy>& op2);
};

// TODO: check how to do this without stripping const behaviour
template<typename CompPolicy>
bool OperationTypeCompare::isEqual( Operation<CompPolicy>& op1,
                                    Operation<CompPolicy>& op2) {
//    return ((const_cast<Operation<CompPolicy>*>(&op1))->getOpType()
//            == (const_cast<Operation<CompPolicy>*>(&op2))->getOpType());
    return op1.getOpType()
            == op2.getOpType();
}

class AccessTypeCompare { //: public ComparePolicy {
public:
    AccessTypeCompare() {
    }
    ;
    virtual ~AccessTypeCompare() {
    }
    template<typename CompPolicy>
    bool isEqual(Operation<CompPolicy>& op1,
                 Operation<CompPolicy>& op2);
};
template<typename CompPolicy>
bool AccessTypeCompare::isEqual(Operation<CompPolicy>& op1,
                                Operation<CompPolicy>& op2) { // both operation types should be memory access. Then we should compare the pattern of access. contiguous, offset, inefficient. etc...
    // We can use the oepration kind to check if its a memory access or not. We can also use the class type. If its a MemoryOperation
    MemoryOperation<CompPolicy>* mem1 = clang::dyn_cast<MemoryOperation<CompPolicy>>(&op1);
    MemoryOperation<CompPolicy>* mem2 = clang::dyn_cast<MemoryOperation<CompPolicy>>(&op2);
    if(mem1 && mem2) {
        return mem1->getAccessType() == mem2->getAccessType();
    }
    // Return true in all other cases. Example, loopOper accessType compared to assignmentOper accessType is undefined. We don't want the comparison of the pattern to fail
    // just because we can not compare oranges to appels in the first place
    return true;
}

class OpTypAcsTypComparator
{
public:
    OpTypAcsTypComparator() {}
    template <typename CompPolicy>
    bool isEqual( Operation<CompPolicy>& op1,
                  Operation<CompPolicy>& op2);
};
template <typename CompPolicy>
bool OpTypAcsTypComparator::isEqual( Operation<CompPolicy>& op1,
                                     Operation<CompPolicy>& op2) { // both operation types should be memory access. Then we should compare the pattern of access. contiguous, offset, inefficient. etc...
    return (OperationTypeCompare().isEqual(op1, op2) && AccessTypeCompare().isEqual(op1, op2));
}

class ComparePolicy {
public:
    ComparePolicy() = delete;
    virtual ~ComparePolicy();
    virtual bool isEqual(const Operation<ComparePolicy>& op1,
                         const Operation<ComparePolicy>& op2)=0;
};

class OperationTypeComparebkp { //: public ComparePolicy {
public:
    OperationTypeComparebkp() {
    }
    ;
    virtual ~OperationTypeComparebkp() {
    }
    ;
    virtual bool isEqual(const Operation<OperationTypeCompare>& op1,
                         const Operation<OperationTypeCompare>& op2);
};

// TODO: check how to do this without stripping const behaviour
bool OperationTypeComparebkp::isEqual(const Operation<OperationTypeCompare>& op1,
                                      const Operation<OperationTypeCompare>& op2) {
    return ((const_cast<Operation<OperationTypeCompare>*>(&op1))->getOpType()
            == (const_cast<Operation<OperationTypeCompare>*>(&op2))->getOpType());
}

class AccessTypeComparebkp { //: public ComparePolicy {
public:
    AccessTypeComparebkp() {
    }
    ;
    virtual ~AccessTypeComparebkp() {
    }
    ;
    virtual bool isEqual(Operation<AccessTypeCompare>& op1,
                         Operation<AccessTypeCompare>& op2);
};

bool AccessTypeComparebkp::isEqual(Operation<AccessTypeCompare>& op1,
                                   Operation<AccessTypeCompare>& op2) { // both operation types should be memory access. Then we should compare the pattern of access. contiguous, offset, inefficient. etc...
    // We can use the oepration kind to check if its a memory access or not. We can also use the class type. If its a MemoryOperation
    MemoryOperation<AccessTypeCompare>* mem1 = clang::dyn_cast<MemoryOperation<AccessTypeCompare>>(&op1);
    MemoryOperation<AccessTypeCompare>* mem2 = clang::dyn_cast<MemoryOperation<AccessTypeCompare>>(&op2);
    if(mem1 && mem2) {
        return mem1->getAccessType() == mem2->getAccessType();
    }
    // Return true in all other cases. Example, loopOper accessType compared to assignmentOper accessType is undefined. We don't want the comparison of the pattern to fail
    // just because we can not compare oranges to appels in the first place
    return true;
}
}
}

#endif // OPTYPACSTYPCOMPARATOR_H
