#ifndef PATTERNTREECREATOR_H
#define PATTERNTREECREATOR_H

#include "Operations.h"
#include "utils/OperationHelpers.h" // Just to be able to debug and print patterns. we can remove this later
#include "IPatternTree.h"

using namespace pattern::matching;

namespace pattern {
namespace processing {

enum TreeType {
    brute_force, hierarchical,
};

template<typename T>
class PatternTreeCreator
{
public:
    PatternTreeCreator(const std::vector<std::shared_ptr<T>>& operations, TreeType type) : _nestedOperations(operations), treeType(type) {}
    virtual ~PatternTreeCreator() {}
    virtual void create() = 0;
    //PatternTree<T>& tree() { return _patternTree; }
    std::shared_ptr<IPatternTree> tree() { return _patternTree; }
protected:
    //pattern::matching::PatternTree<T> _patternTree;
    std::shared_ptr<IPatternTree> _patternTree;
    //pattern::matching::Pattern<T> _pattern;
    // nested operations is a vector that contains all the top level oeprations in a function.
    // This basically means all the operations at scope 2. (scope 1 is the function scope)
    std::vector<std::shared_ptr<T>> _nestedOperations;
    TreeType treeType;
};

template<typename T>
class HierarchicalTreeCreator: public PatternTreeCreator<T>
{
public:
    HierarchicalTreeCreator(const std::vector<std::shared_ptr<T>>& operations);
    void create();
private:
    void doCreate(std::shared_ptr<T> oper);
    void doSetup(std::shared_ptr<T> oper);
    void doCleanup(std::shared_ptr<T> oper);
    void createForNode(std::shared_ptr<T> oper);
    void createIfNode(std::shared_ptr<T> oper);
    void createAssignmentNode(std::shared_ptr<T> oper);
    void checkAndCreateElseNode(std::shared_ptr<T> oper);
    void checkIfLHSorRHSofAssignment(std::shared_ptr<T> oper);
private:
    std::stack<std::shared_ptr<PatternNode<T>>> treeLevelPatternNodes;
    std::stack<std::shared_ptr<Pattern<T>>> treeLevelPatterns;
    int nodeCount;
};

template<typename T>
HierarchicalTreeCreator<T>::HierarchicalTreeCreator(const std::vector<std::shared_ptr<T>>& operations): PatternTreeCreator<T>(operations, TreeType::hierarchical), nodeCount(0) {

}

template<typename T>
void HierarchicalTreeCreator<T>::doSetup(std::shared_ptr<T> oper) {
    /* The most-important case is when we have if-else
     * In that case we have to strip the else part from the if and attach it
     * at the same level as the if.
    */

    if(dyn_cast<loopOperation>(oper.get())) {
        /* 1- If there was a pattern being constructed before we encountered the foroperation,
         *    then we need to finalize that pattern
         * 2- We need to add a pattern node and a pattern with 1 single operation which is the foroperation
         */
        treeLevelPatterns.pop();
        treeLevelPatternNodes.pop();
        std::shared_ptr<Pattern<T>> pattern = std::shared_ptr<Pattern<T>>(new Pattern<T>());
        treeLevelPatterns.push(pattern);
        std::shared_ptr<PatternNode<T>> patternNode = std::shared_ptr<PatternNode<T>>(new PatternNode<T>(pattern,nodeCount++));
        treeLevelPatternNodes.top()->addChild(patternNode);
        treeLevelPatternNodes.push(patternNode);

        pattern->addToPattern(oper);

        /* 1- No need to pop the newly created pattern node or the pattern because we did not push
         *      them on the stacks. We just added the pattern node as a child of the element on the
         *      top of the stack
         * 2- Now create a new pattern and pattern node for the statements inside the loop body.
         *      This is the right thing to do except in the case of an if statement being the first
         *      statemnt in the body. In that case we will pop the empty pattern adn pattern nodes
         *      and create new ones. This is a waste but for now should work.
         */
        std::shared_ptr<Pattern<T>> pattern2 = std::shared_ptr<Pattern<T>>(new Pattern<T>());
        treeLevelPatterns.push(pattern2);
        std::shared_ptr<PatternNode<T>> patternNode2 = std::shared_ptr<PatternNode<T>>(new PatternNode<T>(pattern2,nodeCount++));
        treeLevelPatternNodes.top()->addChild(patternNode2);
        treeLevelPatternNodes.push(patternNode2);

    }
    else if (/*ifOperation* ifOper = */dyn_cast<ifOperation>(oper.get())) {
        // We do things similar to the for loop
        treeLevelPatterns.pop();
        treeLevelPatternNodes.pop();
        std::shared_ptr<Pattern<T>> pattern = std::shared_ptr<Pattern<T>>(new Pattern<T>());
        treeLevelPatterns.push(pattern);
        std::shared_ptr<PatternNode<T>> patternNode = std::shared_ptr<PatternNode<T>>(new PatternNode<T>(pattern,nodeCount++));
        treeLevelPatternNodes.top()->addChild(patternNode);
        treeLevelPatternNodes.push(patternNode);

        pattern->addToPattern(oper);
        // Create the new pattern node and pattern for the thenPart of the if statement
        std::shared_ptr<Pattern<T>> pattern2 = std::shared_ptr<Pattern<T>>(new Pattern<T>());
        treeLevelPatterns.push(pattern2);
        std::shared_ptr<PatternNode<T>> patternNode2 = std::shared_ptr<PatternNode<T>>(new PatternNode<T>(pattern2,nodeCount++));
        treeLevelPatternNodes.top()->addChild(patternNode2);
        treeLevelPatternNodes.push(patternNode2);
    }
    else if(!dyn_cast<varRefOperation>(oper.get()) && ! dyn_cast<numericOperation>(oper.get())) {

        auto poppedTopPattern = treeLevelPatterns.top();
        auto poppedTopPatternNode = treeLevelPatternNodes.top();
        treeLevelPatterns.pop();
        treeLevelPatternNodes.pop();
        if(treeLevelPatterns.top()->size()>0) {
            std::shared_ptr<operation> operAtZero = treeLevelPatterns.top()->at(0);
            ifOperation* ifOper = dyn_cast<ifOperation>(operAtZero.get());
            if(ifOper!=nullptr && oper == ifOper->elsePart()) { // If the pattern is for an if operation
            /* 1- The previous operation was an ifOperation
             * 2- We should finalize the pattern for the body of the if (already done by the pops above)
             * 3- Pop the if single-item pattern and pattern node too.
             * 3- We need to create a pattern node and a pattern for the else part
             * 4- We need to create a pattern and a pattern node for the body of the else.
             */
                // 3
                treeLevelPatterns.pop();
                treeLevelPatternNodes.pop();
                // 4
                std::shared_ptr<Pattern<T>> pattern = std::shared_ptr<Pattern<T>>(new Pattern<T>());
                treeLevelPatterns.push(pattern);
                std::shared_ptr<PatternNode<T>> patternNode = std::shared_ptr<PatternNode<T>>(new PatternNode<T>(pattern,nodeCount++));
                treeLevelPatternNodes.top()->addChild(patternNode);
                treeLevelPatternNodes.push(patternNode);
                pattern->addToPattern(oper);
                // 5
                std::shared_ptr<Pattern<T>> pattern2 = std::shared_ptr<Pattern<T>>(new Pattern<T>());
                treeLevelPatterns.push(pattern2);
                std::shared_ptr<PatternNode<T>> patternNode2 = std::shared_ptr<PatternNode<T>>(new PatternNode<T>(pattern2,nodeCount++));
                treeLevelPatternNodes.top()->addChild(patternNode2);
                treeLevelPatternNodes.push(patternNode2);

            }
            else { // repush the pattern we popped inorder to peek at the next pattern
                treeLevelPatterns.push(poppedTopPattern);
                treeLevelPatternNodes.push(poppedTopPatternNode);
                // Go on about adding the operation to the pattern as we would do normally
                treeLevelPatterns.top()->addToPattern(oper);
            }
        }
        else { // repush the pattern we popped inorder to peek at the next pattern
            treeLevelPatterns.push(poppedTopPattern);
            treeLevelPatternNodes.push(poppedTopPatternNode);
            // Go on about adding the operation to the pattern as we would do normally
            treeLevelPatterns.top()->addToPattern(oper);
        }
    }
}

template<typename T>
void HierarchicalTreeCreator<T>::doCleanup(std::shared_ptr<T> oper) {

    if(dyn_cast<loopOperation>(oper.get())) {
        // Pop the body of the loop
        treeLevelPatterns.pop();
        treeLevelPatternNodes.pop();
        // Pop the for operation and its corresponding pattern and patternNode.
        treeLevelPatterns.pop();
        treeLevelPatternNodes.pop();
        /* Create an empty pattern and pattern node in case there are more statements in the
         * body of the parent (check line 3 below). ex:
         *  if(...) {
         *     ...          (1)
         *     for(..) {
         *       ...        (2)
         *     }
         *     ...          (3)
         *  }
         */
        std::shared_ptr<Pattern<T>> pattern = std::shared_ptr<Pattern<T>>(new Pattern<T>());
        treeLevelPatterns.push(pattern);
        std::shared_ptr<PatternNode<T>> patternNode = std::shared_ptr<PatternNode<T>>(new PatternNode<T>(pattern,nodeCount++));
        treeLevelPatternNodes.top()->addChild(patternNode);
        treeLevelPatternNodes.push(patternNode);
    }
    else if (/*ifOperation* ifOper = */dyn_cast<ifOperation>(oper.get())) {
        /* If the IFOper had an else part, then the following pops will pop
        * the else and the else body. The if and the ifbody would have been already popped
        * by the creation of the else and its body.
        * If the IFOper had no else, then these pops will serve to pop the if and its body
        * Pop the body of the else
        */
        treeLevelPatterns.pop();
        treeLevelPatternNodes.pop();
        // Pop the else operation and its corresponding pattern and patternNode.
        treeLevelPatterns.pop();
        treeLevelPatternNodes.pop();

        /*
         * No need to pop the if part because this was already done
         * when the else node was created. If we change the else node
         * creation to keep the ifnode from being deleted, then we
         * will need to clean up the ifbodynode and ifnode as below.
         *
        // Pop the body of the if
        treeLevelPatterns.pop();
        treeLevelPatternNodes.pop();
        // Pop the if operation and its corresponding pattern and patternNode.
        treeLevelPatterns.pop();
        treeLevelPatternNodes.pop();
        *
        * /

        /* Create an empty pattern and pattern node in case there are more statements in the
         * body of the parent (check line 3 below). ex:
         *  for(...) {
         *     ...          (1)
         *     if(..) {
         *       ...        (2)
         *     }
         *     ...          (3)
         *  }
         */
        std::shared_ptr<Pattern<T>> pattern = std::shared_ptr<Pattern<T>>(new Pattern<T>());
        treeLevelPatterns.push(pattern);
        std::shared_ptr<PatternNode<T>> patternNode = std::shared_ptr<PatternNode<T>>(new PatternNode<T>(pattern,nodeCount++));
        treeLevelPatternNodes.top()->addChild(patternNode);
        treeLevelPatternNodes.push(patternNode);
    }
    else if(binaryOperation* binOper = dyn_cast<binaryOperation>(oper.get())) {
        if(binOper->getOpType() == OperationType::assign) {
            // pop the RHS
            treeLevelPatterns.pop();
            treeLevelPatternNodes.pop();
            // Pop the LHS. Actually the LHS was popped in the checkifLHSorRHS function when we peek to
            // check if the operation is a RHS.
            //treeLevelPatterns.pop();
            //treeLevelPatternNodes.pop();
            // Pop the assignment operation
            treeLevelPatterns.pop();
            treeLevelPatternNodes.pop();

            // Create an empy pattern if there are any remaining operations that might come behind this
            // assignment operation.
            std::shared_ptr<Pattern<T>> pattern = std::shared_ptr<Pattern<T>>(new Pattern<T>());
            treeLevelPatterns.push(pattern);
            std::shared_ptr<PatternNode<T>> patternNode = std::shared_ptr<PatternNode<T>>(new PatternNode<T>(pattern,nodeCount++));
            treeLevelPatternNodes.top()->addChild(patternNode);
            treeLevelPatternNodes.push(patternNode);
        }
    }
}

template<typename T>
void HierarchicalTreeCreator<T>::createForNode(std::shared_ptr<T> oper) {
    /* 1- If there was a pattern being constructed before we encountered the foroperation,
     *    then we need to finalize that pattern
     * 2- We need to add a pattern node and a pattern with 1 single operation which is the foroperation
     */
    treeLevelPatterns.pop();
    treeLevelPatternNodes.pop();
    std::shared_ptr<Pattern<T>> pattern = std::shared_ptr<Pattern<T>>(new Pattern<T>());
    treeLevelPatterns.push(pattern);
    std::shared_ptr<PatternNode<T>> patternNode = std::shared_ptr<PatternNode<T>>(new PatternNode<T>(pattern,nodeCount++));
    treeLevelPatternNodes.top()->addChild(patternNode);
    treeLevelPatternNodes.push(patternNode);

    pattern->addToPattern(oper);

    /* 1- No need to pop the newly created pattern node or the pattern because we did not push
     *      them on the stacks. We just added the pattern node as a child of the element on the
     *      top of the stack
     * 2- Now create a new pattern and pattern node for the statements inside the loop body.
     *      This is the right thing to do except in the case of an if statement being the first
     *      statemnt in the body. In that case we will pop the empty pattern adn pattern nodes
     *      and create new ones. This is a waste but for now should work.
     */
    std::shared_ptr<Pattern<T>> pattern2 = std::shared_ptr<Pattern<T>>(new Pattern<T>());
    treeLevelPatterns.push(pattern2);
    std::shared_ptr<PatternNode<T>> patternNode2 = std::shared_ptr<PatternNode<T>>(new PatternNode<T>(pattern2,nodeCount++));
    treeLevelPatternNodes.top()->addChild(patternNode2);
    treeLevelPatternNodes.push(patternNode2);
}

template<typename T>
void HierarchicalTreeCreator<T>::createIfNode(std::shared_ptr<T> oper) {
    // We do the same things in case its an IF
    // We pop and finialize the previous pattern, then create a pattern with a sinlge node
    // for the ifOper, and then create a new patternnode for the body of the IF.
    createForNode(oper);
}

template<typename T>
void HierarchicalTreeCreator<T>::createAssignmentNode(std::shared_ptr<T> oper) {
    // We do the same things in case its an = assignment
    // We pop and finialize the previous pattern, then create a pattern with a sinlge node
    // for the assignment, and then create a new patternnode for the body of the IF.
    createForNode(oper);
}

template<typename T>
void HierarchicalTreeCreator<T>::checkAndCreateElseNode(std::shared_ptr<T> oper) {
    auto poppedTopPattern = treeLevelPatterns.top();
    auto poppedTopPatternNode = treeLevelPatternNodes.top();
    treeLevelPatterns.pop();
    treeLevelPatternNodes.pop();
    if(treeLevelPatterns.top()->size()>0) {
        std::shared_ptr<operation> operAtZero = treeLevelPatterns.top()->at(0);
        ifOperation* ifOper = dyn_cast<ifOperation>(operAtZero.get());
        if(ifOper!=nullptr && oper == ifOper->elsePart()) { // If the pattern is for an if operation
        /* 1- The previous operation was an ifOperation
         * 2- We should finalize the pattern for the body of the if (already done by the pops above)
         * 3- Pop the if single-item pattern and pattern node too.
         * 4- We need to create a pattern node and a pattern for the else part
         * 5- We need to create a pattern and a pattern node for the body of the else.
         */
            // 3
            treeLevelPatterns.pop();
            treeLevelPatternNodes.pop();
            // 4 - empty patern and patternode
            std::shared_ptr<Pattern<T>> pattern = std::shared_ptr<Pattern<T>>(new Pattern<T>());
            treeLevelPatterns.push(pattern);
            std::shared_ptr<PatternNode<T>> patternNode = std::shared_ptr<PatternNode<T>>(new PatternNode<T>(pattern, nodeCount++));
            treeLevelPatternNodes.top()->addChild(patternNode);
            treeLevelPatternNodes.push(patternNode);
            // 5
            std::shared_ptr<Pattern<T>> pattern2 = std::shared_ptr<Pattern<T>>(new Pattern<T>());
            treeLevelPatterns.push(pattern2);
            std::shared_ptr<PatternNode<T>> patternNode2 = std::shared_ptr<PatternNode<T>>(new PatternNode<T>(pattern2,nodeCount++));
            treeLevelPatternNodes.top()->addChild(patternNode2);
            treeLevelPatternNodes.push(patternNode2);
            // This is usually the first operation in the else part. Could be a binaryOp,
            // another ifOp, a forOp, or anything
            //pattern2->addToPattern(oper);

        }
        else { // repush the pattern we popped inorder to peek at the next pattern
            treeLevelPatterns.push(poppedTopPattern);
            treeLevelPatternNodes.push(poppedTopPatternNode);
            // Go on about adding the operation to the pattern as we would do normally
            //treeLevelPatterns.top()->addToPattern(oper);
        }
    }
    else { // repush the pattern we popped inorder to peek at the next pattern
        treeLevelPatterns.push(poppedTopPattern);
        treeLevelPatternNodes.push(poppedTopPatternNode);
        // Go on about adding the operation to the pattern as we would do normally
        //treeLevelPatterns.top()->addToPattern(oper);
    }
}

template<typename T>
void HierarchicalTreeCreator<T>::checkIfLHSorRHSofAssignment(std::shared_ptr<T> oper) {
    auto poppedTopPattern = treeLevelPatterns.top();
    auto poppedTopPatternNode = treeLevelPatternNodes.top();
    treeLevelPatterns.pop();
    treeLevelPatternNodes.pop();
    if(treeLevelPatterns.top()->size()>0) {
        std::shared_ptr<operation> operAtZero = treeLevelPatterns.top()->at(0);
        binaryOperation* binOper = dyn_cast<binaryOperation>(operAtZero.get());
        /*if(binOper!=nullptr && oper == binOper->lhs()) { // LHS
            // Sometimes the LHS is just a DeclRef so its not a binary opration.
            // In this case, the patternnode will contain an empty pattern
        }*/
        if(binOper!=nullptr && oper == binOper->lhs()) {// RHS
            // 1- No need to pop the LHS pattern
            // 2- create a pattern ndoe adn patter for the RHS
            // 3- No need to pop the LHS pattern because the cleanup function will take care of that.
            std::shared_ptr<Pattern<T>> pattern = std::shared_ptr<Pattern<T>>(new Pattern<T>());
            treeLevelPatterns.push(pattern);
            std::shared_ptr<PatternNode<T>> patternNode = std::shared_ptr<PatternNode<T>>(new PatternNode<T>(pattern,nodeCount++));
            treeLevelPatternNodes.top()->addChild(patternNode);
            treeLevelPatternNodes.push(patternNode);
        }
        else {
            treeLevelPatterns.push(poppedTopPattern);
            treeLevelPatternNodes.push(poppedTopPatternNode);
        }
    }
    else {
        treeLevelPatterns.push(poppedTopPattern);
        treeLevelPatternNodes.push(poppedTopPatternNode);
    }
}

template<typename T>
void HierarchicalTreeCreator<T>::doCreate(std::shared_ptr<T> oper) {
    /* The most-important case is when we have if-else
     * In that case we have to strip the else part from the if and attach it
     * at the same level as the if.
    */

    if(dyn_cast<loopOperation>(oper.get())) {
        // Only checks and creates an else node, and another node for the else body
        // Therefore, the FOR operation will still need to be added to the body of the else
        // and finalize and setup the for accordingly.
        // A few redundant things happening here, like creating an else body, then the for
        // will pop that body and create its own patternnode and its own body patternnode
        checkAndCreateElseNode(oper);
        createForNode(oper);
    }
    else if (/*ifOperation* ifOper = */dyn_cast<ifOperation>(oper.get())) {
        checkAndCreateElseNode(oper);
        createIfNode(oper);
    }
    /*else if(binaryOperation* binOper = dyn_cast<binaryOperation>(oper.get())) {
        // We create a seperate pattern node for the assignment oeprations. This lets us create
        // as pattern for the LHS and a pattern for the RHS of the assignment.
        if(binOper->getOpType() == OperationType::assign) {
            checkAndCreateElseNode(oper);
            createAssignmentNode(oper);
        }
    }*/
    else if(!dyn_cast<varRefOperation>(oper.get()) && ! dyn_cast<numericOperation>(oper.get())) {
        binaryOperation* binOper = dyn_cast<binaryOperation>(oper.get());
        if(binOper && binOper->getOpType() == OperationType::assign) {
            // We create a seperate pattern node for the assignment oeprations. This lets us create
            // as pattern for the LHS and a pattern for the RHS of the assignment.
            checkAndCreateElseNode(oper);
            createAssignmentNode(oper);
        }
        else {
            // Only checks and creates an else node, and another node for the else body
            // Therefore, the operation will still need to be added (to the body) after the else node and body
            // have been created.
            checkIfLHSorRHSofAssignment(oper);
            checkAndCreateElseNode(oper);
            treeLevelPatterns.top()->addToPattern(oper);
        }
    }
}

template<typename T>
void HierarchicalTreeCreator<T>::create() {
    typedef typename std::vector<std::shared_ptr<T>>::iterator iterType;
    iterType from = this->_nestedOperations.begin();
    iterType til = this->_nestedOperations.end();
    /* We create on node for the function. This node will never be popped during doCreate
     * This is a workaround when we have a for loop as the body of the function. The loop
     * will pop the top of the nodeStack (thinking that its finalizing the previous pattern)
     * and will add itself to the new top of the stack. In this case, there won't be any top
     * of the stack because it was already popped. So we add an extra node to workaround thi
     * case
     * The downside is that we will have an empty pattern and node
    */
    std::shared_ptr<Pattern<T>> functionPattern = std::shared_ptr<Pattern<T>>(new Pattern<T>());
    treeLevelPatterns.push(functionPattern);
    std::shared_ptr<PatternNode<T>> functionPatternNode = std::shared_ptr<PatternNode<T>>(new PatternNode<T>(functionPattern,nodeCount++));
    treeLevelPatternNodes.push(functionPatternNode);
    //this->_patternTree = PatternTree<T>(functionPatternNode);
    this->_patternTree = std::make_shared<PatternTree<T>>(functionPatternNode);

    while(from != til) {
        std::shared_ptr<Pattern<T>> completePattern = std::shared_ptr<Pattern<T>>(new Pattern<T>());
        treeLevelPatterns.push(completePattern);
        std::shared_ptr<PatternNode<T>> rootPatternNode = std::shared_ptr<PatternNode<T>>(new PatternNode<T>(completePattern, nodeCount++));
        treeLevelPatternNodes.top()->addChild(rootPatternNode);
        treeLevelPatternNodes.push(rootPatternNode);


        std::function<bool(std::shared_ptr<operation>)> predicateNoVarDecls = [] (std::shared_ptr<operation> toCheck) {
            return !dyn_cast<varDeclOperation>(toCheck.get());
        };

        std::function<void(std::shared_ptr<T>)> doCreateFunc = std::bind(&HierarchicalTreeCreator<T>::doCreate, this, std::placeholders::_1);
        std::function<void(std::shared_ptr<T>)> doCleanupFunc = std::bind(&HierarchicalTreeCreator<T>::doCleanup, this, std::placeholders::_1);

        // We don't need the doSetup, because it will either do what doCreate will do, then in that case
        // we don't need doCreate. We can also refactor the doCreate function into doCreate and dosetup
        // but then we will need to do the checks for the else part (of the if statement) twice which
        // is a waste of resources.
        (*from)->traversePreOrder(doCreateFunc , nullptr, predicateNoVarDecls, *from, nullptr, doCleanupFunc);

        //PatternTree<T> tree(rootPatternNode);

        //treeLevelPatterns.pop();
        //treeLevelPatternNodes.pop();
        // combine the trees created from each major statement in the highest scope
        // into the final pattern tree
        //this->_patternTree.addTree(tree);

        from++;
    }

    /*// Pass over the tree once and remove all empty pattern nodes that have no children
    std::function<void(std::shared_ptr<PatternNode<operation>>&,
                       std::function<std::string(std::shared_ptr<operation>)> )> deleteEmptyNodesWithNoChildren
            = [](std::shared_ptr<PatternNode<operation>>& pNode,
            std::function<std::string(std::shared_ptr<operation>)> callable) {
        // This function will not delete nodes that are empty. It will actually check if the children on this node
        // are empty and have no children themselves. Then it will delete those empty nodes
        if(pNode->children().size() > 0) {
            //std::vector<std::shared_ptr<PatternNode<operation>>> tobeDeleted;
            typedef std::vector<std::shared_ptr<PatternNode<operation>>>::iterator iter;
            iter it = pNode->children().begin();
            iter result = it;
            for( ; it != pNode->children().end() ; it++) {
                if(!( (*it)->children().empty() && (*it)->getPattern()->size() == 0 )) {
                    *result = *it;
                    result++;
                }
            }
            pNode->children().erase(result, pNode->children().end());
        }
    };*/
    // Pass over the tree once and remove all empty pattern nodes that have no children
    std::function<void(std::shared_ptr<IPatternNode>&,
                       std::function<std::string(std::shared_ptr<operation>)> )> deleteEmptyNodesWithNoChildren
            = [](std::shared_ptr<IPatternNode>& pNode,
            std::function<std::string(std::shared_ptr<operation>)> callable) {
        // This function will not delete nodes that are empty. It will actually check if the children on this node
        // are empty and have no children themselves. Then it will delete those empty nodes
        if(pNode->children().size() > 0) {
            //std::vector<std::shared_ptr<IPatternNode>> tobeDeleted;
            typedef std::vector<std::shared_ptr<IPatternNode>>::iterator iter;
            iter it = pNode->children().begin();
            iter result = it;
            for( ; it != pNode->children().end() ; it++) {
                if(!( (*it)->children().empty() && (*it)->getPattern()->size() == 0 )) {
                    *result = *it;
                    result++;
                }
            }
            pNode->children().erase(result, pNode->children().end());
        }
    };
    this->_patternTree->template traverseTreeDFST2BL2R<T,std::string>(deleteEmptyNodesWithNoChildren, nullptr, nullptr);
}

template<typename T>
class BruteForceTreeCreator: public PatternTreeCreator<T>
{
public:
    BruteForceTreeCreator(const std::vector<std::shared_ptr<T>>& operations);
    //~BruteForceTreeCreator() {}
    void create();
private:
    void doCreate(std::shared_ptr<T> oper);
    void doCreate2(std::shared_ptr<T> oper);
private:
    std::shared_ptr<T> currentParentOperation;
    // this indicates if the operation is in the _nestedOperations vector. Meaning the top level operations.
    // As we start recursively traversing the top level operations, we will set this to false.
    // Mainly used to create different patternTrees for each top level oepration.
    bool topLevelOperation;
    std::stack<std::shared_ptr<PatternNode<T>>> treeLevelPatternNodes;
    std::stack<std::shared_ptr<Pattern<T>>> treeLevelPatterns;
};

template<typename T>
BruteForceTreeCreator<T>::BruteForceTreeCreator(const std::vector<std::shared_ptr<T>>& operations) : PatternTreeCreator<T>(operations, TreeType::brute_force) {}
/*
template<typename T>
void BruteForceTreeCreator<T>::doCreate_bkp(std::shared_ptr<T>& oper) {

    if(topLevelOperation) {
        // Create a pattern and a node and add it to tree no matter if the operation is a loop or binary operation

    }
    else if (LoopOperation* loopOper = dyn_cast<LoopOperation>(oper.get())) {
        // Generate a pattern and add it to the tree
        // This pattern will be created again every time a doCreate is called with new operation
        // Thus we should not worry about resetting the patt object after we create the pattern
        // and add it to the tree.
        Pattern<T> patt;
        std::function<void(std::shared_ptr<T>)> patternGen = [&patt] (std::shared_ptr<T>& oneOper) {
            if(!(dyn_cast<varOperation>(oneOper.get()))) {
                patt.addToPattern(oneOper);
            }
        };
        // The patternGen function passed as first argument will populate the pattern patt.
        // Then patt will be wrapped with a patternNode and added to the tree
        oper->traversePostOrder(patternGen , nullptr, oper);
        std::shared_ptr<PatternNode<T>> = std::shared_ptr<PatternNode<T>>(new PatternNode<T>(patt));
        //_patter
    }
}
*/
template<typename T>
void BruteForceTreeCreator<T>::doCreate(std::shared_ptr<T> oper) {
    // 1- add the operation to the pattern no matter what the operation type is (besides varOperation of course)
    if(!(dyn_cast<varRefOperation>(oper.get()))) {
        treeLevelPatterns.top()->addToPattern(oper);
    }
    // 2- if operation is loop, then create a new pattern node and add it as child of the top of the stack.
    // 3- if operation is loop, then push the newly created pattern node onto the stack
    // 4- if operation is loop call traverse on the operation (this should create a new pattern ...) basically recursively call doCreate
    // 5- after traverse returns, the pattern is succesfully created and we can add it into the patternNode.
    // 6- pop the patternNode from the top of the stack
    if (topLevelOperation) {
        // the first operation might be a for loop. In this case we will get into an infinite loop
        // by calling traverse in create() and then we get into doCreate() amd add the loopOperation
        // to the patten. But then also we call traverse again on loopOperation which will in turn call
        // doCreate which will again call traverse on the loop and call doCreate again and again.
        // Therefor we either have to break that cycle by setting the boolean topLevelOperation
        // or we need to traverseChildren rather than traverse node+children.
        // This also prevents a double traversal if the first operation is a for loop. In that case
        // the create() function will call traversePostOrder on the loop and then call docreate(). Docreate()
        // will then call traverseChildren on the for loop again although the traversePostOrder actually
        // does that. So we will need to use the topLevelOperation boolean to avoid that double call.
        topLevelOperation = false;
    }
    else if (/*loopOperation* lOper = */dyn_cast<loopOperation>(oper.get())) {
        // Generate a pattern and add it to the tree
        // This pattern will be created again every time a doCreate is called with new operation
        // Thus we should not worry about resetting the pattern object after we create the pattern
        // and add it to the tree.

        std::shared_ptr<Pattern<T>> pattern = std::shared_ptr<Pattern<T>>(new Pattern<T>());
        treeLevelPatterns.push(pattern);
        std::shared_ptr<PatternNode<T>> patternNode = std::shared_ptr<PatternNode<T>>(new PatternNode<T>(pattern));
        treeLevelPatternNodes.top()->addChild(patternNode);
        treeLevelPatternNodes.push(patternNode);

        pattern->addToPattern(oper);

        std::function<bool(std::shared_ptr<operation>)> predicateNoVarDecls = [] (std::shared_ptr<operation> toCheck) {
            return !dyn_cast<varDeclOperation>(toCheck.get());
        };

        std::function<void(std::shared_ptr<T>)> doCreateFunc = std::bind(&BruteForceTreeCreator<T>::doCreate, this, std::placeholders::_1);
        oper->traverseChildren(doCreateFunc , nullptr, predicateNoVarDecls, oper);

//        std::cout << "Creating pattern for for loop at address : " << lOper->getUnderlyingPtr() << "\n";
//        ::printPattern(*(pattern.get()), [](std::shared_ptr<operation> toPrint) { return std::to_string(toPrint->getOpType()); });
//        std::cout << "\n";
        //std::shared_ptr<PatternNode<T>> = std::shared_ptr<PatternNode<T>>(new PatternNode<T>(patt));
        //patternNode->getPattern() = pattern; // The shared_ptr to pattern has already been set inside the patternNode earlier.
        treeLevelPatterns.pop();
        treeLevelPatternNodes.pop();
    }
    /*else if(dyn_cast<binaryOperation>(oper.get()) || dyn_cast<memOperation>(oper.get()) || dyn_cast<unaryOperation>(oper.get())) {
        treeLevelPatterns.top()->addToPattern(oper);
    }*/
}

template<typename T>
void BruteForceTreeCreator<T>::doCreate2(std::shared_ptr<T> oper) {
    // 1- add the operation to the pattern no matter what the operation type is (besides varOperation of course)
    if(!(dyn_cast<varRefOperation>(oper.get())) && !(dyn_cast<loopOperation>(oper.get()))) {
        treeLevelPatterns.top()->addToPattern(oper);
    }
    // 2- if operation is loop, then create a new pattern node and add it as child of the top of the stack.
    // 3- if operation is loop, then push the newly created pattern node onto the stack
    // 4- if operation is loop call traverse on the operation (this should create a new pattern ...) basically recursively call doCreate
    // 5- after traverse returns, the pattern is succesfully created and we can add it into the patternNode.
    // 6- pop the patternNode from the top of the stack
    if(topLevelOperation)
        topLevelOperation = false;
    else if (/*loopOperation* lOper = */dyn_cast<loopOperation>(oper.get())) {
        // Generate a pattern and add it to the tree
        // This pattern will be created again every time a doCreate is called with new operation
        // Thus we should not worry about resetting the pattern object after we create the pattern
        // and add it to the tree.

        std::shared_ptr<Pattern<T>> pattern = std::shared_ptr<Pattern<T>>(new Pattern<T>());
        treeLevelPatterns.push(pattern);
        std::shared_ptr<PatternNode<T>> patternNode = std::shared_ptr<PatternNode<T>>(new PatternNode<T>(pattern));
        treeLevelPatternNodes.top()->addChild(patternNode);
        treeLevelPatternNodes.push(patternNode);

        pattern->addToPattern(oper);
    }
}

template<typename T>
void BruteForceTreeCreator<T>::create() {
    typedef typename std::vector<std::shared_ptr<T>>::iterator iterType;
    // I was getting error: use of undeclared identifier '_nestedOperations' because its a dependent type
    // In order to overcome this, you must somehow tell the compiler that the name '_nestedOperations' depends on
    // template parameters (that it's a dependent name). Once you do so, the compiler will not try
    // to resolve it when parsing the template; it will postpone resolution until the template is
    // instantiated. At that point, template arguments are known and thus the base class can be checked.

    // You have three ways to mark a member name as dependent:
    // 1-Refer to the name through this, as you're doing: this->_nestedOperations
    // 2-Refer to the name through base-class qualification: BaseClass<T>::_nestedOperations
    // 3-Bring the name into the scope of the derived class:  using BaseClass<T>::_nestedOperations
    iterType from = this->_nestedOperations.begin();
    iterType til = this->_nestedOperations.end();
    std::reverse_iterator<iterType> revFrom(til);
    std::reverse_iterator<iterType> revTil(from);
    while(revFrom != revTil) {
        topLevelOperation = true;
        std::shared_ptr<Pattern<T>> completePattern = std::shared_ptr<Pattern<T>>(new Pattern<T>());
        treeLevelPatterns.push(completePattern);
        std::shared_ptr<PatternNode<T>> rootPatternNode = std::shared_ptr<PatternNode<T>>(new PatternNode<T>(completePattern));
        treeLevelPatternNodes.push(rootPatternNode);
        std::function<bool(std::shared_ptr<operation>)> predicateNoVarDecls = [] (std::shared_ptr<operation> toCheck) {
            return !dyn_cast<varDeclOperation>(toCheck.get());
        };

        std::function<void(std::shared_ptr<T>)> doCreateFunc = std::bind(&BruteForceTreeCreator<T>::doCreate, this, std::placeholders::_1);
        (*revFrom)->traversePostOrder(doCreateFunc , nullptr, predicateNoVarDecls, *revFrom);

//        std::cout << "Creating pattern for operation at first scope at address : " << (*revFrom)->getUnderlyingPtr() << "\n";
//        ::printPattern(*(completePattern.get()), [](std::shared_ptr<operation> toPrint) { return std::to_string(toPrint->getOpType()); });
//        std::cout << "\n";

        PatternTree<T> tree(rootPatternNode);

//        std::cout << "Printing out tree for testing\n";
//        std::function<std::string(std::shared_ptr<operation>)> printOpType = [] (std::shared_ptr<operation> toPrint) { return std::to_string(toPrint->getOpType()); };
//        std::function<void(std::shared_ptr<PatternNode<operation>>&, std::function<std::string(std::shared_ptr<operation>)>)> printPatternWithOpType = std::bind(&printPatternNode, std::placeholders::_1, printOpType);
//        tree.traverseTreeDFSL2RB2T(printPatternWithOpType);


        treeLevelPatterns.pop();
        treeLevelPatternNodes.pop();
        // combine the trees created from each major statement in the highest scope
        // into the final pattern tree
        this->_patternTree.addTree(tree);
//        std::cout << "Printing out tree AGAIN for testing\n";
//        this->_patternTree.traverseTreeDFSL2RB2T(printPatternWithOpType);
//        std::cout << "Printed tree for testing\n";

        revFrom++;
    }
}

}
}

#endif // PATTERNTREECREATOR_H
