#ifndef INSERTPROCESSOR_H
#define INSERTPROCESSOR_H

#include <memory>
#include "utils/ProcessingUtils.h"

class InsertProcessor {
public:
    void operator()(IKnowledgeBase* kb, std::shared_ptr<ParseState> parseState, std::shared_ptr<IPatternTree> patternTree, std::shared_ptr<operation> functionOperation);
};

void InsertProcessor::operator()(IKnowledgeBase* kb, std::shared_ptr<ParseState> parseState, std::shared_ptr<IPatternTree> pTree, std::shared_ptr<operation> functionOperation) {
    // This runId will be the unique id for this run. It will be used to create files with unique names,
    // run dynamic analyzer, pass it to the instrumented executable to associate the captured variables with
    // a specific run.
    int runId = ::generateRunId();
    std::string sRunId = std::to_string(runId);

    // Serializes the pattern tree into json. The first json is the raw json to store int the DB whereas
    // the second is the jointjs json for visualiztion purposes
    serializePatternTree<operation>(pTree, sRunId);

    // Pretty prints the AST tree
    prettyPrintTrees<operation>(functionOperation, pTree, parseState);

    // Post processes the pattern
    postProcessPattern(parseState);

    // the pattern should be processed by now
    parseState->repo->addPattern(parseState->pattern);
    std::cout << "Creating a tree out of pattern (size: "
              << parseState->pattern.size() << " )" << std::endl;

    // This is manually creating a tree from the post-processed pattern
    // TODO: remove this. It's not needed anymore since we programatically
    // create the WHOLE pattern tree at the beginning of this function
    std::ostringstream oss;

    // These variables will be used in all the following:
    // runDefUse(), queryTree(), queryDefUse(), queryTreeMetric(), runEstimator()
    FunctionDecl* function = parseState->function;
    Stmt* functionBody = parseState->function->getBody();
    ASTContext* context = parseState->Context;
    const CFG::BuildOptions bo ;
    std::unique_ptr<CFG> ucfg = CFG::buildCFG(function, functionBody, context, bo);
    CFG* cfg = ucfg.get();
    // End of common variables

    // Run def-use analysis
    DefUse* du = runDefUseAnalysis(function, functionBody, context, cfg, false);

    // Run static analysis
    runStaticAnalysis(functionOperation, du);

    // Match trees by querying the knowledge base
    ITreeQuery* treeQuery;
    ITreeQueryResult* treeQueryResult;

    // Query benchmarks
    SopeAST ast = parseState->repo->getASTs().front();
    std::shared_ptr<IPatternTree> patternTree = parseState->repo->getPatternTrees().front(); // = pTree too
    treeQuery = prepareTreeQuery(ast, patternTree, runId);
    treeQueryResult = insertTree(kb, treeQuery);
    //prepareMetricQuery
    //insertMetrics

    // Run dynamic analyzer. The analyzer will take care of calling the injector
    // and then running the program to capture the runtime variables.
    auto captures = runDynamicAnalysis(runId);

    delete treeQueryResult;
    delete treeQuery; // required because createMockQuery will allocate on heap
    delete du;
}

#endif // INSERTPROCESSOR_H
