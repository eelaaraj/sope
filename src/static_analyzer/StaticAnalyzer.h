#ifndef STATICANALYZER_H
#define STATICANALYZER_H

#include <vector>
#include "../Operations.h"
#include "../utils/OperationHelpers.h"
#include "../defuse/DefUse.h"
#include "clang/AST/AST.h"
//#include "clang/AST/AST.h"
using namespace clang;
using namespace clang::defuse;

class ScopesRelationship {
public:
private:
};

class IStaticAnalyzer
{
public:
    enum StaticAnalyzerKind {
        SAK_SA,
    };
private:
    const StaticAnalyzerKind kind;
public:
    IStaticAnalyzer(StaticAnalyzerKind sak) : kind(sak) {}
    StaticAnalyzerKind getKind() const {
        return kind;
    }
    virtual ~IStaticAnalyzer() {}
    virtual void analyze() = 0;
    // After the AST is analyzed using the analyze() function, the client can getScopesRelationships
    virtual std::vector<ScopesRelationship>& getScopesRelationships() = 0;
};

class ScopeAnalyzer : public IStaticAnalyzer {
public:
    ScopeAnalyzer(std::shared_ptr<operation> root, DefUse* du) : IStaticAnalyzer(SAK_SA), ast(root), defUse(du) { }
    void analyze();
    std::vector<ScopesRelationship>& getScopesRelationships();

    static bool classof(const IStaticAnalyzer* base) {
        return base->getKind() == IStaticAnalyzer::SAK_SA;
    }
private:
    std::shared_ptr<operation> ast;
    DefUse* defUse;
    std::vector<ScopesRelationship> rels;
    std::vector<std::shared_ptr<operation>> loops;
};

template<typename T>
void checkForLoop(std::shared_ptr<T>oper) {
    if(loopOperation* loop = dyn_cast<loopOperation>(oper.get())) {
        std::cout << "\nStatic Analyzer found loop: " << loop->getMainStringLiteral() << std::endl;
    }
}

// Enum for expression values. basically right-hand-sides
/*enum ExpressionValue {
    UnsupportedType,
    NumericType,
    DeclRefExprType, // when its a reference, we will have to query the DefUse results to figure out if the variableReference stays constant or changes.
    BinaryOperatorType, // which could contain numeric, variable references, or more binary expressions
};*/

// Enum for expression types. Will be used to check what is the type of lhs and rhs of all sorts of expressions
enum SupportedExpressionType {
    UnsupportedType,
    NumericType,
    VarDeclType,
    DeclRefExprType, // when its a reference, we will have to query the DefUse results to figure out if the variableReference stays constant or changes.
    BinaryOperatorType, // which could contain numeric, variable references, or more binary expressions
};

using namespace clang;
struct LoopDescription {
    VarDecl* initDecl;
    DeclRefExpr* initRef;
    SupportedExpressionType rangeStartValueType;
    SupportedExpressionType rangeEndValueType;
    Stmt* rangeStartValue;
    Stmt* rangeEndValue;    
    Stmt* rangeIncrement;
    bool rangeInclusive; //i<k, or i>j, or i!=j are are exclusive. i<=j ... is inclusive
    std::string rangeStartStringValue;
    std::string rangeEndStringValue;
    std::string rangeIncrementString;
};

SupportedExpressionType checkExpression(std::shared_ptr<operation> initExpression, DefUse* defUse) {
    if(/*numericOperation* numOp = */dyn_cast<numericOperation>(initExpression.get())) {
        // Meaning the init expression (of the loop init) is a numeric literal and not a var reference
        std::cout << "-expr is a numeral" << std::endl;
        return SupportedExpressionType::NumericType;
    }
    else if(/*varRefOperation* refOp = */dyn_cast<varRefOperation>(initExpression.get())) {
        // Meaning the init expression (of the loop init) is a variable reference
        std::cout << "-expr is a varRef" << std::endl;
        return SupportedExpressionType::DeclRefExprType;
    }
    else if(/*varDeclOperation* refOp = */dyn_cast<varDeclOperation>(initExpression.get())) {
        // Meaning the init expression (of the loop init) is a variable reference
        std::cout << "-expr is a varDecl" << std::endl;
        return SupportedExpressionType::VarDeclType;
    }
    else {
        /* Meaing the init expression is anything else. This could be a binary expression
         * as follows for(int i=j+1;...) or for(int i=2*j OR int i=2+3 or ... These cases are a bit
         * more complicated than the numericOperation and the varRefOperation */
        std::cout << "-expr is unknown. Static Analyzer does not handle this case (neither numeric nor variableReference) of the INIT expression" << std::endl;
        return SupportedExpressionType::UnsupportedType;
    }
}

void replicateBinaryOp(BinaryOperator* binOp) {
    const Type* tp = binOp->getLHS()->getType().getTypePtr();
    //decltype(tp) var;
    if(tp->isIntegerType()) {
        spdlog::get("sopelogger")->debug("binop LHS is Integer type");
    }
    else if(tp->isFloatingType()) {
        spdlog::get("sopelogger")->debug("binop LHS is Floating type");
    }
    else {
        spdlog::get("sopelogger")->debug("binop LHS is of UnSupported type");
        // not supported
    }
}

void checkLoopInit(loopOperation* loop, DefUse* defUse, LoopDescription& desc) {
    if(varDeclOperation* init = dyn_cast<varDeclOperation>(loop->init().get())) {
        // Meaning the loop init is a var decl
        DeclStmt* dStmt = dyn_cast<DeclStmt>(loop->init()->getUnderlyingPtr());
        if(dStmt) {
            VarDecl* dec = dyn_cast<VarDecl>(dStmt->getSingleDecl());
            QualType qt = dec->getType();
            std::cout << "VARDECL type = " << qt.getAsString() << std::endl;
        }
        if(DeclStmt* declStmt = dyn_cast<DeclStmt>(init->getUnderlyingPtr())) {
            if(declStmt->isSingleDecl()) {
                desc.initDecl = dyn_cast<VarDecl>(declStmt->getSingleDecl());
                desc.initRef = nullptr;
                // So we have checked the LHS of the expression or varDecl. Now we can check the RHS.
                desc.rangeStartStringValue = init->init()->getMainStringLiteral();
                checkExpression(init->init(), defUse); // checks the value of the expression or basically RHS
            }
            else {
                std::cout << "Loop INIT is not just one varDecl. It is a DeclStmt with multiple VarDecls" << std::endl;
            }
        }
    }
    else if(binaryOperation* init = dyn_cast<binaryOperation>(loop->init().get())) {
        // Meaning the loop init is a binary expression that assigns a variable reference to a value.
        // Thus it was declared earlier but reassigned in the loop. Ex: for(i=2;...), i=2 is a binary expression.
        replicateBinaryOp(dyn_cast<BinaryOperator>(init->getUnderlyingPtr()));
        if(varRefOperation* initLHS = dyn_cast<varRefOperation>(init->lhs().get())) { // The lhs might be a binary expression such as S[h]
            if(DeclRefExpr* declRef = dyn_cast<DeclRefExpr>(initLHS->getUnderlyingPtr())) {
                desc.initDecl = nullptr;
                desc.initRef = declRef;
                // So we have checked the LHS of the expression or varDecl. Now we can check the RHS.
                desc.rangeStartStringValue = init->rhs()->getMainStringLiteral();
                checkExpression(init->rhs(), defUse);
            }
            else {
                std::cout << "Loop init LHS is not a simple DeclRefExpr. This case has not been implemented yet" << std::endl;
            }
        }
    }
    else {
        std::cout << "Static Analyzer does not handle this case (neither varDecl nor varRefExpr) of the loop INIT" << std::endl;
    }
}

//#include "clang/AST/AST.h"
void checkLoopCondition(loopOperation* loop, DefUse* defUse, LoopDescription& desc) {
    if(binaryOperation* cond = dyn_cast<binaryOperation>(loop->condition().get())) {
        //checkExpression(cond->rhs(), defUse);
        if(clang::BinaryOperator* clangBinOp = dyn_cast<clang::BinaryOperator>(cond->getUnderlyingPtr())) {
            if(clangBinOp->getOpcode() == clang::BinaryOperator::Opcode::BO_GT
                    || clangBinOp->getOpcode() == clang::BinaryOperator::Opcode::BO_LT
                    || clangBinOp->getOpcode() == clang::BinaryOperator::Opcode::BO_NE) {
                /* TODO: what if the loop is of the form : for (i=0; i==4; i++) or for(i; i==j;i++),
                 * then this is neither inclusive or exclusive. In this case the loop might iterate only when i=4
                 * which will not happen. In the second example, it can be a bit complicated and therefore these cases
                 * are better left unhandled. Thoough the execution should be stopped because we might let control
                 * in the ELSE statement below, falsely interpret it as an inclusive range which it is NOT.
                 */
                desc.rangeInclusive = false;
            }
            else {
                desc.rangeInclusive = true;
            }
        }
        if(varRefOperation* lhs = dyn_cast<varRefOperation>(cond->lhs().get())) {
            // The previous IF statement would have decided if the range is inclusive or not.
            std::string offset = "";
            if(!desc.rangeInclusive)
                offset = " - 1";
            desc.rangeEndStringValue = cond->rhs()->getMainStringLiteral() + offset;
            checkExpression(cond->rhs(), defUse);
        }
        else {
            std::cout << "Loop COND LHS is not a simple DeclRefExpr. This case has not been implemented yet." <<std::endl;
        }
    }
    else {
        std::cout << "Static Analyzer does not handle this case (not a binaryOperation) of the loop CONDITION" << std::endl;
    }
}

void checkLoopIncrement(loopOperation* loop, DefUse* defUse, LoopDescription& desc) {
    if(binaryOperation* inc = dyn_cast<binaryOperation>(loop->increment().get())) {
        checkExpression(inc->rhs(), defUse);
    }
    else if(unaryOperation* inc = dyn_cast<unaryOperation>(loop->increment().get())) {
        checkExpression(inc->operand(), defUse);
    }
    else {
        std::cout << "Static Analyzer does not handle this case (neither binaryExpr nor unaryExpr) of the loop INCREMENT" << std::endl;
    }
    /*SupportedExpressionType tp = checkExpression(loop->increment(), defUse);
    if(tp == VarDeclType) {

    }
    else if( tp == BinaryOperatorType) {

    }*/
}

// Sets ScopeAnalyzer::rels after doing the appropriate analysis
void ScopeAnalyzer::analyze() {
    /* traverse the AST and analyze scope relationships. we need to get the following:
     * 1- identify scope variables: variables that are declared or defined in that scope
     * 2- get these variables ranges: the values that they might assume. start to end and increments
     *    The range can be constant such as 0->d where d is a constant OR it can be k->d where k
     *    is a variable declared/defined in a different scope.
     * NOTES: To get where a variable is declared/defined, we need to use DefUse results. That will
     *        tell us which CFGblock the variable was defined in. That variable will usually be defined
     *        in multiple blocks such as the init block of the loop and the increment block of the loop.
     *        The init block will tell us the start value or the start expression, but the increment block
     *        will tell us the increment. The condition block will tell us the end value or expression.
     *        The init and increment are DEFs while the condition is usually a USE. These are the BASIC
     *        ASSUMPTIONS for now. Later we might extend our analysis to more complex loop structures
     *        or even variables that are defined and incremented outside the loop statement (basically
     *        in the body of the loop).
     */
    std::function<void(std::shared_ptr<operation>)> checkForLoopLambda = [this] (std::shared_ptr<operation>oper) {
        if(loopOperation* loop = dyn_cast<loopOperation>(oper.get())) {
            std::cout << "\nStatic Analyzer found loop: " << loop->getMainStringLiteral() << std::endl;
            loops.push_back(oper);
        }
    };

    ast->traversePostOrder(checkForLoopLambda, nullptr, [](std::shared_ptr<operation>) { return true; }, ast);
    std::string methodIterationCount = "";
    for(auto op : loops) {
        loopOperation* loop = dyn_cast<loopOperation>(op.get());
        std::cout << "Init = " << loop->init()->getMainStringLiteral() << std::endl;
        std::cout << "Cond = " << loop->condition()->getMainStringLiteral() << std::endl;
        std::cout << "Incr = " << loop->increment()->getMainStringLiteral() << std::endl;
        LoopDescription desc;
        // Init: will be used to retrieve the loop variable and the start of its range
        checkLoopInit(loop, defUse, desc);
        // Condition: Will mostly be used to figure out the end of the range
        checkLoopCondition(loop, defUse, desc);
        // Increment
        checkLoopIncrement(loop, defUse, desc);

        std::cout << "Loop formula = " << desc.rangeEndStringValue << " - " << desc.rangeStartStringValue<< " + 1" << std::endl;
        std::string range = desc.rangeEndStringValue + " - " + desc.rangeStartStringValue + " + 1";
        //std::cout << "(" << (desc.initDecl ? desc.initDecl->getNameAsString() : desc.initRef->getNameInfo().getAsString())
        //          << ":1->end" << ")" << "Ʃ (" << range << ")" << std::endl;
        std::string loopIterationCount =  "[" + (desc.initDecl ? desc.initDecl->getNameAsString() : desc.initRef->getNameInfo().getAsString())
                                              + ":"+desc.rangeStartStringValue +"->" + desc.rangeEndStringValue + " + 1" + "]" + " Ʃ (";

        std::cout << loopIterationCount << std::endl;//<< (desc.initDecl ? desc.initDecl->getNameAsString() : desc.initRef->getNameInfo().getAsString()) << ")" << std::endl;
        methodIterationCount += loopIterationCount;
    }
    std::cout << methodIterationCount << std::endl;
}

std::vector<ScopesRelationship>& ScopeAnalyzer::getScopesRelationships() {
    return rels;
}

#endif // STATICANALYZER_H
