#ifndef ISTORAGE
#define ISTORAGE

#include "../serialize/PatternTreeSerializer.h"
#include "../serialize/HashTreeSerializer.h"
#include "../IPatternTree.h"
#include "../utils/FileUtils.h"
#include "DBTypes.h"
#include <memory>

using namespace pattern::matching;
/*
 * A COI is a combination of patternTree + hashTree that has not been indexed in the DB
 */

//template<typename T>
class COI : public ICOI {
public:
    COI(int runId, const std::shared_ptr<IPatternTree>& patternTree, const ITree* hashTree);
    const std::shared_ptr<IPatternTree>& patternTree() { return pt; }
    //COI(int runId, const PatternTree<T>* patternTree, const ITree* hashTree);
    //const PatternTree<T>*& patternTree() { return pt; }
    const ITree*& hashTree() { return ht; }
    int runId() { return rId; }
private:
    int rId;
    //const IPatternTree* pt;
    const std::shared_ptr<IPatternTree> pt;
    //const PatternTree<T>* pt;
    const ITree* ht;
};

COI::COI(int runId, const std::shared_ptr<IPatternTree>& patternTree, const ITree* hashTree): rId(runId), pt(patternTree), ht(hashTree) {}

//template<typename T>
//COI<T>::COI(int runId, const PatternTree<T>* patternTree, const ITree* hashTree): rId(runId), pt(patternTree), ht(hashTree) {}

/*
 * A benchmark is a combination of patterntree + hashtree that has been indexed in the db.
 * In a sense this is identical to a COI but a COI is not indexed in the DB.
 */

//template<typename T>
class Benchmark : public IBenchmark {
public:
    ~Benchmark();
    Benchmark(long id, const std::shared_ptr<IPatternTree>& patternTree, const ITree* hashTree);
    Benchmark(ICOI* coi); // promotion
    const std::shared_ptr<IPatternTree>& patternTree() { return pt; }
    //Benchmark(long id, const PatternTree<T>* patternTree, const ITree* hashTree);
    //const PatternTree<T>*& patternTree() { return pt; }
    const ITree*& hashTree() { return ht; }
    long Id() {return dbId; }
private:
    long dbId;
    const std::shared_ptr<IPatternTree> pt;
    //const PatternTree<T>* pt;
    const ITree* ht; // benchmark should manage the hashtree that was deserialized
};

Benchmark::Benchmark(long id, const std::shared_ptr<IPatternTree>& patternTree, const ITree *hashTree): dbId(id), pt(patternTree), ht(hashTree) {}

//template<typename T>
//Benchmark<T>::Benchmark(long id, const PatternTree<T>* patternTree, const ITree *hashTree): dbId(id), pt(patternTree), ht(hashTree) {}

//template<typename T>
Benchmark::Benchmark(ICOI *coi)
    : Benchmark(-1, coi->patternTree(), coi->hashTree()) {

}

Benchmark::~Benchmark() {
    delete ht;
}


class StorageMetrics: public IMetrics {
public:
    StorageMetrics(long runId, DataPointVector* datapoints = nullptr, DataPointMetricVector* metrics = nullptr): rId(runId), dps(datapoints), mps(metrics) {}
    long runId() { return rId; }
    DataPointVector* datapoints() { return dps; }
    DataPointMetricVector*& metrics() { return mps; }
private:
    long rId; // only needed when we store metrics
    DataPointVector* dps;// queried datapoints
    DataPointMetricVector* mps;// returned metrics or they could be captured metrics that we want to store in the DB
};


/*
 * File storage is a DB consisting of disk files. These files are in json format. The index file
 * is loaded when the DB is initialized and unloaded (changes persisted) when the DB is destroyed.
 * All patterntree and hashtree files are in json format too as well as the metrics. These get loaded
 * on demand whenever needed.
 */
class FileStorage;
class FileStorageIterator: public IStorageIterator {
    friend class FileStorage;
public:
    FileStorageIterator(FileStorage* storage);
    virtual ~FileStorageIterator() { /*if(bench) delete bench;*/ }
    //virtual FileStorageIterator begin();
    //virtual FileStorageIterator end();
    //virtual FileStorageIterator next();
    virtual IStorageIterator* operator++(); // pre-increment
    virtual bool operator!=(const IStorageIterator& anotherIt);
    virtual std::unique_ptr<IBenchmark> getBenchmark();
public:
    static bool classof(const IStorageIterator* baseIterator) {
        return baseIterator->getKind() == IStorageIterator::SIK_FILE;
    }
private:
    void set_begin();
    void set_end();
private:
    FileStorage* pStorage;
    json::iterator jIt;
    //IBenchmark* bench;
};

class FileStorage: public IStorage {
    friend class FileStorageIterator;
public:
    FileStorage(const std::string& indexFile); // loads a json file that contains the index
    ~FileStorage();
    IBenchmark* addBenchmark(ICOI* coi);
    virtual void addMetrics(IBenchmark* bench, IMetrics* metrics); // adds metrics to a specific benchmark
    virtual IBenchmark* findBenchmark(ICOI* coi); // finds the benchmark that's similar to the coi
    virtual void findMetrics(IBenchmark* bench, IMetrics* metrics); //bench can be either retrieved from findBenchmark OR can be a COI that has been promoted to IBenchmark. In the latter case we need to check the dbId if its not -1
    IStorageIterator* begin();
    IStorageIterator* end();
    IBenchmark* findBenchmark(long dbId);
private:
    virtual void findMetrics_bkp(IBenchmark* bench, IMetrics* metrics);
    bool unloadIndex(const std::string& filePath);
    json loadIndex(const std::string& indexFile);
    bool addRecordToIndex(ICOI* coi, const std::string& pTreeFilepath, const std::string& hTreeFilepath);
    int findMetricForQuery(IDataPoint* dp, json& starts, json& ends, json& trips);
private:
    json::iterator jIt;
    std::string indexFilepath;
    json index;
};

FileStorage::FileStorage(const std::string& indexFile) : indexFilepath(indexFile){
    index = loadIndex(indexFilepath);
}

FileStorage::~FileStorage() {
    bool unloaded = unloadIndex(indexFilepath);
    if(!unloaded) {
        std::cerr << "Error saving DB Index. Changes might be lost" << std::endl;
    }
}

IStorageIterator* FileStorage::begin() {
    FileStorageIterator* result = new FileStorageIterator(this);
    result->set_begin();
    return result;
}

IStorageIterator* FileStorage::end() {
    FileStorageIterator* result = new FileStorageIterator(this);
    result->set_end();
    return result;
}

FileStorageIterator::FileStorageIterator(FileStorage* storage): IStorageIterator(SIK_FILE), pStorage(storage) /*, jIt(storage->index.begin())*/ {}

bool FileStorageIterator::operator!=(const IStorageIterator& anotherIt) {
    //std::cout << "Operator != called " << std::endl;
    FileStorageIterator* fsi = dyn_cast<FileStorageIterator>(const_cast<IStorageIterator*>(&anotherIt));
    if(this!=fsi) {
        if(jIt!=fsi->jIt) {
            //std::cout << "It1 = "<< (*jIt) << std::endl;
            //std::cout << "It2 = " << *(fsi->jIt) << std::endl;
            return true;
        }
        else {
         return false;
        }
    }
    else {
        return false;
    }
}

void FileStorageIterator::set_begin() {
    jIt = pStorage->index["benchmarks"].begin();
}

void FileStorageIterator::set_end() {
    jIt = pStorage->index["benchmarks"].end();
}
// pre-increment
IStorageIterator* FileStorageIterator::operator++() {
    ++jIt;
    return this;
}

IBenchmark* generateBenchmarkFromRecord(const json& record) {
    std::cout << record << std::endl;
    std::string patternFilename = record["patterntree"];
    std::string hashFilename = record["hashtree"];
    // deserialize pattern tree
    PatternTreeSerializer<operation> pTreeSerializer(patternFilename);
    std::shared_ptr<IPatternTree> pTree = pTreeSerializer.deserialize();
    // deserialize hash tree
    HashTreeSerializer<operation,HashFingerprint> hTreeSerializer(hashFilename, pTreeSerializer.getNodesMap());
    const ITree* hTree = hTreeSerializer.deserialize();
    long dbId = record["id"];
    IBenchmark* benchmark = new Benchmark(dbId, pTree, hTree);
    //std::unique_ptr<IBenchmark> benchmark = std::unique_ptr<Benchmark>(new Benchmark(dbId, pTree, hTree));
    return benchmark;
}

// Lazily generates the benchmarks whenever called.
std::unique_ptr<IBenchmark> FileStorageIterator::getBenchmark() {
    json currentRecord = *jIt;
    return std::unique_ptr<IBenchmark>{generateBenchmarkFromRecord(currentRecord)};
}


// Persists the in-memory json object back to file effectively overwriting the old file.
bool FileStorage::unloadIndex(const std::string& filePath) {
    return overwriteJsonFile(filePath, index);
    /*FileSystem::File f(filePath);
    f.open(FileSystem::File::out, FileSystem::File::text);
    if(f.isGood()) {
        f.putLine(index.dump(), false);
        return true;
    }
    else {
        return false;
    }*/
}

// Loads the DB Index into the index json object
json FileStorage::loadIndex(const std::string& jsonUrl) {
    return deserializeJsonFile(jsonUrl);
}

// Adds a record of the new benchmark in the DB index.
bool FileStorage::addRecordToIndex(ICOI* coi, const std::string& pTreeFilepath, const std::string& hTreeFilepath) {
    json newRecord;
    newRecord["id"] = coi->runId(); // we might need to change the benchmarkId to be different than the coiID
    newRecord["path"] = "";
    newRecord["function"] = "";
    newRecord["metrics"] = ""; // This is empty initially but then will hold the path to the metric index for this benchmarks
    newRecord["patterntree"] = pTreeFilepath;
    newRecord["hashtree"] = hTreeFilepath;
    if(index["benchmarks"].is_null()) { // only happens once when the index is empty
        index["benchmarks"] = json::array();
    }
    index["benchmarks"].push_back(newRecord);
    return true;
}

IBenchmark* FileStorage::addBenchmark(ICOI* coi) {
    std::string runId = std::to_string(coi->runId());
    std::string pTreefName = "./db/ptree_"+runId+".json";
    std::string hTreefName = "./db/htree_"+runId+".json";
    // 1- check if a similar benchmark exists by calling findBenchmark()
    // 2- serialize the pattern and hash trees in the ICOI
    PatternTreeSerializer<operation> pTreeSerializer(pTreefName, coi->patternTree());
    pTreeSerializer.serialize();
    HashTreeSerializer<operation,HashFingerprint> hTreeSerializer(hTreefName, coi->hashTree());
    hTreeSerializer.serialize();
    // 3- store the serialized files on disk and add record for them in the index
    bool pCreated = createJsonFile(pTreefName, pTreeSerializer.serializedDoc());
    bool hCreated = createJsonFile(hTreefName, hTreeSerializer.serializedDoc());
    if(pCreated && hCreated) {
        addRecordToIndex(coi, pTreefName, hTreefName);
    }
    // Didn't use the promotion cstr because it assigns the hashtree to that of the coi. Benchmark has
    // a destructor that deletes the hashtree. BUT that hashtree is managed by a unique_ptr in
    // HashTreeQuery which tries to delete the already deleted hashTree sometime later towards the
    // exit of the program and crashes it. For now this is a fix, but if we need to access the hashTree
    // of the benchmark, we can not
    IBenchmark* bench = new Benchmark(coi->runId(), coi->patternTree(), new HashTree<operation,HashFingerprint>(new HashNode<operation,HashFingerprint>(-1,nullptr)));
    return bench;
}

void FileStorage::addMetrics(IBenchmark* bench, IMetrics* metrics) {
    json& benchmarks = index["benchmarks"];
    std::string dbId = std::to_string(bench->Id());
    int benchmarkerRunId = metrics->runId();
    std::string sBenchmarkerRunId = std::to_string(metrics->runId());
    json* record = nullptr; // using pointer because we can not have an empty reference. We need to refer to the record in the benchmarks because we need to chamge it. If we get it by value, the changes won't persist
    for (json::iterator it = benchmarks.begin(); it != benchmarks.end(); ++it) {
        long id = (*it)["id"];
        if(id == bench->Id()) { // get the record associated with this dbId
            record = &(*it);
            break;
        }
    }
    if(record) {
        json& metricField = (*record)["metrics"];
        std::string currentMetricFilePath = (*record)["metrics"];
        std::string metricFilepath = "./db/metrics_"+dbId+".json";
        bool justCreated = false;
        json metricShards;
        json metricAvgs;
        if(currentMetricFilePath=="") { // if there is no metric record in the index for this dbId
            metricField = metricFilepath;
            justCreated = true;
        }
        if(justCreated) { // also create the corresponding shards file
            metricShards["shards"] = json::array();
            metricShards["avg"] = "./db/metrics_avg_"+dbId+".json";
        }
        else { // if we have a shards file, just load it
            // load master metric file to add the new metrics later as a new shard
            metricShards = deserializeJsonFile(metricField);
            metricAvgs = deserializeJsonFile(metricShards["avg"]);
        }
        // Creates the new metrics shard file
        // The file containing the metrics has already been created by the benchmarking tool. So we only need to add
        // a record that points to those files.
        json metricRecord;
        metricRecord["runId"] = benchmarkerRunId;
        metricRecord["path"] =  "./db/metrics_"+dbId+"_"+sBenchmarkerRunId+".json";
        metricShards["shards"].push_back(metricRecord);

        if(justCreated) {
            createJsonFile(metricFilepath, metricShards);
        }
        else {
            overwriteJsonFile(metricFilepath, metricShards);
        }
        // Now update the running average of the whole dataset
        //updateAveragesFile();

//        MetricSerializer mSerializer("path/to/new/file","metrics collections");
//        mSerializer.serialize();
//        bool mCreated = createJsonFile(metricsfName, mSerializer.serializedDoc());
//        if(mCreated) { // adds the new file as a shard in the master metrics file
//            metricShards["shards"].push_back(metricsfName);
//            updateAveragesFile();
//        }
    }
}

IBenchmark* FileStorage::findBenchmark(long dbId) {
    // TODO: add matching using one of te followng: 1) hash matches OR 2) sentinel matches
    json& benchmarks = index["benchmarks"];
    json record;
    for (json::iterator it = benchmarks.begin(); it != benchmarks.end(); ++it) {
        record = (*it);
        long recordId = record["id"];
        if(recordId == dbId) {
            return generateBenchmarkFromRecord(record);
        }
    }
    return nullptr;
}

IBenchmark* FileStorage::findBenchmark(ICOI* coi) {
    // TODO: add matching using one of te followng: 1) hash matches OR 2) sentinel matches
    json& benchmarks = index["benchmarks"];
    json record;
    for (json::iterator it = benchmarks.begin(); it != benchmarks.end(); ++it) {
        record = (*it);
        /*json htree = loadHashTree(record);
        json ptree = loadPatternTree(record);
        HashTreeSerializer<operation,HashFingerprint> htreeSerializer();
        htreeSerializer().deserialize(); // deserializes json into
        PatternTreeSerializer<operation,HashFingerprint> ptreeSerializer();
        ptreeSerializer().deserialize();*/
    }
    return nullptr;
}

void FileStorage::findMetrics_bkp(IBenchmark* bench, IMetrics *metrics) {
    std::vector<IDataPoint*>& dps = metrics->datapoints()->dataPoints();
    json& benchmarks = index["benchmarks"];
    json* record = nullptr; // using pointer because we can not have an empty reference. We need to refer to the record in the benchmarks because we need to chamge it. If we get it by value, the changes won't persist
    for (json::iterator it = benchmarks.begin(); it != benchmarks.end(); ++it) {
        long id = (*it)["id"];
        if(id == bench->Id()) { // get the record associated with this dbId
            record = &(*it);
            break;
        }
    }
    if(record) {
        std::string metricFilepath = (*record)["metrics"];
        json metricField = (*record)["metrics"];
        if(!metricField.is_null() && !metricFilepath.empty()) {// The null check is because the record might not have a metrics key in the first place if the benchmark was just added to the db index
            //json metricAvgsPath = deserializeJsonFile(metricField)["avg"];
            std::string metricAvgsPath = deserializeJsonFile(metricField)["avg"];
            json metricsAvgs = deserializeJsonFile(metricAvgsPath);
            // TODO: query certain datapoints. Right now everything is loaded and returned to the client
            // IMetrics contains: datapoints to get metrics for: (var=k,value=600,level=3). This asks for
            // metrics of the scope=3,when var k=600.
            json& points = metricsAvgs["points"];
            for(json::iterator pointIt = points.begin(); pointIt != points.end(); pointIt++) { // iterates over each point. A point contains variables and metrics
                json& pointRecord = (*pointIt);
                json& vars = pointRecord["vars"];
                bool queryVarFound = false;
                for(json::iterator varIt = vars.begin(); varIt != vars.end(); varIt++) { // iterates over variables in each point
                    json& varRecord = (*varIt);
                    std::string name = varRecord["name"];
                    int value = varRecord["value"];
                    std::cout << "-Iterating over DB record: " << name << "=" << value << std::endl;
                    std::function<bool(IDataPoint*)> containsVar = [name, value] (IDataPoint* idp) {
                        DataPoint<int>* dp = dyn_cast<DataPoint<int>>(idp);
                        std::function<bool(const std::pair<const std::string, int>&)> queryContainsVar = [name, value](const std::pair<const std::string, int>& pr) {
                            std::cout << "---Query DP: " << pr.first <<" = " << pr.second << std::endl;
                            std::cout << std::boolalpha << (name==pr.first && value == pr.second) << std::endl;
                            return (name==pr.first && value == pr.second);
                        };
                        std::map<std::string, int> dpDatapoints = dp->datapoints();
                        // The following commented line is EVIL. dp->datapoints was returning the map by value. so each call
                        // will return a new map. dp->datapoints.begin() and dp->datapoints().end() were returning two iterators
                        // One is for the beginning of the first map and the other to the end of the second map. They will never meet
                        // and therefore the predicate will run to infinity or break within the STL code.
                        // auto it = std::find_if(dp->datapoints().begin(), dp->datapoints().end(), queryContainsVar);
                        auto it = std::find_if(dpDatapoints.begin(), dpDatapoints.end(), queryContainsVar);
                        std::cout << "--A datapoint in DB matches = " << std::boolalpha << (it!=dpDatapoints.end()) << std::endl;
                        return (it!=dpDatapoints.end());
                    };
                    auto result  = std::find_if(dps.begin(), dps.end(), containsVar);
                    if(result != dps.end()) {
                        queryVarFound = true;
                        std::cout << "-One of the query points was found in DB" << std::endl;
                    }
                }
                if(queryVarFound) { // only get the metrics for this point if the point has one or more of the queried variables
                    json& metricsLevels = pointRecord["metrics"];
                    for(json::iterator levelIt = metricsLevels.begin(); levelIt != metricsLevels.end(); levelIt++) { // iterates over metrics in each point
                        std::cout << "Iterating over another metric" << std::endl;
                        json& levelRecord = (*levelIt);
                        int level = levelRecord["level"];
                        float avg = levelRecord["avg"];
                    }
                }
                //DataPointMetric* dp = new DataPointMetric();
                //dp->setMetric();
                std::cout << (*pointIt) << std::endl;
            }
        }
    }
}

int FileStorage::findMetricForQuery(IDataPoint* idp, json& starts, json& ends, json& trips) {
    DataPoint<int>* dp = dyn_cast<DataPoint<int>>(idp);
    auto dpdps = dp->datapoints();
    // If the datapoint contains everythign we need
    if(dpdps.find("_sope_loop_start_")!=dpdps.end() && dpdps.find("_sope_loop_end_")!=dpdps.end()&& dpdps.find("_sope_loop_trip_count_")!=dpdps.end()) {
        auto start = dpdps["_sope_loop_start_"];
        auto end = dpdps["_sope_loop_end_"];
        auto trip = dpdps["_sope_loop_trip_count_"];
        //std::vector<int> foundAtIndex;
        int index = 0;
        // get all the records tat have the same trip count, start and end as our query
        // TODO: start,end and trip are all stored as strings. These should be stored based on the type.
        // In the capture library, _var_capture_int(..) should store the value in an integer map not a string map as we
        // are doing right now. _var_capture_float_ should store in a float map.
        auto found = std::find_if(trips.begin(), trips.end(), [&index, &start, &end, &trip, &starts, &ends] (std::string current) {
            //std::cout << "db start("<<starts.at(index)<<")" << std::endl;
            //std::cout << "db end ("<<ends.at(index)<<")" << std::endl;
            //std::cout << "db trip("<<current<<")" << std::endl;
            std::string sdbStart = starts.at(index);
            std::string sdbEnd = ends.at(index);
            int dbStart = std::stoi(sdbStart);
            int dbEnd = std::stoi(sdbEnd);
            if(std::stoi(current) == trip && dbStart == start && dbEnd == end) {
                //foundAtIndex.push_back(index);
                return true;
            }
            index++;
            return false;
        });
        if(found!=trips.end())
            return index;
        else {
            return -1;
        }
    }
    else {
        spdlog::get("sopelogger")->error("Query datapoint is malformed. Does not include either start, end, or trip count.");
        return -2;
    }
}

void FileStorage::findMetrics(IBenchmark* bench, IMetrics *metrics) {
    std::vector<IDataPoint*>& dps = metrics->datapoints()->dataPoints();
    json& benchmarks = index["benchmarks"];
    json* record = nullptr; // using pointer because we can not have an empty reference. We need to refer to the record in the benchmarks because we need to chamge it. If we get it by value, the changes won't persist
    for (json::iterator it = benchmarks.begin(); it != benchmarks.end(); ++it) {
        long id = (*it)["id"];
        if(id == bench->Id()) { // get the record associated with this dbId
            record = &(*it);
            break;
        }
    }

    if(record) {
        // First arrange the query datapoints by scope. As we iterate over the DB scopes, we look
        // at the query datapoints for that scope and try to find if they exist in the DB
        std::map<int, std::vector<IDataPoint*>> dpsByScope;
        std::function<void(IDataPoint*)> arrangeDpsByScope = [&dpsByScope] (IDataPoint* dp) {
            for(int level : dp->levels()) {
                dpsByScope[level].push_back(dp);
            }
        };
        // Groups datapoints by scope number. This makes it easier to query the DB.
        std::for_each(dps.begin(), dps.end(), arrangeDpsByScope);

        DataPointMetricVector* dpmv = new DataPointMetricVector();
        std::string metricFilepath = (*record)["metrics"];
        json metricField = (*record)["metrics"];
        if(!metricField.is_null() && !metricFilepath.empty()) {// The null check is because the record might not have a metrics key in the first place if the benchmark was just added to the db index
            json shards = deserializeJsonFile(metricField)["shards"];
            for(json::iterator shardIt = shards.begin(); shardIt!=shards.end(); shardIt++) {
                std::string shardPath = (*shardIt)["path"];
                json shardData = deserializeJsonFile(shardPath);
                json& shardScopes = shardData["scopes"];
                for(json::iterator scopeIt = shardScopes.begin(); scopeIt != shardScopes.end(); scopeIt++) {
                    int scopeNum = (*scopeIt)["scope"];
                    json& starts = (*scopeIt)["vars"]["_sope_loop_start_"];
                    json& ends = (*scopeIt)["vars"]["_sope_loop_end_"];
                    json& trips = (*scopeIt)["vars"]["_sope_loop_trip_count_"];
                    json& metrics = (*scopeIt)["metrics"];
                    std::function<void(IDataPoint*)> findMetricForQueryLambda = [this, &dpmv, &starts, &ends, &trips, &metrics](IDataPoint* dp) {
                        int index = findMetricForQuery(dp, starts, ends, trips);
                        IDataPointMetric* dpm = nullptr;
                        if(index > -1) {
                            //std::cout << "Found metric=" << metrics.at(index).dump() << " at index = " << std::to_string(index) << std::endl;
                            dpm = new DataPointMetric<float>(metrics.at(index),dp);
                        }
                        else {
                            dpm = new DataPointMetric<float>(-1,dp);
                            //std::cout << "Didn't find metric" << std::to_string(index) << std::endl;
                        }
                        dpmv->add(dpm);
                    };
                    if(dpsByScope.find(scopeNum) != dpsByScope.end()) { // if there is a query datapoint for this scope continue, else move on to the next scope
                        std::vector<IDataPoint*>& scopeQueryDps = dpsByScope[scopeNum];
                        std::for_each(scopeQueryDps.begin(), scopeQueryDps.end(), findMetricForQueryLambda);
                    }
                }
            }
        }
        metrics->metrics() = dpmv;
    }
}

#endif // ISTORAGE

