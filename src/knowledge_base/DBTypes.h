#ifndef DBTYPES_H
#define DBTYPES_H

//#include "IKnowledgeBase.h"

class DataPointVector;
class DataPointMetricVector;
/*
 * A COI is a combination of patternTree + hashTree that has not been indexed in the DB
 */
class ICOI {
public:
    virtual ~ICOI() {}
    virtual const std::shared_ptr<IPatternTree>& patternTree() = 0;
    virtual const ITree*& hashTree() = 0;
    virtual int runId() = 0;
};

/*
 * A benchmark is a combination of patterntree + hashtree that has been indexed in the db.
 * In a sense this is identical to a COI but a COI is not indexed in the DB.
 */
class IBenchmark {
public:
    virtual ~IBenchmark() {}
    virtual const std::shared_ptr<IPatternTree>& patternTree() = 0;
    virtual const ITree*& hashTree() = 0;
    virtual long Id() = 0;
};

class IStorageIterator {
public:
    enum StorageIteratorKind {
        SIK_FILE,
    };
    const StorageIteratorKind kind;
public:
    IStorageIterator(StorageIteratorKind dpk) : kind(dpk) {}
    StorageIteratorKind getKind() const {
        return kind;
    }
public:
    virtual ~IStorageIterator() {}
    virtual IStorageIterator* operator++() = 0;
    virtual bool operator !=(const IStorageIterator& anotherIt) = 0;
    virtual std::unique_ptr<IBenchmark> getBenchmark() = 0;
};

class IMetrics {
    // - contains RUN_ID somewhere to indicate which RUN the metrics are for.
    // - should contain a query that indicates which scope in the benchmark we need to get datapoints for. Could be in the form: string funcname, int scope
    // - should contain the different datapoints needed. d=101, d=102, d=103,....
public:
    virtual ~IMetrics() {}
    virtual long runId() = 0;
    virtual DataPointVector* datapoints() = 0;
    virtual DataPointMetricVector*& metrics() = 0;
};

class IStorage {
public:
    virtual ~IStorage() { }
    virtual IBenchmark* findBenchmark(long dbID) = 0; // returns the benchmarks with id = dbId
    virtual IBenchmark* addBenchmark(ICOI* coi) = 0; // coi gets added as a benchmark. The function returns the new benchmark
    virtual void addMetrics(IBenchmark* bench, IMetrics* metrics) = 0; // adds metrics to a specific benchmark
    virtual IBenchmark* findBenchmark(ICOI* coi) = 0; // finds the benchmark that's similar to the coi
    virtual void findMetrics(IBenchmark* bench, IMetrics* metrics) = 0; // finds metrics of a specific benchmark
    virtual IStorageIterator* begin() = 0;
    virtual IStorageIterator* end() = 0;
};

#endif // DBTYPES_H

