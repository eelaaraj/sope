#ifndef IDATAPOINT_H
#define IDATAPOINT_H

#include <limits>
#include <sstream>

/* Metric query should retrieve metrics for a certtain tree. Thus the
 * query object should contain a reference to the tree. In addition the query
 * should have a field that tells the knowledge base if the complete set of metrics
 * is to be retrieved, OR a subset of that, or a few distinct data points.
*/
class IDataPoint {
public:
    enum DataPointKind {
        DPK_DP
    };
    const DataPointKind kind;
public:
    IDataPoint(DataPointKind dpk) : kind(dpk) {}
    virtual ~IDataPoint() {}
    DataPointKind getKind() const {
        return kind;
    }
    virtual std::string toString() = 0;
    virtual float point() = 0;
    //virtual std::vector<std::string> vars() = 0;
    //virtual std::vector<float> points() = 0;
    virtual std::vector<int> levels() = 0;
};

class IDataPointCollection {
public:
    enum DataPointCollectionKind {
        DPCK_DPV, // dp vector
        DPCK_DPR  // dp range
    };
    const DataPointCollectionKind kind;
public:
    IDataPointCollection(DataPointCollectionKind dpk) : kind(dpk) {}
    DataPointCollectionKind getKind() const {
        return kind;
    }
public:
    virtual ~IDataPointCollection() {}
    virtual IDataPoint* next() = 0;
    virtual size_t size() = 0;
    virtual std::string toString() = 0;
};


template <typename T>
class DataPoint: public IDataPoint {
public:
    //DataPoint(const std::vector<std::string>& vnames, const std::vector<T>& values, const std::vector<int>& levels) : IDataPoint(DPK_DP),varNames(vnames), varValues(values), lvls(levels) {}
    DataPoint(const std::map<std::string,T>& datapoints, const std::vector<int>& levels) : IDataPoint(DPK_DP),dps(datapoints), lvls(levels) {}
    DataPoint(T data) : IDataPoint(DPK_DP), dp(data) {}
    T& datapoint() { return dp;}
    static bool classof(const IDataPoint* basePoint) {
        return basePoint->getKind() == IDataPoint::DPK_DP;
    }
    std::string toString() {
        std::ostringstream oss;
        for(auto pr: dps) {
            oss << pr.first << " = " << std::to_string(pr.second) << "," ;
        }
        return oss.str();
    }
    float point() { return static_cast<float>(dp); }
    std::map<std::string, T> datapoints() { return dps; }
    //std::vector<T> values() { return varValues; }
    //std::vector<std::string> vars() { return varNames; }
    std::vector<int> levels() { return lvls; }
private:
    T dp;
    std::map<std::string, T> dps;
    //std::vector<T> varValues;
    //std::vector<std::string> varNames;
    std::vector<int> lvls;
};

class DataPointVector : public IDataPointCollection {
public:
    DataPointVector() : IDataPointCollection(DPCK_DPV) {}
    IDataPoint* next() {
        if(it!=dps.end()) {
            auto tempIt = it;
            //return *it++;
            it++;
            return *tempIt;
        }
        else{
            it = dps.begin();
            return nullptr;
        }
    }
    size_t size() { return dps.size(); }

    void add(IDataPoint* dp) { dps.push_back(dp); }

    std::string toString() {
        std::string s = std::to_string(dps.size());
        return s;
    }

    std::vector<IDataPoint*>& dataPoints() { return dps; }

    static bool classof(const IDataPointCollection* baseCollection) {
        return baseCollection->getKind() == IDataPointCollection::DPCK_DPV;
    }
private:
    std::vector<IDataPoint*> dps;
    std::vector<IDataPoint*>::iterator it = dps.begin();
};

template<typename T>
class DataPointRange : public IDataPointCollection {
public:
    DataPointRange() : IDataPointCollection(DPCK_DPR) {}
    IDataPoint* next() {
        current+= current + increment;
        if(current > inclusiveEnd)
            return nullptr;
        else
            return new DataPoint<T>(current); // TODO: generate the appropriate datapoint with the real values.
    }
    size_t size() { return std::numeric_limits<size_t>::max(); } // TODO: change return to actul size rather than max

    std::string toString() {
        std::string s = std::to_string(size());
        return s;
    }
    static bool classof(const IDataPointCollection* baseCollection) {
        return baseCollection->getKind() == IDataPointCollection::DPCK_DPR;
    }
private:
    T start;
    T inclusiveEnd;
    T increment;
    T current;
};


/* Metric query result should contain the requested metrics. Since the original query
 * might request some unavailable data points, the knowledge base has to figure out what to
 * do with those unavailable datapoints. It could interpolate/extrapolate to get them
 * but that may not be feasible in complicated code where performance depends on many
 * variables in the code. In that case and in the case where the knowledge base is
 * a trivial knowledge base, it could just record the datapoints it finds and inform the
 * client that it could not find the rest.
*/

class IDataPointMetric {
public:
    enum DataPointMetricKind {
        DPMK_DPM,
    };
    const DataPointMetricKind kind;
public:
    IDataPointMetric(DataPointMetricKind dpk) : kind(dpk) {}
    DataPointMetricKind getKind() const {
        return kind;
    }
public:
    virtual ~IDataPointMetric() {}
    virtual std::string toString() = 0;
    virtual IDataPoint* datapoint() = 0;
    virtual float getMetric() = 0;
    virtual void setMetric(float) = 0;
};

class IMetricCollection {
public:
    enum MetricCollectionKind {
        MCK_DPMV, // dp metric vector
    };
    const MetricCollectionKind kind;
public:
    IMetricCollection(MetricCollectionKind dpk) : kind(dpk) {}
    MetricCollectionKind getKind() const {
        return kind;
    }
public:
    //typedef std::iterator<std::random_access_iterator_tag, IDataPointMetric*> metrics_iterator;
    virtual ~IMetricCollection() {}
    virtual size_t size() = 0;
    virtual std::string toString() = 0;
    //virtual metrics_iterator begin() = 0;
    //virtual metrics_iterator end() = 0;
};

/* Metric vector iterator
 */
class DataPointMetricVector;
class MetricVectorIterator: public std::iterator<std::random_access_iterator_tag, IDataPointMetric*> {
public:
    //std::vector<IDataPointMetric*>* p;
    IDataPointMetric** p;
    std::vector<IDataPointMetric*>* v;
public:
    MetricVectorIterator(std::vector<IDataPointMetric*>& x, int offset = 0) : p(&x[offset]) { /**p = x[offset]; std::cout << "iterator init: p = " << p << " | *p = " << *p << std::endl;*/}
    MetricVectorIterator(const MetricVectorIterator& mvit) : p(mvit.p) { /**p = *(mvit.p);*/ }
    MetricVectorIterator(MetricVectorIterator&& mvit) : p(mvit.p) { /**p = *(mvit.p);*/ mvit.p = nullptr; }
    MetricVectorIterator& operator++() { ++p; return *this; }
    bool operator==(const MetricVectorIterator& rhs) {return p==rhs.p;}
    bool operator!=(const MetricVectorIterator& rhs) {return p!=rhs.p;}
    IDataPointMetric*& operator*() {return *p;}
};

template <typename T>
class DataPointMetric : public IDataPointMetric {
public:
    DataPointMetric(T value, IDataPoint* point) : IDataPointMetric(DPMK_DPM), m(value), dp(point) {}
    static bool classof(const IDataPointMetric* baseMetric) {
        return baseMetric->getKind() == IDataPointMetric::DPMK_DPM;
    }
    std::string toString() {
        std::string s = std::to_string(m);
        return s;
    }
    IDataPoint* datapoint() { return dp;}
    float getMetric() { return static_cast<float>(m); }
    void setMetric(float val) { m = static_cast<T>(val); }
private:
    T m; // metric
    IDataPoint* dp; // associates a query datapoint with a metric.
};

class DataPointMetricVector : public IMetricCollection {
public:
    DataPointMetricVector() : IMetricCollection(MCK_DPMV) {}
    static bool classof(const IMetricCollection* baseCollection) {
        return baseCollection->getKind() == IMetricCollection::MCK_DPMV;
    }
    //metrics_iterator begin() { return metrics.begin();}
    //metrics_iterator end() { return metrics.end(); }
    size_t size() { return metrics.size(); }
    std::string toString() {
        std::string s = std::to_string(size());
        return s;
    }
    void add(IDataPointMetric* metric) { metrics.push_back(metric); }
    MetricVectorIterator begin() { return MetricVectorIterator(metrics); }
    MetricVectorIterator end() { return MetricVectorIterator(metrics, metrics.size()); }
private:
    std::vector<IDataPointMetric*> metrics;
};

#endif // IDATAPOINT_H

