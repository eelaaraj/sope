#ifndef QUERYGENERATOR_H
#define QUERYGENERATOR_H

#include <map>
#include <string>
#include <iostream>
#include <vector>
#include "IDataPoint.h"

class QueryGenerator {
public:
    virtual ~QueryGenerator() { }
    virtual IDataPointCollection* queryDatapoints(std::map<std::string, int>& intVars, std::map<std::string, float>& floatVars, std::map<std::string, std::string>& stringVars) = 0;
    //static QueryGenerator* createQueryer();
private:

};

extern "C" QueryGenerator* create_queryer() ;

#endif // QUERYGENERATOR_H
