#ifndef MOCKUTILS_H
#define MOCKUTILS_H

#include "IKnowledgeBase.h"
#include "../serialize/HashTreeSerializer.h"
#include "IStorage.h"

QueryMatch createMockMatch(ITreeQuery* query) {
    QueryMatch qMatch = QueryMatch();
    qMatch.queryTreeOperation = dyn_cast<ASTTreeQuery>(query)->ast;
    qMatch.resultTreeOperation = qMatch.queryTreeOperation;
    /* The mock match will automatically traverse the queryTree and
     * designate the top most loop (the first loop it encounters) as the
     * resultTree. This will be the mock result match will basically is
     * a subtree of the query tree.
     */
    std::function<bool(std::shared_ptr<operation>)> predicateAlwaysTrue = [] (std::shared_ptr<operation> toCheck) {
        return true;
    };
    std::function<void(std::shared_ptr<operation>)> doesNothing = [] (std::shared_ptr<operation> op) { };
    std::shared_ptr<operation> foundForLoop = nullptr;
    std::function<void(std::shared_ptr<operation>)> setupCheckIfLoop = [&foundForLoop] (std::shared_ptr<operation> op) {
        loopOperation* lo = dyn_cast<loopOperation>(op.get());
        if(lo && (foundForLoop==nullptr)) {
            foundForLoop = op;
        }
    };
    qMatch.queryTreeOperation->traverseInOrder(doesNothing, nullptr, predicateAlwaysTrue, qMatch.queryTreeOperation, setupCheckIfLoop);
    if(foundForLoop) {
        std::cout << "Found FOR-loop subtree match = " << foundForLoop->getMainStringLiteral() << std::endl;
        qMatch.resultTreeOperation = foundForLoop;
    }
    return qMatch;
}

ITreeQueryResult* createMockResult(ITreeQuery* query) {
    std::cout << "-- Mock-queried tree --\n";
    std::cout << query->toString();
    // Create a mock matchset which contains a match betweeb the input query tree and itself
    QueryMatch mockMatch = createMockMatch(query);
    ASTTreeQueryResult::MatchSet matchSet;
    matchSet.push_back(mockMatch);
    // End of mock matchset
    // Create a mock result
    ASTTreeQueryResult* concreteResult = new ASTTreeQueryResult();
    concreteResult->queryId = query;
    concreteResult->matchSet.swap(matchSet);
    ITreeQueryResult* result = concreteResult;
    // End of moch result
    std::cout << result->toString();
    return result;
}

ITreeQuery* createMockQuery(std::shared_ptr<operation> queryTreeAST) {
    ASTTreeQuery* query = new ASTTreeQuery();
    query->ast = queryTreeAST;
    return query;
}

IMetricQuery* createMockMetricQuery() {
    ASTMetricQuery* query = new ASTMetricQuery(-1,-1);
    DataPointVector* dpv = new DataPointVector();
    IDataPoint* dp1 = new DataPoint<int>(100);
    IDataPoint* dp2 = new DataPoint<int>(101);
    IDataPoint* dp3 = new DataPoint<int>(112);
    dpv->add(dp1); dpv->add(dp2); dpv->add(dp3);
    query->queryPoints = dpv;
    return query;
}

class MockASTKnowledgeBase : public IKnowledgeBase{
    static bool classof(const IKnowledgeBase* base) {
        return base->getKind() == IKnowledgeBase::KBK_MOCK;
    }
public:
    MockASTKnowledgeBase() : IKnowledgeBase(KBK_MOCK) {}
    ITreeQueryResult* storeCodeTree(ITreeQuery* query);
    void storeCodeMetrics(IMetricQuery* query, ITreeQueryResult* treeQueryResult) ;
    ITreeQueryResult* queryCodeTree(ITreeQuery* query);
    IMetricQueryResult* queryCodeMetrics(IMetricQuery *query, ITreeQueryResult* treeQueryResult);

};

ITreeQueryResult *MockASTKnowledgeBase::storeCodeTree(ITreeQuery* query) {
    std::cout << "-- Mock-storing a tree --\n";
    std::cout << query->toString();
    return nullptr;
}
void MockASTKnowledgeBase::storeCodeMetrics(IMetricQuery* query, ITreeQueryResult* treeQueryResult) {
    std::cout << "-- Mock-storing an additional metric for a certain tree --\n";
    std::cout << query->toString();
}

ITreeQueryResult* MockASTKnowledgeBase::queryCodeTree(ITreeQuery* query) {
    ITreeQueryResult* result = createMockResult(query);
    return result;
}
IMetricQueryResult* MockASTKnowledgeBase::queryCodeMetrics(IMetricQuery *query, ITreeQueryResult *treeQueryResult) {
    std::cout << "-- Mock-queried metrics for a certain tree --\n";
    std::cout << query->toString();
    ASTMetricQueryResult* concreteResult = new ASTMetricQueryResult();
    // Create mock metrics and add them to the result
    DataPointMetricVector* mv = new DataPointMetricVector();
    IDataPoint* first = dyn_cast<DataPointVector>(dyn_cast<ASTMetricQuery>(query)->queryPoints)->dataPoints()[0];
    //IDataPoint* first = dyn_cast<ASTMetricQuery>(query)->queryPoints->next(); // next() is broken
    IDataPointMetric* m1 = new DataPointMetric<float>(23.5f, first);
    mv->add(m1);
    IDataPoint* second = dyn_cast<DataPointVector>(dyn_cast<ASTMetricQuery>(query)->queryPoints)->dataPoints()[1];
    IDataPointMetric* m2 = new DataPointMetric<float>(24.6f, second);
    mv->add(m2);
    IDataPoint* third = dyn_cast<DataPointVector>(dyn_cast<ASTMetricQuery>(query)->queryPoints)->dataPoints()[2];
    IDataPointMetric* m3 = new DataPointMetric<float>(-1.0f, third);
    mv->add(m3);
    concreteResult->metrics = mv;
    // End of mock metrics
    std::cout << concreteResult->toString();
    return concreteResult;
}

/*
 * Class MockHashKnowledgeBase and all its supporting members and util functions.
 */

#include "../SimilaritySearch.h"
//ITreeQuery* createMockFingerprintQuery(std::shared_ptr<operation> queryTreeAST, PatternTree<operation> patternTree) {
ITreeQuery* createMockFingerprintQuery(std::shared_ptr<operation> queryTreeAST, std::shared_ptr<IPatternTree>& patternTree, int runId) {
    HashTreeQuery<operation,HashFingerprint>* query = new HashTreeQuery<operation,HashFingerprint>();
    query->runId = runId;
    query->ast = queryTreeAST;
    query->patternTree = patternTree;
    //FingerprintSearchFunctor<operation> fsf = FingerprintSearchFunctor<operation>(patternTree, nullptr);
    //fsf(); // hashes the pattern tree
    RabinKarpHasher<operation,HashFingerprint> hasher = RabinKarpHasher<operation,HashFingerprint>(patternTree);
    query->fingerprintTree = hasher.hash();
    return query;
}

QueryMatch createMockFingerprintMatch(ITreeQuery* query) {
    QueryMatch qMatch = QueryMatch();
    qMatch.queryTreeOperation = dyn_cast<HashTreeQuery<operation,HashFingerprint>>(query)->ast;
    qMatch.resultTreeOperation = qMatch.queryTreeOperation;
    /* The mock match will automatically traverse the queryTree and
     * designate the top most loop (the first loop it encounters) as the
     * resultTree. This will be the mock result match will basically is
     * a subtree of the query tree.
     */
    std::function<bool(std::shared_ptr<operation>)> predicateAlwaysTrue = [] (std::shared_ptr<operation> toCheck) {
        return true;
    };
    std::function<void(std::shared_ptr<operation>)> doesNothing = [] (std::shared_ptr<operation> op) { };
    std::shared_ptr<operation> foundForLoop = nullptr;
    std::function<void(std::shared_ptr<operation>)> setupCheckIfLoop = [&foundForLoop] (std::shared_ptr<operation> op) {
        loopOperation* lo = dyn_cast<loopOperation>(op.get());
        if(lo && (foundForLoop==nullptr)) {
            foundForLoop = op;
        }
    };
    qMatch.queryTreeOperation->traverseInOrder(doesNothing, nullptr, predicateAlwaysTrue, qMatch.queryTreeOperation, setupCheckIfLoop);
    if(foundForLoop) {
        std::cout << "Found FOR-loop subtree match = " << foundForLoop->getMainStringLiteral() << std::endl;
        qMatch.resultTreeOperation = foundForLoop;
    }
    return qMatch;
}

ITreeQueryResult* createMockFingerprintResult(ITreeQuery* query) {
    std::cout << "-- Mock-queried hashed tree --\n";
    std::cout << query->toString();
    // Create a mock matchset which contains a match betweeb the input query tree and itself
    QueryMatch mockMatch = createMockFingerprintMatch(query);
    ASTTreeQueryResult::MatchSet matchSet;
    matchSet.push_back(mockMatch);
    // End of mock matchset
    // Create a mock result
    ASTTreeQueryResult* concreteResult = new ASTTreeQueryResult();
    concreteResult->queryId = query;
    concreteResult->matchSet.swap(matchSet);
    ITreeQueryResult* result = concreteResult;
    // End of moch result
    std::cout << result->toString();
    return result;
}

class MockHashKnowledgeBase : public IKnowledgeBase{
    static bool classof(const IKnowledgeBase* base) {
        return base->getKind() == IKnowledgeBase::KBK_HASH;
    }
public:
    //MockHashKnowledgeBase() : MockHashKnowledgeBase(nullptr) {}
    MockHashKnowledgeBase() = delete;
    MockHashKnowledgeBase(IStorage* db) : IKnowledgeBase(KBK_HASH), storageDB(db) {}
    ITreeQueryResult *storeCodeTree(ITreeQuery* query);
    void storeCodeMetrics(IMetricQuery* query, ITreeQueryResult* treeQueryResult) ;
    ITreeQueryResult* queryCodeTree(ITreeQuery* query);
    IMetricQueryResult* queryCodeMetrics(IMetricQuery *query, ITreeQueryResult* treeQueryResult);
private:
    /*std::unique_ptr<IBenchmark>*/ QueryMatch findSimilar(ICOI* coi);
    /*std::unique_ptr<IBenchmark>*/ QueryMatch findSimilarPattern(ICOI* coi);
    /*std::unique_ptr<IBenchmark>*/ QueryMatch findSimilarHash(ICOI* coi);
private:
    // Map whose key=fingerprint, and value is a vector or trees. This acts as a map of buckets.
    // Trees that have same hash will be in the same bucket. Any query tree whose fingerprint
    // matches a key in the db will be similar to all the trees in the bucket.
    //std::map<HashFingerprint, std::vector<PatternTree<operation>>> db;
    std::map<HashFingerprint, std::vector<std::shared_ptr<IPatternTree>>> db;
    IStorage* storageDB;
};

// Hash tree using rabin karp hashing
//template<typename T, typename HashType>
//std::unique_ptr<HashTree<operation, HashFingerprint>> hashTreeRK(PatternTree<operation> pTree) {
std::unique_ptr<HashTree<operation, HashFingerprint>> hashTreeRK(std::shared_ptr<IPatternTree> pTree) {
    RabinKarpHasher<operation,HashFingerprint> hasher(pTree);
    std::unique_ptr<HashTree<operation, HashFingerprint>> hTree = hasher.hash(); // create a copy of the hashtree because as soon as this function exits, the hasher and its members ill be destroyed
    HashTreeSerializer<operation,HashFingerprint> hTreeSerializer("./db/json_hash_tree_out.json", hTree.get());
    hTreeSerializer.serialize();
    return hTree;
}

ITreeQueryResult* MockHashKnowledgeBase::storeCodeTree(ITreeQuery* query) {
    std::cout << "-- Mock-storing a hashed tree --\n";
    std::cout << query->toString();
    if(HashTreeQuery<operation,HashFingerprint>* hashQuery = dyn_cast<HashTreeQuery<operation,HashFingerprint>>(query)) {
        // TODO: We might not need to hash here. The tree should be hashed when the query is created.
        // Fix this when the design is final and it is a prerequisite to create the query and hash
        // the tree before anythign can be done with the query.
        std::unique_ptr<HashTree<operation, HashFingerprint>> hashTree = hashTreeRK(hashQuery->patternTree);
        hashQuery->fingerprintTree = std::move(hashTree);
        // insert into DB the patternTree in the appropriate bucket. This has to be done
        // for all/(some important) sub-trees and not just the complete tree.
        db[hashQuery->fingerprintTree->getHash()].push_back(hashQuery->patternTree);
        //delete hashTree; // TODO: For now we delete the tree here. Later we might need it beyond this point and therefore we should remove delete from here
        // Call the Storage equivalent to persist the newly added record
        COI coi(hashQuery->runId, hashQuery->patternTree, hashQuery->fingerprintTree.get());
        std::shared_ptr<IBenchmark> storedBench = std::shared_ptr<IBenchmark> { storageDB->addBenchmark(&coi) };
        HashTreeQueryResult *res = new HashTreeQueryResult();
        QueryMatch qm;
        qm.matchingBench = storedBench;
        qm.benchNode = nullptr;
        qm.coiNode = nullptr;
        res->matchSet = { qm };
        return res;
        //delete coi;
    }
}

// This function is used for testing the metric storage functionality. Tat functionality
// needs a metricquery which contains a valid benchmarkId and a runId corresponding to
// the run by the BENCHMARKER tool. That tool has already generated a json file containing
// the captrued metrics with a name that contains runId such as: metrics_benchID_runID.json
IMetricQuery* createEmptyMetricQuery(int benchId, int runId) {
    DataPointVector* dpv = new DataPointVector();
    IDataPoint* dp1 = new DataPoint<int>({{"k",100}}, {1});
    IDataPoint* dp2 = new DataPoint<int>({{"k",101}}, {1});
    IDataPoint* dp3 = new DataPoint<int>({{"k",112}}, {1});
    dpv->add(dp1); dpv->add(dp2); dpv->add(dp3);
    IMetricQuery* query = new ASTMetricQuery(benchId, runId, dpv);
//    query->benchId() = benchId;
//    query->runId() = runId;
//    query->datapoints() = nullptr;
    return query;
}

void MockHashKnowledgeBase::storeCodeMetrics(IMetricQuery* query, ITreeQueryResult* treeQueryResult) {
    std::cout << "-- Mock-storing an additional metric for a certain hashed tree --\n";
    std::cout << query->toString();
    // Call the FileStorage equivalent to persist the metrics
    //IBenchmark* bench = new Benchmark(-1, )
    IBenchmark* bench = nullptr;
    if(treeQueryResult==nullptr) {
        bench = storageDB->findBenchmark(query->benchId());
    }
    else {
        bench = treeQueryResult->getMatches().front().matchingBench.get();
    }
    // no datapoints or metrics passed. The only thing we need is the runId and benchId.
    // The BENCHMARKER tool would have already captured matrics in a json so the only thing
    // we need is the ID to add that json filepath to the DB index.
    StorageMetrics metrics(query->runId(),nullptr, nullptr);
    storageDB->addMetrics(bench, &metrics);
}

/*std::unique_ptr<IBenchmark>*/ QueryMatch MockHashKnowledgeBase::findSimilarHash(ICOI* coi) {
    // 1- get a new record from storage. Deserialize it into a hashTree
    // 2- traverse the hashtree to find a similar hash
    // 3- Repeat 1
    IStorageIterator* beginIt = storageDB->begin();
    IStorageIterator* endIt = storageDB->end();
    HashMatch<operation,HashFingerprint> bestMatch = { nullptr, nullptr, -1, 0}; // this is the best match amond all trees.
    std::unique_ptr<IBenchmark> bestBench;
    for(;*beginIt!=*endIt; ++(*beginIt)) {
        std::unique_ptr<IBenchmark> currentBench = beginIt->getBenchmark(); // lazily creates a benchmark out of each DB record
        auto hTree = currentBench->hashTree();
        // Check HASH similarity between the COI and this Benchmark
        spdlog::get("sopelogger")->debug("-- Comparing COI HashTree to Benchmark HashTree with ID = {}",currentBench->Id());
        HashSearchFunctor<operation,HashFingerprint> searchFunc(hTree);
        std::function<void(ITreeNode*)> operatorParenthesis = [&searchFunc](ITreeNode* coiNode) { searchFunc(coiNode); };
        const_cast<ITree*>(coi->hashTree())->traverseTreeDFSL2RB2T<void>(operatorParenthesis);
        // Check if currentMatch is the best match
        // If a match exists with a bigger hash value, it means that the hash occured higher in the tree.
        // the >= comparison is used because we start comparing at the top of the tree which is usually an empty node. The next time
        // we encounter the same hashvalue, the node will not be an empty node.
        if(searchFunc.finalMatch().hashValue > bestMatch.hashValue) {
            bestMatch = searchFunc.finalMatch();
            bestBench = std::move(currentBench);
        }
        //spdlog::get("sopelogger")->debug("\n");
    }
    delete beginIt; // This is an inconvenienve but for now must be done because we want to use the baseIterator class to iterate any type of storage
    delete endIt;
    //return bestBench;
    std::cout << "Best match has hash = " << bestMatch.hashValue << std::endl;
    QueryMatch match = { nullptr, nullptr, bestMatch.coiHashNode->patternNode() , bestMatch.benchHashNode->patternNode() , std::shared_ptr<IBenchmark>{std::move(bestBench)} };
    return match;
}

/*std::unique_ptr<IBenchmark>*/ QueryMatch MockHashKnowledgeBase::findSimilarPattern(ICOI* coi) {
    IStorageIterator* beginIt = storageDB->begin();
    IStorageIterator* endIt = storageDB->end();
    Match<operation> bestMatch { nullptr, nullptr, -1, -1, 0};
    std::unique_ptr<IBenchmark> bestBench;
    for(;*beginIt!=*endIt; ++(*beginIt)) {
        std::unique_ptr<IBenchmark> currentBench = beginIt->getBenchmark(); // lazily creates a benchmark out of each DB record
        auto pTree = currentBench->patternTree();
        // Check PATTERN similarity between the COI and this Benchmark
        spdlog::get("sopelogger")->debug("-- Comparing COI PatternTree to Benchmark PatternTree with ID = {}",currentBench->Id());
        SearchFunctor<operation> searchFunc(pTree);
        std::function<void(std::shared_ptr<IPatternNode>&,
                           std::function<void(std::shared_ptr<operation>)>)> operatorParenthesis = searchFunc;
        coi->patternTree()->traverseTreeDFSL2RB2T<operation,void>(operatorParenthesis);
        // Check if currentMatch is the best match
        if(searchFunc.finalMatch().longestMatchInSequence > bestMatch.longestMatchInSequence) {
            bestMatch = searchFunc.finalMatch();
            bestBench = std::move(currentBench);
        }
        //std::cout << std::endl;
        //spdlog::get("sopelogger")->debug("\n");
        //delete currentBench; // lazy benchmark must be deleted since it was created on the heap from within IstorageIterator
    }
    delete beginIt; // This is an inconvenienve but for now must be done because we want to use the baseIterator class to iterate any type of storage
    delete endIt;
    //return bestBench;
    QueryMatch match = { nullptr, nullptr, bestMatch.coiNode, bestMatch.patternNode, std::shared_ptr<IBenchmark>{std::move(bestBench)} };
    return match;
}

// Finds the benchmark thats similar to the code of interest.
/*std::unique_ptr<IBenchmark>*/ QueryMatch MockHashKnowledgeBase::findSimilar(ICOI* coi) {
    auto bestFit = findSimilarHash(coi);
    //auto bestFit = findSimilarPattern(coi);
    return bestFit;
}

// Queries the KB for a benchmark that is similar to the COI
ITreeQueryResult* MockHashKnowledgeBase::queryCodeTree(ITreeQuery* query) {
    //ITreeQueryResult* result = createMockFingerprintResult(query);
    ITreeQueryResult* result = nullptr;
    if(HashTreeQuery<operation,HashFingerprint>* hashQuery = dyn_cast<HashTreeQuery<operation,HashFingerprint>>(query)) {
        COI coi(hashQuery->runId, hashQuery->patternTree, hashQuery->fingerprintTree.get());
        auto bestFit = findSimilar(&coi);
        // create the result object
        HashTreeQueryResult* res = new HashTreeQueryResult();
        res->queryId = query;
        //QueryMatch match = { nullptr, nullptr, std::shared_ptr<IBenchmark>{std::move(bestFit)} };
        //res->matchSet.push_back(match);
        res->matchSet.push_back(bestFit);
        result = res;
    }
    return result;
}
// Queries the KB for certain metrics of the matched benchmark.
IMetricQueryResult* MockHashKnowledgeBase::queryCodeMetrics(IMetricQuery *query, ITreeQueryResult* treeQueryResult) {
    std::cout << "-- Mock-queried metrics for a certain hashed tree --\n";
    std::cout << query->toString();
    IBenchmark* bench = nullptr;
    if(treeQueryResult == nullptr) {
    bench = storageDB->findBenchmark(query->benchId());
    } else {
        bench = treeQueryResult->getMatches().front().matchingBench.get();
    }
    StorageMetrics metrics(query->runId(),dyn_cast<DataPointVector>(query->datapoints()), nullptr); // datapoints should not be null later
    storageDB->findMetrics(bench, &metrics);
    ASTMetricQueryResult* res = new ASTMetricQueryResult();
    res->metrics = metrics.metrics();
    return res;
    /*
    ASTMetricQueryResult* concreteResult = new ASTMetricQueryResult();
    // Create mock metrics and add them to the result
    DataPointMetricVector* mv = new DataPointMetricVector();
    IDataPoint* first = dyn_cast<DataPointVector>(dyn_cast<ASTMetricQuery>(query)->queryPoints)->dataPoints()[0];
    //IDataPoint* first = dyn_cast<ASTMetricQuery>(query)->queryPoints->next(); // next() is broken
    IDataPointMetric* m1 = new DataPointMetric<float>(23.5f, first);
    mv->add(m1);
    IDataPoint* second = dyn_cast<DataPointVector>(dyn_cast<ASTMetricQuery>(query)->queryPoints)->dataPoints()[1];
    IDataPointMetric* m2 = new DataPointMetric<float>(24.6f, second);
    mv->add(m2);
    IDataPoint* third = dyn_cast<DataPointVector>(dyn_cast<ASTMetricQuery>(query)->queryPoints)->dataPoints()[2];
    IDataPointMetric* m3 = new DataPointMetric<float>(-1.0f, third);
    mv->add(m3);
    concreteResult->metrics = mv;
    // End of mock metrics
    std::cout << concreteResult->toString();
    return concreteResult;
    */
}

#endif // MOCKUTILS_H
