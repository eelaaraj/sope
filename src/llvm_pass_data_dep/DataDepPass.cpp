#include "llvm/Pass.h"
#include "llvm/IR/Function.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/IR/LegacyPassManager.h"
#include "llvm/IR/InstrTypes.h"
#include "llvm/Transforms/IPO/PassManagerBuilder.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/Transforms/Utils/BasicBlockUtils.h"
#include "llvm/IR/Module.h"

#include "llvm/Analysis/AliasAnalysis.h"
#include "llvm/Analysis/LoopAccessAnalysis.h"
#include "llvm/Analysis/LoopPass.h"
#include "llvm/Analysis/LoopInfoImpl.h"
#include "llvm/Analysis/ScalarEvolution.h"
#include "llvm/Analysis/MemoryDependenceAnalysis.h"
#include "llvm/Analysis/DependenceAnalysis.h"

#include "llvm/IR/DebugInfo.h"
#include "llvm/IR/DIBuilder.h"
//#include "llvm/DebugInfo/DIContext.h"
#include "llvm/IR/InstIterator.h"
#include "clang/Analysis/FlowSensitive/DataflowValues.h"

using namespace llvm;

static llvm::cl::OptionCategory DataDepPassCategory("Data Dependency Pass");
static llvm::cl::opt<std::string> functionNamesCommaSeperated("functions", llvm::cl::cat(DataDepPassCategory));
static llvm::cl::opt<bool> print_DU_UD("print-uses-users", llvm::cl::cat(DataDepPassCategory));


namespace {
struct DataDepPass : public FunctionPass {
    static char ID;
    std::vector<std::string> functions;
    bool printUserAndUsers;
    DataDepPass() : FunctionPass(ID) {}

    virtual bool doInitialization(Module & M) {
        StringRef commaSepStr(functionNamesCommaSeperated.getValue());
        if(!commaSepStr.empty()) {
            SmallVector<StringRef, 3> sepStrings;
            commaSepStr.split(sepStrings, StringRef(","));
            for(auto s: sepStrings) {
                functions.push_back(s.str());
            }
        }
        printUserAndUsers = print_DU_UD.getValue();
        return false;
    }


    virtual bool runOnFunction(Function &F) {

        std::string functionName = F.getName().str();
        std::function<bool(const std::string&)> predicate = [&functionName](const std::string& vectorElement) {
            return functionName.find(vectorElement)!=std::string::npos; };
        bool funcFound = (!functions.empty() && std::find_if(functions.begin(), functions.end(), predicate)!=functions.end());

        if(funcFound) {
            LLVMContext &Ctx = F.getContext();
            // Get the function to call from our runtime library.
            //      Constant *logopFunc = F.getParent()->getOrInsertFunction(
            //        "logop", Type::getVoidTy(Ctx), Type::getInt32Ty(Ctx), NULL
            //      );
            //	Constant *doubleFunc = F.getParent()->getOrInsertFunction(
            //        "retDouble", Type::getInt32Ty(Ctx), Type::getInt32Ty(Ctx), NULL
            //      );

            //	Constant *getTimeFunc = F.getParent()->getOrInsertFunction(
            //        "getTime", Type::getVoidTy(Ctx), Type::getInt8PtrTy(Ctx), NULL
            //      );

            //	Constant *logFunc = F.getParent()->getOrInsertFunction(
            //        "log", Type::getVoidTy(Ctx), Type::getInt8PtrTy(Ctx), NULL
            //      );

            DependenceAnalysis* DA = &getAnalysis<DependenceAnalysis>();
            //DA->print(llvm::outs(), F.getParent());

            std::string ss;
            int storeLoadCount = 0;
            llvm::raw_string_ostream rso(ss);
            for (inst_iterator SrcI = inst_begin(F), SrcE = inst_end(F);
                 SrcI != SrcE; ++SrcI) {
                rso<<"\nThe instruction is ";
                if (DILocation *Loc = SrcI->getDebugLoc()) { // Here I is an LLVM instruction
                    unsigned Line = Loc->getLine();
                    unsigned Col = Loc->getColumn();
                    StringRef File = Loc->getFilename();
                    StringRef Dir = Loc->getDirectory();
                    rso << " at Line = " << Line  << ":" << Col ;
                }
                rso <<":\n";
                SrcI->print(rso);
                rso <<"\n";
                if(printUserAndUsers) {
//                    if(isa<Instruction>(*SrcI)) {
//                        rso<<"\nThe instruction is = \n";
//                        SrcI->print(rso);
                        if(SrcI->getNumOperands() > 0) {
                            rso << "\nThis instruction Uses: \n";
                            for(Use& u:SrcI->operands()) {
                                if(isa<Instruction>(u)) {
                                    u.get()->print(rso);
                                    rso << "\n";
                                }
                            }
                        }
                        if(SrcI->getNumUses() > 0) {
                            rso << "The USERS of this instruction: \n";
                            for(User* u:SrcI->users()) {
                                u->print(rso);
                                rso << "\n";
                            }
                        }
                        //SrcI->dump();
//                    }
                }

                if (isa<StoreInst>(*SrcI) || isa<LoadInst>(*SrcI)) {
                    storeLoadCount++;
                    for (inst_iterator DstI = SrcI, DstE = inst_end(F);
                         DstI != DstE; ++DstI) {
                        if (isa<StoreInst>(*DstI) || isa<LoadInst>(*DstI)) {
                            rso << "da analyze - ";
                            if (auto D = DA->depends(&*SrcI, &*DstI, true)) {
                                //SrcI->dump();
                                SrcI->print(rso);
                                rso << " -- ";
                                //DstI->dump();
                                DstI->print(rso);
                                rso << " -- ";
                                D->dump(rso);
                                for (unsigned Level = 1; Level <= D->getLevels(); Level++) {
                                    if (D->isSplitable(Level)) {
                                        rso << "da analyze - split level = " << Level;
                                        rso << ", iteration = " << *DA->getSplitIteration(*D, Level);
                                        rso << "!\n";
                                    }
                                }
                            }
                            else {
                               rso << "none!\n";
                            }
                        }
                    }
                }
            }

            llvm::errs() << "store-load count = " << storeLoadCount << "\n";
            llvm::errs() << "Finished wriing to string stream\n";
            llvm::errs() << rso.str() ;
            //      for (auto &B : F) {
            //        for (auto &I : B) {
            //          if (auto *op = dyn_cast<BinaryOperator>(&I)) {
            //            // Insert *after* `op`.
            //            IRBuilder<> builder(op);
            //            builder.SetInsertPoint(&B, ++builder.GetInsertPoint());

            //            // Insert a call to our function.
            //            Value* args[] = {op};
            //	    Value* tim = builder.CreateCall(getTimeFunc, {});
            //	    Value* logTim = builder.CreateCall(logFunc, tim);
            //	    Value* doub = builder.CreateCall(doubleFunc, args);
            //            Value* o = builder.CreateCall(logopFunc, doub);

            //            return true;
            //          }
            //        }
            //      }
        }
        return false;
    }

    virtual void getAnalysisUsage(AnalysisUsage &analysisUsage) const {
        //AU.setPreservesCFG();
        analysisUsage.addRequired<LoopAccessAnalysis>();
            analysisUsage.addPreserved<LoopAccessAnalysis>();
            analysisUsage.addRequired<ScalarEvolution>();
            analysisUsage.addPreserved<ScalarEvolution>();
            analysisUsage.addRequired<AliasAnalysis>();
            analysisUsage.addPreserved<AliasAnalysis>();
            analysisUsage.addRequired<MemoryDependenceAnalysis>();
            analysisUsage.addPreserved<MemoryDependenceAnalysis>();
            analysisUsage.addRequired<DependenceAnalysis>();
            analysisUsage.addPreserved<DependenceAnalysis>();
        //AU.addRequired<DependenceAnalysis>();
    }
};
}

char DataDepPass::ID = 0;

static RegisterPass<DataDepPass> depPass("datadep", "Data Dependency Pass",
                                         false /* Chanegs CFG */,
                                         false /* Analysis Pass */);
