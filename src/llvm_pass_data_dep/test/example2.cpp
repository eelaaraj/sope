#include <iostream>
#include <string>

double function1(int a, double b) {
 return a + b ;
}

double function3(double a, const double* const b) {
 return a + *b ;
}

double function2(double a, double* b) {
 *b = *b *2;
 return a + *b ;
}


int main(int argc, const char** argv) {
    if(argc<3)
	return 1;
    int a = std::stoi(argv[1]);
    double b = std::stod(argv[2]);
    double* pb = &b;
    
    double f = function3(a, &b);
    double c = function1(a, b);

    double d = function1(a, *pb);
    
    double e = function2(c, pb);
    
    printf("%p\n", pb);
printf("%p\n", pb);
    printf("%f\n", *pb);
    printf("%f\n", c);
    printf("%f\n", d);
    //printf("%f\n", e);
    return 0;
}
