#ifndef IHASHTREE_H
#define IHASHTREE_H

#include <functional>

using HashFingerprint = unsigned long;
//using HashTree = std::vector<HashFingerprint>; // TODO: Make HashTree a class which holds a tree of hashes. A node in that tree will hold a hash AND a pointer to the patternnode in the patterntree

class ITreeNode {
public:
    enum TreeNodeKind {
        TNK_PAT, // pattern node
        TNK_HASH, // hash node
    };
public:
    ITreeNode(TreeNodeKind tnk, int uniqueId): kind(tnk), _uniqueId(uniqueId) {}
    virtual ~ITreeNode() {}
    virtual void addChild(ITreeNode* child)=0;
    virtual std::vector<ITreeNode*>& children()=0;
    virtual std::shared_ptr<IPatternNode>& patternNode() = 0;
    virtual int& uniqueId() { return _uniqueId; }
    TreeNodeKind getKind() const {
        return kind;
    }
    int& level() { return _level;}
private:
    const TreeNodeKind kind;
    int _level;
    int _uniqueId;
};

class ITree {
public:
    enum TreeKind {
        TK_PAT, // pattern node
        TK_HASH, // hash node
    };
public:
    ITree(TreeKind tk): kind(tk) {spdlog::get("sopelogger")->info("Creating HashTree at {}", (void*)this);}
    virtual ~ITree() { spdlog::get("sopelogger")->info("Deleting HashTree at {}", (void*)this); }
    virtual ITreeNode* root() = 0;
    TreeKind getKind() const {
        return kind;
    }
    // remember that the value type of a hashnode is a hashtype while it can be something else for another type of node
    // Thus we can not know the type of what we are searching for. And we can't make the virtual function be templatized.
    // Therefore we can create a node around the value and pass it to the search function.
    virtual void findNodeWithValue(ITreeNode* wrapperAroundValue)=0;
    template<typename returnTypename, typename ... types>
    void traverseTreeDFSL2RB2T(std::function<returnTypename (ITreeNode*, types... args)> callable,
                               types&... args,
                               std::function<void (ITreeNode *)> setup = nullptr,
                               std::function<void (ITreeNode *)> cleanup = nullptr) ;
    template<typename returnTypename, typename ... types>
    void traverseTreeDFSL2RB2T(ITreeNode *node,
                               std::function<returnTypename (ITreeNode*, types... args)> callable,
                               types&... args,
                               std::function<void (ITreeNode *)> setup = nullptr,
                               std::function<void (ITreeNode *)> cleanup = nullptr) ;
    template<typename returnTypename, typename ... types>
    void traverseTreeDFST2BL2R(std::function<returnTypename (ITreeNode*, types... args)> callable,
                               types&... args,
                               std::function<void (ITreeNode *)> setup = nullptr,
                               std::function<void (ITreeNode *)> cleanup = nullptr) ;
    template<typename returnTypename, typename ... types>
    void traverseTreeDFST2BL2R(ITreeNode *node,
                               std::function<returnTypename (ITreeNode*, types... args)> callable,
                               types&... args,
                               std::function<void (ITreeNode *)> setup = nullptr,
                               std::function<void (ITreeNode *)> cleanup = nullptr) ;
private:
    const TreeKind kind;
};

/*
 * Traverses the tree DFS from left to right and top to bottom.
 */
template<typename returnTypename, typename ... types>
void ITree::traverseTreeDFST2BL2R(std::function<returnTypename (ITreeNode*, types... args)> callable,
                                  types&... args,
                                  std::function<void (ITreeNode*) > setup,
                                  std::function<void (ITreeNode*) > cleanup)  {
    traverseTreeDFST2BL2R(root(), callable, args..., setup, cleanup);
}

template<typename returnTypename, typename ... types>
void ITree::traverseTreeDFST2BL2R(ITreeNode* node,
                                  std::function<returnTypename (ITreeNode*, types... args)> callable,
                                  types&... args,
                                  std::function<void (ITreeNode*) > setup,
                                  std::function<void (ITreeNode*) > cleanup)  {
    // Call the function on the node
    callable(node, args...);
    // If node specifies certain condition, then we call setup.
    // Example: if the node is a loop, we add indentation to the body of the loop when printing
    if(setup)
        setup(node);
    for (auto pChild : node->children()) {
        traverseTreeDFST2BL2R(pChild, callable, args...,setup, cleanup);
    }
    // Here we can call the cleanup function. An example would be to decrease indentation.
    if(cleanup)
        cleanup(node);
}

/*
 * Traverses the tree DFS from left to right and bottom to top.
 */
template<typename returnTypename, typename ... types>
void ITree::traverseTreeDFSL2RB2T(std::function<returnTypename (ITreeNode*, types... args)> callable,
                                  types&... args,
                                  std::function<void (ITreeNode*) > setup,
                                  std::function<void (ITreeNode*) > cleanup)  {
    traverseTreeDFSL2RB2T(root(), callable, args..., setup, cleanup);
}

template<typename returnTypename, typename ... types>
void ITree::traverseTreeDFSL2RB2T(ITreeNode* node,
                                  std::function<returnTypename (ITreeNode*, types... args)> callable,
                                  types&... args,
                                  std::function<void (ITreeNode*) > setup,
                                  std::function<void (ITreeNode*) > cleanup)  {
    // If node specifies certain condition, then we call setup.
    // Example: if the node is a loop, we add indentation to the body of the loop when printing
    if(setup)
        setup(node);
    for (auto pChild : node->children()) {
        traverseTreeDFSL2RB2T(pChild, callable, args...,setup, cleanup);
    }
    // Call the function on the node
    callable(node, args...);
    // Here we can call the cleanup function. An example would be to decrease indentation.
    if(cleanup)
        cleanup(node);
}

template<typename T, typename HashType>
class HashNode : public ITreeNode {
public:
    HashNode(HashType val, std::shared_ptr<IPatternNode> pNode, int uniqueId=-1) : ITreeNode(TNK_HASH, uniqueId), value(val), _patternNode(pNode) { }
    ~HashNode() ;
    HashType getHash(); // return the hash value of the node
    void addChild(ITreeNode* child) { _children.push_back(child); }
    std::vector<ITreeNode*>& children() { return _children; }
    std::shared_ptr<IPatternNode>& patternNode() { return _patternNode; }
public:
    static bool classof(const ITreeNode* baseNode) {
        return baseNode->getKind() == ITreeNode::TNK_HASH;
    }
private:
    HashType value;
    std::shared_ptr<IPatternNode> _patternNode;
    std::vector<ITreeNode*> _children;
};

template<typename T, typename HashType>
HashNode<T,HashType>::~HashNode() {
    for_each(_children.begin(), _children.end(), std::default_delete<ITreeNode>());
}

template<typename T, typename HashType>
HashType HashNode<T,HashType>::getHash() {
    return value;
}

template<typename T, typename HashType>
class HashTree : public ITree {
public:
    HashTree() : HashTree(nullptr) { }
    HashTree(HashNode<T,HashType>* node) : ITree(TK_HASH), _root(node) { }
    ~HashTree();
    HashType getHash(); // returns the hash of the root node
    ITreeNode* root() { return _root; }
    void findNodeWithValue(ITreeNode* wrapperAroundValue) {} // TODO: not implemented yet
    HashNode<T,HashType>* search(T value) { } // TODO: not implemented yet
    void traverseDFS() { } // TODO: not implemented yet
private:
    HashNode<T,HashType>* _root;
};

template<typename T, typename HashType>
HashTree<T,HashType>::~HashTree() {
    delete _root;
}
template<typename T, typename HashType>
HashType HashTree<T,HashType>::getHash() {
    return _root->getHash();
}

#endif // IHASHTREE_H

