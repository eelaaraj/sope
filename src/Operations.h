/*
 * Operations.h
 *
 *  Created on: Aug 5, 2015
 *      Author: heisenberg
 */

#ifndef OPERATIONS_H_
#define OPERATIONS_H_
//using namespace clang;
#include "llvm/Support/Casting.h"
#include "clang/AST/Stmt.h"
#include "clang/Tooling/Tooling.h"
#include <vector>
#include <memory>
#include <queue>
#include <map>
#include <string>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <functional>
#include <exception>
#include <spdlog/spdlog.h>
//#include "ManualHelpers.h"
// the compare policy will determine how loose or detailed the comparison is.
// It could just compare the type of the operation like datapath operation or memory op
// Or it could be more detailed like is it a regular data path operation or a vectored
// data path operation. Or even more detailed like is it an add or multiply and if its
// a vectored variant, how many operands, the type of operands, etc...

namespace pattern {
namespace matching {

enum class OperationType {
	memory, datapath, memoryLoad, memoryStore, add, multiply, subtract, divide, loop, condition, assign, none,
    less, greater, equals,
    le, /*less or equal*/
    ge, /*greater or equal*/
    ne, /*not equal*/
    function,
    ifCondition,
    elseCondition,
    compoundStmt,
    numeric,
    call, // call expression equivalent in clang. This is base class for all other calls
    cxx_member_call, // member function call
    cxx_operator_call, // operator function call
};
enum class ParallelType {
    none, vector,
};
enum class OperandType {
	immediate, direct, indirect,
};
enum class AccessType {
    contiguous, strobed, random, constant,
};
enum class PerformanceMetric {
	energy, time, power,
};

struct Platform;
//template<typename T, typename ComparePolicy>   // this compare policy should probably be associated with T rather than Pattern<T> because we will compare instances of T rather than instances of Pattern
template<typename CompPolicy>
class Operation {
public:
public:
	enum OperationKind {
		//OK_Operation,
		OK_VariableReferenceOperation,
        OK_ClangVariableRefWrapper,
        OK_UnaryOperation,
        OK_ClangUnaryOperatorWrapper,
		OK_BinaryOperation,
		OK_ClangBinaryOperatorWrapper,
		OK_MemoryOperation,
		OK_ClangSubscriptWrapper,
		OK_LoopOperation,
		OK_ClangForLoopWrapper,
        OK_FunctionOperation,
        OK_ClangFunctionWrapper,
        OK_IfOperation,
        OK_ClangIfWrapper,
		OK_FillerOperation, // Just used as a filler for statement that are not really operations like a compoundstatement
        OK_VariableDeclOperation,
        OK_ClangVariableDeclaration,
        OK_NumericLiteral,
        OK_ClangIntegerLiteral,
        OK_ClangFloatLiteral,
        OK_CallOperation,
        OK_ClangCallOperation,
        OK_ClangCXXMemberCallOperation,
        OK_ClangOperatorCallOperation,
        OK_CompoundStatementOperation,
		OK_ClangCompoundStatementWrapper
	};

private:
	const OperationKind kind;
public:
	OperationKind getKind() const {
		return kind;
	}

	Operation(OperationKind k, const OperationType& op, const ParallelType& par, int numOper,
			OperandType* operTypes) :
			kind(k), _op(op), _parallel(par), _numOperands(
                    numOper), _operandTypes(operTypes) { /*std::cout << "creating operation at " << this << "\n";*/
	}
	//virtual ~Operation()
	//virtual OperationType getOpType() = 0;
	virtual std::string getMainStringLiteral() = 0;
	virtual clang::Stmt* getUnderlyingPtr() {
		return nullptr;
	}
	virtual void traverseInOrder(
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> callable,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> interCallable,
            std::function<bool(std::shared_ptr<Operation<CompPolicy>>)> operationPredicate,
            std::shared_ptr<Operation<CompPolicy>> sharedThisPointer,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> setup = nullptr,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> cleanup = nullptr) = 0;
	virtual void traversePostOrder(
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> callable,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> interCallable,
            std::function<bool(std::shared_ptr<Operation<CompPolicy>>)> operationPredicate,
            std::shared_ptr<Operation<CompPolicy>> sharedThisPointer,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> setup = nullptr,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> cleanup = nullptr) = 0;
    virtual void traversePreOrder(
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> callable,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> interCallable,
            std::function<bool(std::shared_ptr<Operation<CompPolicy>>)> operationPredicate,
            std::shared_ptr<Operation<CompPolicy>> sharedThisPointer,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> setup = nullptr,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> cleanup = nullptr) = 0;
    virtual void traverseChildren(
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> callable,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> interCallable,
            std::function<bool(std::shared_ptr<Operation<CompPolicy>>)> operationPredicate,
            std::shared_ptr<Operation<CompPolicy>> sharedThisPointer,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> setup = nullptr,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> cleanup = nullptr) = 0;

	virtual ~Operation() {
        /*std::cout << "destroying operation at " << this << "\n";*/
	}
//    template<class policy>
//    bool operator==(const Operation<CompPolicy...>& op) { return policy().isEqual(*this, op); }
//    template<class policy, class... policies>
//    bool operator==(const Operation<CompPolicy...>& op) { return policy().isEqual(*this, op) && operator==<policies...>(op); }

//    template<class policy>
//    bool isEqual(const Operation<CompPolicy...>& op) { return policy().isEqual(*this, op); }
//    template<class policy, class... policies>
//    bool isEqual(const Operation<CompPolicy...>& op) { return policy().isEqual(*this, op) && this->isEqual(op); }

    bool isEqual(Operation<CompPolicy>& op) { return CompPolicy().isEqual(*this, op); }

	//template<typename AComparePolicy>
	//bool isEqual(const Operation<CompPolicy...>& op, AComparePolicy policy, CompPolicy... policies);
	virtual OperationType getOpType() {
		OperationType op(_op);
		return op;
	}
private:
	//template<typename AComparePolicy>
	//bool doCompare(const Operation<CompPolicy...>& op, AComparePolicy policy, CompPolicy... policies);
private:
	OperationType _op;
	ParallelType _parallel; // whether its a standard or vector operation
	int _numOperands;
	OperandType* _operandTypes; // stores the type of each operand left to right.
	///std::tuple<CompPolicy...> comparisonPolicies;
};

template<typename CompPolicy>
class FillerOperation: public Operation<CompPolicy> {
public:
    FillerOperation(typename Operation<CompPolicy>::OperationKind kind,
			const ParallelType& par, int numOper, OperandType* operTypes) :
            Operation<CompPolicy>(kind, OperationType::none, par, numOper,
					operTypes) {

	}

    virtual ~FillerOperation() {}

    static bool classof(const Operation<CompPolicy>* baseOperation) {
		return baseOperation->getKind()
                >= Operation<CompPolicy>::OK_FillerOperation
				&& baseOperation->getKind()
                        <= Operation<CompPolicy>::OK_ClangFloatLiteral;
	}

	void traverseInOrder(
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> callable,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> interCallable,
            std::function<bool(std::shared_ptr<Operation<CompPolicy>>)> operationPredicate,
            std::shared_ptr<Operation<CompPolicy>> sharedThisPointer,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> setup = nullptr,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> cleanup = nullptr) {
        //std::cout << "Traverse in-order called on Filler Operation" << "\n";
        spdlog::get("sopelogger")->debug("Traverse in-order called on Filler Operation");
	}
	void traversePostOrder(
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> callable,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> interCallable,
            std::function<bool(std::shared_ptr<Operation<CompPolicy>>)> operationPredicate,
            std::shared_ptr<Operation<CompPolicy>> sharedThisPointer,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> setup = nullptr,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> cleanup = nullptr) {
        //std::cout << "Traverse post-order called on filler operation"
        //        << "\n";
        spdlog::get("sopelogger")->debug("Traverse post-order called on filler operation");
	}

    void traversePreOrder(
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> callable,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> interCallable,
            std::function<bool(std::shared_ptr<Operation<CompPolicy>>)> operationPredicate,
            std::shared_ptr<Operation<CompPolicy>> sharedThisPointer,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> setup = nullptr,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> cleanup = nullptr) {
        //std::cout << "Traverse pre-order called on filler operation"
        //        << "\n";
        spdlog::get("sopelogger")->debug("Traverse pre-order called on filler operation");
    }

    void traverseChildren(
                std::function<void(std::shared_ptr<Operation<CompPolicy>>)> callable,
                std::function<void(std::shared_ptr<Operation<CompPolicy>>)> interCallable,
            std::function<bool(std::shared_ptr<Operation<CompPolicy>>)> operationPredicate,
            std::shared_ptr<Operation<CompPolicy>> sharedThisPointer,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> setup = nullptr,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> cleanup = nullptr) {
        //std::cout << "Traverse children called on filler operation" << "\n";
        spdlog::get("sopelogger")->debug("Traverse children called on filler operation");
    }

    virtual std::string getMainStringLiteral() { return "Filler operation string literal" ; }
private:
};

template<typename CompPolicy>
class CallOperation: public Operation<CompPolicy> {
public:
    CallOperation(typename Operation<CompPolicy>::OperationKind kind, const OperationType& opType,
                  std::string callee,
                  const ParallelType& par,
                  int numOper, OperandType* operTypes) :
                  Operation<CompPolicy>(kind, opType, par, numOper,
                          operTypes), calleeName(callee) { }
    virtual ~CallOperation(){ //std::cout << "destroying call operation at " << this << "\n";
                              spdlog::get("sopelogger")->debug("destroying call operation at {}", (void*)this);
                            }

    virtual std::string getMainStringLiteral() {
        std::stringstream out;
        out << calleeName << "(" << _args.size() << " args)";
        return out.str();
    }

    static bool classof(const Operation<CompPolicy>* baseOperation) {
        return baseOperation->getKind()
                >= Operation<CompPolicy>::OK_CallOperation
                && baseOperation->getKind()
                        <= Operation<CompPolicy>::OK_ClangOperatorCallOperation;
    }

    std::string& callee() { return calleeName; }

    std::vector<std::shared_ptr<Operation<CompPolicy>>>& args() {return _args; }

    void addArg(std::shared_ptr<Operation<CompPolicy>> arg) { _args.push_back(arg); }

    virtual void traverseInOrder(
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> callable,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> interCallable,
            std::function<bool(std::shared_ptr<Operation<CompPolicy>>)> operationPredicate,
            std::shared_ptr<Operation<CompPolicy>> sharedThisPointer,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> setup = nullptr,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> cleanup = nullptr) {
        if(operationPredicate(sharedThisPointer)) {
            if(setup)
                setup(sharedThisPointer);
            callable(sharedThisPointer);
            typedef typename std::vector<std::shared_ptr<Operation<CompPolicy>>>::iterator iterType;
            iterType from = _args.begin();
            iterType til = _args.end();
            while(from != til) {
                if(interCallable)
                    interCallable(sharedThisPointer);
                (*from)->traverseInOrder(callable, interCallable, operationPredicate, *from, setup, cleanup);
                from++;
            }
            if(cleanup)
                cleanup(sharedThisPointer);
        }
    }
    virtual void traversePostOrder(
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> callable,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> interCallable,
            std::function<bool(std::shared_ptr<Operation<CompPolicy>>)> operationPredicate,
            std::shared_ptr<Operation<CompPolicy>> sharedThisPointer,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> setup = nullptr,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> cleanup = nullptr) {
        if(operationPredicate(sharedThisPointer)) {
            if(setup)
                setup(sharedThisPointer);
            callable(sharedThisPointer);
            typedef typename std::vector<std::shared_ptr<Operation<CompPolicy>>>::iterator iterType;
            iterType from = _args.begin();
            iterType til = _args.end();
            while(from != til) {
                if(interCallable)
                    interCallable(sharedThisPointer);
                (*from)->traversePostOrder(callable, interCallable, operationPredicate, *from, setup, cleanup);
                from++;
            }
            if(cleanup)
                cleanup(sharedThisPointer);
        }
    }
    virtual void traversePreOrder(
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> callable,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> interCallable,
            std::function<bool(std::shared_ptr<Operation<CompPolicy>>)> operationPredicate,
            std::shared_ptr<Operation<CompPolicy>> sharedThisPointer,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> setup = nullptr,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> cleanup = nullptr) {
        if(operationPredicate(sharedThisPointer)) {
            if(setup)
                setup(sharedThisPointer);
            callable(sharedThisPointer);
            typedef typename std::vector<std::shared_ptr<Operation<CompPolicy>>>::iterator iterType;
            iterType from = _args.begin();
            iterType til = _args.end();
            while(from != til) {
                if(interCallable)
                    interCallable(sharedThisPointer);
                (*from)->traversePreOrder(callable, interCallable, operationPredicate, *from, setup, cleanup);
                from++;
            }
            if(cleanup)
                cleanup(sharedThisPointer);
        }
    }
    virtual void traverseChildren(
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> callable,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> interCallable,
            std::function<bool(std::shared_ptr<Operation<CompPolicy>>)> operationPredicate,
            std::shared_ptr<Operation<CompPolicy>> sharedThisPointer,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> setup = nullptr,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> cleanup = nullptr) {
        if(operationPredicate(sharedThisPointer)) {
            if(setup)
                setup(sharedThisPointer);
            typedef typename std::vector<std::shared_ptr<Operation<CompPolicy>>>::iterator iterType;
            iterType from = _args.begin();
            iterType til = _args.end();
            while(from != til) {
                (*from)->traversePostOrder(callable, interCallable, operationPredicate, *from, setup, cleanup);
                if(interCallable)
                    interCallable(sharedThisPointer);
                from++;
            }
            if(cleanup)
                cleanup(sharedThisPointer);
        }
    }

private:
    std::string calleeName;
    std::vector<std::shared_ptr<Operation<CompPolicy>>> _args;
};

template<typename CompPolicy>
class UnaryOperation: public Operation<CompPolicy> {
public:
    UnaryOperation(typename Operation<CompPolicy>::OperationKind kind, const OperationType& opType,
            const std::string& operationCode, const std::string& onlyOperand,
            const ParallelType& par,
            int numOper, OperandType* operTypes) :
            Operation<CompPolicy>(kind, opType, par, numOper,
                    operTypes), _operationCode(operationCode), _onlyOperandLiteral(onlyOperand){

    }
    virtual ~UnaryOperation() {}

    static bool classof(const Operation<CompPolicy>* baseOperation) {
        return baseOperation->getKind()
                >= Operation<CompPolicy>::OK_UnaryOperation
                && baseOperation->getKind()
                        <= Operation<CompPolicy>::OK_ClangUnaryOperatorWrapper;
    }

    virtual std::string getMainStringLiteral() {
        if(_isPostOperator)
            return _onlyOperandLiteral + _operationCode;
        else
            return _operationCode + _onlyOperandLiteral;
    }

    void traverseInOrder(
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> callable,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> interCallable,
            std::function<bool(std::shared_ptr<Operation<CompPolicy>>)> operationPredicate,
            std::shared_ptr<Operation<CompPolicy>> sharedThisPointer,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> setup = nullptr,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> cleanup = nullptr) {
        if(operationPredicate(sharedThisPointer)) {
            if(setup)
                setup(sharedThisPointer);
            if(_isPostOperator)
                _onlyOperand->traverseInOrder(callable, interCallable, operationPredicate,_onlyOperand, setup, cleanup);
            if (interCallable)
                interCallable(sharedThisPointer);
            callable(sharedThisPointer);
            if (interCallable)
                interCallable(sharedThisPointer);
            if(!_isPostOperator)
                _onlyOperand->traverseInOrder(callable, interCallable, operationPredicate,_onlyOperand, setup, cleanup);
            if(cleanup)
                cleanup(sharedThisPointer);
        }
    }

    void traversePostOrder(
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> callable,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> interCallable,
            std::function<bool(std::shared_ptr<Operation<CompPolicy>>)> operationPredicate,
            std::shared_ptr<Operation<CompPolicy>> sharedThisPointer,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> setup = nullptr,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> cleanup = nullptr) {
        if(operationPredicate(sharedThisPointer)) {
            if(setup)
                setup(sharedThisPointer);
            if(_isPostOperator)
                _onlyOperand->traversePostOrder(callable, interCallable, operationPredicate, _onlyOperand, setup, cleanup);
            if (interCallable)
                interCallable(sharedThisPointer);
            callable(sharedThisPointer);
            if (interCallable)
                interCallable(sharedThisPointer);
            if(!_isPostOperator)
                _onlyOperand->traversePostOrder(callable, interCallable, operationPredicate, _onlyOperand, setup, cleanup);
            if(cleanup)
                cleanup(sharedThisPointer);
        }
    }

    void traversePreOrder(
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> callable,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> interCallable,
            std::function<bool(std::shared_ptr<Operation<CompPolicy>>)> operationPredicate,
            std::shared_ptr<Operation<CompPolicy>> sharedThisPointer,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> setup = nullptr,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> cleanup = nullptr) {
        if(operationPredicate(sharedThisPointer)) {
            if(setup)
                setup(sharedThisPointer);
            if(_isPostOperator)
                _onlyOperand->traversePreOrder(callable, interCallable, operationPredicate,_onlyOperand, setup, cleanup);
            if (interCallable)
                interCallable(sharedThisPointer);
            callable(sharedThisPointer);
            if (interCallable)
                interCallable(sharedThisPointer);
            if(!_isPostOperator)
                _onlyOperand->traversePreOrder(callable, interCallable, operationPredicate,_onlyOperand, setup, cleanup);
            if(cleanup)
                cleanup(sharedThisPointer);
        }
    }

    void traverseChildren(
                std::function<void(std::shared_ptr<Operation<CompPolicy>>)> callable,
                std::function<void(std::shared_ptr<Operation<CompPolicy>>)> interCallable,
            std::function<bool(std::shared_ptr<Operation<CompPolicy>>)> operationPredicate,
            std::shared_ptr<Operation<CompPolicy>> sharedThisPointer,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> setup = nullptr,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> cleanup = nullptr) {
        if(operationPredicate(sharedThisPointer)){
            if(setup)
                setup(sharedThisPointer);
            _onlyOperand->traverseChildren(callable, interCallable, operationPredicate,_onlyOperand, setup, cleanup);
            if(cleanup)
                cleanup(sharedThisPointer);
        }
    }

    std::shared_ptr<Operation<CompPolicy>>& operand() {
        return _onlyOperand;
    }

    std::string& opCode() {
        return _operationCode;
    }
    std::string& onlyOperandLiteral() {
        return _onlyOperandLiteral;
    }

   bool& isPostOperator() {
        return _isPostOperator;
    }
private:
    bool _isPostOperator;
    std::string _operationCode;
    std::string _onlyOperandLiteral;
    std::shared_ptr<Operation<CompPolicy>> _onlyOperand;
};

template<typename CompPolicy>
class BinaryOperation: public Operation<CompPolicy> {
public:
    BinaryOperation(typename Operation<CompPolicy>::OperationKind kind, const OperationType& opType,
			const std::string& operationCode, const std::string& firstOperand,
			const std::string& secondOperand, const ParallelType& par,
			int numOper, OperandType* operTypes) :
            Operation<CompPolicy>(kind, opType, par, numOper,
					operTypes), _operationCode(operationCode), _firstOperand(
					firstOperand), _secondOperand(secondOperand) {

	}
    virtual ~BinaryOperation() {}

    static bool classof(const Operation<CompPolicy>* baseOperation) {
		return baseOperation->getKind()
                >= Operation<CompPolicy>::OK_BinaryOperation
				&& baseOperation->getKind()
                        <= Operation<CompPolicy>::OK_ClangBinaryOperatorWrapper;
	}

    virtual std::string getMainStringLiteral() {
		return _firstOperand + _operationCode + _secondOperand;
	}

    void traverseInOrder(
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> callable,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> interCallable,
            std::function<bool(std::shared_ptr<Operation<CompPolicy>>)> operationPredicate,
            std::shared_ptr<Operation<CompPolicy>> sharedThisPointer,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> setup = nullptr,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> cleanup = nullptr) {
        if(operationPredicate(sharedThisPointer)) {
            if(setup)
                setup(sharedThisPointer);
            _lhsOperand->traverseInOrder(callable, interCallable, operationPredicate,_lhsOperand, setup, cleanup);
            if (interCallable)
                interCallable(sharedThisPointer);
            callable(sharedThisPointer);
            if (interCallable)
                interCallable(sharedThisPointer);
            _rhsOperand->traverseInOrder(callable, interCallable, operationPredicate, _rhsOperand, setup, cleanup);
            if(cleanup)
                cleanup(sharedThisPointer);
        }
    }

    // In all traversals, the rhs has to be traversed before the lhs because
    // in sequential languages the rhs is executed before the lhs.
    // I do it mostly everywhere. BUT I do not do it in traverseINORDER because
    // i only use that type of traversal to print the statements and I need to
    // print the lhs before the rhs.
	void traversePostOrder(
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> callable,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> interCallable,
            std::function<bool(std::shared_ptr<Operation<CompPolicy>>)> operationPredicate,
            std::shared_ptr<Operation<CompPolicy>> sharedThisPointer,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> setup = nullptr,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> cleanup = nullptr) {
        if(operationPredicate(sharedThisPointer)) {
            if(setup)
                setup(sharedThisPointer);
            _rhsOperand->traversePostOrder(callable, interCallable, operationPredicate, _rhsOperand, setup, cleanup);
            if (interCallable)
                interCallable(sharedThisPointer);
            _lhsOperand->traversePostOrder(callable, interCallable, operationPredicate, _lhsOperand, setup, cleanup);
            if (interCallable)
                interCallable(sharedThisPointer);
            callable(sharedThisPointer);
            if(cleanup)
                cleanup(sharedThisPointer);
        }
	}

    void traversePreOrder(
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> callable,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> interCallable,
            std::function<bool(std::shared_ptr<Operation<CompPolicy>>)> operationPredicate,
            std::shared_ptr<Operation<CompPolicy>> sharedThisPointer,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> setup = nullptr,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> cleanup = nullptr) {
        if(operationPredicate(sharedThisPointer)) {
            if(setup)
                setup(sharedThisPointer);
            callable(sharedThisPointer);
            if (interCallable)
                interCallable(sharedThisPointer);
            _rhsOperand->traversePreOrder(callable, interCallable, operationPredicate, _rhsOperand, setup, cleanup);
            if (interCallable)
                interCallable(sharedThisPointer);
            _lhsOperand->traversePreOrder(callable, interCallable, operationPredicate,_lhsOperand, setup, cleanup);
            if(cleanup)
                cleanup(sharedThisPointer);
        }
    }

    void traverseChildren(
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> callable,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> interCallable,
            std::function<bool(std::shared_ptr<Operation<CompPolicy>>)> operationPredicate,
            std::shared_ptr<Operation<CompPolicy>> sharedThisPointer,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> setup = nullptr,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> cleanup = nullptr) {
        if(operationPredicate(sharedThisPointer)) {
            if(setup)
                setup(sharedThisPointer);
            _rhsOperand->traverseChildren(callable, interCallable, operationPredicate, _rhsOperand, setup, cleanup);
            if (interCallable)
                interCallable(sharedThisPointer);
            _lhsOperand->traverseChildren(callable, interCallable, operationPredicate,_lhsOperand, setup, cleanup);
            if(cleanup)
                cleanup(sharedThisPointer);
        }
    }

    std::shared_ptr<Operation<CompPolicy>>& lhs() {
		return _lhsOperand;
	}
    std::shared_ptr<Operation<CompPolicy>>& rhs() {
		return _rhsOperand;
	}

	std::string& opCode() {
		return _operationCode;
	}
	std::string& firstOperand() {
		return _firstOperand;
	}
	std::string& secondOperand() {
		return _secondOperand;
	}
private:
	std::string _operationCode;
	std::string _firstOperand;
	std::string _secondOperand;
    // We should probably make the oeprands Pointers to operations like Operation<CompPolicy>* firstOperand ...
    std::shared_ptr<Operation<CompPolicy>> _lhsOperand;
    std::shared_ptr<Operation<CompPolicy>> _rhsOperand;
};

template<typename CompPolicy>
class LoopOperation: public Operation<CompPolicy> {
public:
    LoopOperation(typename Operation<CompPolicy>::OperationKind kind,
            int loopId, int numIter, const std::string& iterLiteral,
			const std::string& iterStart, const std::string& iterEnd,
			const ParallelType& par, int numOper, OperandType* operTypes) :
            Operation<CompPolicy>(kind, OperationType::loop, par, numOper,
                    operTypes), _loopId(loopId), _numIterations(numIter), _iterLiteral(
                    iterLiteral), _iterStart(iterStart), _iterEnd(iterEnd) { //std::cout << "creating loop operation at " << this << "\n";
                                                                             spdlog::get("sopelogger")->debug("creating loop operation at {}", (void*)this);

	}
    virtual ~LoopOperation(){ //std::cout << "destroying loop operation at " << this << "\n";
                              spdlog::get("sopelogger")->debug("destroying loop operation at {}", (void*)this);
                            }

    virtual std::string getMainStringLiteral() {
        //return _iterLiteral + ":" + _iterStart + "->" + _iterEnd;
        return _initStatement->getMainStringLiteral() + ";" + _conditionStatement->getMainStringLiteral() + ";" + _incrementStatement->getMainStringLiteral();
	}

    static bool classof(const Operation<CompPolicy>* baseOperation) {
		return baseOperation->getKind()
                >= Operation<CompPolicy>::OK_LoopOperation
				&& baseOperation->getKind()
                        <= Operation<CompPolicy>::OK_ClangForLoopWrapper;
	}

    std::vector<std::shared_ptr<Operation<CompPolicy>>>& statements() {return _statements;}

    void addStatement(std::shared_ptr<Operation<CompPolicy>> oper) {_statements.push_back(oper);}

    void traverseInOrder(
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> callable,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> interCallable,
            std::function<bool(std::shared_ptr<Operation<CompPolicy>>)> operationPredicate,
            std::shared_ptr<Operation<CompPolicy>> sharedThisPointer,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> setup = nullptr,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> cleanup = nullptr) {
        /*callable(sharedThisPointer);
        typedef typename std::vector<std::shared_ptr<Operation<CompPolicy>>>::iterator iterType;
        iterType from = _statements.begin();
        iterType til = _statements.end();
        std::reverse_iterator<iterType> revFrom(til);
        std::reverse_iterator<iterType> revTil(from);
        while(revFrom != revTil) {
            if(interCallable)
            interCallable(sharedThisPointer);
            (*revFrom)->traverseInOrder(callable, interCallable, *revFrom);
            revFrom++;
        }*/
        if(operationPredicate(sharedThisPointer)) {
            if(setup)
                setup(sharedThisPointer);
            callable(sharedThisPointer);
            typedef typename std::vector<std::shared_ptr<Operation<CompPolicy>>>::iterator iterType;
            iterType from = _statements.begin();
            iterType til = _statements.end();
            while(from != til) {
                if(interCallable)
                    interCallable(sharedThisPointer);
                (*from)->traverseInOrder(callable, interCallable, operationPredicate, *from, setup, cleanup);
                from++;
            }
            if(cleanup)
                cleanup(sharedThisPointer);
        }
	}

    void traversePostOrder(std::function<void(std::shared_ptr<Operation<CompPolicy>>)> callable,
                           std::function<void(std::shared_ptr<Operation<CompPolicy>>)> interCallable,
                           std::function<bool(std::shared_ptr<Operation<CompPolicy>>)> operationPredicate,
                           std::shared_ptr<Operation<CompPolicy>> sharedThisPointer,
                           std::function<void(std::shared_ptr<Operation<CompPolicy>>)> setup = nullptr,
                           std::function<void(std::shared_ptr<Operation<CompPolicy>>)> cleanup = nullptr) {
        // Loops should be traverse inorder. For now at least
        //traverseInOrder(callable, interCallable, sharedThisPointer);

        // What we did below is have the LoopStmt traverse inOrder. But the
        // statements inside the loop traverse in postOrder.
        /*callable(sharedThisPointer);
        typedef typename std::vector<std::shared_ptr<Operation<CompPolicy>>>::iterator iterType;
        iterType from = _statements.begin();
        iterType til = _statements.end();
        std::reverse_iterator<iterType> revFrom(til);
        std::reverse_iterator<iterType> revTil(from);
        while(revFrom != revTil) {
            if(interCallable)
            interCallable(sharedThisPointer);
            (*revFrom)->traversePostOrder(callable, interCallable, *revFrom);
            revFrom++;
        }*/
        if(operationPredicate(sharedThisPointer)) {
            if(setup)
                setup(sharedThisPointer);
            callable(sharedThisPointer);
            typedef typename std::vector<std::shared_ptr<Operation<CompPolicy>>>::iterator iterType;
            iterType from = _statements.begin();
            iterType til = _statements.end();
            while(from != til) {
                if(interCallable)
                    interCallable(sharedThisPointer);
                (*from)->traversePostOrder(callable, interCallable, operationPredicate, *from, setup, cleanup);
                from++;
            }
            if(cleanup)
                cleanup(sharedThisPointer);
        }
	}

    void traversePreOrder(
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> callable,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> interCallable,
            std::function<bool(std::shared_ptr<Operation<CompPolicy>>)> operationPredicate,
            std::shared_ptr<Operation<CompPolicy>> sharedThisPointer,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> setup = nullptr,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> cleanup = nullptr) {
        /*callable(sharedThisPointer);
        typedef typename std::vector<std::shared_ptr<Operation<CompPolicy>>>::iterator iterType;
        iterType from = _statements.begin();
        iterType til = _statements.end();
        std::reverse_iterator<iterType> revFrom(til);
        std::reverse_iterator<iterType> revTil(from);
        while(revFrom != revTil) {
            if(interCallable)
            interCallable(sharedThisPointer);
            (*revFrom)->traversePreOrder(callable, interCallable, *revFrom);
            revFrom++;
        }*/
        if(operationPredicate(sharedThisPointer)) {
            if(setup)
                setup(sharedThisPointer);
            callable(sharedThisPointer);
            typedef typename std::vector<std::shared_ptr<Operation<CompPolicy>>>::iterator iterType;
            iterType from = _statements.begin();
            iterType til = _statements.end();
            while(from != til) {
                if(interCallable)
                    interCallable(sharedThisPointer);
                (*from)->traversePreOrder(callable, interCallable, operationPredicate, *from, setup, cleanup);
                from++;
            }
            if(cleanup)
                cleanup(sharedThisPointer);
        }
    }

    void traverseChildren(
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> callable,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> interCallable,
            std::function<bool(std::shared_ptr<Operation<CompPolicy>>)> operationPredicate,
            std::shared_ptr<Operation<CompPolicy>> sharedThisPointer,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> setup = nullptr,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> cleanup = nullptr) {
        /*typedef typename std::vector<std::shared_ptr<Operation<CompPolicy>>>::iterator iterType;
        iterType from = _statements.begin();
        iterType til = _statements.end();
        std::reverse_iterator<iterType> revFrom(til);
        std::reverse_iterator<iterType> revTil(from);
        while(revFrom != revTil) {
            (*revFrom)->traversePostOrder(callable, interCallable, *revFrom);
            revFrom++;
            if(interCallable)
            interCallable(sharedThisPointer);
        }*/
        if(operationPredicate(sharedThisPointer)) {
            if(setup)
                setup(sharedThisPointer);
            typedef typename std::vector<std::shared_ptr<Operation<CompPolicy>>>::iterator iterType;
            iterType from = _statements.begin();
            iterType til = _statements.end();
            while(from != til) {
                (*from)->traversePostOrder(callable, interCallable, operationPredicate, *from, setup, cleanup);
                from++;
                if(interCallable)
                    interCallable(sharedThisPointer);
            }
            if(cleanup)
                cleanup(sharedThisPointer);
        }
    }

    std::shared_ptr<Operation<CompPolicy>>& init() { return _initStatement; }

    std::shared_ptr<Operation<CompPolicy>>& condition() { return _conditionStatement; }

    std::shared_ptr<Operation<CompPolicy>>& increment() { return _incrementStatement; }

    int getLoopId() { return _loopId; }
private:
    int _loopId; // unique Id of the loop inside the function
	int _numIterations;
	std::string _iterLiteral;
	std::string _iterStart;
	std::string _iterEnd;
    std::vector<std::shared_ptr<Operation<CompPolicy>>> _statements;
    std::shared_ptr<Operation<CompPolicy>> _initStatement;
    std::shared_ptr<Operation<CompPolicy>> _conditionStatement;
    std::shared_ptr<Operation<CompPolicy>> _incrementStatement;
};

template<typename CompPolicy>
class CompoundStatementOperation: public Operation<CompPolicy> {
public:
    CompoundStatementOperation(typename Operation<CompPolicy>::OperationKind kind,
            const ParallelType& par, int numOper, OperandType* operTypes) :
            Operation<CompPolicy>(kind, OperationType::compoundStmt, par, numOper,
                    operTypes) {

    }
    virtual ~CompoundStatementOperation(){}

    virtual std::string getMainStringLiteral() {
        return "Compound Stmt Operation";
    }

    static bool classof(const Operation<CompPolicy>* baseOperation) {
        return baseOperation->getKind()
                >= Operation<CompPolicy>::OK_CompoundStatementOperation
                && baseOperation->getKind()
                        <= Operation<CompPolicy>::OK_ClangCompoundStatementWrapper;
    }

    std::vector<std::shared_ptr<Operation<CompPolicy>>>& statements() {return _statements;}

    void addStatement(std::shared_ptr<Operation<CompPolicy>> oper) {_statements.push_back(oper);}

    void traverseInOrder(
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> callable,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> interCallable,
            std::function<bool(std::shared_ptr<Operation<CompPolicy>>)> operationPredicate,
            std::shared_ptr<Operation<CompPolicy>> sharedThisPointer,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> setup = nullptr,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> cleanup = nullptr) {
        if(operationPredicate(sharedThisPointer)) {
            if(setup)
                setup(sharedThisPointer);
            callable(sharedThisPointer);
            typedef typename std::vector<std::shared_ptr<Operation<CompPolicy>>>::iterator iterType;
            iterType from = _statements.begin();
            iterType til = _statements.end();
            while(from != til) {
                if(interCallable)
                    interCallable(sharedThisPointer);
                (*from)->traverseInOrder(callable, interCallable, operationPredicate, *from, setup, cleanup);
                from++;
            }
            if(cleanup)
                cleanup(sharedThisPointer);
        }
    }

    void traversePostOrder(std::function<void(std::shared_ptr<Operation<CompPolicy>>)> callable,
                           std::function<void(std::shared_ptr<Operation<CompPolicy>>)> interCallable,
                           std::function<bool(std::shared_ptr<Operation<CompPolicy>>)> operationPredicate,
                           std::shared_ptr<Operation<CompPolicy>> sharedThisPointer,
                           std::function<void(std::shared_ptr<Operation<CompPolicy>>)> setup = nullptr,
                           std::function<void(std::shared_ptr<Operation<CompPolicy>>)> cleanup = nullptr) {
        // Loops should be traverse inorder. For now at least
        //traverseInOrder(callable, interCallable, sharedThisPointer);
        if(operationPredicate(sharedThisPointer)) {
            if(setup)
                setup(sharedThisPointer);
            callable(sharedThisPointer);
            typedef typename std::vector<std::shared_ptr<Operation<CompPolicy>>>::iterator iterType;
            iterType from = _statements.begin();
            iterType til = _statements.end();
            while(from != til) {
                if(interCallable)
                    interCallable(sharedThisPointer);
                (*from)->traversePostOrder(callable, interCallable, operationPredicate, *from, setup, cleanup);
                from++;
            }
            if(cleanup)
                cleanup(sharedThisPointer);
        }
    }

    void traversePreOrder(
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> callable,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> interCallable,
            std::function<bool(std::shared_ptr<Operation<CompPolicy>>)> operationPredicate,
            std::shared_ptr<Operation<CompPolicy>> sharedThisPointer,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> setup = nullptr,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> cleanup = nullptr) {
        if(operationPredicate(sharedThisPointer)) {
            if(setup)
                setup(sharedThisPointer);
            callable(sharedThisPointer);
            typedef typename std::vector<std::shared_ptr<Operation<CompPolicy>>>::iterator iterType;
            iterType from = _statements.begin();
            iterType til = _statements.end();
            while(from != til) {
                if(interCallable)
                    interCallable(sharedThisPointer);
                (*from)->traversePreOrder(callable, interCallable, operationPredicate, *from, setup, cleanup);
                from++;
            }
            if(cleanup)
                cleanup(sharedThisPointer);
        }
    }

    void traverseChildren(
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> callable,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> interCallable,
            std::function<bool(std::shared_ptr<Operation<CompPolicy>>)> operationPredicate,
            std::shared_ptr<Operation<CompPolicy>> sharedThisPointer,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> setup = nullptr,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> cleanup = nullptr) {
        if(operationPredicate(sharedThisPointer)) {
            if(setup)
                setup(sharedThisPointer);
            typedef typename std::vector<std::shared_ptr<Operation<CompPolicy>>>::iterator iterType;
            iterType from = _statements.begin();
            iterType til = _statements.end();
            while(from != til) {
                (*from)->traversePostOrder(callable, interCallable, operationPredicate, *from, setup, cleanup);
                from++;
                if(interCallable)
                    interCallable(sharedThisPointer);
            }
            if(cleanup)
                cleanup(sharedThisPointer);
        }
    }

private:
    std::vector<std::shared_ptr<Operation<CompPolicy>>> _statements;
};

template<typename CompPolicy>
class FunctionOperation: public Operation<CompPolicy> {
public:
    FunctionOperation(typename Operation<CompPolicy>::OperationKind kind,
                      const std::string& functionName,
            const ParallelType& par, int numOper, OperandType* operTypes) :
            Operation<CompPolicy>(kind, OperationType::function, par, numOper,
                    operTypes), _functionName(functionName) {

    }
    virtual ~FunctionOperation(){}

    virtual std::string getMainStringLiteral() {
        return _functionName;
    }

    static bool classof(const Operation<CompPolicy>* baseOperation) {
        return baseOperation->getKind()
                >= Operation<CompPolicy>::OK_FunctionOperation
                && baseOperation->getKind()
                        <= Operation<CompPolicy>::OK_ClangFunctionWrapper;
    }

    std::vector<std::shared_ptr<Operation<CompPolicy>>>& statements() {return _statements;}

    void addStatement(std::shared_ptr<Operation<CompPolicy>> oper) {_statements.push_back(oper);}

    void traverseInOrder(
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> callable,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> interCallable,
            std::function<bool(std::shared_ptr<Operation<CompPolicy>>)> operationPredicate,
            std::shared_ptr<Operation<CompPolicy>> sharedThisPointer,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> setup = nullptr,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> cleanup = nullptr) {
        if(operationPredicate(sharedThisPointer)) {
            if(setup)
                setup(sharedThisPointer);
            callable(sharedThisPointer);
            typedef typename std::vector<std::shared_ptr<Operation<CompPolicy>>>::iterator iterType;
            iterType from = _statements.begin();
            iterType til = _statements.end();
            while(from != til) {
                if(interCallable)
                    interCallable(sharedThisPointer);
                (*from)->traverseInOrder(callable, interCallable, operationPredicate, *from, setup, cleanup);
                from++;
            }
            if(cleanup)
                cleanup(sharedThisPointer);
        }
    }

    void traversePostOrder(std::function<void(std::shared_ptr<Operation<CompPolicy>>)> callable,
                           std::function<void(std::shared_ptr<Operation<CompPolicy>>)> interCallable,
                           std::function<bool(std::shared_ptr<Operation<CompPolicy>>)> operationPredicate,
                           std::shared_ptr<Operation<CompPolicy>> sharedThisPointer,
                           std::function<void(std::shared_ptr<Operation<CompPolicy>>)> setup = nullptr,
                           std::function<void(std::shared_ptr<Operation<CompPolicy>>)> cleanup = nullptr) {
        // Loops should be traverse inorder. For now at least
        //traverseInOrder(callable, interCallable, sharedThisPointer);
        if(operationPredicate(sharedThisPointer)) {
            if(setup)
                setup(sharedThisPointer);
            callable(sharedThisPointer);
            typedef typename std::vector<std::shared_ptr<Operation<CompPolicy>>>::iterator iterType;
            iterType from = _statements.begin();
            iterType til = _statements.end();
            while(from != til) {
                if(interCallable)
                    interCallable(sharedThisPointer);
                (*from)->traversePostOrder(callable, interCallable, operationPredicate, *from, setup, cleanup);
                from++;
            }
            if(cleanup)
                cleanup(sharedThisPointer);
        }
    }

    void traversePreOrder(
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> callable,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> interCallable,
            std::function<bool(std::shared_ptr<Operation<CompPolicy>>)> operationPredicate,
            std::shared_ptr<Operation<CompPolicy>> sharedThisPointer,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> setup = nullptr,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> cleanup = nullptr) {
        if(operationPredicate(sharedThisPointer)) {
            if(setup)
                setup(sharedThisPointer);
            callable(sharedThisPointer);
            typedef typename std::vector<std::shared_ptr<Operation<CompPolicy>>>::iterator iterType;
            iterType from = _statements.begin();
            iterType til = _statements.end();
            while(from != til) {
                if(interCallable)
                    interCallable(sharedThisPointer);
                (*from)->traversePreOrder(callable, interCallable, operationPredicate, *from, setup, cleanup);
                from++;
            }
            if(cleanup)
                cleanup(sharedThisPointer);
        }
    }

    void traverseChildren(
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> callable,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> interCallable,
            std::function<bool(std::shared_ptr<Operation<CompPolicy>>)> operationPredicate,
            std::shared_ptr<Operation<CompPolicy>> sharedThisPointer,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> setup = nullptr,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> cleanup = nullptr) {

        if(operationPredicate(sharedThisPointer)) {
            if(setup)
                setup(sharedThisPointer);
            typedef typename std::vector<std::shared_ptr<Operation<CompPolicy>>>::iterator iterType;
            iterType from = _statements.begin();
            iterType til = _statements.end();
            while(from != til) {
                (*from)->traversePostOrder(callable, interCallable, operationPredicate, *from, setup, cleanup);
                from++;
                if(interCallable)
                    interCallable(sharedThisPointer);
            }
            if(cleanup)
                cleanup(sharedThisPointer);
        }
    }

    std::shared_ptr<Operation<CompPolicy>>& name() { return _functionName; }

private:
    std::string _functionName;
    std::vector<std::shared_ptr<Operation<CompPolicy>>> _statements;
};

template<typename CompPolicy>
class IfOperation: public Operation<CompPolicy> {
public:
    IfOperation(typename Operation<CompPolicy>::OperationKind kind,
            const ParallelType& par, int numOper, OperandType* operTypes) :
            Operation<CompPolicy>(kind, OperationType::ifCondition, par, numOper,
                    operTypes) {

    }
    virtual ~IfOperation(){}

    virtual std::string getMainStringLiteral() {
        return "if(" + _binaryCondition->getMainStringLiteral() + ")";
    }

    static bool classof(const Operation<CompPolicy>* baseOperation) {
        return baseOperation->getKind()
                >= Operation<CompPolicy>::OK_IfOperation
                && baseOperation->getKind()
                        <= Operation<CompPolicy>::OK_ClangIfWrapper;
    }

    //std::vector<std::shared_ptr<Operation<CompPolicy>>>& statements() {return _statements;}

    //void addStatement(std::shared_ptr<Operation<CompPolicy>> oper) {_statements.push_back(oper);}

    void traverseInOrder(
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> callable,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> interCallable,
            std::function<bool(std::shared_ptr<Operation<CompPolicy>>)> operationPredicate,
            std::shared_ptr<Operation<CompPolicy>> sharedThisPointer,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> setup = nullptr,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> cleanup = nullptr) {
        if(operationPredicate(sharedThisPointer)) {
            if(setup)
                setup(sharedThisPointer);
            callable(sharedThisPointer);
            if(interCallable)
                interCallable(sharedThisPointer);
            _then->traverseInOrder(callable, interCallable, operationPredicate, _then, setup, cleanup);
            if(_else) {
                if(interCallable)
                    interCallable(sharedThisPointer);
                if(_else)
                    _else->traverseInOrder(callable, interCallable, operationPredicate, _else, setup, cleanup);
            }
            if(cleanup)
                cleanup(sharedThisPointer);
        }
    }

    void traversePostOrder(std::function<void(std::shared_ptr<Operation<CompPolicy>>)> callable,
                           std::function<void(std::shared_ptr<Operation<CompPolicy>>)> interCallable,
                           std::function<bool(std::shared_ptr<Operation<CompPolicy>>)> operationPredicate,
                           std::shared_ptr<Operation<CompPolicy>> sharedThisPointer,
                           std::function<void(std::shared_ptr<Operation<CompPolicy>>)> setup = nullptr,
                           std::function<void(std::shared_ptr<Operation<CompPolicy>>)> cleanup = nullptr) {
        // Loops should be traverse inorder. For now at least
        //traverseInOrder(callable, interCallable, sharedThisPointer);
        if(operationPredicate(sharedThisPointer)) {
            if(setup)
                setup(sharedThisPointer);
            callable(sharedThisPointer);
            if(interCallable)
                interCallable(sharedThisPointer);
            _then->traversePostOrder(callable, interCallable, operationPredicate, _then, setup, cleanup);
            if(_else) {
                if(interCallable)
                    interCallable(sharedThisPointer);
                if(_else)
                    _else->traversePostOrder(callable, interCallable, operationPredicate, _else, setup, cleanup);
            }
            if(cleanup)
                cleanup(sharedThisPointer);
        }
    }

    void traversePreOrder(
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> callable,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> interCallable,
            std::function<bool(std::shared_ptr<Operation<CompPolicy>>)> operationPredicate,
            std::shared_ptr<Operation<CompPolicy>> sharedThisPointer,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> setup = nullptr,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> cleanup = nullptr) {
        if(operationPredicate(sharedThisPointer)) {
            if(setup)
                setup(sharedThisPointer);
            callable(sharedThisPointer);
            if(interCallable)
                interCallable(sharedThisPointer);
            _then->traversePreOrder(callable, interCallable, operationPredicate, _then, setup, cleanup);
            if(_else) {
                if(interCallable)
                    interCallable(sharedThisPointer);
                if(_else)
                    _else->traversePreOrder(callable, interCallable, operationPredicate, _else, setup, cleanup);
            }
            if(cleanup)
                cleanup(sharedThisPointer);
        }
    }

    void traverseChildren(
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> callable,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> interCallable,
            std::function<bool(std::shared_ptr<Operation<CompPolicy>>)> operationPredicate,
            std::shared_ptr<Operation<CompPolicy>> sharedThisPointer,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> setup = nullptr,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> cleanup = nullptr) {
        if(operationPredicate(sharedThisPointer)) {
            if(setup)
                setup(sharedThisPointer);
            _then->traversePostOrder(callable, interCallable, operationPredicate, _then, setup, cleanup);
            if(_else) {
                if(interCallable)
                    interCallable(sharedThisPointer);
                if(_else)
                    _else->traversePostOrder(callable, interCallable, operationPredicate, _else, setup, cleanup);
            }
            if(cleanup)
                cleanup(sharedThisPointer);
        }
    }

    std::shared_ptr<Operation<CompPolicy>>& thenPart() { return _then; }
    std::shared_ptr<Operation<CompPolicy>>& elsePart() { return _else; }
    std::shared_ptr<Operation<CompPolicy>>& binaryCondition() { return _binaryCondition; }

private:
    //std::string _functionName;
    //std::vector<std::shared_ptr<Operation<CompPolicy>>> _statements;
    std::shared_ptr<Operation<CompPolicy>> _binaryCondition;
    std::shared_ptr<Operation<CompPolicy>> _then;
    std::shared_ptr<Operation<CompPolicy>> _else;
};

template<typename CompPolicy>
class MemoryOperation: public Operation<CompPolicy> {
public:
    MemoryOperation(typename Operation<CompPolicy>::OperationKind kind,
			const AccessType& access, const std::string& variableLiteral,
			const std::string& indexLiteral, const OperationType& op,
			const ParallelType& par, int numOper, OperandType* operTypes) :
            Operation<CompPolicy>(kind, op, par, numOper, operTypes), _accessType(
					access), _indexLiteral(indexLiteral), _variableLiteral(
					variableLiteral) {

	}
    virtual ~MemoryOperation(){}

    static bool classof(const Operation<CompPolicy>* baseOperation) {
		return baseOperation->getKind()
                >= Operation<CompPolicy>::OK_MemoryOperation
				&& baseOperation->getKind()
                        <= Operation<CompPolicy>::OK_ClangSubscriptWrapper;
	}

    virtual AccessType& getAccessType() {
        return _accessType;
    }

    virtual std::string getMainStringLiteral() {
		return _variableLiteral + "[" + _indexLiteral + "]";
	}

    std::shared_ptr<Operation<CompPolicy>>& lhsVariable() {
		return _lhsVariable;
	}
    std::shared_ptr<Operation<CompPolicy>>& rhsIndex() {
		return _rhsIndex;
	}

	void traverseInOrder(
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> callable,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> interCallable,
            std::function<bool(std::shared_ptr<Operation<CompPolicy>>)> operationPredicate,
            std::shared_ptr<Operation<CompPolicy>> sharedThisPointer,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> setup = nullptr,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> cleanup = nullptr) {
        if(operationPredicate(sharedThisPointer)) {
            if(setup)
                setup(sharedThisPointer);
            _lhsVariable->traverseInOrder(callable, interCallable, operationPredicate, _lhsVariable, setup, cleanup);
            if (interCallable)
                interCallable(sharedThisPointer);
            callable(sharedThisPointer);
            if (interCallable)
                interCallable(sharedThisPointer);
            _rhsIndex->traverseInOrder(callable, interCallable, operationPredicate, _rhsIndex, setup, cleanup);
            if(cleanup)
                cleanup(sharedThisPointer);
        }
	}

    void traversePostOrder(
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> callable,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> interCallable,
            std::function<bool(std::shared_ptr<Operation<CompPolicy>>)> operationPredicate,
            std::shared_ptr<Operation<CompPolicy>> sharedThisPointer,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> setup = nullptr,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> cleanup = nullptr) {
        if(operationPredicate(sharedThisPointer)) {
            if(setup)
                setup(sharedThisPointer);
            _rhsIndex->traverseInOrder(callable, interCallable, operationPredicate, _rhsIndex, setup, cleanup);
            if (interCallable)
                interCallable(sharedThisPointer);
            _lhsVariable->traverseInOrder(callable, interCallable, operationPredicate, _lhsVariable, setup, cleanup);
            if (interCallable)
                interCallable(sharedThisPointer);
            callable(sharedThisPointer);
            if(cleanup)
                cleanup(sharedThisPointer);
        }
	}

    void traversePreOrder(
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> callable,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> interCallable,
            std::function<bool(std::shared_ptr<Operation<CompPolicy>>)> operationPredicate,
            std::shared_ptr<Operation<CompPolicy>> sharedThisPointer,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> setup = nullptr,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> cleanup = nullptr) {
        if(operationPredicate(sharedThisPointer)) {
            if(setup)
                setup(sharedThisPointer);
            callable(sharedThisPointer);
            if (interCallable)
                interCallable(sharedThisPointer);
            _rhsIndex->traversePreOrder(callable, interCallable, operationPredicate, _rhsIndex, setup, cleanup);
            if (interCallable)
                interCallable(sharedThisPointer);
            _lhsVariable->traversePreOrder(callable, interCallable, operationPredicate, _lhsVariable, setup, cleanup);
            if(cleanup)
                cleanup(sharedThisPointer);
        }
    }

    void traverseChildren(
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> callable,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> interCallable,
            std::function<bool(std::shared_ptr<Operation<CompPolicy>>)> operationPredicate,
            std::shared_ptr<Operation<CompPolicy>> sharedThisPointer,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> setup = nullptr,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> cleanup = nullptr) {
        if(operationPredicate(sharedThisPointer)) {
            if(setup)
                setup(sharedThisPointer);
            _rhsIndex->traverseChildren(callable, interCallable, operationPredicate, _rhsIndex, setup, cleanup);
            if (interCallable)
                interCallable(sharedThisPointer);
            _lhsVariable->traverseChildren(callable, interCallable, operationPredicate, _lhsVariable, setup, cleanup);
            if(cleanup)
                cleanup(sharedThisPointer);
        }
    }

	std::string& variable() {
		return _variableLiteral;
	}
	std::string& index() {
		return _indexLiteral;
	}
private:
	AccessType _accessType;
	std::string _indexLiteral;
	std::string _variableLiteral;
    std::shared_ptr<Operation<CompPolicy>> _lhsVariable;
    std::shared_ptr<Operation<CompPolicy>> _rhsIndex;
};


template<typename CompPolicy>
class VariableDeclOperation: public Operation<CompPolicy> {
public:
    VariableDeclOperation(
            typename Operation<CompPolicy>::OperationKind kind,
            const std::string& variableLiteral, const OperationType& op,
            const ParallelType& par, int numOper, OperandType* operTypes) :
            Operation<CompPolicy>(kind, op, par, numOper, operTypes), _variableLiteral(
                    variableLiteral) {

    }

    virtual ~VariableDeclOperation(){ //std::cout << "Destroyed VariableDeclarationOperation at : " << this << std::endl;
                                      spdlog::get("sopelogger")->debug("Destroyed VariableDeclarationOperation at : {}",(void*) this);
                                    }

    static bool classof(const Operation<CompPolicy>* baseOperation) {
        return baseOperation->getKind()
                >= Operation<CompPolicy>::OK_VariableDeclOperation
                && baseOperation->getKind()
                        <= Operation<CompPolicy>::OK_ClangVariableDeclaration;
    }

    virtual std::string getMainStringLiteral() {
        return _variableLiteral + " = " + (_init ? init()->getMainStringLiteral() : "uninitialized");// + _initExpression;
    }

    void traverseInOrder(
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> callable,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> interCallable,
            std::function<bool(std::shared_ptr<Operation<CompPolicy>>)> operationPredicate,
            std::shared_ptr<Operation<CompPolicy>> sharedThisPointer,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> setup = nullptr,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> cleanup = nullptr) {
        if(operationPredicate(sharedThisPointer)) {
            if(setup)
                setup(sharedThisPointer);
            callable(sharedThisPointer);
            if (interCallable)
                interCallable(sharedThisPointer);
            if(_init) // varDecl could be uninitialized.
                _init->traverseInOrder(callable, interCallable, operationPredicate, _init, setup, cleanup);
            if(cleanup)
                cleanup(sharedThisPointer);
        }
    }

    void traversePostOrder(
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> callable,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> interCallable,
            std::function<bool(std::shared_ptr<Operation<CompPolicy>>)> operationPredicate,
            std::shared_ptr<Operation<CompPolicy>> sharedThisPointer,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> setup = nullptr,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> cleanup = nullptr) {
        if(operationPredicate(sharedThisPointer)) {
            if(setup)
                setup(sharedThisPointer);
            //traverseInOrder(callable, interCallable, operationPredicate, sharedThisPointer);
            callable(sharedThisPointer);
            if (interCallable)
                interCallable(sharedThisPointer);
            if(_init) // varDecl could be uninitialized.
                _init->traversePostOrder(callable, interCallable, operationPredicate, _init, setup, cleanup);
            if(cleanup)
                cleanup(sharedThisPointer);
        }
    }


    void traversePreOrder(
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> callable,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> interCallable,
            std::function<bool(std::shared_ptr<Operation<CompPolicy>>)> operationPredicate,
            std::shared_ptr<Operation<CompPolicy>> sharedThisPointer,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> setup = nullptr,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> cleanup = nullptr) {
        if(operationPredicate(sharedThisPointer)) {
            if(setup)
                setup(sharedThisPointer);
            //traverseInOrder(callable, interCallable, operationPredicate, sharedThisPointer);
            callable(sharedThisPointer);
            if (interCallable)
                interCallable(sharedThisPointer);
            if(_init) // varDecl could be uninitialized.
                _init->traversePreOrder(callable, interCallable, operationPredicate, _init, setup, cleanup);
            if(cleanup)
                cleanup(sharedThisPointer);
        }
    }

    void traverseChildren(
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> callable,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> interCallable,
            std::function<bool(std::shared_ptr<Operation<CompPolicy>>)> operationPredicate,
            std::shared_ptr<Operation<CompPolicy>> sharedThisPointer,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> setup = nullptr,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> cleanup = nullptr) {
        if(operationPredicate(sharedThisPointer)) {
            if(setup)
                setup(sharedThisPointer);
            //traverseInOrder(callable, interCallable, operationPredicate, sharedThisPointer);
            callable(sharedThisPointer);
            if (interCallable)
                interCallable(sharedThisPointer);
            if(_init) // varDecl could be uninitialized.
                _init->traversePostOrder(callable, interCallable, operationPredicate, _init, setup, cleanup);
            if(cleanup)
                cleanup(sharedThisPointer);
        }
    }

    std::string& variableLiteral() {
        return _variableLiteral;
    }

    std::string& initExpression() {
        return _initExpression;
    }

    std::shared_ptr<Operation<CompPolicy>>& init() {
        return _init;
    }
private:
    std::string _variableLiteral;
    std::string _initExpression;
    std::shared_ptr<Operation<CompPolicy>> _init;
};


template<typename CompPolicy>
class VariableReferenceOperation: public Operation<CompPolicy> {
public:
	VariableReferenceOperation(
            typename Operation<CompPolicy>::OperationKind kind,
			const std::string& variableLiteral, const OperationType& op,
			const ParallelType& par, int numOper, OperandType* operTypes) :
            Operation<CompPolicy>(kind, op, par, numOper, operTypes), _variableLiteral(
					variableLiteral) {

	}

    virtual ~VariableReferenceOperation(){ //std::cout << "Destroyed VariableReferenceOperation at : " << this << std::endl;
                                           spdlog::get("sopelogger")->debug("Destroyed VariableReferenceOperation at : {}",(void*) this);
                                         }

    static bool classof(const Operation<CompPolicy>* baseOperation) {
		return baseOperation->getKind()
                >= Operation<CompPolicy>::OK_VariableReferenceOperation
				&& baseOperation->getKind()
                        <= Operation<CompPolicy>::OK_ClangVariableRefWrapper;
	}

    virtual std::string getMainStringLiteral() {
		return _variableLiteral;
	}

    void traverseInOrder(
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> callable,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> interCallable,
            std::function<bool(std::shared_ptr<Operation<CompPolicy>>)> operationPredicate,
            std::shared_ptr<Operation<CompPolicy>> sharedThisPointer,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> setup = nullptr,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> cleanup = nullptr) {
        if(operationPredicate(sharedThisPointer)) {
            // We don't usualy need to setup or cleanup before/after declRefs
            // So we avoid the overhead of checking is the setup and cleanup
            // functions are nullptrs and the calls to setup and cleanup in
            // user code
            callable(sharedThisPointer);
        }
    }

    void traversePostOrder(
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> callable,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> interCallable,
            std::function<bool(std::shared_ptr<Operation<CompPolicy>>)> operationPredicate,
            std::shared_ptr<Operation<CompPolicy>> sharedThisPointer,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> setup = nullptr,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> cleanup = nullptr) {
        traverseInOrder(callable, interCallable, operationPredicate, sharedThisPointer, setup, cleanup);
    }


    void traversePreOrder(
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> callable,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> interCallable,
            std::function<bool(std::shared_ptr<Operation<CompPolicy>>)> operationPredicate,
            std::shared_ptr<Operation<CompPolicy>> sharedThisPointer,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> setup = nullptr,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> cleanup = nullptr) {
        traverseInOrder(callable, interCallable, operationPredicate, sharedThisPointer, setup, cleanup);
    }

    void traverseChildren(
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> callable,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> interCallable,
            std::function<bool(std::shared_ptr<Operation<CompPolicy>>)> operationPredicate,
            std::shared_ptr<Operation<CompPolicy>> sharedThisPointer,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> setup = nullptr,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> cleanup = nullptr) {
        traverseInOrder(callable, interCallable, operationPredicate, sharedThisPointer, setup, cleanup);
    }

    std::string& variableLiteral() {
		return _variableLiteral;
	}

    std::shared_ptr<Operation<CompPolicy>>& variableDeclaration() {
        return _variable;
    }
private:
	std::string _variableLiteral;
    std::shared_ptr<Operation<CompPolicy>> _variable;
};

template<typename CompPolicy>
class NumericLiteral: public Operation<CompPolicy> {
public:
    NumericLiteral(
            typename Operation<CompPolicy>::OperationKind kind,
            const std::string& numericLiteral,
            const ParallelType& par, int numOper, OperandType* operTypes) :
            Operation<CompPolicy>(kind, OperationType::numeric, par, numOper, operTypes), _numericLiteral(
                    numericLiteral) {

    }

    virtual ~NumericLiteral(){ //std::cout << "Destroyed NumericLiteral at : " << this << std::endl;
                               spdlog::get("sopelogger")->debug("Destroyed NumericLiteral at : {}", (void*)this);
                             }

    static bool classof(const Operation<CompPolicy>* baseOperation) {
        return baseOperation->getKind()
                >= Operation<CompPolicy>::OK_NumericLiteral
                && baseOperation->getKind()
                        <= Operation<CompPolicy>::OK_ClangFloatLiteral;
    }

    virtual std::string getMainStringLiteral() {
        return _numericLiteral;
    }

    void traverseInOrder(
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> callable,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> interCallable,
            std::function<bool(std::shared_ptr<Operation<CompPolicy>>)> operationPredicate,
            std::shared_ptr<Operation<CompPolicy>> sharedThisPointer,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> setup = nullptr,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> cleanup = nullptr) {
        if(operationPredicate(sharedThisPointer)) {
            callable(sharedThisPointer);
        }
    }

    void traversePostOrder(
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> callable,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> interCallable,
            std::function<bool(std::shared_ptr<Operation<CompPolicy>>)> operationPredicate,
            std::shared_ptr<Operation<CompPolicy>> sharedThisPointer,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> setup = nullptr,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> cleanup = nullptr) {
        traverseInOrder(callable, interCallable, operationPredicate, sharedThisPointer, setup, cleanup);
    }


    void traversePreOrder(
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> callable,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> interCallable,
            std::function<bool(std::shared_ptr<Operation<CompPolicy>>)> operationPredicate,
            std::shared_ptr<Operation<CompPolicy>> sharedThisPointer,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> setup = nullptr,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> cleanup = nullptr) {
        traverseInOrder(callable, interCallable, operationPredicate, sharedThisPointer, setup, cleanup);
    }

    void traverseChildren(
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> callable,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> interCallable,
            std::function<bool(std::shared_ptr<Operation<CompPolicy>>)> operationPredicate,
            std::shared_ptr<Operation<CompPolicy>> sharedThisPointer,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> setup = nullptr,
            std::function<void(std::shared_ptr<Operation<CompPolicy>>)> cleanup = nullptr) {
        traverseInOrder(callable, interCallable, operationPredicate, sharedThisPointer, setup, cleanup);
    }

    std::string& numericLiteral() {
        return _numericLiteral;
    }

    std::shared_ptr<Operation<CompPolicy>>& numeric() {
        return _numeric;
    }
private:
    std::string _numericLiteral;
    std::shared_ptr<Operation<CompPolicy>> _numeric;
};

//template<typename policy, typename... CompPolicy>
//inline bool Operation<CompPolicy...>::operator==(const Operation<CompPolicy...>& op) {
//    return policy().isEqual(*this, op);
//}
//template<typename policy, typename... policies, typename... CompPolicy>
//inline bool Operation<CompPolicy...>::operator==(const Operation<CompPolicy...>& op) {
    //return CompPolicy().isEqual(*this, op);
    //return policy().isEqual(*this, op) && operator==(op)<policies, CompPolicy>;
    //return doCompare(op);
    //return isEqual(op, CompPolicy... policies);
//}

/*
 template<typename...CompPolicy>
 template<typename AComparePolicy>
 inline bool Operation<CompPolicy...>::isEqual(const Operation<CompPolicy...>& op, AComparePolicy policy, CompPolicy... policies ) {
 return policy().isEqual(*this, op) && isEqual(op, policies...);
 }*/
template<typename T>
class Sequence;

template<typename T> //, typename ComparePolicy>   // this compare policy should probably be associated with T rather than Pattern<T> because we will compare instances of T rather than instances of Pattern
class Pattern {
	friend class Sequence<T>;
public:
	using iteratorType = typename std::vector<std::shared_ptr<T>>::iterator;
public:
	Pattern() {	}
	Pattern(const Pattern<T>& p); // copy ctr will be used to create child patterns from the parent by removing some operations fromthe parent later
	int size() const;
	void addToPattern(std::shared_ptr<T> sptr);
	//std::shared_ptr<T> removeFromPattern(int index);
	Pattern<T> subPattern(int start, int last) const;
	std::shared_ptr<T> at(int index) const;
	iteratorType begin() { return _patterns.begin(); }
	iteratorType end() { return _patterns.end(); }
	iteratorType erase(iteratorType first, iteratorType last) { return _patterns.erase(first, last); }
/*	iteratorType operator++() { return ++_it; }
	iteratorType operator--() { return --_it; }*/

private:
	std::vector<std::shared_ptr<T>> _patterns;
	//iteratorType _it;
};

template<typename T>
Pattern<T>::Pattern(const Pattern<T>& p) :
		_patterns(p._patterns) {
}

template<typename T>
int Pattern<T>::size() const {
	return _patterns.size();
}

template<typename T>
void Pattern<T>::addToPattern(std::shared_ptr<T> sptr) {
	_patterns.push_back(sptr);
}

template<typename T>
Pattern<T> Pattern<T>::subPattern(int start, int last) const {
	Pattern<T> newPattern;
	typedef typename std::vector<std::shared_ptr<T>>::const_iterator iteratorType;
	iteratorType startElement = _patterns.begin() + start;
	iteratorType lastElement = _patterns.begin() + last + 1; // add 1 to the iterator since the copy will copy from start to enditerator-1
	newPattern._patterns = std::vector < std::shared_ptr
			< T >> (startElement, lastElement);
	return newPattern;
}

template<typename T>
std::shared_ptr<T> Pattern<T>::at(int index) const {
	return _patterns[index];
}

template<typename T>
class Sequence {
public:
	Sequence() {}
	Sequence(const Pattern<T> pattern); // promotion constructor from pattern
	int size() const;
	void addToSequence(std::shared_ptr<T> sptr);
	std::shared_ptr<T> at(int index) const;
private:
	std::vector<std::shared_ptr<T>> _sequence;
	std::map<std::shared_ptr<Platform>, std::vector<int>> _performanceMetrics; //key is a ptr to platform, value is an int (p.s. the index into the vector is the id of the metric)
};

template<typename T>
Sequence<T>::Sequence(const Pattern<T> pattern) {
	_sequence = pattern._patterns;
}

template<typename T>
int Sequence<T>::size() const {
	return _sequence.size();
}

template<typename T>
void Sequence<T>::addToSequence(std::shared_ptr<T> sptr) {
	_sequence.push_back(sptr);
}

template<typename T>
std::shared_ptr<T> Sequence<T>::at(int index) const {
	return _sequence[index];
}

struct SearchOutput {
	int matchIndex; // The index of the full match
	int partialMatchIndex; // the index where the longest match starts
	int partialMatchLongestMatch; // the "largest number of operations from the pattern" that were matched in the sequence
	friend std::ostream& operator<<(std::ostream& stream,
			const SearchOutput& searchOutput);
    //friend llvm::raw_ostream& operator<<(llvm::raw_ostream& stream,
    //        const SearchOutput& searchOutput);
};

std::ostream& printPartOfResult(std::ostream& stream, const std::string& label,
		int value, int w = 3, const std::string& seperator = " | ") {
	stream << seperator << label << " = " << std::setw(w) << std::right
			<< value;
	return stream;
}

std::ostream& operator<<(std::ostream& stream,
		const SearchOutput& searchOutput) {
	stream << "Match : ";
	printPartOfResult(stream, "MatchIndex", searchOutput.matchIndex);
	printPartOfResult(stream, "PartialMatchIndex",
			searchOutput.partialMatchIndex);
	printPartOfResult(stream, "PartialMatchLength",
			searchOutput.partialMatchLongestMatch);
	return stream;
}

/*llvm::raw_ostream& printPartOfResult(llvm::raw_ostream& stream, const std::string& label,
        int value, int w = 3, const std::string& seperator = " | ") {
    stream << seperator << label << " = " << value;
    return stream;
}

llvm::raw_ostream& operator<<(llvm::raw_ostream& stream,
        const SearchOutput& searchOutput) {
    stream << "Match : ";
    printPartOfResult(stream, "MatchIndex", searchOutput.matchIndex);
    printPartOfResult(stream, "PartialMatchIndex",
            searchOutput.partialMatchIndex);
    printPartOfResult(stream, "PartialMatchLength",
            searchOutput.partialMatchLongestMatch);
    return stream;
}
*/

template<typename T>
class KMP {
public:
	KMP() = delete;
    ~KMP();
    KMP(const Pattern<T>& p);
	static void buildMismatchTable(const Pattern<T>& p, int* table);
	void buildMismatchTable() {
        KMP<T>::buildMismatchTable(this->_p, this->_table);
	}
	// we need the *& because partialMatches was not initialized. If we just use int* partialMatches,
	// then we will get a copy of a zero pointer and we will instantiate that but the
	// original partialMatches will still be a zero pointer
	static SearchOutput search(const Sequence<T>& s, const Pattern<T>& p,
			const int* table, int*& partialMatches);
	SearchOutput search(const Sequence<T>& s) {
        return KMP<T>::search(s, this->_p, this->_table, this->_partialMatches);
	}
	;
	const int* getPartialMatches() {
		return _partialMatches;
	}
	;

private:
	Pattern<T> _p; // could also be T[] _pattern;
	int* _table;
	int* _partialMatches;
};

//KMP::KMP() {}
template<typename T>
KMP<T>::~KMP() {
    delete[] _table;
    delete[] _partialMatches; // initialized in the search function
}

template<typename T>
KMP<T>::KMP(const Pattern<T>& p) :
		_p(p), _table(new int[p.size()]) {
	buildMismatchTable();
}

// Return a zero-based position in the sequence at which the pattern word is found
template<typename T>
SearchOutput KMP<T>::search(const Sequence<T>& s, const Pattern<T>& p,
		const int* table, int*& partialMatches) { //TODO: make sure this const expresison makes the array const and not the pointer
	int m = 0; // the beginning of the current match in the input sequence
	int i = 0; // the position of the current character in the pattern word
	//partialMatches[s.size()] =  {0}; // This didnt really initialize the array to all 0s
	partialMatches = new int[s.size()];
	std::fill_n(partialMatches, s.size(), 0); // This works for initialization to 0

	SearchOutput so;
	so.matchIndex = -1;
	so.partialMatchIndex = -1;
	so.partialMatchLongestMatch = 0;
	while (m + i < s.size()) {
        //if (*(p.at(i)) == *(s.at(m + i))) {
        if ((*(p.at(i))).isEqual(*(s.at(m + i)))) {
			if (i == p.size() - 1) {
				//return m;
				so.matchIndex = m;
				so.partialMatchIndex = -1;
				so.partialMatchLongestMatch = 0;
				return so;
			}
			i += 1;
		} else {
			if (table[i] > -1) {
				partialMatches[m] = i; // Save the partial match by saving the length of the match which starts at m in the sequence
				if (i > so.partialMatchLongestMatch) { // if the current length of the match exceeds the longest match, update the longest match length and index
					so.partialMatchIndex = m;
					so.partialMatchLongestMatch = i;
				}
				m = m + i - table[i];
				i = table[i];
			} else {
				partialMatches[m] = i; // Save the partial match by saving the length of the match which starts at m in the sequence
				if (i > so.partialMatchLongestMatch) { // if the current length of the match exceeds the longest match, update the longest match length and index
					so.partialMatchIndex = m;
					so.partialMatchLongestMatch = i;
				}
				i = 0;
				m = m + 1;
			}
		}
	}
	//return -1; // we reach here if the whole sequence was searched without any hit.
	return so;
}

template<typename T>
void KMP<T>::buildMismatchTable(const Pattern<T>& p, int* table) {
// Using wikipedia pseudo code to implement this. We could also use others
// which mostly differ at the positions they start from and the initial value
// of T[0]
	int pos = 2; // the current position we are computing in T
	int cnd = 0; // the zero-based index in W of the next character in the current candidate substring

	table[0] = -1;
	table[1] = 0;

	while (pos < p.size()) {
		// first case, the substring continues
        //if (*(p.at(pos - 1)) == *(p.at(cnd))) {
        if ((*(p.at(pos - 1))).isEqual(*(p.at(cnd)))) {
			cnd += 1;
			table[pos] = cnd;
			pos += 1;
		}
		//second case, it doesnt
		else if (cnd > 0) {
			cnd = table[cnd];
		}
		// third case, we run out of candidates. cnd=0;
		else {
			table[pos] = 0;
			pos += 1;
		}
	}

}


struct PlatformProperties {
public:
	//OperationType supportedOps[];
	//OperationType insignificantOps[];
	//int TPI[]; // time per instruction in nanosec;
	//float EPI[]; // Energy per instruction in nano Joules;
};

struct Platform {
public:
	std::string name;
	std::string manufacturer;
	int identifier;
	PlatformProperties properties;
};

}
}
#endif /* OPERATIONS_H_ */
