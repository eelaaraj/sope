#ifndef PASS1PROCESSOR
#define PASS1PROCESSOR

#include <map>
#include "clang/AST/RecursiveASTVisitor.h"
#include "../utils/TypeDefinitions.h"
#include "../ClangOperationWrappers.h"
#include "../Comparator.h"
#include "../utils/OperationHelpers.h"
#include "../PatternPostProcessor.h"
#include "../PatternTreeCreator.h"
#include "../proc/IASTProcessor.h"
#include "../utils/Utils.h"
#include "../ParseState.h"
#include "../Injector.h"
#include <spdlog/spdlog.h>

using namespace clang;
using namespace clang::tooling;
using namespace pattern::matching::wrapper;
using namespace pattern::matching;

/*
 * This processor processes the first pass in the AST traversal. The 1st pass
 * looks for all function calls whther they are callExpr or cxxMemberCallExpr
 * or operatorCallExpr and adds them to the function names that are provided
 * as arguments to this pass. Thus the initial function names will indicate which
 * functions need to be inspected to find function calls in them. The found calls
 * will be added to that same function vector. After pass 1 we will have all function
 * calls in the function vector which is in turn passed on to pass 2. Pass 2 will use
 * that vector to look up for functions to traverse and parse. This way the functions
 * that were initally requested by the developer PLUS any functions they call will be
 * inspected in pass 2.
 *
 */

class Pass1Processor : public IASTProcessor
{
public:
    Pass1Processor(std::shared_ptr<ParseState> pState, const std::shared_ptr<IASTProcessor>& selfPointer);// : parseState(pState) { /*processorStack.push(this);*/}
    ~Pass1Processor() {
        if(parseState->assignmentOperChildren)
            delete parseState->assignmentOperChildren;
        if(parseState->functionParentMap)
            delete parseState->functionParentMap;
        //std::cout << "destroying Pass1 Processor at "<< this << "\n";
        spdlog::get("sopelogger")->debug("destroying Pass1 Processor at {}", (void*) this);
    }

    bool processCallExpr(CallExpr* callExpr);
    bool processFunctionDecl(FunctionDecl* functionDecl); // sets the scope to 1
    void processNode(std::shared_ptr<IASTProcessor> parentWithCallback);
    void callBack(std::shared_ptr<operation> childCallerOperation);
    void setSelfSharedPointer(std::shared_ptr<IASTProcessor> selfPointer);
private:
    void resetStateVariables();
private:
    std::shared_ptr<ParseState> parseState;
    // We pass in a reference of a shared pointer because we don't want to increment the
    // use_count() of the shared_ptr. If we pass in the shared_ptr by value, then the
    // use_count will be 2 instantaneouly (1 by the caller who created the ClangASTProcessor,
    // and 1 for the self pointer). This will in turn prevent the shared_ptr from destroying
    // the instance because the count will never reach 0. It will decrement once when the
    // caller class gets destroyed, but that's it. The clangASTprocessor will keep a self-pointer
    // to itself and will never get destroyed.
    std::weak_ptr<IASTProcessor> selfSharedPointer;
};


Pass1Processor::Pass1Processor(std::shared_ptr<ParseState> pState, const std::shared_ptr<IASTProcessor>& selfPointer) : parseState(pState), selfSharedPointer(selfPointer) { }

void Pass1Processor::setSelfSharedPointer(std::shared_ptr<IASTProcessor> selfPointer) {
    selfSharedPointer = selfPointer;
}


void Pass1Processor::resetStateVariables() {
    // Shouldn't reset "function" variable because we check it in the else statement in the VisitFuncDecl
    parseState->function = nullptr;
    parseState->numScopes = 1; // The function scope is 1
    parseState->loop = nullptr;
    if(parseState->functionParentMap) {
        //std::cout << "Before deleting parent map\n";
        spdlog::get("sopelogger")->debug("Before deleting parent map");
        delete parseState->functionParentMap;
        //std::cout << "After deleting parent map\n";
        spdlog::get("sopelogger")->debug("After deleting parent map");
        parseState->functionParentMap = nullptr;
    }
    parseState->currentParent = nullptr;
    parseState->isForLoopInit = true;
    parseState->isForLoopCondition = false;
    parseState->isForLoopIncrement = false;
    parseState->isNewAssignmentStmt = true;
    parseState->isUnaryOperation = false;
    parseState->currentAssignmentNode = nullptr;
    parseState->assignmentLHS = nullptr;
    parseState->assignmentRHS = nullptr;
    if (parseState->assignmentOperChildren) {
      delete parseState->assignmentOperChildren;
      parseState->assignmentOperChildren = nullptr;
    }
    parseState->pattern = Pattern<operation>();
    // These should be empty. If they're not, then there's something wrong usually.
    if(!parseState->clangOperationStack.empty() || !parseState->clangOperationWrapperStack.empty() || !parseState->processedOperationStack.empty() || !parseState->operationParentStack.empty() || !parseState->operationOnlyStack.empty()) {
        std::cerr << "One of the stacks was not empty upon reset" << std::endl;
    }
    while(!parseState->clangOperationStack.empty())
        parseState->clangOperationStack.pop();
    while(!parseState->clangOperationWrapperStack.empty())
        parseState->clangOperationWrapperStack.pop();
    while(!parseState->processedOperationStack.empty())
        parseState->processedOperationStack.pop();
    while(!parseState->operationParentStack.empty())
        parseState->operationParentStack.pop();
    while(!parseState->operationOnlyStack.empty())
        parseState->operationOnlyStack.pop();
}

bool Pass1Processor::processFunctionDecl(FunctionDecl* functionDecl) {
    // resets all variables including "function" variable. We don't that variable anymore
    // since we already finalized processing in the previous check.
    resetStateVariables();
    assert(parseState);

    if (!parseState->functionNames.empty() && std::find(parseState->functionNames.begin(), parseState->functionNames.end(), functionDecl->getNameAsString()) != parseState->functionNames.end()) {
        std::cout << "Visiting function = " << functionDecl->getNameAsString() << std::endl;
        parseState->function = functionDecl;
    }
    else {
        parseState->function = nullptr;
    }
    return true;
}

bool Pass1Processor::processCallExpr(CallExpr* callExpr) {
    if(parseState->function) {
        std::string funcDeclName = callExpr->getDirectCallee()->getNameAsString();
        // If the called function is not already in the vector (in the case the Class user passed it as CL argument), add it
        if(std::find(parseState->functionNames.begin(), parseState->functionNames.end(), funcDeclName) == parseState->functionNames.end())
            parseState->functionNames.push_back(funcDeclName);
    }
    return true;
}


void Pass1Processor::processNode(std::shared_ptr<IASTProcessor> parentWithCallback) {

}


void Pass1Processor::callBack(std::shared_ptr<operation> childCallerOperation) {

}

#endif // PASS1PROCESSOR

