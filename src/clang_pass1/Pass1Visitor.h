#ifndef PASS1VISITOR
#define PASS1VISITOR

#include <string>
#include <vector>
#include <iostream>
#include "clang/AST/ASTConsumer.h"
#include "clang/AST/RecursiveASTVisitor.h"
#include "clang/Frontend/CompilerInstance.h"
#include "clang/Frontend/FrontendAction.h"
#include "clang/Tooling/Tooling.h"
#include "clang/Tooling/Refactoring.h"
#include "clang/Tooling/CommonOptionsParser.h"
#include "clang/Rewrite/Frontend/Rewriters.h"
#include "clang/Rewrite/Core/Rewriter.h"
#include "../ClangOperationWrappers.h"
#include "../utils/OperationHelpers.h"
#include "../Repository.h"
#include "../utils/Utils.h"
#include "Pass1Processor.h"
#include <stack>
#include <spdlog/spdlog.h>

template<typename OperationTypename>
class FindCallsVisitor
        : public RecursiveASTVisitor<FindCallsVisitor<OperationTypename>> {
public:
    explicit FindCallsVisitor(ASTContext *Context, std::shared_ptr<Repository<OperationTypename>> repository, const std::string& fName, std::vector<std::string>& funcNames, bool injection)
        : functions(funcNames) {
        parseState = std::shared_ptr<ParseState>(new ParseState());
        parseState->Context = Context;
        parseState->repo = repository;
        parseState->fileName = fName;
        parseState->functionNames = funcNames;
        parseState->replacements = nullptr;
        parseState->inject = injection;
        parseState->function = nullptr;
        parseState->loop = nullptr;
        parseState->currentAssignmentNode = nullptr;
        parseState->assignmentLHS = nullptr;
        parseState->assignmentRHS = nullptr;
        parseState->assignmentOperChildren = nullptr;
        parseState->functionParentMap = nullptr;
        parseState->currentParent = nullptr;
        std::shared_ptr<Pass1Processor> clASTProcessor = std::shared_ptr<Pass1Processor>(new Pass1Processor(parseState, astProcessor));

        astProcessor = clASTProcessor;
        // We need to set the self pointer here. It does not work the way we do it in the constructor unless we pass
        // as reference to the astProcessor shared_ptr. If we just pass the shared_ptr by value, then the weak_ptr
        // in clangASTProcessor will be set to the initial address value of astProcessor shared_ptr which is nullptr.
        clASTProcessor->setSelfSharedPointer(astProcessor);

        std::cout << "Repository use count visitor = " << repository.use_count() << "\n";
    }

    virtual ~FindCallsVisitor() {
        std::cout << "Destroying Call visitor" <<"\n";
        spdlog::get("sopelogger")->debug("Destroying Call visitor");
        functions.swap(parseState->functionNames);
    }

    bool VisitCXXRecordDecl(CXXRecordDecl *Declaration) {
        if(parseState->Context->getSourceManager().isInMainFile(Declaration->getLocation()))
        {
            FullSourceLoc FullLocation = parseState->Context->getFullLoc(Declaration->getLocStart());
            if (FullLocation.isValid())
                std::cout << "Found declaration at "
                          << FullLocation.printToString(parseState->Context->getSourceManager()) << "\n ";
        }
        return true;
    }

    bool VisitCallExpr(CallExpr* callExpr) {
        if(parseState->function) {
            std::cout << "Found call expr = " << callExpr->getDirectCallee()->getNameAsString() << std::endl;
            bool ret = astProcessor->processCallExpr(callExpr);
            RecursiveASTVisitor<FindCallsVisitor>::TraverseDecl(callExpr->getDirectCallee());
        }
        return true;
    }

    bool VisitFunctionDecl(FunctionDecl* functionDecl) {
        return astProcessor->processFunctionDecl(functionDecl);
    }

private:
    std::shared_ptr<ParseState> parseState;
    std::shared_ptr<Pass1Processor> astProcessor;
    std::vector<std::string>& functions;
};

template<typename OperationTypename>
class FindCallsConsumer : public clang::ASTConsumer {
public:
    explicit FindCallsConsumer(ASTContext *Context, std::shared_ptr<Repository<OperationTypename>> repo, const std::string& fileName, std::vector<std::string>& functionNames, bool inject)
        : Visitor(Context, repo, fileName, functionNames, inject)  {
        //std::cout << "Repository use count consumer= " << repo.use_count() << "\n";
    }

    virtual ~FindCallsConsumer() {}

    virtual void HandleTranslationUnit(clang::ASTContext &Context) {
        Visitor.TraverseDecl(Context.getTranslationUnitDecl());
    }
private:
    FindCallsVisitor<OperationTypename> Visitor;
};

template<typename OperationTypename>
class FindCallsAction : public clang::ASTFrontendAction {
public:
    FindCallsAction(const std::string& fName, std::vector<std::string>& funcNames, std::shared_ptr<Repository<OperationTypename>> repo, bool injection) : fileName(fName), functionNames(funcNames), repository(repo), inject(injection){
        //std::cout << "Repository use count action= " << repository.use_count() << "\n";
    }
    virtual ~FindCallsAction() {
        //std::cout << "destroying FindNamedClassAction\n";
    }

    virtual std::unique_ptr<clang::ASTConsumer> CreateASTConsumer(
            clang::CompilerInstance &Compiler, llvm::StringRef InFile) {
        return std::unique_ptr<clang::ASTConsumer>(
                    new FindCallsConsumer<OperationTypename>(&Compiler.getASTContext(), repository, fileName, functionNames, inject));
    }
private:
    std::string fileName;
    std::vector<std::string>& functionNames;
    std::shared_ptr<Repository<OperationTypename>> repository;
    Replacements* replacements;
    bool inject;
};

template<typename OperationTypename>
class FindCallsActionFactory: public FrontendActionFactory {
public:
    FindCallsActionFactory(std::string fName, std::vector<std::string>& funcNames, std::shared_ptr<Repository<OperationTypename>> repo, bool injection) : fileName(fName), functionNames(funcNames), repository(repo), inject(injection){
        //std::cout << "Repository use count factory = " << repository.use_count() << "\n";
    }

    clang::FrontendAction* create() {
        //std::cout << "Repository use count factory::create()= " << repository.use_count() << "\n";
        return new FindCallsAction<OperationTypename>(fileName, functionNames, repository, inject);
    }
public:
    std::string fileName;
    std::vector<std::string>& functionNames;
    std::shared_ptr<Repository<OperationTypename>> repository;
    Replacements* replacements;
    bool inject;
};

#endif // PASS1VISITOR

