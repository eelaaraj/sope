#ifndef HASHTREESERIALIZER
#define HASHTREESERIALIZER

#include "../Operations.h"
#include "../utils/OperationHelpers.h"
#include "../FileSystem.h"
#include "../knowledge_base/IKnowledgeBase.h"
#include "../json.hpp"
#include "../utils/FileUtils.h"
#include <stack>
#include <sstream>
using namespace pattern::matching;
using json = nlohmann::json;

template<typename T, typename HashType>
class HashTreeSerializer {
public:
    HashTreeSerializer(const std::string &filename, const ITree* const tree, const std::map<int,std::shared_ptr<IPatternNode>>& patternNodesMap);
    HashTreeSerializer(const std::string& filename, const std::map<int,std::shared_ptr<IPatternNode>>& patternNodesMap);
    HashTreeSerializer(const std::string& filename, const ITree* tree);
    bool serialize();
    bool serialize(const ITree* const tree);//, std::function<> callable);
    json& serializedDoc() { return currentNodeJson; }
    const ITree* deserialize();
    const ITree* deserializedTree() {return _tree;}
private:
    /*const ITree* */ void deserializeTree(json hashTreeJson, std::stack<ITreeNode*>& parentStack);
private:
    /* setup should keep track of who's the parent of who, for the sake of creating
     * the links in the json. The parent id will be the source id and the target id
     * will be the child id. For Ids we can use the address of the objects.
     * Thus we will need to keep track of parent addresses in a stack.
     *
     */
    void setup(ITreeNode* node);
    /* cleanup should pop the current parent address.
     */
    void cleanup(ITreeNode* node);
    //void processPattern(Pattern<operation>& pattern,std::function<std::string(std::shared_ptr<operation>)> callable);
    void processHashNode(ITreeNode* hashNode);//, std::string indentation = "");

    void setup2(ITreeNode* node);

    void cleanup2(ITreeNode* node);

    void processHashNode2(ITreeNode* hashNode);
private:
    std::string _filename;
    const ITree* _tree;
    std::stack<ITreeNode*> _parentStack;
    std::stack<json> _parentStack2;
    std::string _json;

    std::string nodes = "\"nodes\":[";
    std::string children = "\"children\":[";
    std::string openbracket = "[";
    std::string closedbracket = "]";
    std::string type = "\"type\":";
    std::string ops = "\"ops\":";
    std::string seperator = ",";
    std::string openbrace = "{";
    std::string closedbrace = "}";
    std::string id = "\"id\":";
    std::string value = "\"value\":";
    std::string quote = "\"";

    json currentNodeJson;
    std::map<int,std::shared_ptr<IPatternNode>> pNodesMap;
};

template<typename T, typename HashType>
HashTreeSerializer<T,HashType>::HashTreeSerializer(const std::string &filename, const std::map<int,std::shared_ptr<IPatternNode>>& patternNodesMap):
    HashTreeSerializer(filename, nullptr, patternNodesMap) {}

template<typename T, typename HashType>
HashTreeSerializer<T,HashType>::HashTreeSerializer(const std::string &filename, const ITree* const tree):
    HashTreeSerializer(filename, tree, {}) {}

template<typename T, typename HashType>
HashTreeSerializer<T,HashType>::HashTreeSerializer(const std::string &filename, const ITree* const tree, const std::map<int,std::shared_ptr<IPatternNode>>& patternNodesMap):
    _filename(filename), _tree(tree), pNodesMap(patternNodesMap) {}

template<typename T, typename HashType>
void HashTreeSerializer<T,HashType>::setup(ITreeNode* node) {
/*    if(!_parentStack.empty()) {
        std::shared_ptr<PatternNode<operation>> parent = _parentStack.top();
        // If the node is the first child, then we don't need seperators. If we add a seperator, we will
        // end up with a malformed json in the children:[] array.
        if(node != parent->children().front())
            _json+= seperator;
    }
    _json+= openbrace;
*/
    _parentStack.push(node);
}

template<typename T, typename HashType>
void HashTreeSerializer<T,HashType>::cleanup(ITreeNode* node) {
    // cleanup will cloes the bracket after the children of the popped node have been all processed.
    //if(!node->children().empty())
    _json+= closedbracket;
    _json+= closedbrace;
    _parentStack.pop();
}

template<typename T, typename HashType>
void HashTreeSerializer<T,HashType>::setup2(ITreeNode* node) {
    _parentStack2.push(currentNodeJson);
}

template<typename T, typename HashType>
void HashTreeSerializer<T,HashType>::cleanup2(ITreeNode* node) {
    json childNode = _parentStack2.top();
    _parentStack2.pop();
    // This means that we processed all the nodes. Now we set the currentNode to be the last popped node.
    // Then We will make this node a child of the root node as follows: json["nodes"] = lastPoppedNode.
    if(_parentStack2.empty()) {
        currentNodeJson.clear();
        currentNodeJson["nodes"] = json::array();
        currentNodeJson["nodes"].push_back(childNode);
    }
    else { // There are more nodes in the stack and therefore the currently popped node is the child of the node on top of the stack.
        json& parent = _parentStack2.top();
        parent["children"].push_back(childNode);
    }
}

/*template<typename T, typename HashType>
void HashTreeSerializer<T,HashType>::processPattern(Pattern<operation>& pattern, std::function<std::string(std::shared_ptr<operation>)> callable) {
     // "ops":["type":"0","type":"3"] // example: 0 = for and 3 = multiplication
    _json += ops + openbracket;
     for (int i = 0; i < pattern.size(); i++) {
         //std::stringstream ss;
         //ss << pattern.at(i)->getOpType();
         _json += openbrace + type + quote + std::to_string(static_cast<int>(pattern.at(i)->getOpType())) + quote + closedbrace;
         if(i != pattern.size()-1)
             _json += seperator;
     }
     _json+= closedbracket;
}*/

template<typename T, typename HashType>
void HashTreeSerializer<T,HashType>::processHashNode(ITreeNode* hashNode){//, std::string indentation) {
    //std::cout << indentation;
    // Here we should generate most of the json
    std::stringstream nodeId;
    nodeId << hashNode;
    if(!_parentStack.empty()) {
        ITreeNode* parent = _parentStack.top();
        // If the node is the first child, then we don't need seperators. If we add a seperator, we will
        // end up with a malformed json in the children:[] array.
        if(hashNode != parent->children().front())
            _json+= seperator;
    }
    _json+= openbrace;
    //_json+= openbrace + id + quote + nodeId.str() + quote + seperator;
    _json+= id + quote + nodeId.str() + quote + seperator;
    std::string hashValue = std::to_string((dyn_cast<HashNode<T,HashType>>(hashNode))->getHash());
    _json+= value + hashValue ;
    //processPattern(*(patternNode->getPattern()), callable);
    _json+= seperator + children;
}

template<typename T, typename HashType>
void HashTreeSerializer<T,HashType>::processHashNode2(ITreeNode* hashNode) {
    // Here we should generate most of the json
    std::stringstream nodeId;
    nodeId << hashNode;
    json jsonNode;
    jsonNode["id"] = hashNode->uniqueId(); //nodeId.str();
    jsonNode["pid"] = hashNode->patternNode()->uniqueId(); // pattern id.
    jsonNode["value"] = (dyn_cast<HashNode<T,HashType>>(hashNode))->getHash();
    jsonNode["children"] = json::array();
    currentNodeJson = jsonNode;
//    if(!_parentStack2.empty()) {
//        json parent = _parentStack2.top();
//        parent["children"].push_back(jsonNode);
//    }
}

template<typename T, typename HashType>
bool HashTreeSerializer<T,HashType>::serialize() {
    return serialize(_tree);
}

// Here I hard code the return type of inner function in the callable function because json should be string
// Thus anything returned by any function should be string. Even ints !!
template<typename T, typename HashType>
bool HashTreeSerializer<T,HashType>::serialize(
        const ITree* const tree) {//,
        //std::function<void (std::shared_ptr<PatternNode<T> > &, std::function<std::string (std::shared_ptr<T>)>, types... args)> callable){
    /* traverse the tree top to down and left to right and serialize
     * The setup and cleanup are specific for this generator so, we don't
     * need to pass them in. We implement them in this class instead.
     */

    /* Here we don't need any variadic template args because we are not really doing any formatting
     * or indentation. So skip this argument all together.
     */

    /* Callable should only be used to set the name/text of the cells and no more.
     * All other attributes of the json will be position and size properties
     */

    /*std::function<std::string(std::shared_ptr<operation>)> printCode =
            [] (std::shared_ptr<operation> toPrint) {
        std::cout << toPrint->getMainStringLiteral() << std::endl;
        return toPrint->getMainStringLiteral();
    };*/

    //HashNode<T,HashType>
    std::function<void(ITreeNode*)> printHashNodes
            = std::bind(&HashTreeSerializer<T,HashType>::processHashNode, this, std::placeholders::_1);

    std::function<void(ITreeNode*)>
                       doSetup
            = std::bind(&HashTreeSerializer<T,HashType>::setup, this, std::placeholders::_1);

    std::function<void(ITreeNode*)>
                       doCleanup
            = std::bind(&HashTreeSerializer<T,HashType>::cleanup, this, std::placeholders::_1);

    // Start nlohmann::json creation
    std::function<void(ITreeNode*)> printHashNodes2
            = std::bind(&HashTreeSerializer<T,HashType>::processHashNode2, this, std::placeholders::_1);

    std::function<void(ITreeNode*)>
                       doSetup2
            = std::bind(&HashTreeSerializer<T,HashType>::setup2, this, std::placeholders::_1);

    std::function<void(ITreeNode*)>
                       doCleanup2
            = std::bind(&HashTreeSerializer<T,HashType>::cleanup2, this, std::placeholders::_1);


    (const_cast<ITree*>(tree))->template traverseTreeDFST2BL2R<void>(printHashNodes2, doSetup2, doCleanup2);
    //std::cout << " === Serializing into nlohmann::json instead of string === " << std::endl;
    //std::cout << currentNodeJson << std::endl;
    spdlog::get("sopelogger")->debug(" === Serializing Hash Tree into nlohmann::json instead of string === ");
    spdlog::get("sopelogger")->debug("{}", currentNodeJson.dump());
    // End of nlohmann::json creation

    _json+= openbrace + nodes;
    (const_cast<ITree*>(tree))->template traverseTreeDFST2BL2R<void>(printHashNodes, doSetup, doCleanup);
    //(const_cast<HashTree<T,HashType>&>(tree)).template traverseTreeDFST2BL2R<std::string>(printHashNodes, doSetup, doCleanup);
    _json+= closedbracket + closedbrace;
    //std::cout << " ================ HASH-TREE SERIALIZED JSON ==============" << std::endl;
    //std::cout << _json << std::endl;
    spdlog::get("sopelogger")->debug(" ================ HASH-TREE SERIALIZED JSON ==============");
    spdlog::get("sopelogger")->debug("{}", _json);

    // Removed the file persistence from here. The KB/IStorage will do that instead
    /*if(!FileSystem::File::exists(_filename)) {
        FileSystem::File f(_filename);
        f.open(FileSystem::File::out, FileSystem::File::text);
        if(f.isGood())
            f.putLine(_json, false);
    }
    else {
        //std::cerr << "File " << _filename <<  " already exists" << std::endl;
        spdlog::get("sopelogger")->error("File {} already exists", _filename);
    }
    */
    return true;
}

template<typename T, typename HashType>
const ITree* HashTreeSerializer<T,HashType>::deserialize() {
    std::stack<ITreeNode*> parentStack;
    json hTreeJson = deserializeJsonFile(_filename);
    deserializeTree(hTreeJson["nodes"], parentStack);
    return _tree;
}

template<typename T, typename HashType>
/*const ITree* */ void HashTreeSerializer<T,HashType>::deserializeTree(json hashTreeJson, std::stack<ITreeNode*>& parentStack) {
    for (json::iterator it = hashTreeJson.begin(); it != hashTreeJson.end(); ++it) {
        json hashChildren = (*it)["children"];
        HashType hashVal = (*it)["value"];
        std::shared_ptr<IPatternNode> patternNode = nullptr;
        int uniqueId = (*it)["id"];
        int pid = (*it)["pid"];
        if(pNodesMap.size() >0){
            patternNode = pNodesMap[pid];
        }
        ITreeNode* node = new HashNode<T,HashType>(hashVal, patternNode, uniqueId); // TODO: make the patternNode point to a valid node by searching for it in the patternTree

        //node->level() = (*it)["level"];
        if(!parentStack.empty()) {
            parentStack.top()->addChild(node);
        }
        parentStack.push(node);
        deserializeTree(hashChildren, parentStack);
    }
    if(parentStack.size() == 1) {
        // create the patern tree and add this node as root
        auto root = parentStack.top();
        parentStack.pop();
        ITree* ht = new HashTree<T,HashType>(dyn_cast<HashNode<T,HashType>>(root));
        std::ostringstream oss;
        std::function<void(ITreeNode*)> printNode = [&oss](ITreeNode* node) { oss << /*std::setfill(' ') << std::setw(2*node->level()) << "" <<*/ (dyn_cast<HashNode<T,HashType>>(node))->getHash() << std::endl; };
        ht->template traverseTreeDFST2BL2R<void>(printNode);
        spdlog::get("sopelogger")->debug("{}",oss.str());
        _tree = ht;
        //return ht;
    }
    else if(!parentStack.empty()) {
        parentStack.pop();
    }
}

#endif // HASHTREESERIALIZER

