#ifndef PATTERNTREESERIALIZER
#define PATTERNTREESERIALIZER

#include "../Operations.h"
#include "../utils/OperationHelpers.h"
#include "../FileSystem.h"
#include "../json.hpp"
#include "../utils/FileUtils.h"
#include <stack>
#include <sstream>
using namespace pattern::matching;
using json = nlohmann::json;

template<typename T>
class PatternTreeSerializer {
public:
    //PatternTreeSerializer(const std::string& filename, const PatternTree<T>& tree);
    PatternTreeSerializer(const std::string& filename, const std::shared_ptr<IPatternTree>& tree);
    PatternTreeSerializer(const std::string& filename);
    bool serialize();
    std::shared_ptr<IPatternTree> deserialize();
    //bool serialize(const PatternTree<T>& tree);//, std::function<> callable);
    bool serialize(const std::shared_ptr<IPatternTree>& tree);//, std::function<> callable);
    json& serializedDoc() { return currentNodeJson; }
    std::shared_ptr<IPatternTree>& deserializedTree() { return _tree; }
    std::map<int, std::shared_ptr<IPatternNode>>& getNodesMap() { return nodesMap; }
private:
    /*std::shared_ptr<IPatternTree>*/void  deserializeTree(json patternTreeJson, std::stack<std::shared_ptr<IPatternNode>>& parentStack);
private:
    /* setup should keep track of who's the parent of who, for the sake of creating
     * the links in the json. The parent id will be the source id and the target id
     * will be the child id. For Ids we can use the address of the objects.
     * Thus we will need to keep track of parent addresses in a stack.
     *
     */
    void setup(std::shared_ptr<PatternNode<T>> node);
    /* cleanup should pop the current parent address.
     */
    void cleanup(std::shared_ptr<PatternNode<T>> node);
    void processPattern(Pattern<operation>& pattern,std::function<std::string(std::shared_ptr<operation>)> callable);
    void processPatternNode(std::shared_ptr<PatternNode<operation>>& patternNode, std::function<std::string(std::shared_ptr<operation>)> callable);//, std::string indentation = "");

    /*void setup2(std::shared_ptr<PatternNode<T>> node);
    void cleanup2(std::shared_ptr<PatternNode<T>> node);
    //void processPattern2(Pattern<operation>& pattern,std::function<std::string(std::shared_ptr<operation>)> callable);
    void processPatternNode2(std::shared_ptr<PatternNode<operation>>& patternNode, std::function<std::string(std::shared_ptr<operation>)> callable);
*/
    void setup2(std::shared_ptr<IPatternNode> node);
    void cleanup2(std::shared_ptr<IPatternNode> node);
    //void processPattern2(Pattern<operation>& pattern,std::function<std::string(std::shared_ptr<operation>)> callable);
    void processPatternNode2(std::shared_ptr<IPatternNode>& patternNode, std::function<std::string(std::shared_ptr<operation>)> callable);
private:
    std::string _filename;
    //PatternTree<T> _tree;
    //std::stack<std::shared_ptr<PatternNode<T>>> _parentStack;
    std::shared_ptr<IPatternTree> _tree;
    std::stack<std::shared_ptr<IPatternNode>> _parentStack;
    std::stack<json> _parentStack2;
    std::string _json;
    json currentNodeJson;

    std::string cells = "\"nodes\":[";
    std::string children = "\"children\":[";
    std::string openbracket = "[";
    std::string closedbracket = "]";
    std::string type = "\"type\":";
    std::string ops = "\"ops\":";
    std::string seperator = ",";
    std::string openbrace = "{";
    std::string closedbrace = "}";
    std::string id = "\"id\":";
    std::string level = "\"level\":";
    std::string quote = "\"";

    std::map<int, std::shared_ptr<IPatternNode>> nodesMap;
};


/*template<typename T>
PatternTreeSerializer<T>::PatternTreeSerializer(const std::string &filename, const PatternTree<T> &tree):
    _filename(filename), _tree(tree) {}*/

template<typename T>
PatternTreeSerializer<T>::PatternTreeSerializer(const std::string &filename, const std::shared_ptr<IPatternTree> &tree):
    _filename(filename), _tree(tree) {}

template<typename T>
PatternTreeSerializer<T>::PatternTreeSerializer(const std::string &filename):
    PatternTreeSerializer(filename, nullptr) {}

template<typename T>
void PatternTreeSerializer<T>::setup(std::shared_ptr<PatternNode<T>> node) {
/*    if(!_parentStack.empty()) {
        std::shared_ptr<PatternNode<operation>> parent = _parentStack.top();
        // If the node is the first child, then we don't need seperators. If we add a seperator, we will
        // end up with a malformed json in the children:[] array.
        if(node != parent->children().front())
            _json+= seperator;
    }
    _json+= openbrace;
*/
    _parentStack.push(node);
}

template<typename T>
void PatternTreeSerializer<T>::cleanup(std::shared_ptr<PatternNode<T>> node) {
    // cleanup will cloes the bracket after the children of the popped node have been all processed.
    //if(!node->children().empty())
    _json+= closedbracket;
    _json+= closedbrace;
    _parentStack.pop();
}

template<typename T>
void PatternTreeSerializer<T>::setup2(std::shared_ptr<IPatternNode> node) {
    _parentStack2.push(currentNodeJson);
}

template<typename T>
void PatternTreeSerializer<T>::cleanup2(std::shared_ptr<IPatternNode> node) {
    json childNode = _parentStack2.top();
    _parentStack2.pop();
    // This means that we processed all the nodes. Now we set the currentNode to be the last popped node.
    // Then We will make this node a child of the root node as follows: json["nodes"] = lastPoppedNode.
    if(_parentStack2.empty()) {
        currentNodeJson.clear();
        currentNodeJson["nodes"] = json::array();
        currentNodeJson["nodes"].push_back(childNode);
    }
    else { // There are more nodes in the stack and therefore the currently popped node is the child of the node on top of the stack.
        json& parent = _parentStack2.top();
        parent["children"].push_back(childNode);
    }
}

template<typename T>
void PatternTreeSerializer<T>::processPatternNode2(std::shared_ptr<IPatternNode>& patternNode, std::function<std::string(std::shared_ptr<operation>)> callable){
    // Here we should generate most of the json
    std::stringstream nodeId;
    nodeId << patternNode.get();
    json jsonNode;
    jsonNode["id"] = patternNode->uniqueId(); // nodeId.str();
    jsonNode["level"] = patternNode->level();
    jsonNode["ops"] = json::array();
    int patternSize = patternNode->getPattern()->size();
    for (int i = 0; i < patternSize; i++) {
        json pattern;
        pattern["type"] = static_cast<int>(patternNode->getPattern()->at(i)->getOpType());
        if(clforOperation* clfor = dyn_cast<clforOperation>(patternNode->getPattern()->at(i).get())) {
            pattern["loopid"] = clfor->getLoopId();
        }
        jsonNode["ops"].push_back(pattern);
    }
    jsonNode["children"] = json::array();
    currentNodeJson = jsonNode;
}

template<typename T>
void PatternTreeSerializer<T>::processPattern(Pattern<operation>& pattern, std::function<std::string(std::shared_ptr<operation>)> callable) {
     // "ops":["type":"0","type":"3"] // example: 0 = for and 3 = multiplication
    _json += ops + openbracket;
     for (int i = 0; i < pattern.size(); i++) {
         //std::stringstream ss;
         //ss << pattern.at(i)->getOpType();
         _json += openbrace + type /*+ quote*/ + std::to_string(static_cast<int>(pattern.at(i)->getOpType())) /*+ quote*/ + closedbrace;
         if(i != pattern.size()-1)
             _json += seperator;
     }
     _json+= closedbracket;
}

template<typename T>
void PatternTreeSerializer<T>::processPatternNode(std::shared_ptr<PatternNode<operation>>& patternNode, std::function<std::string(std::shared_ptr<operation>)> callable){//, std::string indentation) {
    //std::cout << indentation;
    // Here we should generate most of the json
    std::stringstream nodeId;
    nodeId << patternNode.get();
    if(!_parentStack.empty()) {
        std::shared_ptr<PatternNode<operation>> parent = _parentStack.top();
        // If the node is the first child, then we don't need seperators. If we add a seperator, we will
        // end up with a malformed json in the children:[] array.
        if(patternNode != parent->children().front())
            _json+= seperator;
    }
    _json+= openbrace;
    //_json+= openbrace + id + quote + nodeId.str() + quote + seperator;
    _json+= id + quote + nodeId.str() + quote + seperator;
    _json+= level + std::to_string(patternNode->level()) + seperator;
    processPattern(*(patternNode->getPattern()), callable);
    _json+= seperator + children;
}

template<typename T>
bool PatternTreeSerializer<T>::serialize() {
    return serialize(_tree);
}

// Here I hard code the return type of inner function in the callable function because json should be string
// Thus anything returned by any function should be string. Even ints !!
template<typename T>
//bool PatternTreeSerializer<T>::serialize(const PatternTree<T>& tree) {
bool PatternTreeSerializer<T>::serialize(const std::shared_ptr<IPatternTree>& tree) {
        //std::function<void (std::shared_ptr<PatternNode<T> > &, std::function<std::string (std::shared_ptr<T>)>, types... args)> callable){
    /* traverse the tree top to down and left to right and serialize
     * The setup and cleanup are specific for this generator so, we don't
     * need to pass them in. We implement them in this class instead.
     */

    /* Here we don't need any variadic template args because we are not really doing any formatting
     * or indentation. So skip this argument all together.
     */

    /* Callable should only be used to set the name/text of the cells and no more.
     * All other attributes of the json will be position and size properties
     */

    /*std::function<std::string(std::shared_ptr<operation>)> printCode =
            [] (std::shared_ptr<operation> toPrint) {
        std::cout << toPrint->getMainStringLiteral() << std::endl;
        return toPrint->getMainStringLiteral();
    };*/

    /*
    std::function<void(std::shared_ptr<PatternNode<operation>>&,
                       std::function<std::string(std::shared_ptr<operation>)> )> printPatternOperations
            = std::bind(&PatternTreeSerializer<T>::processPatternNode, this, std::placeholders::_1, nullptr);//, std::placeholders::_3);

    std::function<void(std::shared_ptr<PatternNode<operation>>)>
                       doSetup
            = std::bind(&PatternTreeSerializer<T>::setup, this, std::placeholders::_1);

    std::function<void(std::shared_ptr<PatternNode<operation>>)>
                       doCleanup
            = std::bind(&PatternTreeSerializer<T>::cleanup, this, std::placeholders::_1);
    */

    // Start of nlohmann::json serialization
    std::function<void(std::shared_ptr<IPatternNode>&,
                       std::function<std::string(std::shared_ptr<operation>)> )> printPatternOperations2
            = std::bind(&PatternTreeSerializer<T>::processPatternNode2, this, std::placeholders::_1, nullptr);//, std::placeholders::_3);

    std::function<void(std::shared_ptr<IPatternNode>)>
                       doSetup2
            = std::bind(&PatternTreeSerializer<T>::setup2, this, std::placeholders::_1);

    std::function<void(std::shared_ptr<IPatternNode>)>
                       doCleanup2
            = std::bind(&PatternTreeSerializer<T>::cleanup2, this, std::placeholders::_1);

    //(const_cast<PatternTree<T>&>(tree)).template traverseTreeDFST2BL2R<std::string>(printPatternOperations2, doSetup2, doCleanup2);
    tree->template traverseTreeDFST2BL2R<T,std::string>(printPatternOperations2, doSetup2, doCleanup2);

    //std::cout << " === Serializing into nlohmann::json instead of string === " << std::endl;
    //std::cout << currentNodeJson << std::endl;
    spdlog::get("sopelogger")->debug(" === Serializing Pattern Tree into nlohmann::json instead of string === ");
    spdlog::get("sopelogger")->debug("{}", currentNodeJson.dump());
    // End of nlohman::json serialization

    /*
    _json+= openbrace + cells;
    (const_cast<PatternTree<T>&>(tree)).template traverseTreeDFST2BL2R<std::string>(printPatternOperations, doSetup, doCleanup);
    _json+= closedbracket + closedbrace;
    //std::cout << " ================ PATTERN-TREE SERIALIZED JSON ==============" << std::endl;
    //std::cout << _json << std::endl;
    spdlog::get("sopelogger")->debug(" ================ PATTERN-TREE SERIALIZED JSON ==============");
    spdlog::get("sopelogger")->debug("{}", _json);
    */
    // Removed file storage from here. The KB/IStorage will do that instead.
    /*if(!FileSystem::File::exists(_filename)) {
        FileSystem::File f(_filename);
        f.open(FileSystem::File::out, FileSystem::File::text);
        if(f.isGood()) {
            //f.putLine(_json, false);
            f.putLine(currentNodeJson.dump(), false);
        }
    }
    else {
        //std::cerr << "File " << _filename <<  " already exists" << std::endl;
        spdlog::get("sopelogger")->error("File {} already exists", _filename);
    }
    */

    return true;
}


template<typename T>
std::shared_ptr<IPatternTree> PatternTreeSerializer<T>::deserialize() {
    std::stack<std::shared_ptr<IPatternNode>> parentStack;
    json pTreeJson = deserializeJsonFile(_filename);
    /*auto tree =*/ deserializeTree(pTreeJson["nodes"], parentStack);
    return _tree;
}
#include "../utils/Utils.h"
template<typename T>
/*std::shared_ptr<IPatternTree>*/ void PatternTreeSerializer<T>::deserializeTree(json patternTreeJson, std::stack<std::shared_ptr<IPatternNode>>& parentStack) {

    for (json::iterator it = patternTreeJson.begin(); it != patternTreeJson.end(); ++it) {
        json ops = (*it)["ops"];
        std::shared_ptr<Pattern<T>> pat = std::make_shared<Pattern<T>>();
        // Deserializing operations inside the pattern. For now, we only care about the ForOperations
        // TODO: deserialize all types of operations
        for(json::iterator opIt = ops.begin(); opIt != ops.end(); ++opIt) {
            int type = (*opIt)["type"];
            std::shared_ptr<T> op = nullptr;
            if(type == 8) {
                int loopId = -1;
                if(! ((*opIt)["loopid"].is_null()) ) {
                    loopId = (*opIt)["loopid"];
                }
                op = makeForOperation(nullptr, loopId);
            }
            if(op) {
                pat->addToPattern(op);
            }
        }
        /*for(json::iterator opIt = patternTree.begin(); opIt != patternTree.end(); ++opIt) {
            int type = (*it)["type"];*/
            /*enum class OperationType {
                0 memory, 1 datapath, 2 memoryLoad, 3 memoryStore, 4 add, 5 multiply, 6 subtract, 7 divide, 8 loop, 9 condition, 10 assign, 11 none,
                12 less, greater, equals,
                15 le,
                16 ge,
                17 ne,
                18 function,
                19 ifCondition,
                20 elseCondition,
                21 compoundStmt,
                22 numeric,
                23 call, // call expression equivalent in clang. This is base class for all other calls
                24 cxx_member_call, // member function call
                25 cxx_operator_call, // operator function call
            };*/
            /*std::shared_ptr<T> op;
            if(type==2 || type == 3) {
                op = makeSubscriptOperation(nullptr, type);
            }
            else if(type == 4 || type == 5 || type == 6 || type ==7 || type == 12 || type == 13 || type == 14 || type == 15 || type == 16 || type == 17) {
                op = makeBinaryOperation(nullptr);
            }
            else if(type == 8) {
                op = makeForOperation(nullptr);
            }
            else if(type == 9) {
                op = makeIfOperation(nullptr);
            }
            else if(type == 10) { // TODO: figure out a way to identidy compound operations
                op = makeBinaryOperation(nullptr);
            }
            else if(type == 18) {
                op = makeFunctionOperation(nullptr);
            }
            else if(type == 21) {
                op = makeCompoundOperation(nullptr);
            }
            else if(type == 22) { // TODO: We must find a way to different between a numeric floatOperation and a numeric intOperation
                op = makeIntLiteralOperation(nullptr);
            }
            else if(type == 23 || type == 24) {
                op = makeCallOperation(nullptr, type);
            }
            pat->addToPattern(op);
        }*/
        json patternChildren = (*it)["children"];
        //std::shared_ptr<PatternNode<T>> node = std::make_shared<PatternNode<T>>(pat);
        std::shared_ptr<IPatternNode> node = std::make_shared<PatternNode<T>>(pat);

        //node->getPattern() = pat;
        node->level() = (*it)["level"];
        node->uniqueId() = (*it)["id"];
        nodesMap[node->uniqueId()] = node; // add the node to the map which will be used to link the corresponding hashnode and pattern node

        if(!parentStack.empty()) {
            parentStack.top()->addChild(node);
        }
        parentStack.push(node);
        deserializeTree(patternChildren, parentStack);
    }
    if(parentStack.size() == 1) {
        // create the patern tree and add this node as root
        auto root = parentStack.top();
        parentStack.pop();
        //PatternTree<T> pt(root);
        std::shared_ptr<IPatternTree> pt = std::make_shared<PatternTree<T>>(root);
        //std::function<std::string(std::shared_ptr<operation>)> printCode = [] (std::shared_ptr<operation> toPrint) { return toPrint->getMainStringLiteral(); };
        std::ostringstream oss;
        std::function<std::string(std::shared_ptr<operation>)> printDeserializedOp = [] (std::shared_ptr<operation> toPrint) {
            std::string res = "";
            if(clforOperation* forOp = dyn_cast<clforOperation>(toPrint.get())) {
                res = "loopId="+std::to_string(forOp->getLoopId());
            }
            return res;
        };
        std::function<void(std::shared_ptr<IPatternNode>&,
                           std::function<std::string(std::shared_ptr<operation>)>, std::string)> printPatternOperations
                       = std::bind(&printPatternNodeOperations, std::placeholders::_1, printDeserializedOp, std::ref(oss), std::placeholders::_3);
        std::string indent = ""; // had to create an empty strign and pass indent to traverse instead of passing "" directly inline. This is because "" is a char* and not a string, so the template specialization will have to be traverseDFS<string,char*>
        //pt.template traverseTreeDFST2BL2R<std::string, std::string>(printPatternOperations, indent);
        pt->template traverseTreeDFST2BL2R<T,std::string, std::string>(printPatternOperations, indent);
        spdlog::get("sopelogger")->debug("{}", oss.str());
        _tree = pt;
        //return pt;
    }
    else if(!parentStack.empty()) {
        parentStack.pop();
        //return nullptr;
    }
}


#endif // PATTERNTREESERIALIZER

