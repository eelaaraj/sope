#ifndef JOINTJSONGENERATOR
#define JOINTJSONGENERATOR
/*
 * Joint JS renders graphs on paper. The graphs can be created manually and
 * cells added to it manually. Or they can be created automatically from a json
 * file. The graph will have an array of cells and these cells can be either
 * nodes or links. To specify if the cell is a node or link, we need to specify
 * the type of the cell as a json property of the cell object (basic.Rect or link).
 *
 * Each cell whether link or node, will have a unique id. In additon to their id,
 * links will have source id and target id for the endpoint of the link. The graph
 * uses these unique id to know how to connect the nodes via links.
 *
 * Example JSON: "{"cells":[
 * {"type":"basic.Rect","position":{"x":100,"y":30},"size":{"width":100,"height":30},
 * "angle":0,"id":"37705d2e-0f97-40ab-ad1d-cdf9bc576a8b","z":1,
 * "attrs":{"rect":{"fill":"blue"},"text":{"fill":"white","text":"my box"}}},
 * {"type":"basic.Rect","position":{"x":406,"y":84},"size":{"width":100,"height":30},
 * "angle":0,"id":"cd5d2a2e-07c9-4220-91f2-3245777ff47d","embeds":"","z":2,
 * "attrs":{"rect":{"fill":"blue"},"text":{"fill":"white","text":"my box"}}},
 * {"type":"link","source":{"id":"37705d2e-0f97-40ab-ad1d-cdf9bc576a8b"},
 * "target":{"id":"cd5d2a2e-07c9-4220-91f2-3245777ff47d"},
 * "id":"9ef776fa-68c0-4ebb-a501-1d74242a061d","z":3,"attrs":{}}]}"
 *
 */

#include "../Operations.h"
#include "../utils/OperationHelpers.h"
#include <stack>
#include <sstream>
using namespace pattern::matching;

template<typename T>
class JointJsonGenerator {
public:
    //JointJsonGenerator(const std::string& filename, const PatternTree<T>& tree);
    JointJsonGenerator(const std::string& filename, const std::shared_ptr<IPatternTree>& tree);
    bool serialize();
    //bool serialize(const PatternTree<T>& tree);//, std::function<> callable);
    bool serialize(const std::shared_ptr<IPatternTree>& tree);//, std::function<> callable);
private:
    /* setup should keep track of who's the parent of who, for the sake of creating
     * the links in the json. The parent id will be the source id and the target id
     * will be the child id. For Ids we can use the address of the objects.
     * Thus we will need to keep track of parent addresses in a stack.
     *
     */
    /*
    void setup(std::shared_ptr<PatternNode<T>> node);
    void cleanup(std::shared_ptr<PatternNode<T>> node);
    void processPattern(Pattern<operation>& pattern,std::function<std::string(std::shared_ptr<operation>)> callable);
    void processPatternNode(std::shared_ptr<PatternNode<operation>>& patternNode, std::function<std::string(std::shared_ptr<operation>)> callable);//, std::string indentation = "");
    */
    void setup(std::shared_ptr<IPatternNode> node);
    /* cleanup should pop the current parent address.
     */
    void cleanup(std::shared_ptr<IPatternNode> node);
    void processPattern(Pattern<operation>& pattern,std::function<std::string(std::shared_ptr<operation>)> callable);
    void processPatternNode(std::shared_ptr<IPatternNode>& patternNode, std::function<std::string(std::shared_ptr<operation>)> callable);//, std::string indentation = "");
private:
    std::string _filename;
    //PatternTree<T> _tree;
    //std::stack<std::shared_ptr<PatternNode<T>>> _parentStack;
    std::shared_ptr<IPatternTree> _tree;
    std::stack<std::shared_ptr<IPatternNode>> _parentStack;
    std::string _json;
    /* {"type":"basic.Rect","position":{"x":100,"y":30},"size":{"width":100,"height":30},
     * "angle":0,"id":"37705d2e-0f97-40ab-ad1d-cdf9bc576a8b","z":1,
     * "attrs":{"rect":{"fill":"blue"},"text":{"fill":"white","text":"my box"}}},
     */
    std::string cells = "\"cells\":[";
    std::string closedbracket = "]";
    std::string attrs = "\"attrs\":{";
    std::string rect = "\"rect\":{\"fill\":\"blue\"}";
    std::string seperator = ",";
    std::string textAttribute = "\"text\":{\"fill\":\"white\",";
    std::string text = "\"text\":";
    std::string openbrace = "{";
    std::string closedbrace = "}";
    std::string rectType = "\"type\":\"basic.Rect\"";
    std::string rectSize = "\"size\":{\"width\":100,\"height\":30},\"angle\":0,\"z\":1,";
    std::string id = "\"id\":";
    std::string linkType = "\"type\":\"link\",\"z\":3,\"attrs\":{},";
    std::string linksource = "\"source\":{\"id\":";
    std::string linktarget = "\"target\":{\"id\":";
    std::string quote = "\"";
    std::string forBox = "\"rect\": { \"fill\": \"#E74C3C\", \"rx\": 0, \"ry\": 0, \"stroke-width\": 0 }";
    std::string ifelseBox = "\"rect\": { \"fill\": \"#F39C12\", \"rx\": 0, \"ry\": 0, \"stroke-width\": 0 }";
};

/*template<typename T>
JointJsonGenerator<T>::JointJsonGenerator(const std::string &filename, const PatternTree<T> &tree):
    _filename(filename), _tree(tree) {}
*/
template<typename T>
JointJsonGenerator<T>::JointJsonGenerator(const std::string &filename, const std::shared_ptr<IPatternTree> &tree):
    _filename(filename), _tree(tree) {}

template<typename T>
void JointJsonGenerator<T>::setup(std::shared_ptr<IPatternNode> node) {
    _parentStack.push(node);
}

template<typename T>
void JointJsonGenerator<T>::cleanup(std::shared_ptr<IPatternNode> node) {
    _parentStack.pop();
}

template<typename T>
void JointJsonGenerator<T>::processPattern(Pattern<operation>& pattern, std::function<std::string(std::shared_ptr<operation>)> callable) {
     // "attrs":{"rect":{"fill":"blue"},"text":{"fill":"white","text":"my box"}}
    std::string rectangle;;
    if((pattern.size() > 0) && (dyn_cast<loopOperation>(pattern.at(0).get())))
        rectangle = forBox;
    else if((pattern.size() > 0) && (dyn_cast<ifOperation>(pattern.at(0).get())))
        rectangle = ifelseBox;
    else rectangle = rect;
    _json += attrs + rectangle + seperator + textAttribute + text + quote;
     for (int i = 0; i < pattern.size(); i++) {
         _json += callable(pattern.at(i)) + ",";
     }
     _json+= quote + closedbrace + closedbrace;
}

template<typename T>
void JointJsonGenerator<T>::processPatternNode(std::shared_ptr<IPatternNode>& patternNode, std::function<std::string(std::shared_ptr<operation>)> callable){//, std::string indentation) {
    //std::cout << indentation;
    // Here we should generate most of the json
    std::stringstream nodeId;
    nodeId << patternNode.get();
    if(!_parentStack.empty())
        _json+= seperator;
    _json+= openbrace + rectType + seperator + rectSize + id + quote + nodeId.str() + quote + seperator;
    processPattern(*(patternNode->getPattern()), callable);
    _json+= closedbrace;
    if(!_parentStack.empty()) {
        //createLink();
        std::stringstream linkIds;
        linkIds << seperator << openbrace << linkType << linksource << quote << _parentStack.top().get() << quote << closedbrace
                << seperator << linktarget << quote << patternNode.get() << quote << closedbrace << closedbrace ;
        _json+= linkIds.str();
    }
    //std::cout << std::endl;
}

template<typename T>
bool JointJsonGenerator<T>::serialize() {
    return serialize(_tree);
}

// Here I hard code the return type of inner function in the callable function because json should be string
// Thus anything returned by any function should be string. Even ints !!
template<typename T>
bool JointJsonGenerator<T>::serialize(
        const std::shared_ptr<IPatternTree>& tree) {//,
        //std::function<void (std::shared_ptr<PatternNode<T> > &, std::function<std::string (std::shared_ptr<T>)>, types... args)> callable){
    /* traverse the tree top to down and left to right and serialize
     * The setup and cleanup are specific for this generator so, we don't
     * need to pass them in. We implement them in this class instead.
     */

    /* Here we don't need any variadic template args because we are not really doing any formatting
     * or indentation. So skip this argument all together.
     */

    /* Callable should only be used to set the name/text of the cells and no more.
     * All other attributes of the json will be position and size properties
     */

    std::function<std::string(std::shared_ptr<operation>)> printCode =
            [] (std::shared_ptr<operation> toPrint) {
        //std::cout << toPrint->getMainStringLiteral() << std::endl;
        return toPrint->getMainStringLiteral();
    };

    std::function<void(std::shared_ptr<IPatternNode>&,
                       std::function<std::string(std::shared_ptr<operation>)> )> printPatternOperations
            = std::bind(&JointJsonGenerator<T>::processPatternNode, this, std::placeholders::_1, printCode);//, std::placeholders::_3);

    std::function<void(std::shared_ptr<IPatternNode>)>
                       doSetup
            = std::bind(&JointJsonGenerator<T>::setup, this, std::placeholders::_1);

    std::function<void(std::shared_ptr<IPatternNode>)>
                       doCleanup
            = std::bind(&JointJsonGenerator<T>::cleanup, this, std::placeholders::_1);

    _json+= openbrace + cells;
    //(const_cast<PatternTree<T>&>(tree)).template traverseTreeDFST2BL2R<std::string>(printPatternOperations, doSetup, doCleanup);
    tree->template traverseTreeDFST2BL2R<T,std::string>(printPatternOperations, doSetup, doCleanup);
    _json+= closedbracket + closedbrace;
    //std::cout << " ================ JSON ==============" << std::endl;
    //std::cout << _json << std::endl;
    spdlog::get("sopelogger")->debug(" ================ Pattern Tree in JOINT-JSON Format ==============");
    spdlog::get("sopelogger")->debug("{}", _json);

    return true;
}

#endif // JOINTJSONGENERATOR

