            TODO List
====================================
- Keep some information about the type of arguments to an operation. Is it an int mu;tiply or double multiple for example.
- Support while loops (in operation, clangoperation, whileprocessor, patterntree, and injection)
- Leaf matching in the pattern tree, then walking up the tree to find parent scopes matched.


            TOSHOW List
=====================================
S1- Benchmark small operations like A[contiguous], A[non-contiguous], X * Y, X + Y, A[cont]*B[cont], A[non-cont]*B[cont],....
    Then show how we can estimate the performance of let's say A[some expr] += B[some expr] * C[some expr] better by using sentinel
    matching rather than operation by operation matching (such as, metric(array read alone) + metric(some expr) + metric(array read
    alone) + metric(multiplication alone) + metric(memory write alone) + metric(some indexing expr) )

S2-Then Benchmark the above by wrapping two parent loops and benchmarking them. Then show if its better to use the metric we got
    from the parents OR its better to use the metric from S1 (the innner-most metric and then multiply it my the number of iterations
    in the two parent loops)
