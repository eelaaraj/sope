#ifndef IPATTERNTREE_H
#define IPATTERNTREE_H

#include <vector>
#include <memory>
#include "Operations.h"
#include "./utils/TypeDefinitions.h"

/* We build a tree out of the patterns
 * This helps us know which patterns to test/search.
 * Parent pattern is the largest most inclusive pattern
 * Children of the parent are smaller parts of the parent pattern
 *
 * If the parent pattern matches then we do not check any of the children.
 * If not then we will need to match the children patterns to figure out
 * what portion of the code is matching
 *
 */

namespace pattern {
namespace matching {


class IPatternNode {
public:
    enum NodeKind {
        NK_PAT, // pattern node
    };
public:
    IPatternNode(NodeKind tnk): IPatternNode(tnk, -1, -1) {}
    IPatternNode(NodeKind tnk, int uniqueId, int level): kind(tnk), _level(level), _uniqueId(uniqueId) {}
    virtual ~IPatternNode() {}
    virtual void addChild(std::shared_ptr<IPatternNode> child)=0;
    virtual std::shared_ptr<Pattern<operation>>& getPattern() = 0;
    virtual std::vector<std::shared_ptr<IPatternNode>>& children() = 0;
    virtual int getParentPatternIndex() const = 0;

    NodeKind getKind() const {
        return kind;
    }
    virtual int& level() { return _level;}
    virtual int& uniqueId() { return _uniqueId;}
private:
    const NodeKind kind;
    int _level;
    int _uniqueId;
};

template<typename T>
class PatternNode : public IPatternNode {
public:
    PatternNode() = delete;
    //PatternNode(const std::shared_ptr<Pattern<T>>& pattern);
    PatternNode(const std::shared_ptr<Pattern<T>>& pattern,
            int id = -1, int parentPatternIndex = -1);
    void addChild(std::shared_ptr<IPatternNode> child);
    std::shared_ptr<Pattern<T>>& getPattern() {
        return _pattern;
    }
    std::vector<std::shared_ptr<IPatternNode>>& children() {return _children;}
    int getParentPatternIndex() const {return _parentPatternIndex;}
    //int& level() { return _level; }
    //int search(const Sequence<T>& s);
public:
    static bool classof(const IPatternNode* baseNode) {
        return baseNode->getKind() == NodeKind::NK_PAT;
    }
private:
    std::shared_ptr<Pattern<T>> _pattern;
    std::vector<std::shared_ptr<IPatternNode>> _children;
    int _parentPatternIndex;// This indicates where the child pattern lies in the parent pattern
    //int _level;
};

template<typename T>
PatternNode<T>::PatternNode(const std::shared_ptr<Pattern<T>>& pattern, int id,
        int parentPatternIndex) : IPatternNode(NK_PAT, id, -1),
        _pattern(pattern), _parentPatternIndex(parentPatternIndex) { // parentIndex = -1 usually for a root or a node that has a filler NOOP parent
}

template<typename T>
void PatternNode<T>::addChild(std::shared_ptr<IPatternNode> child) {
    _children.push_back(child);
}

class IPatternTree {
public:
    enum TreeKind {
        TK_PAT, // pattern node
    };
public:
    IPatternTree(TreeKind tk): kind(tk) {}
    virtual ~IPatternTree() { spdlog::get("sopelogger")->info("Deleting PatternTree"); }
    virtual std::shared_ptr<IPatternNode> root() = 0;
    TreeKind getKind() const {
        return kind;
    }
    template<typename T, typename returnTypename>
    void traverseTreeBFS(
            std::function<void(std::shared_ptr<IPatternNode>& , std::function<returnTypename (std::shared_ptr<T>)>)> callable) {
        traverseTreeBFS(root(), callable);
    }
    template<typename T, typename returnTypename>
    void traverseTreeBFS(std::shared_ptr<IPatternNode> node,
            std::function<void (std::shared_ptr<IPatternNode > &, std::function<returnTypename (std::shared_ptr<T>)>)> callable);
    void traverseTreeT2B(std::shared_ptr<IPatternNode> node); // Traverse tree top to bottom
    void traverseTreeB2T();
    template<typename T, typename returnTypename, typename ...types>
    void traverseTreeDFST2BL2R(
            std::function<void (std::shared_ptr<IPatternNode > &, std::function<returnTypename (std::shared_ptr<T>)>, types... args)> callable,
            types&... args, std::function<void (std::shared_ptr<IPatternNode >) > setup = nullptr,
            std::function<void (std::shared_ptr<IPatternNode >) > cleanup = nullptr) {
        traverseTreeDFST2BL2R(root(), callable, args..., setup, cleanup);
    }
    template<typename T, typename returnTypename, typename ... types>
    void traverseTreeDFST2BL2R(
            std::shared_ptr<IPatternNode> node,
            std::function<void (std::shared_ptr<IPatternNode > &, std::function<returnTypename (std::shared_ptr<T>)>, types... args)> callable,
            types&... args, std::function<void (std::shared_ptr<IPatternNode >) > setup = nullptr,
            std::function<void (std::shared_ptr<IPatternNode >) > cleanup = nullptr);

    template<typename T, typename returnTypename, typename ...types>
    void traverseTreeDFSL2RB2T(
            std::function<void (std::shared_ptr<IPatternNode > &, std::function<returnTypename (std::shared_ptr<T>)>, types... args)> callable,
            types&... args, std::function<void (std::shared_ptr<IPatternNode >) > setup = nullptr,
            std::function<void (std::shared_ptr<IPatternNode >) > cleanup = nullptr) {
        traverseTreeDFSL2RB2T(root(), callable, args..., setup, cleanup);
    }

    template<typename T, typename returnTypename, typename ... types>
    void traverseTreeDFSL2RB2T(
            std::shared_ptr<IPatternNode> node,
            std::function<void (std::shared_ptr<IPatternNode > &, std::function<returnTypename (std::shared_ptr<T>)>, types... args)> callable,
            types&... args, std::function<void (std::shared_ptr<IPatternNode >) > setup = nullptr,
            std::function<void (std::shared_ptr<IPatternNode >) > cleanup = nullptr);
    //int search(const Sequence<T>& sequence);

    virtual void addTree(IPatternTree* anotherTree) = 0;

    //std::string serialize();
private:
    const TreeKind kind;
};


template<typename T>
class PatternTree : public IPatternTree {
public:
    /*PatternTree() : PatternTree(nullptr) {
    }*/
    PatternTree(const std::shared_ptr<IPatternNode>& root) : IPatternTree(TK_PAT), _root(root) {
    }
    virtual std::shared_ptr<IPatternNode> root() {
        return _root;
    }
    void setRoot(std::shared_ptr<PatternNode<T>> root) {
        _root = root;
    }

    int search(const Sequence<T>& sequence);

    virtual void addTree(IPatternTree* anotherTree);

    std::string serialize();
public:
    static bool classof(const IPatternTree* baseTree) {
        return baseTree->getKind() == TreeKind::TK_PAT;
    }
private:
    std::shared_ptr<IPatternNode> _root;
};

template<typename T>
std::shared_ptr<Pattern<T>> combinePatterns(const std::shared_ptr<Pattern<T>>& pattern1, const std::shared_ptr<Pattern<T>>& pattern2) {
    std::shared_ptr<Pattern<T>> newPattern = std::shared_ptr<Pattern<T>>(new Pattern<T>(*pattern1));
    // I didn't want to change Pattern class, so I uesd the copy constructor to copy pattern1 into the new pattern
    // and then iterated over the second pattern and added all its elements one by one to the new pattern
    for(auto it = pattern2->begin(); it != pattern2->end(); it++) {
        newPattern->addToPattern(*it);
    }
    return newPattern;
}

template<typename T>
void PatternTree<T>::addTree(IPatternTree* anotherTree) {
    if(anotherTree->root() && _root) {
        std::shared_ptr<Pattern<T>> anotherTreePattern = anotherTree->root()->getPattern();
        std::shared_ptr<Pattern<T>> thisTreePattern = _root->getPattern();
        std::shared_ptr<Pattern<T>> newPattern = combinePatterns(thisTreePattern, anotherTreePattern);
        std::shared_ptr<IPatternNode> newPatternNode = std::shared_ptr<IPatternNode>(new PatternNode<T>(newPattern));
        newPatternNode->addChild(_root);
        newPatternNode->addChild(anotherTree->root());
        _root = newPatternNode;
    }
    else if(!anotherTree->root() && !_root) {
        // both treePatterns are nulls. Shouldn't happen but we need to take care of that. We do
        // that by creating an empty tree
        std::shared_ptr<Pattern<T>> newPattern = std::shared_ptr<Pattern<T>>(new Pattern<T>());
        std::shared_ptr<IPatternNode> newPatternNode = std::shared_ptr<IPatternNode>(new PatternNode<T>(newPattern));
        _root = newPatternNode;
    }
    else { // only one of them is null. Thus use the one which isnt null and set it as the tree
        std::shared_ptr<Pattern<T>> newPattern = (!anotherTree->root()) ? _root->getPattern() : anotherTree->root()->getPattern();
        std::shared_ptr<IPatternNode> newPatternNode = (!anotherTree->root()) ? _root : anotherTree->root();
        _root = newPatternNode;
    }
}

template<typename T, typename returnTypename>
void IPatternTree::traverseTreeBFS(std::shared_ptr<IPatternNode> node,
        std::function<void(std::shared_ptr<IPatternNode>&, std::function<returnTypename(std::shared_ptr<T>)>)> callable) {
    std::queue < std::shared_ptr<IPatternNode>>q;
    q.push(node);
    while (q.size() > 0) {
        std::shared_ptr<IPatternNode> currentNode = q.front();
        // the nullptr in the second argument of callable, should be a function of the above signature.
        // I could not find a way to pass it from the caller of traverseTreeBFS. Thus I had to bind
        // callable to a function which has a placeholder for the first argument, and a binded second
        // argument which will gets called from within callable.
        // The call is made as follows:
        // -(inner function) std::function<std::string(std::shared_ptr<operation>)> printOpType = [] (std::shared_ptr<operation> toPrint) { return std::to_string(toPrint->getOpType()); };
        // -(callable) std::function<void(std::shared_ptr<PatternNode<operation>>&, std::function<std::string(std::shared_ptr<operation>)>)> printPatternWithOpType = std::bind(&printPatternNode, std::placeholders::_1, printOpType);
        // -(calling traverseBFS) tree.traverseTreeBFS(printPatternWithOpType);
        // Thus here callable is bound to a function that has an unknown first arg, but the second arg will be
        // bound to printOpType no matter if we call printPatternWithOpType with a different second arg.
        // Example : printPatternWithOpType(patternNode, someOtherFunction), it will ignore someOtherFunction
        // and just use printOpType.
        callable(currentNode, nullptr);
        q.pop();
        for (auto pChild : currentNode->children()) {
            q.push(pChild);
        }
    }
}

/*
 * Traverses the tree DFS from top to bottom and left to right.
 */
template<typename T, typename returnTypename, typename ... types>
void IPatternTree::traverseTreeDFST2BL2R(std::shared_ptr<IPatternNode> node,
        std::function<void (std::shared_ptr<IPatternNode > &, std::function<returnTypename (std::shared_ptr<T>)>, types... args)> callable,
        types&... args, std::function<void (std::shared_ptr<IPatternNode >) > setup,
        std::function<void (std::shared_ptr<IPatternNode >) > cleanup) {

    // Call the function on the node
    callable(node, nullptr, args...);

    // If node specifies certain condition, then we call setup.
    // Example: if the node is a loop, we add indentation to the body of the loop when printing
    if(setup)
        setup(node);
    for (auto pChild : node->children()) {
        traverseTreeDFST2BL2R(pChild, callable, args...,setup, cleanup);
    }
    // Here we can call the cleanup function. An example would be to decrease indentation.
    if(cleanup)
        cleanup(node);
}


/*
 * Traverses the tree DFS from left to right and bottom to top.
 */
template<typename T, typename returnTypename, typename ... types>
void IPatternTree::traverseTreeDFSL2RB2T(std::shared_ptr<IPatternNode> node,
        std::function<void (std::shared_ptr<IPatternNode > &, std::function<returnTypename (std::shared_ptr<T>)>, types... args)> callable,
        types&... args, std::function<void (std::shared_ptr<IPatternNode >) > setup,
        std::function<void (std::shared_ptr<IPatternNode >) > cleanup) {
    // If node specifies certain condition, then we call setup.
    // Example: if the node is a loop, we add indentation to the body of the loop when printing
    if(setup)
        setup(node);
    for (auto pChild : node->children()) {
        traverseTreeDFSL2RB2T(pChild, callable, args...,setup, cleanup);
    }
    // Here we can call the cleanup function. An example would be to decrease indentation.
    if(cleanup)
        cleanup(node);

    // Call the function on the node
    callable(node, nullptr, args...);
}


/* // Original PatternNode
template<typename T>
class PatternNode : public IPatternNode {
public:
    PatternNode() = delete;
    //PatternNode(const std::shared_ptr<Pattern<T>>& pattern);
    PatternNode(const std::shared_ptr<Pattern<T>>& pattern,
            int parentPatternIndex = -1);
    void addChild(std::shared_ptr<PatternNode<T>> child);
    std::shared_ptr<Pattern<T>>& getPattern() {
        return _pattern;
    }
    std::vector<std::shared_ptr<PatternNode<T>>>& children() {return _children;}
    int getParentPatternIndex() const {return _parentPatternIndex;}
    int& level() { return _level; }
    //int search(const Sequence<T>& s);
private:
    std::shared_ptr<Pattern<T>> _pattern;
    std::vector<std::shared_ptr<PatternNode<T>>> _children;
    int _parentPatternIndex;// This indicates where the child pattern lies in the parent pattern
    int _level;
};

template<typename T>
PatternNode<T>::PatternNode(const std::shared_ptr<Pattern<T>>& pattern,
        int parentPatternIndex) :
        _pattern(pattern), _parentPatternIndex(parentPatternIndex), _level(-1) { // parentIndex = -1 usually for a root or a node that has a filler NOOP parent
}

template<typename T>
void PatternNode<T>::addChild(std::shared_ptr<PatternNode<T>> child) {
    _children.push_back(child);
}
*/ // End of original patternNode class

/*class IPatternNode {
    virtual ~IPatternNode() {}
};

class IPatternTree {
public:
    virtual ~IPatternTree() {}
    virtual std::shared_ptr<IPatternNode> root();
    virtual void setRoot(std::shared_ptr<IPatternNode> root);
    template<typename returnTypename>
    virtual void traverseTreeBFS(
            std::function<void(std::shared_ptr<IPatternNode>& , std::function<returnTypename (std::shared_ptr<T>)>)> callable) ;
    template<typename returnTypename>
    virtual void traverseTreeBFS(std::shared_ptr<IPatternNode> node,
            std::function<void (std::shared_ptr<IPatternNode > &, std::function<returnTypename (std::shared_ptr<T>)>)> callable);
    virtual void traverseTreeT2B(std::shared_ptr<IPatternNode> node); // Traverse tree top to bottom
    virtual void traverseTreeB2T();
    template<typename returnTypename, typename ...types>
    virtual void traverseTreeDFST2BL2R(
            std::function<void (std::shared_ptr<IPatternNode > &, std::function<returnTypename (std::shared_ptr<T>)>, types... args)> callable,
            types&... args, std::function<void (std::shared_ptr<IPatternNode >) > setup = nullptr,
            std::function<void (std::shared_ptr<IPatternNode >) > cleanup = nullptr) ;
    template<typename returnTypename, typename ... types>
    virtual void traverseTreeDFST2BL2R(
            std::shared_ptr<IPatternNode> node,
            std::function<void (std::shared_ptr<IPatternNode > &, std::function<returnTypename (std::shared_ptr<T>)>, types... args)> callable,
            types&... args, std::function<void (std::shared_ptr<IPatternNode >) > setup = nullptr,
            std::function<void (std::shared_ptr<IPatternNode >) > cleanup = nullptr);

    template<typename returnTypename, typename ...types>
    virtual void traverseTreeDFSL2RB2T(
            std::function<void (std::shared_ptr<IPatternNode > &, std::function<returnTypename (std::shared_ptr<T>)>, types... args)> callable,
            types&... args, std::function<void (std::shared_ptr<IPatternNode >) > setup = nullptr,
            std::function<void (std::shared_ptr<IPatternNode >) > cleanup = nullptr) ;
    template<typename returnTypename, typename ... types>
    virtual void traverseTreeDFSL2RB2T(
            std::shared_ptr<IPatternNode> node,
            std::function<void (std::shared_ptr<IPatternNode > &, std::function<returnTypename (std::shared_ptr<T>)>, types... args)> callable,
            types&... args, std::function<void (std::shared_ptr<IPatternNode >) > setup = nullptr,
            std::function<void (std::shared_ptr<IPatternNode >) > cleanup = nullptr);
    virtual int search(const Sequence<T>& sequence);

    virtual void addTree(IPatternTree* anotherTree);

    virtual std::string serialize();
};
*/

/* // Original PatternTree class
template<typename T>
class PatternTree {
public:
    PatternTree() :
            _root(nullptr) {
    }
    PatternTree(const std::shared_ptr<PatternNode<T>>& root) :
            _root(root) {
    }
    std::shared_ptr<PatternNode<T>> root() {
        return _root;
    }
    void setRoot(std::shared_ptr<PatternNode<T>> root) {
        _root = root;
    }
    template<typename returnTypename>
    void traverseTreeBFS(
            std::function<void(std::shared_ptr<PatternNode<T>>& , std::function<returnTypename (std::shared_ptr<T>)>)> callable) {
        traverseTreeBFS(_root, callable);
    }
    template<typename returnTypename>
    void traverseTreeBFS(std::shared_ptr<PatternNode<T>> node,
            std::function<void (std::shared_ptr<PatternNode<T> > &, std::function<returnTypename (std::shared_ptr<T>)>)> callable);
    void traverseTreeT2B(std::shared_ptr<PatternNode<T>> node); // Traverse tree top to bottom
    void traverseTreeB2T();
    template<typename returnTypename, typename ...types>
    void traverseTreeDFST2BL2R(
            std::function<void (std::shared_ptr<PatternNode<T> > &, std::function<returnTypename (std::shared_ptr<T>)>, types... args)> callable,
            types&... args, std::function<void (std::shared_ptr<PatternNode<T> >) > setup = nullptr,
            std::function<void (std::shared_ptr<PatternNode<T> >) > cleanup = nullptr) {
        traverseTreeDFST2BL2R(_root, callable, args..., setup, cleanup);
    }
    template<typename returnTypename, typename ... types>
    void traverseTreeDFST2BL2R(
            std::shared_ptr<PatternNode<T>> node,
            std::function<void (std::shared_ptr<PatternNode<T> > &, std::function<returnTypename (std::shared_ptr<T>)>, types... args)> callable,
            types&... args, std::function<void (std::shared_ptr<PatternNode<T> >) > setup = nullptr,
            std::function<void (std::shared_ptr<PatternNode<T> >) > cleanup = nullptr);

    template<typename returnTypename, typename ...types>
    void traverseTreeDFSL2RB2T(
            std::function<void (std::shared_ptr<PatternNode<T> > &, std::function<returnTypename (std::shared_ptr<T>)>, types... args)> callable,
            types&... args, std::function<void (std::shared_ptr<PatternNode<T> >) > setup = nullptr,
            std::function<void (std::shared_ptr<PatternNode<T> >) > cleanup = nullptr) {
        traverseTreeDFSL2RB2T(_root, callable, args..., setup, cleanup);
    }

    template<typename returnTypename, typename ... types>
    void traverseTreeDFSL2RB2T(
            std::shared_ptr<PatternNode<T>> node,
            std::function<void (std::shared_ptr<PatternNode<T> > &, std::function<returnTypename (std::shared_ptr<T>)>, types... args)> callable,
            types&... args, std::function<void (std::shared_ptr<PatternNode<T> >) > setup = nullptr,
            std::function<void (std::shared_ptr<PatternNode<T> >) > cleanup = nullptr);
    int search(const Sequence<T>& sequence);

    void addTree(PatternTree<T>& anotherTree);

    std::string serialize();
private:
    std::shared_ptr<PatternNode<T>> _root;
};

template<typename T>
std::shared_ptr<Pattern<T>> combinePatterns(const std::shared_ptr<Pattern<T>>& pattern1, const std::shared_ptr<Pattern<T>>& pattern2) {
    std::shared_ptr<Pattern<T>> newPattern = std::shared_ptr<Pattern<T>>(new Pattern<T>(*pattern1));
    // I didn't want to change Pattern class, so I uesd the copy constructor to copy pattern1 into the new pattern
    // and then iterated over the second pattern and added all its elements one by one to the new pattern
    for(auto it = pattern2->begin(); it != pattern2->end(); it++) {
        newPattern->addToPattern(*it);
    }
    return newPattern;
}

template<typename T>
void PatternTree<T>::addTree(PatternTree<T> &anotherTree) {
    if(anotherTree.root() && _root) {
        std::shared_ptr<Pattern<T>> anotherTreePattern = anotherTree.root()->getPattern();
        std::shared_ptr<Pattern<T>> thisTreePattern = _root->getPattern();
        std::shared_ptr<Pattern<T>> newPattern = combinePatterns(thisTreePattern, anotherTreePattern);
        std::shared_ptr<PatternNode<T>> newPatternNode = std::shared_ptr<PatternNode<T>>(new PatternNode<T>(newPattern));
        newPatternNode->addChild(_root);
        newPatternNode->addChild(anotherTree.root());
        _root = newPatternNode;
    }
    else if(!anotherTree.root() && !_root) {
        // both treePatterns are nulls. Shouldn't happen but we need to take care of that. We do
        // that by creating an empty tree
        std::shared_ptr<Pattern<T>> newPattern = std::shared_ptr<Pattern<T>>(new Pattern<T>());
        std::shared_ptr<PatternNode<T>> newPatternNode = std::shared_ptr<PatternNode<T>>(new PatternNode<T>(newPattern));
        _root = newPatternNode;
    }
    else { // only one of them is null. Thus use the one which isnt null and set it as the tree
        std::shared_ptr<Pattern<T>> newPattern = (!anotherTree.root()) ? _root->getPattern() : anotherTree.root()->getPattern();
        std::shared_ptr<PatternNode<T>> newPatternNode = (!anotherTree.root()) ? _root : anotherTree.root();
        _root = newPatternNode;
    }
}

template<typename T> template< typename returnTypename>
void PatternTree<T>::traverseTreeBFS(std::shared_ptr<PatternNode<T>> node,
        std::function<void(std::shared_ptr<PatternNode<T>>&, std::function<returnTypename(std::shared_ptr<T>)>)> callable) {
    std::queue < std::shared_ptr<PatternNode<T>>>q;
    q.push(node);
    while (q.size() > 0) {
        std::shared_ptr<PatternNode<T>> currentNode = q.front();
        // the nullptr in the second argument of callable, should be a function of the above signature.
        // I could not find a way to pass it from the caller of traverseTreeBFS. Thus I had to bind
        // callable to a function which has a placeholder for the first argument, and a binded second
        // argument which will gets called from within callable.
        // The call is made as follows:
        // -(inner function) std::function<std::string(std::shared_ptr<operation>)> printOpType = [] (std::shared_ptr<operation> toPrint) { return std::to_string(toPrint->getOpType()); };
        // -(callable) std::function<void(std::shared_ptr<PatternNode<operation>>&, std::function<std::string(std::shared_ptr<operation>)>)> printPatternWithOpType = std::bind(&printPatternNode, std::placeholders::_1, printOpType);
        // -(calling traverseBFS) tree.traverseTreeBFS(printPatternWithOpType);
        // Thus here callable is bound to a function that has an unknown first arg, but the second arg will be
        // bound to printOpType no matter if we call printPatternWithOpType with a different second arg.
        // Example : printPatternWithOpType(patternNode, someOtherFunction), it will ignore someOtherFunction
        // and just use printOpType.
        callable(currentNode, nullptr);
        q.pop();
        for (auto pChild : currentNode->children()) {
            q.push(pChild);
        }
    }
}


// Traverses the tree DFS from top to bottom and left to right.
template<typename T> template<typename returnTypename, typename ... types>
void PatternTree<T>::traverseTreeDFST2BL2R(std::shared_ptr<PatternNode<T>> node,
        std::function<void (std::shared_ptr<PatternNode<T> > &, std::function<returnTypename (std::shared_ptr<T>)>, types... args)> callable,
        types&... args, std::function<void (std::shared_ptr<PatternNode<T> >) > setup,
        std::function<void (std::shared_ptr<PatternNode<T> >) > cleanup) {

    // Call the function on the node
    callable(node, nullptr, args...);

    // If node specifies certain condition, then we call setup.
    // Example: if the node is a loop, we add indentation to the body of the loop when printing
    if(setup)
        setup(node);
    for (auto pChild : node->children()) {
        traverseTreeDFST2BL2R(pChild, callable, args...,setup, cleanup);
    }
    // Here we can call the cleanup function. An example would be to decrease indentation.
    if(cleanup)
        cleanup(node);
}

// Traverses the tree DFS from left to right and bottom to top.
template<typename T> template<typename returnTypename, typename ... types>
void PatternTree<T>::traverseTreeDFSL2RB2T(std::shared_ptr<PatternNode<T>> node,
        std::function<void (std::shared_ptr<PatternNode<T> > &, std::function<returnTypename (std::shared_ptr<T>)>, types... args)> callable,
        types&... args, std::function<void (std::shared_ptr<PatternNode<T> >) > setup,
        std::function<void (std::shared_ptr<PatternNode<T> >) > cleanup) {
    // If node specifies certain condition, then we call setup.
    // Example: if the node is a loop, we add indentation to the body of the loop when printing
    if(setup)
        setup(node);
    for (auto pChild : node->children()) {
        traverseTreeDFSL2RB2T(pChild, callable, args...,setup, cleanup);
    }
    // Here we can call the cleanup function. An example would be to decrease indentation.
    if(cleanup)
        cleanup(node);

    // Call the function on the node
    callable(node, nullptr, args...);
}
*/ // End of original patterntree class

/*
 * Extracts child patterns from parent Pattern.
 * @patternBreakers is a vector containing all the operations that a pattern can start with
 * @props is the platform properties. It includes the operations that are important or problematic/significant
 */
template<typename T>
void extractChildPatterns(const std::shared_ptr<PatternNode<T>>& patternNode,
        const PlatformProperties& props,
        const std::vector<T>& patternBreakers) {
    std::shared_ptr<Pattern<T>> pattern = patternNode->getPattern();
    //extractChildPatterns(root);
    for (int i = 1; i < pattern->size(); i++) { // should probably start at 1. matching with zero will give us the original pattern and not a child pattern
        typename std::vector<T>::const_iterator it = patternBreakers.begin();
        /*while(it != patternBreakers.end()) {
         if(*(*(pattern->at(i))) == *it) {
         // If the two operations match then we need to create a new pattern AND a new patternNode
         }
         it++;
         }*/
    }
}

template<typename T>
PatternTree<T> buildPatternTree(const Pattern<T>& pattern,
        const PlatformProperties& props,
        const std::vector<T>& patternBreakers) {
    std::shared_ptr<PatternNode<T>> root = std::shared_ptr < PatternNode
            < T >> (new PatternNode<T>(pattern));

    extractChildPatterns(root, props, patternBreakers);
    PatternTree<T> pTree(root);
    return pTree;
}

}
}

#endif // IPATTERNTREE_H

