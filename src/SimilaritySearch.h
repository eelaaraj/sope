#ifndef SIMILARITYSEARCH
#define SIMILARITYSEARCH

#include "Operations.h"
#include "knowledge_base/IKnowledgeBase.h"
#include <spdlog/spdlog.h>

using namespace pattern::matching;

template<typename T>
struct Match;

template<typename T>
std::ostream& operator<<(std::ostream& stream, const Match<T>& match); // This was necessary since compiler was complaining that a non-template version of << was being created

template<typename T>
struct Match {
    //std::shared_ptr<PatternNode<T>> patternNode;
    std::shared_ptr<IPatternNode> coiNode; // added recently to indicate which coi ndoe we matched
    std::shared_ptr<IPatternNode> patternNode; // added early and meant the matching benchmark node
    int matchIndex;
    int longestMatchStartIndex;
    int longestMatchInSequence;
    friend std::ostream& operator<<<>(std::ostream& stream,
            const Match<T>& match);
};

template<typename T>
class SearchFunctor {
public:
    /*SearchFunctor(const Sequence<T>& sequence) :
            _sequence(sequence), _searchResult(-1) {
    }*/
    SearchFunctor(const std::shared_ptr<IPatternTree>& tree) :
            _tree(tree), _searchResult(-1) {
    }
    std::vector<Match<T>>& fullMatches() { return _fullMatches; }
    std::vector<Match<T>>& partialMatches() { return _partialMatches; }
    Match<T>& finalMatch() { return _finalMatch; }
    /*SearchFunctor(const PatternTree<T>& tree) :
            _tree(tree), _searchResult(-1) {
    }*/
    //void operator()(std::shared_ptr<PatternNode<T>> patternNode);
    // Added this operator() overload just to satisfy the PatternTree traverseTreeXXX() format. TODO: Should probably think of a beter solution
    template<typename returnTypename, typename ...types>
    //void operator()(std::shared_ptr<PatternNode<T>> patternNode, std::function<returnTypename(std::shared_ptr<T>, types... args)> callable = nullptr);
    void operator()(std::shared_ptr<IPatternNode> patternNode, std::function<returnTypename(std::shared_ptr<T>, types... args)> callable = nullptr);

private:
    std::vector<Match<T>> _fullMatches;
    std::vector<Match<T>> _partialMatches;
    Match<T> _finalMatch;
    //Sequence<T> _sequence;
    //PatternTree<T> _tree;
    std::shared_ptr<IPatternTree> _tree;
    int _searchResult;
};

template<typename T>
std::ostream& operator<<(std::ostream& stream, const Match<T>& match) {
    stream << "Match : ";
    //printPartOfResult(stream, "Pattern index",
    //		(match.patternNode)->getParentPatternIndex());
    printPartOfResult(stream, "MatchIndex", match.matchIndex);
    printPartOfResult(stream, "PartialMatchIndex",
            match.longestMatchStartIndex);
    printPartOfResult(stream, "PartialMatchLength",
            match.longestMatchInSequence);
    return stream;
}
/*
template<typename T>
void SearchFunctor<T>::operator()(std::shared_ptr<PatternNode<T>> patternNode) {
    //this->_searchResult = patternNode.search(_sequence);
    Match<T> currentMatch;
    KMP<T> kmp(*(patternNode->getPattern()));
    SearchOutput so = kmp.search(_sequence);
    currentMatch.patternNode = patternNode;
    currentMatch.matchIndex = so.matchIndex;
    currentMatch.longestMatchStartIndex = so.partialMatchIndex;
    currentMatch.longestMatchInSequence = so.partialMatchLongestMatch;

    //std::cout << "Search Result = " << this->_searchResult << std::endl;
    // For legacy reasons leave searchResult = matchIndex;
    //this->_searchResult = so.matchIndex;
    //printResult("SearchResult", this->_searchResult);

    std::cout << currentMatch << std::endl;
}
*/
template<typename T>
void printPatternCopiedVersion(Pattern<T>& pattern, std::function<std::string(std::shared_ptr<T>)> callable, std::ostream& os) {
    /*if (pattern.size() == 0)
        std::cout << "pattern is empty/null" << std::endl;
    std::cout << "in print pattern " << std::endl;
    for (int i = 0; i < pattern.size(); i++) {
        std::cout << pattern.at(i).get() << ",";
        std::cout << pattern.at(i).get()->getKind() << ",";
        std::cout << pattern.at(i).get()->getOpType() << ",";
        std::cout << std::endl;
    }*/
    os << "operations: ";
     for (int i = 0; i < pattern.size(); i++) {
         //std::cout << pattern.at(i)->getOpType() << ",";
         os << callable(pattern.at(i)) << ",";
     }
     os << "\n";
}

template<typename T>
void printPatternNodeOperationsCopiedVersion(std::shared_ptr<IPatternNode>& patternNode, std::function<std::string(std::shared_ptr<T>)> callable, std::ostream& os, std::string indentation = "") {
    os << indentation;
    printPatternCopiedVersion(*(patternNode->getPattern()), callable, os);
    os << std::endl;
}

template<typename T>
void printSequenceCopiedVersion(Sequence<T>& pattern, std::function<std::string(std::shared_ptr<T>)> callable, std::ostream& os) {
    os << "operations: ";
     for (int i = 0; i < pattern.size(); i++) {
         //std::cout << pattern.at(i)->getOpType() << ",";
         std::cout << callable(pattern.at(i)) << ",";
     }
     os << "\n";
}
// Note: for now we dont even use callable. As i said this extra argument was added to comply with traverseTree() function signatures.
template<typename T> template<typename returnTypename, typename ...types>
void SearchFunctor<T>::operator()(std::shared_ptr<IPatternNode> patternNode, std::function<returnTypename(std::shared_ptr<T>, types...args)> callable) {
    //this->_searchResult = patternNode.search(_sequence);
    std::ostringstream oss;

    std::function<std::string(std::shared_ptr<T>)> printOpType = [] (std::shared_ptr<T> toPrint) { return std::to_string(static_cast<int>(toPrint->getOpType())); };
    std::function<std::string(std::shared_ptr<T>)> printCode = [] (std::shared_ptr<T> toPrint) { return toPrint->getMainStringLiteral(); };
    std::function<void(std::shared_ptr<IPatternNode>&,
                       std::function<std::string(std::shared_ptr<T>)>, std::string)> printPatternOperations
            = std::bind(&printPatternNodeOperationsCopiedVersion<T>, std::placeholders::_1, printCode, std::ref(oss), std::placeholders::_3);

    oss << "Pattern ";
    printPatternCopiedVersion(*(patternNode->getPattern().get()), printOpType, oss);
    printPatternOperations(patternNode,nullptr, "");
    //printSequenceCopiedVersion(_sequenc;e, printOpType);

    // Traverse the SEQUENCE tree and match each pattern-node in the sequece
    // with the input pattern-node
    Match<T> finalMatch = {nullptr, nullptr, -1, -1, 0};

    /* We keep track of all matches. Later on, we may need to check those to
     * select the best match. One match could be a partial match of 4, and
     * another could be a partial match of 3. Maybe the 3 operations that are
     * matched are more important(have more weight) than the 4 operations
     */
    std::vector<Match<T>> fullMatches;
    std::vector<Match<T>> partialMatches;

    std::function<void(std::shared_ptr<IPatternNode>&,
                       std::function<void(std::shared_ptr<T>)>)> searchLambda
            = [&oss, &finalMatch, &patternNode, printOpType, printPatternOperations, &fullMatches, &partialMatches]
            (std::shared_ptr<IPatternNode> sequencePatternNode, std::function<void(std::shared_ptr<T>)> arg2 = nullptr) {

        // TODO: The following if condition checks if the patterns are not empty. This
        // check can be moved into the KMP search function.
        oss << "Sequence ";
        printPatternCopiedVersion(*(sequencePatternNode->getPattern().get()), printOpType, oss);
        printPatternOperations(sequencePatternNode, nullptr, "");

        if(patternNode->getPattern()->size() > 0 && sequencePatternNode->getPattern()->size() > 0) {
            Match<T> currentMatch;
            KMP<T> kmp(*(patternNode->getPattern()));
            //SearchOutput so = kmp.search(_sequence);
            SearchOutput so = kmp.search(*(sequencePatternNode->getPattern()));
            currentMatch.coiNode = patternNode;
            currentMatch.patternNode = sequencePatternNode;
            currentMatch.matchIndex = so.matchIndex;
            currentMatch.longestMatchStartIndex = so.partialMatchIndex;
            currentMatch.longestMatchInSequence = so.partialMatchLongestMatch;
            if((finalMatch.matchIndex == -1 && currentMatch.matchIndex >=0) // full match
                    || (finalMatch.matchIndex==-1 && finalMatch.longestMatchInSequence < currentMatch.longestMatchInSequence)) {
                finalMatch.coiNode = currentMatch.coiNode;
                finalMatch.patternNode = currentMatch.patternNode;
                finalMatch.matchIndex = currentMatch.matchIndex;
                finalMatch.longestMatchStartIndex = currentMatch.longestMatchStartIndex;
                finalMatch.longestMatchInSequence = currentMatch.longestMatchInSequence;
            }
            if(currentMatch.matchIndex >=0) { // full match
                fullMatches.push_back(currentMatch);
                oss << "** matched fully **\n";
            }
            else if(currentMatch.longestMatchInSequence > 0) { // parital match
                partialMatches.push_back(currentMatch);
                oss << "** matched partially at " << currentMatch.longestMatchStartIndex << ": " << currentMatch.longestMatchInSequence << " **\n";
            }
        }
    };

    // Have to specify template
    _tree->template traverseTreeDFSL2RB2T<T,void>(searchLambda);

    _fullMatches = fullMatches;
    _partialMatches = partialMatches;
    _finalMatch = finalMatch;
    //std::cout << "Search Result = " << this->_searchResult << std::endl;
    // For legacy reasons leave searchResult = matchIndex;
    //this->_searchResult = so.matchIndex;
    //printResult("SearchResult", this->_searchResult);

    oss << "---- Full matches: " << fullMatches.size() << " -- Partial: " << partialMatches.size() << " ----" << std::endl;
    oss << finalMatch;// << std::endl;
    spdlog::get("sopelogger")->debug("{}", oss.str());
}

// HashMatch class and HashSearch functor classes
template<typename T,typename HashType>
struct HashMatch;

template<typename T,typename HashType>
std::ostream& operator<<(std::ostream& stream, const HashMatch<T,HashType>& match); // This was necessary since compiler was complaining that a non-template version of << was being created

template<typename T, typename HashType>
struct HashMatch {
    //std::shared_ptr<PatternNode<T>> patternNode;
    ITreeNode* coiHashNode; // Each hashNode has a pointer to its corresponding patternNode if we need it
    ITreeNode* benchHashNode;
    int matchLevel;
    HashType hashValue;
    friend std::ostream& operator<<<>(std::ostream& stream,
            const HashMatch<T,HashType>& match);
};


template<typename T, typename HashType>
std::ostream& operator<<(std::ostream& stream, const HashMatch<T,HashType>& match) {
    stream << "Hash Match : ";
    /*printPartOfResult(stream, "COINode",
            match.coiHashNode);
    printPartOfResult(stream, "BenchNode",
            match.benchHashNode);*/
    printPartOfResult(stream, "MatchLevel", match.matchLevel);
    printPartOfResult(stream, "Hash", match.hashValue);
    return stream;
}

template<typename T, typename HashType>
class HashSearchFunctor {
public:
    HashSearchFunctor(const ITree* hashTree) :
            _tree(hashTree), _finalMatch{nullptr,nullptr, -1, 0}, _searchResult(-1) {
    }
    std::vector<HashMatch<T,HashType>>& fullMatches() { return _fullMatches; }
    std::vector<HashMatch<T,HashType>>& partialMatches() { return _partialMatches; }
    HashMatch<T,HashType>& finalMatch() { return _finalMatch; }
    // Added this operator() overload just to satisfy the PatternTree traverseTreeXXX() format. TODO: Should probably think of a beter solution
    //template<typename returnTypename, typename ...types>
    void operator()(ITreeNode* hashNode);

private:
    std::vector<HashMatch<T,HashType>> _fullMatches;
    std::vector<HashMatch<T,HashType>> _partialMatches;
    HashMatch<T,HashType> _finalMatch;
    const ITree* _tree;
    int _searchResult;
};

// Note: for now we dont even use callable. As i said this extra argument was added to comply with traverseTree() function signatures.
template<typename T, typename HashType> //template<typename returnTypename, typename ...types>
void HashSearchFunctor<T,HashType>::operator()(ITreeNode* coiNode) {
    std::ostringstream oss;
    HashMatch<T,HashType> match = {nullptr, nullptr, -1, 0};

    /* Keep track of all matches in case we need to analyze those matches. A match
     * at a higher level could be better than a match at a lower level
     */
    std::vector<HashMatch<T,HashType>> fullMatches;
    std::vector<HashMatch<T, HashType>> partialMatches;

    std::function<void(ITreeNode*)> searchLambda
            = [&oss, &match, this, &coiNode, &fullMatches, &partialMatches]
            (ITreeNode* benchmarkNode) {
        if(coiNode != benchmarkNode) { // if they are not the same node
            auto coiHash = dyn_cast<HashNode<T,HashType>>(coiNode);
            auto benchHash = dyn_cast<HashNode<T,HashType>>(benchmarkNode);
            // condition: the two nodes are not null, and the hash values are equal, and this node has a hashvalue > than any of the nodes encountered so far
            if(coiHash && benchHash && ( coiHash->getHash() == benchHash->getHash() ) ) {
                match =  { coiNode, benchmarkNode, benchmarkNode->level(), coiHash->getHash() };
                //std::cout << "Best match so far = " <<  _finalMatch.hashValue << std::endl;
                //std::cout << "Current match = " <<  match.hashValue << std::endl;
                if( _finalMatch.hashValue < coiHash->getHash()) {
                    _finalMatch = match;
                }
                fullMatches.push_back(match);
            }
        }
    };

    // Have to specify template
    const_cast<ITree*>(_tree)->template traverseTreeDFSL2RB2T<void>(searchLambda);

    _fullMatches = fullMatches;
    _partialMatches = partialMatches;
    //_finalMatch = finalMatch;

    oss << "---- Full Hash matches (" << dyn_cast<HashNode<T,HashType>>(coiNode)->getHash() << ") : " << fullMatches.size() << " -- Partial: " << partialMatches.size() << " ----" << std::endl;
    oss << _finalMatch;// << std::endl;
    spdlog::get("sopelogger")->debug("{}", oss.str());
}


//using HashFingerprint = unsigned long;

template<typename T, typename ...types>
using HashFunctionType = std::function<void(std::shared_ptr<PatternNode<T>>&, types&... args)>; //std::function<void(std::shared_ptr<PatternNode<T>>&, std::function<void(std::shared_ptr<T>)>, types&... args)>;

template<typename T, typename HashType>
class RabinKarpHasher {
public:
    //RabinKarpHasher(const PatternTree<T>& tree) : patternTree(tree) {}
    RabinKarpHasher(const std::shared_ptr<IPatternTree>& tree) : patternTree(tree) {}
    std::unique_ptr<HashTree<T,HashType>> hash();
private:
    //HashTree<T,HashType>* hashTree;
    //PatternTree<T> patternTree; // member tree
    std::shared_ptr<IPatternTree> patternTree; // member tree
};

template<typename T, typename HashType>
std::unique_ptr<HashTree<T,HashType>> RabinKarpHasher<T,HashType>::hash() {
    //std::cout << "-- Hashing Tree using Rabin-Karp Hasher --\n";
    spdlog::get("sopelogger")->debug("-- Hashing Tree using Rabin-Karp Hasher --");
    std::stack<HashFingerprint> hashStack;
    std::stack<HashNode<T,HashType>*> hashTreeNodes;
    int nodeCount = 0;

    /*int treeLevel = 0;
    std::function<void (std::shared_ptr<PatternNode<T> >) > setup = [&treeLevel] (std::shared_ptr<PatternNode<T> > patternNode) {
        treeLevel++;
    };
    std::function<void (std::shared_ptr<PatternNode<T> >) > cleanup = [&treeLevel] (std::shared_ptr<PatternNode<T> > patternNode) {
        treeLevel--;
    };*/

    int primeNumber = 101;
    // In the following lambda, I use two ways of passing variables. The first we pass the fingerprint stack using the variadic template
    // in the traverseTreeDFS function. The other way is through capturing. I did the capture for the hashTreeNodes.
    std::function<void(std::shared_ptr<IPatternNode>& patternNode, std::function<void(std::shared_ptr<T>)>, std::stack<HashFingerprint>&)>  hasher =
            [primeNumber, &hashTreeNodes, &nodeCount] (std::shared_ptr<IPatternNode>& patternNode, std::function<void(std::shared_ptr<T>)>, std::stack<HashFingerprint>& hashes) {

        int treeLevel = patternNode->level();
        //std::cout << "Fingerprinting PatternNode " <<  patternNode.get() << " of size = " << patternNode->getPattern()->size() << " at level = " << treeLevel << std::endl;
        spdlog::get("sopelogger")->debug("Fingerprinting PatternNode {} of size = {} at level = {}",  (void*) patternNode.get(), patternNode->getPattern()->size(), treeLevel);
        // add the children hashes AND add the current node's value (value * [prime_number ^ node_level] )
        // the children hashes can be on a stack. The current node will pop as many hashes as it has children.
        int numChildren = patternNode->children().size();
        //std::cout << "---Children count = " << numChildren << std::endl;
        //std::cout << "---Hash stack size = " << hashes.size() << std::endl;
        spdlog::get("sopelogger")->debug("---Children count = {}",numChildren);
        spdlog::get("sopelogger")->debug("---Hash stack size = {}", hashes.size());
        HashFingerprint currentHash = 0;
        for(int i=0; i<numChildren; i++) {
            HashFingerprint childHash = hashes.top();
            hashes.pop();
            currentHash += childHash;
        }
        // TODO: This is not a good value. In this case, Any node that has the same number of children will
        // have the same fingerprint. Instead, we need to look at the contents of the pattern or actually traverse the AST instead of the pattern tree.
        int nodeValue = patternNode->getPattern()->size();
        currentHash += nodeValue * pow(primeNumber,treeLevel);
        //std::cout << "---Current Node hash w/o chilren = " << nodeValue << " * ( " << primeNumber << " ^ " << treeLevel << " ) = " << nodeValue * pow(primeNumber,treeLevel) << std::endl;
        //std::cout << "---Hash = " << currentHash << std::endl;
        spdlog::get("sopelogger")->debug("---Current Node hash w/o chilren = {} * ( {} ^ {} ) = {}", nodeValue, primeNumber, treeLevel, nodeValue * pow(primeNumber,treeLevel));
        spdlog::get("sopelogger")->debug("---Hash = {} ", currentHash);
        hashes.push(currentHash); // Push the current hash which will in turn be a child hash with respect to its parent.

        HashNode<T,HashType>* currentNode = new HashNode<T,HashType>(currentHash, patternNode, nodeCount++);
        // We could have integrated the following loop into the previous for loop that pops the hash fingerprints
        // but it would force us to refactor some code.
        for(int i=0; i<numChildren; i++) {
            HashNode<T,HashType>* childNode = hashTreeNodes.top();
            hashTreeNodes.pop();
            currentNode->addChild(childNode);
        }
        hashTreeNodes.push(currentNode);
    };

    std::function<void(std::shared_ptr<IPatternNode>& , std::function<void(std::shared_ptr<T>)>, std::stack<HashFingerprint>&)> hashFunc =
            std::bind(hasher, std::placeholders::_1, nullptr, std::placeholders::_3);
    patternTree->template traverseTreeDFSL2RB2T<T,void>(hashFunc, hashStack, nullptr, nullptr); // traverse the pattern tree and hashes each sub-tree roted at each pattern node

    auto hashTree = std::unique_ptr<HashTree<T,HashType>>(new HashTree<T,HashType>(hashTreeNodes.top()));
    hashTreeNodes.pop();
    return hashTree;
}

template<typename T>
class FingerprintSearchFunctor {
public:
    //FingerprintSearchFunctor(const PatternTree<T>& tree, HashFunctionType<T> hashFunc) : _tree(tree), _kb(nullptr), hashFunction(hashFunc) {
    FingerprintSearchFunctor(const std::shared_ptr<IPatternTree>& tree, HashFunctionType<T> hashFunc) : _tree(tree), _kb(nullptr), hashFunction(hashFunc) {
        // Hash the tree here if it wasn't hashed before.
    }
    FingerprintSearchFunctor(const HashFingerprint& hash) : _hash(hash), _kb(nullptr) { }

    FingerprintSearchFunctor(IKnowledgeBase* kb, HashFunctionType<T> hashFunc) : _kb(kb), hashFunction(hashFunc) { }
    //template<typename ...types>
    //static void hash(std::shared_ptr<PatternNode<T>> patternNode, HashFunctionType<T,types...> hashingFunction, types&... args);

    template<typename ...types>
    static void hash(std::shared_ptr<PatternNode<T>>& patternNode, std::function<void(std::shared_ptr<T>)>, types&...args);

    // Added operator() to use this class as a hashing class for test purposes.
    void operator()();

    // The callable argument is there to just satisfy the PatternTree traverseTreeXXX() format.
    template<typename returnTypename, typename ...types>
    void operator()(std::shared_ptr<PatternNode<T>> patternNode, std::function<returnTypename(std::shared_ptr<T>, types... args)> callable = nullptr);
    template<typename returnTypename, typename ...types>
    void operator()(std::shared_ptr<IPatternNode> patternNode, std::function<returnTypename(std::shared_ptr<T>, types... args)> callable = nullptr);
private:
    std::vector<Match<T>> _searchResults;
    //PatternTree<T> _tree;
    std::shared_ptr<IPatternTree> _tree;
    HashFingerprint _hash;
    IKnowledgeBase* _kb;
    HashFunctionType<T> hashFunction;
};

template<typename T, typename ...types>
HashFunctionType<T, types...> SomeHashFunction
        = []
        (std::shared_ptr<PatternNode<T>> sequencePatternNode, types&... args) {
    std::cout << "Fingerprinting PatternNode " <<  sequencePatternNode.get() << " of size = " << sequencePatternNode->getPattern()->size() << std::endl;
};

template<typename T, typename ...types>
HashFunctionType<T> RabinKarpHashing
        = []
        (std::shared_ptr<PatternNode<T>> sequencePatternNode, std::stack<HashFingerprint> args) {
    std::cout << "Fingerprinting PatternNode " <<  sequencePatternNode.get() << " of size = " << sequencePatternNode->getPattern()->size() << std::endl;
    // add the children hashes AND add the current node's value (value * [prime_number ^ node_level] )
    // the children hashes can be on a stack. The current node will pop as many hashes as it has children.
    int numChildren = sequencePatternNode->children().size();

};

// The hash function should be able to hash a pattern tree or a subtree rooted
// at a certain patternnode (the pattern node and its children and subchildren ...)
/*template<typename T> template<typename ...types>
void FingerprintSearchFunctor<T>::hash(std::shared_ptr<PatternNode<T>> patternNode, HashFunctionType<T,types...> hashingFunction, types&...args) {
    //tree.traverseTreeDFSL2RB2T(hashingFunction);
    hashingFunction(patternNode, nullptr);
    //return 987654321;
}*/

template<typename T> template<typename ...types>
void FingerprintSearchFunctor<T>::hash(std::shared_ptr<PatternNode<T>>& patternNode, std::function<void(std::shared_ptr<T>)>, types&...args) {
    //tree.traverseTreeDFSL2RB2T(hashingFunction);
    //hashingFunction(patternNode, nullptr);
    //return 987654321;
}


/* Operator() lets me do the following: FingerprintSearchFunctor hasher(treeStore[i]); hasher();
 * This is a convenience method that allows me to test the hashing function without doing any
 * search. The search implementation should be left to the other operator() implementation.
 */
template<typename T>// template<typename ...types>
void FingerprintSearchFunctor<T>::operator()() {
    /* Reminder: The pattern tree is traversed as follows: We get to a pattern node, and call callable on that node (after
     * setup and before cleanup). So if we want to traverse the pattern tree rather than the sequence tree, we will need
     * to pass the pattern tree to the functor rather than the sequence tree.
     * In this case, we are just setting up the mechanism to do fingerprint searches. Thus we will need to pass the pattern
     * tree and traverse that and hash each subtree. Then send the hashes to the Knowledge-base to be matched.
     * In the SearchFuntor class above, we call the oeprator() from ASTSequenceBuilder as follows:
     * -SearchFunctor(SequenceTree);
     * -patternTree.traverse(searchFunctor);
     * What this does is, first it taverses the pattern tree and at each node in the tree, we call the search functor. What
     * that does in return is traverse the sequence tree and match against the pattern node. So it is an n-squared operation.
     * We can do better but it is just a demo for now.
     */
    //std::cout << "-- Hashing Tree using FingerprintSearchFunc Hasher --\n";
    spdlog::get("sopelogger")->debug("-- Hashing Tree using FingerprintSearchFunc Hasher --");
    std::stack<HashFingerprint> hashStack;
    //FingerprintSearchFunctor<T>::hash<>(nullptr, nullptr);
    std::function<int(double,double)> test1 = [] (double, double) { return 5;};
    std::function<int(double,double)> test2 = std::bind(test1,std::placeholders::_1, 2.3);
    test2(3.3,4.0);

    //int maxInt = std::numeric_limits<int>::max();
    /*int treeLevel = 0;
    std::function<void (std::shared_ptr<PatternNode<T> >) > setup = [&treeLevel] (std::shared_ptr<PatternNode<T> > patternNode) {
        treeLevel++;
    };
    std::function<void (std::shared_ptr<PatternNode<T> >) > cleanup = [&treeLevel] (std::shared_ptr<PatternNode<T> > patternNode) {
        treeLevel--;
    };*/

    int primeNumber = 101;
    std::function<void(std::shared_ptr<IPatternNode>& patternNode, std::function<void(std::shared_ptr<T>)>, std::stack<HashFingerprint>&)>  hasher =
            [primeNumber] (std::shared_ptr<IPatternNode>& patternNode, std::function<void(std::shared_ptr<T>)>, std::stack<HashFingerprint>& hashes) {
        //tree.traverseTreeDFSL2RB2T(hashingFunction);
        //hashingFunction(patternNode, nullptr);
        //return 987654321;
        int treeLevel = patternNode->level();
        //std::cout << "Fingerprinting PatternNode " <<  patternNode.get() << " of size = " << patternNode->getPattern()->size() << " at level = " << treeLevel << std::endl;
        spdlog::get("sopelogger")->debug("Fingerprinting PatternNode {} of size = {} at level = {}", (void*)patternNode.get(), patternNode->getPattern()->size(), treeLevel);

        // add the children hashes AND add the current node's value (value * [prime_number ^ node_level] )
        // the children hashes can be on a stack. The current node will pop as many hashes as it has children.
        int numChildren = patternNode->children().size();
        //std::cout << "---Children count = " << numChildren << std::endl;
        //std::cout << "---Hash stack size = " << hashes.size() << std::endl;
        spdlog::get("sopelogger")->debug("---Children count = {}" , numChildren);
        spdlog::get("sopelogger")->debug("---Hash stack size = {}", hashes.size());

        HashFingerprint currentHash = 0;
        for(int i=0; i<numChildren; i++) {
            HashFingerprint childHash = hashes.top();
            hashes.pop();
            currentHash += childHash;
        }
        // TODO: This is not a good value. In this case, Any node that has the same number of children will
        // have the same fingerprint. Instead, we need to look at the contents of the pattern or actually traverse the AST instead of the pattern tree.
        int nodeValue = patternNode->getPattern()->size();
        currentHash += nodeValue * pow(primeNumber,treeLevel);
        //std::cout << "---Current Node hash w/o chilren = " << nodeValue << " * ( " << primeNumber << " ^ " << treeLevel << " ) = " << nodeValue * pow(primeNumber,treeLevel) << std::endl;
        //std::cout << "---Hash = " << currentHash << std::endl;
        spdlog::get("sopelogger")->debug("---Current Node hash w/o chilren = {} * ( {} ^ {} ) = {}", nodeValue, primeNumber, treeLevel, nodeValue * pow(primeNumber,treeLevel));
        spdlog::get("sopelogger")->debug("---Hash = {}", currentHash);
        hashes.push(currentHash); // Push the current hash which will in turn be a child hash with respect to its parent.
    };

   std::function<void(std::shared_ptr<IPatternNode>& , std::function<void(std::shared_ptr<T>)>, std::stack<HashFingerprint>&)> hashFunc =
           std::bind(hasher, std::placeholders::_1, nullptr, std::placeholders::_3);
//   std::function<void(std::shared_ptr<PatternNode<T>>& , std::function<void(std::shared_ptr<T>)>, std::stack<HashFingerprint>)> hashFunc =
//            std::bind(&FingerprintSearchFunctor<T>::hash<std::stack<HashFingerprint>>, std::placeholders::_1, nullptr, std::placeholders::_3);
    _tree->template traverseTreeDFSL2RB2T<T,void>(hashFunc, hashStack, nullptr, nullptr); // traverse the pattern tree and hashes each sub-tree roted at each pattern node
}

template<typename T> template<typename returnTypename, typename ...types>
void FingerprintSearchFunctor<T>::operator()(std::shared_ptr<PatternNode<T>> patternNode, std::function<returnTypename(std::shared_ptr<T>, types... args)> callable) {
    /* Reminder: The pattern tree is traversed as follows: We get to a pattern node, and call callable on that node (after
     * setup and before cleanup). So if we want to traverse the pattern tree rather than the sequence tree, we will need
     * to pass the pattern tree to the functor rather than the sequence tree.
     * In this case, we are just setting up the mechanism to do fingerprint searches. Thus we will need to pass the pattern
     * tree and traverse that and hash each subtree. Then send the hashes to the Knowledge-base to be matched.
     * In the SearchFuntor class above, we call the oeprator() from ASTSequenceBuilder as follows:
     * -SearchFunctor(SequenceTree);
     * -patternTree.traverse(searchFunctor);
     * What this does is, first it taverses the pattern tree and at each node in the tree, we call the search functor. What
     * that does in return is traverse the sequence tree and match against the pattern node. So it is an n-squared operation.
     * We can do better but it is just a demo for now.
     */
//    std::function<void(std::shared_ptr<PatternNode<T>>&,
//                       std::function<void(std::shared_ptr<T>)>)> hashFunc =
//            std::bind(FingerprintSearchFunctor<T>::hash, std::placeholders::_1, SomeHashFunction<T>);
//    _tree.traverseTreeDFSL2RB2T(hashFunc);
}

#endif // SIMILARITYSEARCH
