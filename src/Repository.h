/*
 * Repository.h
 *
 *  Created on: Aug 6, 2015
 *      Author: heisenberg
 */

#ifndef REPOSITORY_H_
#define REPOSITORY_H_

#include <vector>
#include "Operations.h"
#include <spdlog/spdlog.h>

template<typename OperationTypename>
class Repository {
public:
    Repository() {}
    ~Repository() { //std::cout << "destroying repository at " << this << "\n";
                    spdlog::get("sopelogger")->debug("destroying repository at {}",(void*) this);
                  }
	using PatternStore = std::vector<Pattern<OperationTypename>>;
    //using PatternTreeStore = std::vector<PatternTree<OperationTypename>>;
    using PatternTreeStore = std::vector<std::shared_ptr<IPatternTree>>;

    using patternIt = typename PatternStore::iterator;
    using patternTreeIt = typename PatternTreeStore::iterator;
    using SopeAST = std::shared_ptr<OperationTypename>;
    using ASTStore = std::vector<SopeAST>;
    using ASTIt = typename ASTStore::iterator;

	void addPattern(const Pattern<OperationTypename>& pattern) {
		patterns.push_back(pattern);
	}
    /*void addPatternTree(const PatternTree<OperationTypename>& pattern) {
        patternTrees.push_back(pattern);
    }*/
    void addPatternTree(const std::shared_ptr<IPatternTree>& pattern) {
        patternTrees.push_back(pattern);
    }
    void addAbstractSyntaxTree(const std::shared_ptr<OperationTypename> root) {
        asts.push_back(root);
    }

	PatternStore getPatterns() { return patterns; }
    PatternTreeStore getPatternTrees() { return patternTrees; }
    ASTStore getASTs() { return asts; }
private:
	PatternStore patterns;
    PatternTreeStore patternTrees;
    ASTStore asts;
};



#endif /* REPOSITORY_H_ */
