#include "llvm/Pass.h"
#include "llvm/IR/Function.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/IR/LegacyPassManager.h"
#include "llvm/IR/InstrTypes.h"
#include "llvm/Transforms/IPO/PassManagerBuilder.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/Transforms/Utils/BasicBlockUtils.h"
#include "llvm/IR/Module.h"
using namespace llvm;

namespace {
  struct InjectorPass : public FunctionPass {
    static char ID;
    InjectorPass() : FunctionPass(ID) {}

    virtual bool runOnFunction(Function &F) {
      // Get the function to call from our runtime library.
      LLVMContext &Ctx = F.getContext();
      Constant *logopFunc = F.getParent()->getOrInsertFunction(
        "logop", Type::getVoidTy(Ctx), Type::getInt32Ty(Ctx), NULL
      );
	Constant *doubleFunc = F.getParent()->getOrInsertFunction(
        "retDouble", Type::getInt32Ty(Ctx), Type::getInt32Ty(Ctx), NULL
      );

	Constant *getTimeFunc = F.getParent()->getOrInsertFunction(
        "getTime", Type::getVoidTy(Ctx), Type::getInt8PtrTy(Ctx), NULL
      );

	Constant *logFunc = F.getParent()->getOrInsertFunction(
        "log", Type::getVoidTy(Ctx), Type::getInt8PtrTy(Ctx), NULL
      );

      for (auto &B : F) {
        for (auto &I : B) {
          if (auto *op = dyn_cast<BinaryOperator>(&I)) {
            // Insert *after* `op`.
            IRBuilder<> builder(op);
            builder.SetInsertPoint(&B, ++builder.GetInsertPoint());

            // Insert a call to our function.
            Value* args[] = {op};
	    Value* tim = builder.CreateCall(getTimeFunc, {});
	    Value* logTim = builder.CreateCall(logFunc, tim);
	    Value* doub = builder.CreateCall(doubleFunc, args);
            Value* o = builder.CreateCall(logopFunc, doub);

            return true;
          }
        }
      }

      return false;
    }
  };
}

char InjectorPass::ID = 0;

// Automatically enable the pass.
// http://adriansampson.net/blog/clangpass.html -- Left this to give credit
static void registerInjectorPass(const PassManagerBuilder &,
                         legacy::PassManagerBase &PM) {
  PM.add(new InjectorPass());
}
static RegisterStandardPasses
  RegisterMyPass(PassManagerBuilder::EP_EarlyAsPossible,
                 registerInjectorPass);
