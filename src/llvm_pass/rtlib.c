#include <stdio.h>
#include <string>
#include <ctime>
#include <iostream>
#include <iomanip>
#include <chrono>
#include <algorithm>

void logop(int i) {
    printf("computed: %i\n", i);
}

void log(std::string s) {
    std::cout << "s=" << s << std::endl;
}

int retDouble(int i) {
    return i*2;
}

std::string getTime() {
	std::time_t now = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
	std::string stringTime = std::ctime(&now);
    	std::replace(stringTime.begin(), stringTime.end(), ' ', '-');
	return stringTime;
}
