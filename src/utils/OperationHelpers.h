/*
 * OperationHelpers.h
 *
 *  Created on: Aug 5, 2015
 *      Author: heisenberg
 */

#ifndef OPERATIONHELPERS_H_
#define OPERATIONHELPERS_H_

#include "../Operations.h"
#include "../Comparator.h"
#include "../IPatternTree.h"
#include "TypeDefinitions.h"
#include <iomanip>
#include "dirent.h" // Needed to navigate directories in unix usign c++
using namespace pattern::matching;

void expect(const std::string& s) {
	std::cout << "Expect ";
	std::cout << s << std::endl;
}

void title(const std::string& s) {
	std::cout << std::endl << std::string(s.size() + 8, '*') << std::endl;
	std::cout << "--- " << s << " ---" << std::endl;
	std::cout << std::string(s.size() + 8, '*') << std::endl;
}

void printResult(const std::string& label, int r, int w = 3) {
	std::cout << std::endl << label << " = ";
	std::cout << std::setw(w) << std::right << r << std::endl;
}

void printPattern(Pattern<operation>& pattern, std::function<std::string(std::shared_ptr<operation>)> callable, std::ostream& os) {
	/*if (pattern.size() == 0)
		std::cout << "pattern is empty/null" << std::endl;
	std::cout << "in print pattern " << std::endl;
	for (int i = 0; i < pattern.size(); i++) {
		std::cout << pattern.at(i).get() << ",";
		std::cout << pattern.at(i).get()->getKind() << ",";
		std::cout << pattern.at(i).get()->getOpType() << ",";
		std::cout << std::endl;
	}*/
    //std::cout << " operations: ";
    os << " operations: ";
	 for (int i = 0; i < pattern.size(); i++) {
         //std::cout << pattern.at(i)->getOpType() << ",";
         //std::cout << callable(pattern.at(i)) << ",";
         os << callable(pattern.at(i)) << ",";
	 }
}

void printPatternNode(std::shared_ptr<IPatternNode>& patternNode, std::function<std::string(std::shared_ptr<operation>)> callable, std::ostream& os) {
    /*std::cout << std::endl << "Pattern:";
	std::cout << std::left << " | size : " << std::setw(3) << std::right
			<< patternNode->getPattern()->size();
	std::cout << std::left << " | index : " << std::setw(3) << std::right
			<< patternNode->getParentPatternIndex() << " |";
    printPattern(*(patternNode->getPattern()), callable, os);
    std::cout << std::endl;*/

    os << std::endl << "Pattern:";
    os << std::left << " | size : " << std::setw(3) << std::right
            << patternNode->getPattern()->size();
    os << std::left << " | index : " << std::setw(3) << std::right
            << patternNode->getParentPatternIndex() << " |";
    printPattern(*(patternNode->getPattern()), callable, os);
    os << std::endl;
}

//void printPatternNodeOperations(std::shared_ptr<PatternNode<operation>>& patternNode, std::function<std::string(std::shared_ptr<operation>)> callable, std::ostream& os, std::string indentation = "") {
void printPatternNodeOperations(std::shared_ptr<IPatternNode>& patternNode, std::function<std::string(std::shared_ptr<operation>)> callable, std::ostream& os, std::string indentation = "") {
    /*if(patternNode->level()!=-1) {
        std::cout << std::setfill(' ') << std::setw(2*patternNode->level()) << "";
    }
    else {
        std::cout << indentation;
    }
    std::cout << " level: " << patternNode->level();
    printPattern(*(patternNode->getPattern()), callable, os);
    std::cout << std::endl;*/

    if(patternNode->level()!=-1) {
        os << std::setfill(' ') << std::setw(2*patternNode->level()) << "";
    }
    else {
        os << indentation;
    }
    os << " level: " << patternNode->level();
    printPattern(*(patternNode->getPattern()), callable, os);
    os << std::endl;
}

void displayMetrics(const std::map<int, std::map<int, std::map<int, double>>>& metrics) {
	std::map<int, std::map<int, std::map<int, double>>>::const_iterator platformIt = metrics.begin();
	while(platformIt != metrics.end()) {
		std::cout << "platform_id = " << std::setw(3) << std::right << platformIt->first << std::endl;
		std::map<int, std::map<int, double>>::const_iterator seqIt = platformIt->second.begin();
		while(seqIt != platformIt->second.end()) {
			std::cout << "   sequence_id = " << std::setw(2) << std::right << seqIt->first << std::endl;
			std::map<int, double>::const_iterator metricIt = seqIt->second.begin();
			while(metricIt != seqIt->second.end()) {
				std::cout << "      metric_id " << std::setw(5) << std::right << "=" << std::setw(5) << metricIt->first << std::endl;
				std::cout << "      metric_value " << std::setw(2) << std::right << "=" << std::setw(5) << metricIt->second << std::endl;
				metricIt++;
			}
			seqIt++;
		}
		platformIt++;
	}
}

Pattern<operation> createCroutPattern() {
	/* This is the crout code that we will manually create the pattern for
	 *
	 void Crout(int d,double*S,double*D){
	 for(int k=0;k<d;++k){
	 for(int i=k;i<d;++i){
	 double sum=0.;
	 for(int p=0;p<k;++p)sum+=D[i*d+p]*D[p*d+k];
	 D[i*d+k]=S[i*d+k]-sum; // not dividing by diagonals
	 }
	 for(int j=k+1;j<d;++j){
	 double sum=0.;
	 for(int p=0;p<k;++p)sum+=D[k*d+p]*D[p*d+j];
	 D[k*d+j]=(S[k*d+j]-sum)/D[k*d+k];
	 }
	 }
	 }
	 */
	// Creates the first for loop k
	Pattern<operation> p1;
	OperationType op = OperationType::loop;
    ParallelType pt = ParallelType::none;
	int numOper = 2;
	OperandType operType[2]; // = new OperandType[2];
	operType[0] = OperandType::direct;
	operType[1] = OperandType::immediate;
	int iterCount = -1;
    int loopId = 0;
	std::string iterLiteral = "k";
	std::string iterStart = "0";
	std::string iterEnd = "d";
	std::shared_ptr<operation> sptr = std::shared_ptr < operation
            > (new loopOperation(operation::OK_LoopOperation, loopId, iterCount,
					iterLiteral, iterStart, iterEnd, pt, numOper, operType));
	p1.addToPattern(sptr);
	// creates the second for loop i from k->d
	iterLiteral = "i";
	iterStart = "k";
	iterEnd = "d";
	sptr = std::shared_ptr < operation
            > (new loopOperation(operation::OK_LoopOperation, loopId, iterCount,
					iterLiteral, iterStart, iterEnd, pt, numOper, operType));
	p1.addToPattern(sptr);
	//creates the third for loop j from 0->d
	iterLiteral = "p";
	iterStart = "0";
	iterEnd = "k";
	sptr = std::shared_ptr < operation
            > (new loopOperation(operation::OK_LoopOperation,loopId, iterCount,
					iterLiteral, iterStart, iterEnd, pt, numOper, operType));
	p1.addToPattern(sptr);
	// creates two memory access operations for both D[] in the third loop
	op = OperationType::memoryLoad;
	AccessType access = AccessType::contiguous; // contiguous in the deepest loop
	std::string indexLiteral = "i*d+p";
	sptr = std::shared_ptr < operation
			> (new memOperation(operation::OK_MemoryOperation, access, "",
					indexLiteral, op, pt, numOper, operType));
	p1.addToPattern(sptr);

	access = AccessType::strobed; // contiguous in the deepest loop
	indexLiteral = "p*d+k";
	sptr = std::shared_ptr < operation
			> (new memOperation(operation::OK_MemoryOperation, access, "",
					indexLiteral, op, pt, numOper, operType));
	p1.addToPattern(sptr);
	// creates two operations (a multiply and add) in the third loop
	op = OperationType::multiply;
	sptr = std::shared_ptr < operation
			> (new binaryOperation(operation::OK_BinaryOperation, op, "", "", "", pt, numOper,
					operType));
	p1.addToPattern(sptr);
	// add operation
	op = OperationType::add;
	sptr = std::shared_ptr < operation
			> (new binaryOperation(operation::OK_BinaryOperation, op, "", "", "", pt, numOper,
					operType));
	p1.addToPattern(sptr);
	// creates two memory operations (One memory store for D and one memory read for S)and one subtract.
	// The last two memory ops are strobed
	// S[] load
	op = OperationType::memoryLoad;
	access = AccessType::strobed;
	indexLiteral = "i*d+k";
	sptr = std::shared_ptr < operation
			> (new memOperation(operation::OK_MemoryOperation, access, "",
					indexLiteral, op, pt, numOper, operType));
	p1.addToPattern(sptr);
	// Subtract
	op = OperationType::add; // Its a subtraction but addition should work as well
	sptr = std::shared_ptr < operation
			> (new binaryOperation(operation::OK_BinaryOperation, op, "", "", "", pt, numOper,
					operType));
	p1.addToPattern(sptr);
	// D[] store
	op = OperationType::memoryStore;
	sptr = std::shared_ptr < operation
			> (new memOperation(operation::OK_MemoryOperation, access, "",
					indexLiteral, op, pt, numOper, operType));
	p1.addToPattern(sptr);

	// Creates the second nested loop j from k+1 -> d
	/*for(int j=k+1;j<d;++j){
	 double sum=0.;
	 for(int p=0;p<k;++p)sum+=D[k*d+p]*D[p*d+j];
	 D[k*d+j]=(S[k*d+j]-sum)/D[k*d+k];
	 }*/
	iterLiteral = "j";
	iterStart = "k+1";
	iterEnd = "d";
	sptr = std::shared_ptr < operation
            > (new loopOperation(operation::OK_LoopOperation, loopId, iterCount,
					iterLiteral, iterStart, iterEnd, pt, numOper, operType));
	p1.addToPattern(sptr);
	//creates the third for loop j from 0->d
	iterLiteral = "p";
	iterStart = "0";
	iterEnd = "k";
	sptr = std::shared_ptr < operation
            > (new loopOperation(operation::OK_LoopOperation, loopId, iterCount,
					iterLiteral, iterStart, iterEnd, pt, numOper, operType));
	p1.addToPattern(sptr);
	// creates two memory access operations for both D[] in the third loop
	op = OperationType::memoryLoad;
	access = AccessType::contiguous; // contiguous in the deepest loop
	indexLiteral = "k*d+p";
	sptr = std::shared_ptr < operation
			> (new memOperation(operation::OK_MemoryOperation, access, "",
					indexLiteral, op, pt, numOper, operType));
	p1.addToPattern(sptr);

	access = AccessType::strobed; // contiguous in the deepest loop
	indexLiteral = "p*d+j";
	sptr = std::shared_ptr < operation
			> (new memOperation(operation::OK_MemoryOperation, access, "",
					indexLiteral, op, pt, numOper, operType));
	p1.addToPattern(sptr);
	// creates two operations (a multiply and add) in the third loop
	op = OperationType::multiply;
	sptr = std::shared_ptr < operation
			> (new binaryOperation(operation::OK_BinaryOperation, op, "", "", "", pt, numOper,
					operType));
	p1.addToPattern(sptr);
	// add operation
	op = OperationType::add;
	sptr = std::shared_ptr < operation
			> (new binaryOperation(operation::OK_BinaryOperation, op, "", "", "", pt, numOper,
					operType));
	p1.addToPattern(sptr);
	// creates two memory operations (One memory store for D and one memory read for S)and one subtract.
	// The last two memory ops are strobed
	// S[] load
	op = OperationType::memoryLoad;
	access = AccessType::contiguous;
	indexLiteral = "k*d+j";
	sptr = std::shared_ptr < operation
			> (new memOperation(operation::OK_MemoryOperation, access, "",
					indexLiteral, op, pt, numOper, operType));
	p1.addToPattern(sptr);
	// Subtract
	op = OperationType::add; // Its a subtraction but addition should work as well
	sptr = std::shared_ptr < operation
			> (new binaryOperation(operation::OK_BinaryOperation, op, "", "", "", pt, numOper,
					operType));
	p1.addToPattern(sptr);
	// Divide
	op = OperationType::multiply; // Its a divide but estimate as divide
	sptr = std::shared_ptr < operation
			> (new binaryOperation(operation::OK_BinaryOperation, op, "", "", "", pt, numOper,
					operType));
	p1.addToPattern(sptr);
	// D[] store
	op = OperationType::memoryStore;
	sptr = std::shared_ptr < operation
			> (new memOperation(operation::OK_MemoryOperation, access, "",
					indexLiteral, op, pt, numOper, operType));
	p1.addToPattern(sptr);

	expect("Pattern p1.size() = 20");
	std::cout << p1.size() << std::endl;

	return p1;
}

Sequence<operation> createIncompleteCroutSequence() {
	// Creates the first for loop k
	Sequence<operation> s1;
	OperationType op = OperationType::loop;
    ParallelType pt = ParallelType::none;
	int numOper = 2;
	OperandType operType[2]; // = new OperandType[2];
	operType[0] = OperandType::direct;
	operType[1] = OperandType::immediate;
	int iterCount = -1;
    int loopId = 0;
	std::string iterLiteral = "k";
	std::string iterStart = "0";
	std::string iterEnd = "d";
	// Added this duplicate operation to test the matching
	std::shared_ptr<operation> sptr = std::shared_ptr < operation
            > (new loopOperation(operation::OK_LoopOperation, loopId, iterCount,
					iterLiteral, iterStart, iterEnd, pt, numOper, operType));
	s1.addToSequence(sptr);

	sptr = std::shared_ptr < operation
            > (new loopOperation(operation::OK_LoopOperation, loopId, iterCount,
					iterLiteral, iterStart, iterEnd, pt, numOper, operType));
	s1.addToSequence(sptr);
	// creates the second for loop i from k->d
	iterLiteral = "i";
	iterStart = "k";
	iterEnd = "d";
	sptr = std::shared_ptr < operation
            > (new loopOperation(operation::OK_LoopOperation, loopId, iterCount,
					iterLiteral, iterStart, iterEnd, pt, numOper, operType));
	s1.addToSequence(sptr);
	//creates the third for loop j from 0->d
	iterLiteral = "p";
	iterStart = "0";
	iterEnd = "k";
	sptr = std::shared_ptr < operation
            > (new loopOperation(operation::OK_LoopOperation, loopId, iterCount,
					iterLiteral, iterStart, iterEnd, pt, numOper, operType));
	s1.addToSequence(sptr);
	// creates two memory access operations for both D[] in the third loop
	op = OperationType::memoryLoad;
	AccessType access = AccessType::contiguous; // contiguous in the deepest loop
	std::string indexLiteral = "i*d+p";
	sptr = std::shared_ptr < operation
			> (new memOperation(operation::OK_MemoryOperation, access, "",
					indexLiteral, op, pt, numOper, operType));
	s1.addToSequence(sptr);

	access = AccessType::strobed; // contiguous in the deepest loop
	indexLiteral = "p*d+k";
	sptr = std::shared_ptr < operation
			> (new memOperation(operation::OK_MemoryOperation, access, "",
					indexLiteral, op, pt, numOper, operType));
	s1.addToSequence(sptr);
	// creates two operations (a multiply and add) in the third loop
	op = OperationType::multiply;
	sptr = std::shared_ptr < operation
			> (new binaryOperation(operation::OK_BinaryOperation, op, "", "", "", pt, numOper,
					operType));
	s1.addToSequence(sptr);
	// add operation
	op = OperationType::add;
	sptr = std::shared_ptr < operation
			> (new binaryOperation(operation::OK_BinaryOperation, op, "", "", "", pt, numOper,
					operType));
	s1.addToSequence(sptr);
	// creates two memory operations (One memory store for D and one memory read for S)and one subtract.
	// The last two memory ops are strobed
	// S[] load
	op = OperationType::memoryLoad;
	access = AccessType::strobed;
	indexLiteral = "i*d+k";
	sptr = std::shared_ptr < operation
			> (new memOperation(operation::OK_MemoryOperation, access, "",
					indexLiteral, op, pt, numOper, operType));
	s1.addToSequence(sptr);
	// Subtract
	op = OperationType::add; // Its a subtraction but addition should work as well
	sptr = std::shared_ptr < operation
			> (new binaryOperation(operation::OK_BinaryOperation, op, "", "", "", pt, numOper,
					operType));
	s1.addToSequence(sptr);
	// D[] store
	op = OperationType::memoryStore;
	sptr = std::shared_ptr < operation
			> (new memOperation(operation::OK_MemoryOperation, access, "",
					indexLiteral, op, pt, numOper, operType));
	s1.addToSequence(sptr);

	expect("Sequence crout.size() = 11");
	std::cout << s1.size() << std::endl;

	return s1;
}

Sequence<operation> createDolittleSequence() {
	// Creates the first for loop k
	Sequence<operation> s1;
	OperationType op = OperationType::loop;
    ParallelType pt = ParallelType::none;
	int numOper = 2;
	OperandType operType[2]; // = new OperandType[2];
	operType[0] = OperandType::direct;
	operType[1] = OperandType::immediate;
	int iterCount = -1;
    int loopId = 0;
	std::string iterLiteral = "k";
	std::string iterStart = "0";
	std::string iterEnd = "d";

	std::shared_ptr<operation> sptr = std::shared_ptr < operation
            > (new loopOperation(operation::OK_LoopOperation, loopId, iterCount,
					iterLiteral, iterStart, iterEnd, pt, numOper, operType));
	s1.addToSequence(sptr);

	// creates the second for loop i from k->d
	iterLiteral = "j";
	iterStart = "k";
	iterEnd = "d";
	sptr = std::shared_ptr < operation
            > (new loopOperation(operation::OK_LoopOperation, loopId, iterCount,
					iterLiteral, iterStart, iterEnd, pt, numOper, operType));
	s1.addToSequence(sptr);
	//creates the third for loop j from 0->d
	iterLiteral = "p";
	iterStart = "0";
	iterEnd = "k";
	sptr = std::shared_ptr < operation
            > (new loopOperation(operation::OK_LoopOperation, loopId, iterCount,
					iterLiteral, iterStart, iterEnd, pt, numOper, operType));
	s1.addToSequence(sptr);
	// creates two memory access operations for both D[] in the third loop
	op = OperationType::memoryLoad;
	AccessType access = AccessType::contiguous; // contiguous in the deepest loop
	std::string indexLiteral = "k*d+p";
	sptr = std::shared_ptr < operation
			> (new memOperation(operation::OK_MemoryOperation, access, "",
					indexLiteral, op, pt, numOper, operType));
	s1.addToSequence(sptr);

	access = AccessType::strobed; // contiguous in the deepest loop
	indexLiteral = "p*d+j";
	sptr = std::shared_ptr < operation
			> (new memOperation(operation::OK_MemoryOperation, access, "",
					indexLiteral, op, pt, numOper, operType));
	s1.addToSequence(sptr);
	// creates two operations (a multiply and add) in the third loop
	op = OperationType::multiply;
	sptr = std::shared_ptr < operation
			> (new binaryOperation(operation::OK_BinaryOperation, op, "", "", "", pt, numOper,
					operType));
	s1.addToSequence(sptr);
	// add operation
	op = OperationType::add;
	sptr = std::shared_ptr < operation
			> (new binaryOperation(operation::OK_BinaryOperation, op, "", "", "", pt, numOper,
					operType));
	s1.addToSequence(sptr);
	// creates two memory operations (One memory store for D and one memory read for S)and one subtract.
	// The last two memory ops are strobed
	// S[] load
	op = OperationType::memoryLoad;
	access = AccessType::contiguous;
	indexLiteral = "k*d+j";
	sptr = std::shared_ptr < operation
			> (new memOperation(operation::OK_MemoryOperation, access, "",
					indexLiteral, op, pt, numOper, operType));
	s1.addToSequence(sptr);
	// Subtract
	op = OperationType::add; // Its a subtraction but addition should work as well
	sptr = std::shared_ptr < operation
			> (new binaryOperation(operation::OK_BinaryOperation, op, "", "", "", pt, numOper,
					operType));
	s1.addToSequence(sptr);
	// D[] store
	op = OperationType::memoryStore;
	sptr = std::shared_ptr < operation
			> (new memOperation(operation::OK_MemoryOperation, access, "",
					indexLiteral, op, pt, numOper, operType));
	s1.addToSequence(sptr);

	// Creates the second nested loop
	// creates the second for loop i from k+1->d
	iterLiteral = "i";
	iterStart = "k+1";
	iterEnd = "d";
	sptr = std::shared_ptr < operation
            > (new loopOperation(operation::OK_LoopOperation, loopId, iterCount,
					iterLiteral, iterStart, iterEnd, pt, numOper, operType));
	s1.addToSequence(sptr);
	//creates the third for loop p from 0->d
	iterLiteral = "p";
	iterStart = "0";
	iterEnd = "k";
	sptr = std::shared_ptr < operation
            > (new loopOperation(operation::OK_LoopOperation, loopId, iterCount,
					iterLiteral, iterStart, iterEnd, pt, numOper, operType));
	s1.addToSequence(sptr);
	// creates two memory access operations for both D[] in the third loop
	op = OperationType::memoryLoad;
	access = AccessType::contiguous; // contiguous in the deepest loop
	indexLiteral = "i*d+p";
	sptr = std::shared_ptr < operation
			> (new memOperation(operation::OK_MemoryOperation, access, "",
					indexLiteral, op, pt, numOper, operType));
	s1.addToSequence(sptr);

	access = AccessType::strobed; // contiguous in the deepest loop
	indexLiteral = "p*d+k";
	sptr = std::shared_ptr < operation
			> (new memOperation(operation::OK_MemoryOperation, access, "",
					indexLiteral, op, pt, numOper, operType));
	s1.addToSequence(sptr);
	// creates two operations (a multiply and add) in the third loop
	op = OperationType::multiply;
	sptr = std::shared_ptr < operation
			> (new binaryOperation(operation::OK_BinaryOperation, op, "", "", "", pt, numOper,
					operType));
	s1.addToSequence(sptr);
	// add operation
	op = OperationType::add;
	sptr = std::shared_ptr < operation
			> (new binaryOperation(operation::OK_BinaryOperation, op, "", "", "", pt, numOper,
					operType));
	s1.addToSequence(sptr);
	// creates two memory operations (One memory store for D and one memory read for S)and one subtract.
	// The last two memory ops are strobed
	// S[] load
	op = OperationType::memoryLoad;
	access = AccessType::strobed;
	indexLiteral = "i*d+k";
	sptr = std::shared_ptr < operation
			> (new memOperation(operation::OK_MemoryOperation, access, "",
					indexLiteral, op, pt, numOper, operType));
	s1.addToSequence(sptr);
	// Subtract
	op = OperationType::add; // Its a subtraction but addition should work as well
	sptr = std::shared_ptr < operation
			> (new binaryOperation(operation::OK_BinaryOperation, op, "", "", "", pt, numOper,
					operType));
	s1.addToSequence(sptr);
	// Divide
	op = OperationType::multiply; // Its a subtraction but addition should work as well
	sptr = std::shared_ptr < operation
			> (new binaryOperation(operation::OK_BinaryOperation, op, "", "", "", pt, numOper,
					operType));
	s1.addToSequence(sptr);
	// D[] store
	op = OperationType::memoryStore;
	sptr = std::shared_ptr < operation
			> (new memOperation(operation::OK_MemoryOperation, access, "",
					indexLiteral, op, pt, numOper, operType));
	s1.addToSequence(sptr);

	expect("Sequence dolittle.size() = 20");
	std::cout << s1.size() << std::endl;

	return s1;
}

template<typename T>
PatternTree<T> createCroutPatternTree(const Pattern<T>& pattern) {
	/*void Crout(int d,double*S,double*D){
	 for(int k=0;k<d;++k){
	 for(int i=k;i<d;++i){
	 double sum=0.;
	 for(int p=0;p<k;++p)sum+=D[i*d+p]*D[p*d+k];
	 D[i*d+k]=S[i*d+k]-sum; // not dividing by diagonals
	 }
	 for(int j=k+1;j<d;++j){
	 double sum=0.;
	 for(int p=0;p<k;++p)sum+=D[k*d+p]*D[p*d+j];
	 D[k*d+j]=(S[k*d+j]-sum)/D[k*d+k];
	 }
	 }
	 }*/
	typedef std::shared_ptr<PatternNode<T>> spn;
	typedef std::shared_ptr<Pattern<T>> sp;

	sp rootPattern = sp(new Pattern<T>(pattern));
	spn root = spn(new PatternNode<T>(rootPattern, -1));

	sp p1 = sp(new Pattern<T>(pattern.subPattern(1, 9)));
	spn child1 = spn(new PatternNode<T>(p1, 1));
	root->addChild(child1);

	// The second nested loop
	sp p2 = sp(new Pattern<T>(pattern.subPattern(10, 19)));
	spn child2 = spn(new PatternNode<T>(p2, 10));
	root->addChild(child2);

	// The third loop plus the last statement of the parent loop
	sp p1p1 = sp(new Pattern<T>(pattern.subPattern(2, 9)));
	spn c1child1 = spn(new PatternNode<T>(p1p1, 2));
	child1->addChild(c1child1);

	sp p2p1 = sp(new Pattern<T>(pattern.subPattern(11, 19)));
	spn c2child1 = spn(new PatternNode<T>(p2p1, 11));
	child2->addChild(c2child1);

	// Only the third loop
	sp p1p2 = sp(new Pattern<T>(pattern.subPattern(2, 6)));
	spn c1child2 = spn(new PatternNode<T>(p1p2, 2));
	child1->addChild(c1child2);
	c1child1->addChild(c1child2);

	sp p2p2 = sp(new Pattern<T>(pattern.subPattern(11, 15)));
	spn c2child2 = spn(new PatternNode<T>(p2p2, 11));
	child2->addChild(c2child2);
	c2child1->addChild(c2child2);

	PatternTree<T> tree(root);
	//tree.traverseTreeBFS(printPattern);
	return tree;
}

#endif /* OPERATIONHELPERS_H_ */
