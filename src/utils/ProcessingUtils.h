#ifndef PROCESSINGUTILS_H
#define PROCESSINGUTILS_H

#include "../knowledge_base/MockUtils.h"
#include "clang/Analysis/CFGStmtMap.h"
#include "clang/Basic/SourceManager.h"
#include "../estimator/Estimator.h"
#include "../static_analyzer/StaticAnalyzer.h"
#include "../Repository.h"

json loadJson(const std::string jsonUrl) {
    return deserializeJsonFile(jsonUrl);
}

json loadIndex(const std::string jsonUrl) {
    return loadJson(jsonUrl);
}

void printDBIndex(json dbIndex) {
    json benchmarks = dbIndex["benchmarks"];
    for (json::iterator it = benchmarks.begin(); it != benchmarks.end(); ++it) {
        //std::cout << "Path = " << (*it)["path"] << ", Function = " << (*it)["function"] << std::endl;
        spdlog::get("sopelogger")->debug("Path = {}, Function = {}", (*it)["path"].dump(), (*it)["function"].dump());
    }
}

void doTraversePatternJson(json patternTree, int level, std::ostream& os) {
    level++;
    for (json::iterator it = patternTree.begin(); it != patternTree.end(); ++it) {
        json ops = (*it)["ops"];
        os << std::setfill(' ') << std::setw(2*level) << "" << ops.dump() << std::endl;
        json patternChildren = (*it)["children"];
        doTraversePatternJson(patternChildren, level, os);
    }
}

void traversePatternJson(json patternTree, int level) {
    std::ostringstream oss;
    doTraversePatternJson(patternTree, level, oss);
    spdlog::get("sopelogger")->debug("{}", oss.str());
}

DefUse* runDefUseAnalysis(FunctionDecl* function, Stmt* functionBody, ASTContext* context, CFG* cfg, bool verbose) {
    // Getting DefUse at this point. We already have the AST and PatternTree here
    DefUse* du = DefUse::BuildDefUseChains(function, functionBody, context, 0, cfg, 0, verbose);
    // End of getting DefUse
    return du;
}

void queryDefUse(DefUse* du, CFG* cfg, std::shared_ptr<ParseState> parseState, ITreeQueryResult* treeQueryResult) {
    // Find the block that contains the matched subtree
    // We can use the already created parseState->functionParentMap and CFGStmtMap to get the blockId
    // of the result subtree.
    std::cout << "\n======================" << std::endl;
    std::cout << "Querying DefUse Chains" << std::endl;
    std::cout << "======================" << std::endl;
    ASTContext* context = parseState->Context;
    // Solution from : http://stackoverflow.com/questions/13760197/how-to-get-basic-block-id-of-a-statement-inside-visit-methods
    std::unique_ptr<CFGStmtMap> cfgMap = llvm::make_unique<CFGStmtMap>(*(CFGStmtMap::Build(cfg, parseState->functionParentMap)));
    std::vector<QueryMatch> matchSet;
    if(ASTTreeQueryResult* res = dyn_cast<ASTTreeQueryResult>(treeQueryResult)) {
        matchSet = res->matchSet;
    }
    else if (HashTreeQueryResult* res = dyn_cast<HashTreeQueryResult>(treeQueryResult)) {
        matchSet = res->matchSet;
    }
    //ASTTreeQueryResult::MatchSet matchSet = dyn_cast<ASTTreeQueryResult>(treeQueryResult)->matchSet;
    std::cout << "HERE matchset size = " << matchSet.size() << std::endl;
    for(QueryMatch& match : matchSet) {
        std::cout << "Matched node has pattern of size = " << match.coiNode->getPattern()->size() << std::endl;
        unsigned blockId = cfgMap->getBlock(match.coiNode->getPattern()->at(0)->getUnderlyingPtr())->getBlockID();
        //unsigned blockId = cfgMap->getBlock(match.resultTreeOperation->getUnderlyingPtr())->getBlockID();
        std::cout << "Matched subtree rooted at blockId = " <<  blockId << std::endl;
        // This returns the variables that are used (actually the declrefexpr that reach this block) in the block
        std::vector<DeclRefExpr const*> BlockDataRefs = du->queryRefBlockData(blockId);
        std::vector<VarDecl const*> BlockDataDecls = du->queryDeclBlockData(blockId);

        clang::BeforeThanCompare<SourceLocation> before = clang::BeforeThanCompare<SourceLocation>(context->getSourceManager());
        for(DeclRefExpr const* ref : BlockDataRefs) {
            bool isBefore = before(ref->getLocStart(), match.coiNode->getPattern()->at(0)->getUnderlyingPtr()->getLocStart());
            //bool isBefore = before(ref->getLocStart(), match.resultTreeOperation->getUnderlyingPtr()->getLocStart());
            std::cout << "Checking if DeclRefExpr " << ref->getNameInfo().getAsString() << " is defined before the current scope ? " << (isBefore?"yep":"nope") << std::endl;
            if(isBefore) {
                    std::cout << "Inject:_captureVar(" << ref->getDecl()->getNameAsString() << ") at line = "
                              << context->getSourceManager().getSpellingLineNumber(ref->getLocStart()) + 1 << std::endl;
            }
        }
        for(VarDecl const* decl : BlockDataDecls) {
            bool isBefore = before(decl->getLocStart(), match.coiNode->getPattern()->at(0)->getUnderlyingPtr()->getLocStart());
            //bool isBefore = before(decl->getLocStart(), match.resultTreeOperation->getUnderlyingPtr()->getLocStart());
            std::cout << "Checking if VarDecl " << decl->getNameAsString() << " is defined before the current scope ? " << (isBefore?"yep":"nope") << std::endl;
            if(isBefore) {
                    std::cout << "Inject:_captureVar(" << decl->getNameAsString() << ") at line = "
                              << context->getSourceManager().getSpellingLineNumber(decl->getLocStart()) + 1 << std::endl;
            }
        }
        std::cout << "Inject: _captureVar(ALL_LOOP_VARIABLES) at line = "
                  << match.coiNode->getPattern()->at(0)->getUnderlyingPtr()->getLocStart().printToString(context->getSourceManager()) << std::endl;
                  //<< match.resultTreeOperation->getUnderlyingPtr()->getLocStart().printToString(context->getSourceManager()) << std::endl;
        std::cout << "Capturing the key variables of the loop (basically iteration 0) can help us understand "
                     "more what's happening in the loop. After that capture, we need to exit the loop or the "
                     "whole application somehow __terminate__()." << std::endl;
    }
    // End-of finding the block
}


template<typename T>
//void deserializePatternTree(json patternTreeJson, std::stack<std::shared_ptr<PatternNode<T>>>& parentStack) {
/*std::unique_ptr<PatternTree<T>>*/ void deserializePatternTree(json patternTreeJson, std::stack<std::shared_ptr<IPatternNode>>& parentStack) {

    for (json::iterator it = patternTreeJson.begin(); it != patternTreeJson.end(); ++it) {
        json ops = (*it)["ops"];
        std::shared_ptr<Pattern<T>> pat = std::make_shared<Pattern<T>>();
        /*for(json::iterator opIt = patternTree.begin(); opIt != patternTree.end(); ++opIt) {
            int type = (*it)["type"];*/
            /*enum class OperationType {
                0 memory, 1 datapath, 2 memoryLoad, 3 memoryStore, 4 add, 5 multiply, 6 subtract, 7 divide, 8 loop, 9 condition, 10 assign, 11 none,
                12 less, greater, equals,
                15 le,
                16 ge,
                17 ne,
                18 function,
                19 ifCondition,
                20 elseCondition,
                21 compoundStmt,
                22 numeric,
                23 call, // call expression equivalent in clang. This is base class for all other calls
                24 cxx_member_call, // member function call
                25 cxx_operator_call, // operator function call
            };*/
            /*std::shared_ptr<T> op;
            if(type==2 || type == 3) {
                op = makeSubscriptOperation(nullptr, type);
            }
            else if(type == 4 || type == 5 || type == 6 || type ==7 || type == 12 || type == 13 || type == 14 || type == 15 || type == 16 || type == 17) {
                op = makeBinaryOperation(nullptr);
            }
            else if(type == 8) {
                op = makeForOperation(nullptr);
            }
            else if(type == 9) {
                op = makeIfOperation(nullptr);
            }
            else if(type == 10) { // TODO: figure out a way to identidy compound operations
                op = makeBinaryOperation(nullptr);
            }
            else if(type == 18) {
                op = makeFunctionOperation(nullptr);
            }
            else if(type == 21) {
                op = makeCompoundOperation(nullptr);
            }
            else if(type == 22) { // TODO: We must find a way to different between a numeric floatOperation and a numeric intOperation
                op = makeIntLiteralOperation(nullptr);
            }
            else if(type == 23 || type == 24) {
                op = makeCallOperation(nullptr, type);
            }
            pat->addToPattern(op);
        }*/
        json patternChildren = (*it)["children"];
        //std::shared_ptr<PatternNode<T>> node = std::make_shared<PatternNode<T>>(pat);
        std::shared_ptr<IPatternNode> node = std::make_shared<PatternNode<T>>(pat);

        //node->getPattern() = pat;
        node->level() = (*it)["level"];
        if(!parentStack.empty()) {
            parentStack.top()->addChild(node);
        }
        parentStack.push(node);
        deserializePatternTree<T>(patternChildren, parentStack);
    }
    if(parentStack.size() == 1) {
        // create the patern tree and add this node as root
        auto root = parentStack.top();
        parentStack.pop();
        //PatternTree<T> pt(root);
        std::shared_ptr<IPatternTree> pt = std::make_shared<PatternTree<T>>(root);
        //std::function<std::string(std::shared_ptr<operation>)> printCode = [] (std::shared_ptr<operation> toPrint) { return toPrint->getMainStringLiteral(); };
        std::ostringstream oss;
        std::function<void(std::shared_ptr<IPatternNode>&,
                           std::function<std::string(std::shared_ptr<operation>)>, std::string)> printPatternOperations
                       = std::bind(&printPatternNodeOperations, std::placeholders::_1, nullptr, std::ref(oss), std::placeholders::_3);
        std::string indent = ""; // had to create an empty strign and pass indent to traverse instead of passing "" directly inline. This is because "" is a char* and not a string, so the template specialization will have to be traverseDFS<string,char*>
        //pt.template traverseTreeDFST2BL2R<std::string, std::string>(printPatternOperations, indent);
        pt->template traverseTreeDFST2BL2R<T,std::string, std::string>(printPatternOperations, indent);
        spdlog::get("sopelogger")->debug("{}", oss.str());
    }
    else if(!parentStack.empty()) {
        parentStack.pop();
    }
}

template<typename T, typename HashType>
/*std::unique_ptr<PatternTree<T>>*/ void deserializeHashTree(json hashTreeJson, std::stack<ITreeNode*>& parentStack) {
    for (json::iterator it = hashTreeJson.begin(); it != hashTreeJson.end(); ++it) {
        json hashChildren = (*it)["children"];
        HashType hashVal = (*it)["value"];
        ITreeNode* node = new HashNode<T,HashType>(hashVal, nullptr); // TODO: make the patternNode point to a valid node by searching for it in the patternTree
        //node->level() = (*it)["level"];
        if(!parentStack.empty()) {
            parentStack.top()->addChild(node);
        }
        parentStack.push(node);
        deserializeHashTree<T,HashType>(hashChildren, parentStack);
    }
    if(parentStack.size() == 1) {
        // create the patern tree and add this node as root
        auto root = parentStack.top();
        parentStack.pop();
        HashTree<T,HashType> pt(dyn_cast<HashNode<T,HashType>>(root));
        std::ostringstream oss;
        std::function<void(ITreeNode*)> printNode = [&oss](ITreeNode* node) { oss << /*std::setfill(' ') << std::setw(2*node->level()) << "" <<*/ (dyn_cast<HashNode<T,HashType>>(node))->getHash() << std::endl; };
        pt.template traverseTreeDFST2BL2R<void>(printNode);
        spdlog::get("sopelogger")->debug("{}",oss.str());
    }
    else if(!parentStack.empty()) {
        parentStack.pop();
    }
}

void doTraverseHashJson(json hashTree, int level, std::ostream& os) {
    level++;
    for (json::iterator it = hashTree.begin(); it != hashTree.end(); ++it) {
        json hash = (*it)["value"];
        os << std::setfill(' ') << std::setw(2*level) << "" << hash << std::endl;
        json hashChildren = (*it)["children"];
        doTraverseHashJson(hashChildren, level, os);
    }
}

void traverseHashJson(json hashTree, int level) {
    std::ostringstream oss;
    doTraverseHashJson(hashTree, level, oss);
    spdlog::get("sopelogger")->debug("{}", oss.str());
}

void searchJsonDB(json dbIndex) {
    json benchmarks = dbIndex["benchmarks"];
    for (json::iterator it = benchmarks.begin(); it != benchmarks.end(); ++it) {
        json ptree = loadJson((*it)["patterntree"])["nodes"];
        json htree = loadJson((*it)["hashtree"])["nodes"];
        //std::cout << "\n === PatternTree " << (*it)["function"] << " JSON ===" << std::endl;
        spdlog::get("sopelogger")->debug("\n === PatternTree {} JSON ===", (*it)["function"].dump());
        traversePatternJson(ptree, 0);
        //std::stack<std::shared_ptr<PatternNode<operation>>> patternParentStack;
        std::stack<std::shared_ptr<IPatternNode>> patternParentStack;
        //std::cout << "\n === Deserializing " << (*it)["function"] << " JSON into PatternTree ===" << std::endl;
        spdlog::get("sopelogger")->debug("\n === Deserializing {} JSON into PatternTree ===", (*it)["function"].dump());
        deserializePatternTree<operation>(ptree, patternParentStack);
        //std::cout << "\n === HashTree " << (*it)["function"] << " JSON ===" << std::endl;
        spdlog::get("sopelogger")->debug("\n === HashTree {} JSON ===", (*it)["function"].dump());
        traverseHashJson(htree,0);
        std::stack<ITreeNode*> hashParentStack;
        //std::cout << "\n === Deserializing " << (*it)["function"] << " JSON into HashTree ===" << std::endl;
        spdlog::get("sopelogger")->debug("\n === Deserializing {} JSON into HashTree ===", (*it)["function"].dump());
        deserializeHashTree<operation,HashFingerprint>(htree, hashParentStack);
        //std::cout << hashTree.dump(2) << std::endl;
    }
}

// Loads the captured variables from the database (whether json file, xml,
// or even physical db like mongo) and loads them into an in-memory DB
json loadCapturedData(const std::string jsonUrl, int runId) {
    return loadJson(jsonUrl);
}

using SopeAST = Repository<operation>::SopeAST;
// Prepares the treeQuery. TODO: replace createMockFingerprintQuery() with this function in MockUtils or DBUtils
ITreeQuery* prepareTreeQuery(SopeAST ast, std::shared_ptr<IPatternTree> patternTree, int runId) {
    HashTreeQuery<operation,HashFingerprint>* query = new HashTreeQuery<operation,HashFingerprint>();
    query->runId = runId;
    query->ast = ast;
    query->patternTree = patternTree;
    //FingerprintSearchFunctor<operation> fsf = FingerprintSearchFunctor<operation>(patternTree, nullptr);
    //fsf(); // hashes the pattern tree
    RabinKarpHasher<operation,HashFingerprint> hasher = RabinKarpHasher<operation,HashFingerprint>(patternTree);
    query->fingerprintTree = hasher.hash();
    return query;
}

// Prepares the metric query.
// TODO: replace benchmarkId, with the shared_ptr<IBenchmark> because at this point we should
// hve already queried for matching benchmarks and gotten the result back. If we ONLY use
// benchId, then we have to query the KB for the benchmark mtchign that ID and therefore
// we incur the cost of an extra unecessary query
IMetricQuery* prepareMetricQuery(IDataPointCollection* dps, long benchId, long runId) {
    IMetricQuery* query = new ASTMetricQuery(benchId, runId, dps);
    return query;
}

// Inserts a tree in the KB
ITreeQueryResult* insertTree(IKnowledgeBase* kb, ITreeQuery* query) {
    return kb->storeCodeTree(query);
}

// Queries the KB for records that match to the COI
ITreeQueryResult* queryTrees(IKnowledgeBase* kb, ITreeQuery*& treeQuery) {
    ITreeQueryResult* treeQueryResult = kb->queryCodeTree(treeQuery);
    return treeQueryResult;
}

// Inserts metrics for a specific benchmark
void insertMetrics(IKnowledgeBase* kb, IMetricQuery* query, ITreeQueryResult* treeQueryResult) {
    kb->storeCodeMetrics(query, treeQueryResult);
}

// Queries the KB for metrics of the matched benchmarked. The datapoints have
// been already perpared in the metricQuery object
IMetricQueryResult* queryMetrics(IKnowledgeBase* kb, IMetricQuery*& metricQuery, ITreeQueryResult* treeQueryResult) {
    IMetricQueryResult* mqr = kb->queryCodeMetrics(metricQuery, treeQueryResult);
    return mqr;
}

void queryTreeAndMetrics(IKnowledgeBase* kb, SopeAST ast, std::shared_ptr<IPatternTree> patternTree, IDataPointCollection* dps, int runId) {
    ITreeQuery* treeQuery = prepareTreeQuery(ast, patternTree, runId);
    ITreeQueryResult* tqResult = queryTrees(kb, treeQuery);
    std::shared_ptr<IBenchmark> bench = tqResult->getMatches().front().matchingBench;
    IMetricQuery* metricQuery = prepareMetricQuery(dps, bench->Id(), runId);
    IMetricQueryResult* mqResult = queryMetrics(kb, metricQuery, tqResult);

    // we delete the tree queries since we don't need them anymore.
    // We don't delete the metrics result and its query since we need them in Estimator
    delete tqResult;
    delete treeQuery;
}

// Tests the KB/DB fucntionalities. In reality, we don't go through all of those operations.
// We usually query if there are matching trees. Then somewhere else, we request metrics for
// those the matched trees.
void queryMatchingTrees(ITreeQuery*& treeQuery, ITreeQueryResult*& treeQueryResult, IMetricQuery*& metricQuery, IMetricQueryResult*& metricQueryResult, std::shared_ptr<ParseState> parseState) {
    // Start-of Knowledge base queries
    std::cout << "\n======================\n";
    std::cout << "Querying KnowledgeBase\n";
    std::cout << "======================\n";
    MockASTKnowledgeBase concreteMock = MockASTKnowledgeBase();
    IKnowledgeBase* mockBase = &concreteMock;
    treeQuery = createMockQuery(parseState->repo->getASTs().front());
    treeQueryResult = mockBase->queryCodeTree(treeQuery);
    /*ASTMetricQuery concreteMetricQuery = ASTMetricQuery();
    // Creating a vector of datapoints to query
    DataPointVector dpv;
    DataPoint<int> dp1 = DataPoint<int>(100);
    DataPoint<int> dp2 = DataPoint<int>(101);
    DataPoint<int> dp3 = DataPoint<int>(102);
    dpv.add(&dp1); dpv.add(&dp2); dpv.add(&dp3);
    concreteMetricQuery.queryPoints = &dpv;
    // End of creating IDataPointCollection*/
    metricQuery = createMockMetricQuery();
    metricQueryResult = mockBase->queryCodeMetrics(metricQuery, treeQueryResult);
    mockBase->storeCodeTree(treeQuery);
    mockBase->storeCodeMetrics(metricQuery, treeQueryResult);
    // End of querying KnowledgeBase
}

void queryMatchingTreeFingerprints(ITreeQuery*& treeQuery, ITreeQueryResult*& treeQueryResult, IMetricQuery*& metricQuery, IMetricQueryResult*& metricQueryResult, std::shared_ptr<ParseState> parseState, int runId) {
    // Start-of Knowledge base queries
    std::cout << "\n==========================\n";
    std::cout << "Querying Hash KnowledgeBase\n";
    std::cout << "===========================\n";
    IStorage* storage = new FileStorage("./db/index.json");
    MockHashKnowledgeBase concreteMock = MockHashKnowledgeBase(storage);
    IKnowledgeBase* mockBase = &concreteMock;
    treeQuery = createMockFingerprintQuery(parseState->repo->getASTs().front(), parseState->repo->getPatternTrees().front(), runId);
    treeQueryResult = mockBase->queryCodeTree(treeQuery);
    for(QueryMatch& treeMatch: treeQueryResult->getMatches()) {
        spdlog::get("sopelogger")->debug("Found tree match with id = {}", treeMatch.matchingBench->Id());
    }
    /*ASTMetricQuery concreteMetricQuery = ASTMetricQuery();
    // Creating a vector of datapoints to query
    DataPointVector dpv;
    DataPoint<int> dp1 = DataPoint<int>(100);
    DataPoint<int> dp2 = DataPoint<int>(101);
    DataPoint<int> dp3 = DataPoint<int>(102);
    dpv.add(&dp1); dpv.add(&dp2); dpv.add(&dp3);
    concreteMetricQuery.queryPoints = &dpv;
    // End of creating IDataPointCollection*/
    //metricQuery = createMockMetricQuery();
    metricQuery = createEmptyMetricQuery(96565394, 1);
    metricQueryResult = mockBase->queryCodeMetrics(metricQuery, treeQueryResult);
    mockBase->storeCodeTree(treeQuery);
    mockBase->storeCodeMetrics(metricQuery, treeQueryResult);
    delete storage; // Need to deleted storage because the destructor will serialize the json object which contains all the changes we have made.
    // End of querying KnowledgeBase
}

/* Does the Injection/Instrumentation. Takes the randomly-generated ID as input and
 * passes it to the injector. The injector then injects the required functions together
 * with the passed ID. When the instrumented executable is ran, the captured variables
 * will be Id'ed using that unique ID. When the in-memory captured data is serialized
 * to a XML or JSON, this ID will be used as part of the filename, and will also be included
 * in the file as one of the properties.
 */
void doInstrumentation(std::string path, std::string filename, std::string commaSeperatedFunctions, int runId) {
    std::cout << "\n=====================" << std::endl;
    std::cout << "Instrumenting File(s)" << std::endl;
    std::cout << "=====================" << std::endl;
    json config = loadJson("./config.json");
    std::string space = " ";
    std::string llvmSrcPath = config["llvm-src"];
    std::string llvmBuildPath = config["/llvm-build/path"_json_pointer];
    std::string binPath = llvmBuildPath + config["/llvm-build/bin-path"_json_pointer].dump();
    std::string clangExec = binPath + config["/llvm-build/clang"_json_pointer].dump();
    std::string optExec = binPath + config["/llvm-build/opt"_json_pointer].dump();
    std::string llcExec = binPath + config["/llvm-build/llc"_json_pointer].dump();
    std::string capturerPath = config["/capturer/path"_json_pointer];
    std::string captureLibPath = "";
    for(std::string objectFile : config["/capturer/libs"_json_pointer]) {
        captureLibPath += capturerPath + objectFile + space;
    }
    std::string passLibPath = config["/injector/path"_json_pointer].dump() + config["/injector/so"_json_pointer].dump();
    std::string passName = "-"+config["/injector/pass"_json_pointer].dump();
    std::string passOptions = "--functions="+commaSeperatedFunctions; // "--options --id=runId";
/*
    std::string llvmSrcPath = "$HOME/Documents/build-3.7.1";
    std::string clangPath =  "$HOME/Documents/build-3.7.1";//"$HOME/Documents/clang-3.7.1";
    std::string binPath = clangPath + "/bin";
    std::string clangExec =  binPath + "/clang++ ";
    std::string optExec = binPath + "/opt";
    std::string llcExec = binPath + "/llc";
    std::string captureLibPath = "../build-metric-capture-injector-Clang_3_7_1-Default/metric_capture/CMakeFiles/MetricCapture.dir/rtlib.cpp.o ../build-metric-capture-injector-Clang_3_7_1-Default/metric_capture/CMakeFiles/MetricCapture.dir/FileSystem.cpp.o";
    std::string passLibPath = "../build-metric-capture-injector-Clang_3_7_1-Default/injector/libInjectorPass.so"; // "./src/llvm_pass/build/injector/libInjectorPass.so";
    std::string passName = "-injector";// "-inject";
    std::string passOptions = "--functions="+commaSeperatedFunctions; // "--options --id=runId";

    std::string llvmCXXConfig = "`" + binPath+"/llvm-config --cxxflags`";
    std::string clangIncludes = "-I"+llvmSrcPath+"/tools/clang/include" + space + "-I"+llvmSrcPath+"/tools/clang/include";
    std::string llvmLDConfig = "`" + binPath+"/llvm-config --ldflags --libs --system-libs`";
    std::string clangLibs = "-Wl,--start-group -lclangAST -lclangAnalysis -lclangBasic -lclangDriver -lclangEdit -lclangFrontend -lclangFrontendTool -lclangLex "
            "-lclangParse -lclangSema -lclangEdit -lclangASTMatchers -lclangRewrite -lclangRewriteFrontend -lclangStaticAnalyzerFrontend -lclangStaticAnalyzerCheckers "
            "-lclangStaticAnalyzerCore  -lclangSerialization -lclangToolingCore -lclangTooling -Wl,--end-group";
    clangLibs = "-Wl,--start-group -lclangAST -lclangAnalysis -lclangBasic -lclangDriver -lclangEdit -lclangFrontend -lclangFrontendTool -lclangLex -lclangParse -lclangSema -lclangEdit -lclangASTMatchers -lclangRewrite -lclangRewriteFrontend -lclangStaticAnalyzerFrontend -lclangStaticAnalyzerCheckers -lclangStaticAnalyzerCore  -lclangSerialization -lclangToolingCore -lclangTooling -Wl,--end-group";
*/
    std::string bcFile = filename + ".bc";
    std::string bcInstrumented = filename + "_inst.bc";
    std::string asmFile = filename + ".s";
    std::string objFile = filename + ".o";

    // 1- Compile code into llvm bitcode: clang -emit-llvm code.c -c -o code.bc
    std::string cmd = clangExec /*+ space + llvmCXXConfig + space + llvmLDConfig + space + clangLibs + space + clangIncludes + space*/ + " -emit-llvm " + filename + space+ "-c -std=c++11 -O0 -o" + space + bcFile;
    int returnVal = std::system(cmd.c_str());
    std::cout << "Command: " << cmd << " returned " << returnVal << std::endl;
    // 2- Run the clang optimizer OPT : opt -load mypass.so -mypass --options < code.bc > code_inst.bc.
    cmd = optExec + " -load " + passLibPath + space + passName + space + passOptions + " < " + bcFile + " > " + bcInstrumented;
    returnVal = std::system(cmd.c_str());
    std::cout << "Command: " << cmd << " returned " << returnVal << std::endl;
    // 3- Compile the bitcode into assembly: llc code.bc -o code.s ;
    cmd = llcExec + space + bcInstrumented + " -o " + asmFile;
    returnVal = std::system(cmd.c_str());
    std::cout << "Command: " << cmd << " returned " << returnVal << std::endl;
    // 4- Use your favorite assembler and Link against everything else we need: gcc code.s -o code.o
    cmd = clangExec + space /*"clang++ "*/ + asmFile + space + captureLibPath + space + "-std=c++11 -o" + space + objFile;
    returnVal = std::system(cmd.c_str());
    std::cout << "Command: " << cmd << " returned " << returnVal << std::endl;
    // 5- Run the final instrumented executable: ./code.o
    cmd = objFile + space + "200";
    returnVal = std::system(cmd.c_str());
    std::cout << "Command: " << cmd << " returned " << returnVal << std::endl;
}

// Runs an executable
void runInstrumentedExecutable(const std::string& command, int runId) {
    std::cout << "\n===============================" << std::endl;
    std::cout << "Running Instrumented Executable" << std::endl;
    std::cout << "===============================" << std::endl;
    std::string cmd = "../TestingProjectExec -r"+std::to_string(runId);
    int returnVal = std::system(cmd.c_str());
    std::cout << "Command: " << cmd << " returned " << returnVal << std::endl;
}
int generateRunId() {
    std::srand(std::time(0));
    int uniqueID = rand();
    return uniqueID;
}

void runStaticAnalysis(std::shared_ptr<operation> oper, DefUse* defUse) {
    std::cout << "\n==============" << std::endl;
    std::cout << "StaticAnalyzer" << std::endl;
    std::cout << "==============" << std::endl;
    ScopeAnalyzer sa = ScopeAnalyzer(oper, defUse);
    sa.analyze();
}

json getFunctionCaptures(json& captures, const std::string& functionName) {
    json funcs = captures["functions"];
    json foundRecord = nullptr;
    for (json::iterator it = funcs.begin(); it != funcs.end(); ++it) {
        std::string funcName = (*it)["name"];
        if(funcName.find(functionName) != std::string::npos) {
            foundRecord = *it;
            break;
        }
    }
    return foundRecord;
}

json runDynamicAnalysis(int runId) {
    pid_t pid = fork();
    char* argv[] = {"-al",NULL};
    if(pid ==0) {
        // If execv fails, it will use the parent image. Basically if it can't find the executable,
        // it will fallback to running the image of the parent. Thus we will see the child do the
        // the same as the parent and we will see the console print double what it prints usually.
        execv("/bin/ls", argv);
        std::cout << "After EXEC" << std::endl; // this should not print since we changed the image that the child proc is running.
    }
    else if(pid>0) {
        std::cout << "PID = " << pid << std::endl;
    }
    else {
        std::cout << "ERROR in FORK" << std::endl;
    }

    std::cout << "\n================" << std::endl;
    std::cout << "Dynamic Analyzer" << std::endl;
    std::cout << "================" << std::endl;
    /* 1- run the injector using information from the def-use analysis to instrument capture calls
     *    for the key variables.
    */
    doInstrumentation("path/to/file", "./test/test3.cpp", "Crouti", runId);
    //doInstrumentation("path/to/file", "./src/llvm_pass/example.cpp", runId);

    std::string file;
    DefUse* du;
    // runId will be passed into the Capturer's init function where it will be stored until the capturer
    // is closed when it will be used to serialized the captured data into a file ot physical db.
    //::instrumentCode(file, du, runId);
    /*
     * 2a- Capture system information + system state
     *
     * 2b- Run the code-of-interest instrumented executable to capture the realtime values of those key vars
     *    Each run will have a randomly-generated unique ID that will be used as part of the session ID or
     *    filename that will store the captured variables after they are serialzied from the DB.
    */
    std::string hwfile = "./db/hw" + std::to_string(runId) + ".json";
    std::string cmd = "lshw -sanitize -json > " + hwfile;
    int returnVal = std::system(cmd.c_str());
    std::cout << "Command: " << cmd << " returned " << returnVal << std::endl;

    ::runInstrumentedExecutable("mockExec", runId);
    /* 3- Verbose: Print the var values. These values have to be read from a file or a physical DB like
     *    mongodb. This is because the instrumented executable will be ran in a different process and unless
     *    we implement inter-process comm, we will have to write the results (i.e. the captured variables
     *    in our im-memory db) into a physical file or db.
    */
    std::string execName = "mockExec";
    runId = 123456;
    std::string uniqueFileName = "./test/"+execName+std::to_string(runId)+".json"; // ./test/mockExec123456.json
    json captureDb = loadCapturedData(uniqueFileName, runId);
// Uncomment the following block to allow for json querying. This works on clang that is compiled with exceptions and cxx-exceptions. Thus heisenberg but not vostro
/*    std::cout << "\n=====================" << std::endl;
    std::cout << "Querying Capture JSON" << std::endl;
    std::cout << "=====================" << std::endl;
    std::cout << "Capture run_id = " << captureDb["run_id"] << std::endl;
    json funcs = captureDb["functions"];
    std::string hardcodedName = "int main(int, char*[])";
    json foundRecord = nullptr;
    std::cout << "found record should be null ? " << foundRecord.is_null() << std::endl;
    for (json::iterator it = funcs.begin(); it != funcs.end(); ++it) {
        if((*it)["name"] == hardcodedName) {
            foundRecord = *it;
            std::cout << *it << "\n";
            break;
        }
    }
    std::cout << "found record should not be null ? " << foundRecord.is_null() << std::endl;
    // We found a json record for the function we captured.
    // Now get the variables we captured in that function
    json foundVar = nullptr;
    if(!foundRecord.is_null()) {
        json capturedVars = foundRecord["captured_vars"];
        std::string hardcodedVarName = "k";
        for (json::iterator it = capturedVars.begin(); it != capturedVars.end(); ++it) {
            if((*it)["name"] == hardcodedVarName) {
                foundVar = *it;
                std::cout << "Printing the whole record = " << *it << "\n";
                std::cout << "Extracting the value of k = " << (*it)["value"] << "\n";
                break;
            }
        }
    }
*/
    // printCapturedData();
    return captureDb;
}

// static_cast is used to convert "enum class" values to ints.
std::function<std::string(std::shared_ptr<operation>)> printOpKind = [] (std::shared_ptr<operation> toPrint) { return std::to_string(toPrint->getKind()); };
std::function<std::string(std::shared_ptr<operation>)> printOpType = [] (std::shared_ptr<operation> toPrint) { return std::to_string(static_cast<int>(toPrint->getOpType())); };
std::function<std::string(std::shared_ptr<operation>)> printCode = [] (std::shared_ptr<operation> toPrint) { return toPrint->getMainStringLiteral(); };
std::function<bool(std::shared_ptr<operation>)> predicateAlwaysTrue = [] (std::shared_ptr<operation> toCheck) { return true; };
std::function<bool(std::shared_ptr<operation>)> predicateNoVarDecls = [] (std::shared_ptr<operation> toCheck) {
    return !dyn_cast<varDeclOperation>(toCheck.get());
};

template<typename T>
//PatternTree<T> createPatternTree(clfunctionOperation* func) {
std::shared_ptr<IPatternTree> createPatternTree(clfunctionOperation* func) {

    std::ostringstream oss;
    oss << "=== Creating Pattern Tree ===" << std::endl;
    std::function<void(std::shared_ptr<IPatternNode>&,
                       std::function<std::string(std::shared_ptr<operation>)>)> printPatternOperations
            = std::bind(&printPatternNodeOperations, std::placeholders::_1, printCode, std::ref(oss), "");

    // This lambda will be run on cleanup while traversing nodes in the pattern tree. It will assign
    // levels to nodes. Leaf nodes are at level 0 even if they appear higher in the tree. All other non-leaf
    // nodes are at max(childrenLevels) + 1;
    std::function<void(std::shared_ptr<IPatternNode>)> nodeLevelCleanup =
            [] (std::shared_ptr<IPatternNode> node) {
        int currentNodeLevel = -1;
        if(node->children().size() == 0) {
            node->level() = 0;
        }
        else {
            std::for_each(node->children().begin(), node->children().end(), [&currentNodeLevel](std::shared_ptr<IPatternNode> child) { currentNodeLevel = std::max(currentNodeLevel, child->level()); } );
            node->level() = currentNodeLevel + 1;
        }
    };

    //pattern::processing::BruteForceTreeCreator<operation> bfTreeCreator(func->statements());
    pattern::processing::HierarchicalTreeCreator<operation> bfTreeCreator(func->statements());
    bfTreeCreator.create();
    //bfTreeCreator.tree().traverseTreeDFSL2RB2T<std::string>(printPatternWithOpType, setup, cleanup);
    // This next step is important. We traverse the pattern tree and assign levels to the nodes. The node level will
    // later be used when hashing the pattern nodes into a hashTree.
    //bfTreeCreator.tree().traverseTreeDFST2BL2R<std::string, std::string>(printPatternOperations, coutFormatString, nullptr, nodeLevelCleanup);
    bfTreeCreator.tree()->traverseTreeDFST2BL2R<T,std::string>(printPatternOperations, nullptr, nodeLevelCleanup);
    spdlog::get("sopelogger")->debug("{}",oss.str());
    return bfTreeCreator.tree();
}

// Does all the processing to the pattern after it is generated. As an example, we can remove the
// unecessary operations. As an example if we have D[]=S[], we remove the assignment since the arraySubscript
// in D already means a memory store. So no need for =;
void postProcessPattern(std::shared_ptr<ParseState> parseState) {
    //std::cout << "Post processing pattern for memStore-assign reduction" << std::endl;
    spdlog::get("sopelogger")->debug("Post processing pattern for memStore-assign reduction");

    // We need to process the pattern before its added to repo
    std::ostringstream processingStream;
    processingStream << "\nKind Before processing: ";
    printPattern(parseState->pattern, printOpKind, processingStream);
    processingStream << "\nType Before processing: ";
    printPattern(parseState->pattern, printOpType,processingStream);
    pattern::processing::AssignmentReducer<operation> processor(parseState->pattern);
    processor.process();
    processingStream << "\nKind After processing: ";
    printPattern(parseState->pattern, printOpKind, processingStream);
    processingStream << "\nType After processing: ";
    printPattern(parseState->pattern, printOpType,processingStream);
    processingStream << "\nProcessed" << std::endl;
    spdlog::get("sopelogger")->debug("{}", processingStream.str());
}

template<typename T>
//void serializePatternTree(const PatternTree<T>& pTree, const std::string& sRunId) {
void serializePatternTree(const std::shared_ptr<IPatternTree>& pTree, const std::string& sRunId) {
    PatternTreeSerializer<operation> jsonSerializer("./db/ptree_"+sRunId+".json", pTree);
    jsonSerializer.serialize();
    // will load the index of the complete DB. This index will contain information
    // about each benchmarked/stored tree (and its corresponding json file path)
    spdlog::get("sopelogger")->debug("=== Printing DB Index ===");
    json kb = loadIndex("./db/index.json");
    printDBIndex(kb);
    searchJsonDB(kb);
    //matchCOI(); // tries to find a match for the COI in the KB
    //benchmarkCOI(); // benchmarks the COI by adding its patterntree and hash to the KB index and also should instrument the code and run it to capture its metrics.

    JointJsonGenerator<operation> jointJsonGen("./db/json_output.json", pTree);
    jointJsonGen.serialize();//, nullptr);
}

void prettyPrintASTTree(std::shared_ptr<operation> childCallerOperation, std::shared_ptr<ParseState> parseState) {
    // Traverse the function (not the tree) and print in-order (to regenerate source code)
    // then traverse post-order to generate the pattern from the function
    // This is not necessary. Just used for printing and display. This is because
    // we already created the pattern tree which contains everything we need.
    //std::cout << "-------------------------- Start: Traversal" << std::endl;
    spdlog::get("sopelogger")->debug("-------------------------- Start: Traversal");

    //std::function<void(std::shared_ptr<operation>)> patternGenFunc = std::bind(&ClangASTProcessor::patternGenerator, this, std::placeholders::_1);
    std::function<void(std::shared_ptr<operation>)> patternGenFunc = std::bind(&patternGenerator, std::placeholders::_1, parseState);
    std::ostringstream oss;
    std::function<void(std::shared_ptr<operation>)> boundFormatter = std::bind(&printFormatter, std::placeholders::_1, std::ref(oss));
    std::function<void(std::shared_ptr<operation>)> boundPrinter = std::bind(&printOperation, std::placeholders::_1, std::ref(oss));
    childCallerOperation->traverseInOrder(boundPrinter, boundFormatter, predicateAlwaysTrue, childCallerOperation);
    spdlog::get("sopelogger")->debug(oss.str());
    // Generates the pattern by traversing the AST. The pattern is temporarily stored in the parseState struct to be used by others before its later stored in the repository
    childCallerOperation->traversePostOrder(patternGenFunc, nullptr, predicateNoVarDecls, childCallerOperation);

    //std::cout << "\n-------------------------- Finished All traversal"<< std::endl;
    spdlog::get("sopelogger")->debug("-------------------------- Finished All traversal");
}

template <typename T>
void prettyPrintPatternTree(std::shared_ptr<IPatternTree>& pTree) {
    std::ostringstream oss;
    oss << "=== Prettyprint Pattern Tree ===" << std::endl;
    std::function<void(std::shared_ptr<IPatternNode>&,
                       std::function<std::string(std::shared_ptr<operation>)>, std::string)> printPatternOperations
            = std::bind(&printPatternNodeOperations, std::placeholders::_1, printCode, std::ref(oss), std::placeholders::_3);

    std::vector<std::string> indentation;
    std::string coutFormatString = "";

    std::function<void(std::shared_ptr<IPatternNode>)> setup =
            [&indentation, &coutFormatString] (std::shared_ptr<IPatternNode> node) {
        if(node->getPattern()->size() >0 && (dyn_cast<loopOperation>(node->getPattern()->at(0).get()) || dyn_cast<ifOperation>(node->getPattern()->at(0).get()) ) ) {
            indentation.push_back("\t");
            coutFormatString = coutFormatString + "\t";
        }
    };
    std::function<void(std::shared_ptr<IPatternNode>)> cleanup =
            [&indentation,&coutFormatString] (std::shared_ptr<IPatternNode> node) {
        if(node->getPattern()->size() >0 && (dyn_cast<loopOperation>(node->getPattern()->at(0).get()) || dyn_cast<ifOperation>(node->getPattern()->at(0).get()) ) ) {
            indentation.erase(indentation.end()-1);
            //coutFormatString = "\n";
            std::for_each(indentation.begin(), indentation.end(), [&coutFormatString](std::string tab) { coutFormatString = coutFormatString + tab; } );
        }
    };

    pTree->template traverseTreeDFST2BL2R<T,std::string, std::string>(printPatternOperations, coutFormatString, setup, cleanup);
    spdlog::get("sopelogger")->debug("{}",oss.str());
}

template <typename T>
void prettyPrintTrees(std::shared_ptr<operation> childCallerOperation, std::shared_ptr<IPatternTree> pTree, std::shared_ptr<ParseState> parseState) {
    prettyPrintASTTree(childCallerOperation, parseState);
    prettyPrintPatternTree<T>(pTree);
}

#endif // PROCESSINGUTILS_H
