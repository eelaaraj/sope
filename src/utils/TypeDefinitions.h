#ifndef TYPEDEFINITIONS_H
#define TYPEDEFINITIONS_H

#include "../ClangOperationWrappers.h"
#include "../Comparator.h"
using namespace pattern::matching::wrapper;

//typedef Operation<OperationTypeCompare, AccessTypeCompare> operation;
//typedef BinaryOperation<OperationTypeCompare, AccessTypeCompare> binaryOperation;
//typedef LoopOperation<OperationTypeCompare, AccessTypeCompare> loopOperation;
//typedef MemoryOperation<OperationTypeCompare, AccessTypeCompare> memOperation;
typedef Operation<OpTypAcsTypComparator> operation;
typedef BinaryOperation<OpTypAcsTypComparator> binaryOperation;
typedef UnaryOperation<OpTypAcsTypComparator> unaryOperation;
typedef LoopOperation<OpTypAcsTypComparator> loopOperation;
typedef MemoryOperation<OpTypAcsTypComparator> memOperation;
typedef VariableReferenceOperation<OpTypAcsTypComparator> varRefOperation;
typedef VariableDeclOperation<OpTypAcsTypComparator> varDeclOperation;
typedef FunctionOperation<OpTypAcsTypComparator> funcOperation;
typedef IfOperation<OpTypAcsTypComparator> ifOperation;
typedef CompoundStatementOperation<OpTypAcsTypComparator> compoundOperation;
typedef NumericLiteral<OpTypAcsTypComparator> numericOperation;
typedef CallOperation<OpTypAcsTypComparator> callOperation;

typedef ClangVarDeclWrapper<OpTypAcsTypComparator> clvarDeclOperation;

typedef ClangBinaryOperationWrapper<OpTypAcsTypComparator> clbinOperation;
typedef ClangSubscriptWrapper<OpTypAcsTypComparator> clsubsOperation;
typedef ClangForLoopWrapper<OpTypAcsTypComparator> clforOperation;
typedef ClangVariableDeclRefWrapper<OpTypAcsTypComparator> clvarRefOperation;
typedef ClangIntegerLiteralWrapper<OpTypAcsTypComparator> clintOperation;
typedef ClangFloatLiteralWrapper<OpTypAcsTypComparator> clfloatOperation;
typedef ClangUnaryOperationWrapper<OpTypAcsTypComparator> clunaryOperation;
typedef ClangFunctionWrapper<OpTypAcsTypComparator> clfunctionOperation;
typedef ClangIfWrapper<OpTypAcsTypComparator> clifOperation;
typedef ClangCompoundStatementWrapper<OpTypAcsTypComparator> clcompoundOperation;
typedef ClangCallWrapper<OpTypAcsTypComparator> clcallOperation;

#endif // TYPEDEFINITIONS_H

