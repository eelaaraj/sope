#ifndef UTILS_H
#define UTILS_H
#include "ClangOperationWrappers.h"
#include "OperationHelpers.h"
#include "Operations.h"
#include "ParseState.h"

static std::string getOperationTypeForDebugging(std::shared_ptr&lt;operation&gt; oper) {
    if(dyn_cast&lt;clbinOperation&gt;(oper.get()))
        return "clbinOperation (" + oper-&gt;getMainStringLiteral() + ") ";
    else if(dyn_cast&lt;clsubsOperation&gt;(oper.get()))
        return "clsubsOperation ";
    else if(dyn_cast&lt;clvarRefOperation&gt;(oper.get()))
        return "clvarRefOperation " + oper-&gt;getMainStringLiteral();
    else if(dyn_cast&lt;clforOperation&gt;(oper.get()))
        return "clforOperation ";
    else if(dyn_cast&lt;clvarDeclOperation&gt;(oper.get()))
        return "clvarDeclOperation " + oper-&gt;getMainStringLiteral();
    else if(dyn_cast&lt;clintOperation&gt;(oper.get()))
        return "clintOperation " + oper-&gt;getMainStringLiteral();
    else if(dyn_cast&lt;clfloatOperation&gt;(oper.get()))
        return "clfloatOperation " + oper-&gt;getMainStringLiteral();
    else if(dyn_cast&lt;clunaryOperation&gt;(oper.get()))
        return "clunaryOperation " + oper-&gt;getMainStringLiteral();
    else if(dyn_cast&lt;clifOperation&gt;(oper.get()))
        return "clifOperation (" + oper-&gt;getMainStringLiteral() + ") ";
    else if(dyn_cast&lt;clcompoundOperation&gt;(oper.get()))
        return "clcompoundOperation {" + oper-&gt;getMainStringLiteral() + "} ";
    else
        return "UNKNOWNOperation"; // shouldnt reach here
}

std::shared_ptr&lt;clbinOperation&gt; makeBinaryOperation(BinaryOperator* binOper) {
    std::string opCode = binOper-&gt;getOpcodeStr().str();
    OperationType opType;
    if (opCode.compare("+")==0){
        opType = OperationType::add;
    }else if (opCode.compare("-")==0){
        opType = OperationType::subtract;
    }else if (opCode.compare("*")==0) {
        opType = OperationType::multiply;
    } else if (opCode.compare("==")==0) {
        opType = OperationType::equals;
    } else if (opCode.compare("!=")==0) {
        opType = OperationType::ne;
    } else if (opCode.find("=")!=std::string::npos) {
        // This is assigned in the initial stages of the processing. Later the assignment operator
        // will eventually either become a save to local variable or memoryStore
        opType = OperationType::assign;
    } else if (opCode.compare("/")==0) {
        opType = OperationType::divide;
    } else if (opCode.compare("&lt;=")==0) {
        opType = OperationType::le;
    } else if (opCode.compare("&gt;=")==0) {
        opType = OperationType::ge;
    } else if (opCode.compare("&lt;")==0) {
        opType = OperationType::less;
    }else if (opCode.compare("&gt;")==0) {
        opType = OperationType::greater;
    }

    return std::shared_ptr &lt; clbinOperation
            &gt; (new clbinOperation(
                    binOper, opType, opCode));
}

std::shared_ptr&lt;clforOperation&gt; makeForOperation(ForStmt* forStmt) {
    return std::shared_ptr&lt;clforOperation&gt;(new clforOperation(forStmt));
}

std::shared_ptr&lt;clfunctionOperation&gt; makeFunctionOperation(FunctionDecl* funcDecl) {
    return std::shared_ptr&lt;clfunctionOperation&gt;(new clfunctionOperation(funcDecl));
}

std::shared_ptr&lt;clcompoundOperation&gt; makeCompoundOperation(CompoundStmt* compoundStmt) {
    return std::shared_ptr&lt;clcompoundOperation&gt;(new clcompoundOperation(compoundStmt));
}

std::shared_ptr&lt;clifOperation&gt; makeIfOperation(IfStmt* ifStmt) {
    return std::shared_ptr&lt;clifOperation&gt;(new clifOperation(ifStmt));
}

std::shared_ptr&lt;clunaryOperation&gt; makeUnaryOperation(UnaryOperator* unaryOper) {
    std::string opCode = unaryOper-&gt;getOpcodeStr(unaryOper-&gt;getOpcode()).str();
    OperationType opType;
    if (opCode.compare("++")==0){
        opType = OperationType::add;
    }else if (opCode.compare("--")==0){
        opType = OperationType::subtract;
    }

    std::shared_ptr&lt;clunaryOperation&gt; temp =  std::shared_ptr &lt;clunaryOperation&gt; (new clunaryOperation(unaryOper, opType, opCode));
    temp-&gt;isPostOperator() = unaryOper-&gt;isPostfix();
    return temp;
}

std::shared_ptr&lt;clsubsOperation&gt; makeSubscriptOperation(ArraySubscriptExpr* subsOper, OperationType&amp; opType) {
    return std::shared_ptr &lt; clsubsOperation
            &gt; (new clsubsOperation(
                   subsOper, opType));
}

std::shared_ptr&lt;clvarDeclOperation&gt; makeVarDeclOperation(VarDecl* varDecl, DeclStmt* varDeclStmt) {
    std::shared_ptr&lt;clvarDeclOperation&gt; temp =  std::shared_ptr &lt; clvarDeclOperation
            &gt; (new clvarDeclOperation(
                   varDecl, varDeclStmt));
    return temp;
}

std::shared_ptr&lt;clvarRefOperation&gt; makeVarRefOperation(DeclRefExpr* declRefExpr) {
    std::shared_ptr&lt;clvarRefOperation&gt; temp = std::shared_ptr&lt;clvarRefOperation&gt;(new clvarRefOperation(declRefExpr, declRefExpr-&gt;getDecl()-&gt;getNameAsString()));
    //temp-&gt;variableDeclaration() = declMap[declRefExpr-&gt;getDecl()];
    return temp;
}

std::shared_ptr&lt;clintOperation&gt; makeIntLiteralOperation(IntegerLiteral* intLiteral) {
    std::shared_ptr&lt;clintOperation&gt; temp = std::shared_ptr&lt;clintOperation&gt;(new clintOperation(intLiteral));
    return temp;
}

std::shared_ptr&lt;clfloatOperation&gt; makeFloatLiteralOperation(FloatingLiteral* floatLiteral) {
    std::shared_ptr&lt;clfloatOperation&gt; temp = std::shared_ptr&lt;clfloatOperation&gt;(new clfloatOperation(floatLiteral));
    return temp;
}

/*
   * Removes all the variables from one scope by searching for the key=scope.
   * Then remove the found entry from the map. This will basically remove (key,pair)
   * which is the (scopeNumber, vector of variables). Thus all the variables in
   * that scope will be removed
  */
  void clearScopeVariables(int scopeNumber, std::shared_ptr&lt;ParseState&gt; parseState) {
      ScopeTracker&lt;operation&gt;::ScopeIterator poppedScopeVariables = parseState-&gt;scopeTracker.scopeVariablesMap.find(scopeNumber); // Clear the variables of the popped scope
      std::cout &lt;&lt; "Before clearScopeVariables VariableChanges size = " &lt;&lt; parseState-&gt;scopeTracker.variableChanges.size() &lt;&lt; std::endl;
      if(poppedScopeVariables != parseState-&gt;scopeTracker.scopeVariablesMap.end()) {
          // First also remove the variables that are defined in that scope
          // from the variableChanges map. This is because there might be redeclations
          // of the same variables later on and we dont want to be mislead
          std::vector&lt;std::shared_ptr&lt;operation&gt;&gt;::iterator deletedScopeVariableIt = poppedScopeVariables-&gt;second.begin();
          while(deletedScopeVariableIt != poppedScopeVariables-&gt;second.end()) {
              std::map&lt;std::shared_ptr&lt;operation&gt;, std::set&lt;int&gt;&gt;::iterator searchResult = parseState-&gt;scopeTracker.variableChanges.find(*deletedScopeVariableIt);
              if(searchResult != parseState-&gt;scopeTracker.variableChanges.end()) {
                  parseState-&gt;scopeTracker.variableChanges.erase(searchResult);
                  std::cout &lt;&lt; " Deleted a record from variableChanges container" &lt;&lt; std::endl;
              }
              deletedScopeVariableIt++;
          }
          parseState-&gt;scopeTracker.scopeVariablesMap.erase(poppedScopeVariables);
      }
      std::cout &lt;&lt; "After clearScopeVariables VariableChanges size = " &lt;&lt; parseState-&gt;scopeTracker.variableChanges.size() &lt;&lt; std::endl;
  }

static void printOperation(std::shared_ptr&lt;operation&gt; oper) {
    if(binaryOperation* binOper = dyn_cast&lt;binaryOperation&gt;(oper.get())) {
        std::cout &lt;&lt; binOper-&gt;opCode() ;// &lt;&lt; "\n";
    }
    else if(memOperation* memOper = dyn_cast&lt;memOperation&gt;(oper.get())) {
        std::cout &lt;&lt; "[@]";// &lt;&lt; "\n";
    }
    else if(varOperation* varOper = dyn_cast&lt;varOperation&gt;(oper.get())) {
        std::cout &lt;&lt; varOper-&gt;getMainStringLiteral();// &lt;&lt; "\n";
    }
    else if(loopOperation* lOper = dyn_cast&lt;loopOperation&gt;(oper.get())) {
        std::cout &lt;&lt; "LOOP: " &lt;&lt; lOper-&gt;statements().size() &lt;&lt; " statements, init = "
                  &lt;&lt; getOperationTypeForDebugging(lOper-&gt;init())
                  &lt;&lt; ", cond = " &lt;&lt; getOperationTypeForDebugging(lOper-&gt;condition())
                  &lt;&lt; ", inc = " &lt;&lt; getOperationTypeForDebugging(lOper-&gt;increment());
    }
    else if(clvarDeclOperation* varDecl = dyn_cast&lt;clvarDeclOperation&gt;(oper.get())) {
        //std::cout &lt;&lt; "=" ;
        std::cout &lt;&lt; oper-&gt;getMainStringLiteral();
    }
    else if(clifOperation* ifOper = dyn_cast&lt;clifOperation&gt;(oper.get())) {
        std::cout &lt;&lt; "IF: cond = " &lt;&lt; getOperationTypeForDebugging(ifOper-&gt;binaryCondition());
    }
    else if(clcompoundOperation* compOper = dyn_cast&lt;clcompoundOperation&gt;(oper.get())) {
        std::cout &lt;&lt; "{ : " &lt;&lt; compOper-&gt;statements().size();
    }
    else if(dyn_cast&lt;clintOperation&gt;(oper.get())) {
        std::cout &lt;&lt; oper-&gt;getMainStringLiteral();
    }
    else if(dyn_cast&lt;clfloatOperation&gt;(oper.get())) {
        std::cout &lt;&lt; oper-&gt;getMainStringLiteral();
    }
}

static void printFormatter(std::shared_ptr&lt;operation&gt; oper) {    
    if(dyn_cast&lt;funcOperation&gt;(oper.get())) {
            std::cout &lt;&lt; std::endl;
    }
    else if(dyn_cast&lt;loopOperation&gt;(oper.get())) {
            std::cout &lt;&lt; std::endl;
    }
    else if(dyn_cast&lt;compoundOperation&gt;(oper.get())) {
        std::cout &lt;&lt; std::endl;
    }
    else if(dyn_cast&lt;ifOperation&gt;(oper.get())) {
        std::cout &lt;&lt; std::endl;
    }
    else {
        std::cout &lt;&lt; " ";
    }
}


bool checkUnaryOperation(std::shared_ptr&lt;operation&gt; nextOperation) {
    if(dyn_cast&lt;clvarDeclOperation&gt;(nextOperation.get())) {
        std::cout &lt;&lt; "CheckUnaryOperation = true" &lt;&lt; std::endl;
        return true;
    }
    else {
        std::cout &lt;&lt; "CheckUnaryOperation = false" &lt;&lt; std::endl;
        return false;
    }
}

#endif // UTILS_H

