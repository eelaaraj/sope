#ifndef FILEUTILS_H
#define FILEUTILS_H

#include <string>
#include "../json.hpp"

using json = nlohmann::json;

// Creates a json file from the json object
bool createJsonFile(const std::string& filename, json data) {
    if(!FileSystem::File::exists(filename)) {
        FileSystem::File f(filename);
        f.open(FileSystem::File::out, FileSystem::File::text);
        if(f.isGood())
            f.putLine(data.dump(), false);
    }
    else {
        std::cerr << "File " << filename <<  " already exists" << std::endl;
        return false;
    }
    return true;
}

bool overwriteJsonFile(const std::string& filePath, json& jsonData) {
    FileSystem::File f(filePath);
    f.open(FileSystem::File::out, FileSystem::File::text);
    if(f.isGood()) {
        f.putLine(jsonData.dump(), false);
        return true;
    }
    else {
        return false;
    }
}

// Loads the contents of a json file into a json object
json deserializeJsonFile(const std::string jsonUrl) {
    std::ifstream is(jsonUrl);
    std::string str;
    if (is) {
        // get length of file:
        is.seekg(0, is.end);
        int length = is.tellg();
        is.seekg(0, is.beg);

        str.resize(length, ' '); // reserve space
        char* begin = &*str.begin();

        is.read(begin, length);
        is.close();
    } else {
        std::cout << "Could not open " << jsonUrl << "\n";
    }
    json jsn = json::parse(str);
    return jsn;
}

bool createCXXFile(const std::string& filePath, std::string content) {
    FileSystem::File f(filePath);
    f.open(FileSystem::File::out, FileSystem::File::text);
    if(f.isGood()) {
        f.putLine(content, false);
        return true;
    }
    else {
        return false;
    }
}


#endif // FILEUTILS_H

