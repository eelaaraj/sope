#ifndef UTILS_H
#define UTILS_H
#include "../ClangOperationWrappers.h"
#include "OperationHelpers.h"
#include "../Operations.h"
#include "../ParseState.h"
#include <spdlog/spdlog.h>

static std::string getOperationTypeForDebugging(std::shared_ptr<operation> oper) {
    if(dyn_cast<clbinOperation>(oper.get()))
        return "clbinOperation (" + oper->getMainStringLiteral() + ") ";
    else if(dyn_cast<clsubsOperation>(oper.get()))
        return "clsubsOperation ";
    else if(dyn_cast<clvarRefOperation>(oper.get()))
        return "clvarRefOperation " + oper->getMainStringLiteral();
    else if(dyn_cast<clforOperation>(oper.get()))
        return "clforOperation ";
    else if(dyn_cast<clvarDeclOperation>(oper.get()))
        return "clvarDeclOperation " + oper->getMainStringLiteral();
    else if(dyn_cast<clintOperation>(oper.get()))
        return "clintOperation " + oper->getMainStringLiteral();
    else if(dyn_cast<clfloatOperation>(oper.get()))
        return "clfloatOperation " + oper->getMainStringLiteral();
    else if(dyn_cast<clunaryOperation>(oper.get()))
        return "clunaryOperation " + oper->getMainStringLiteral();
    else if(dyn_cast<clifOperation>(oper.get()))
        return "clifOperation (" + oper->getMainStringLiteral() + ") ";
    else if(dyn_cast<clcompoundOperation>(oper.get()))
        return "clcompoundOperation {" + oper->getMainStringLiteral() + "} ";
    else
        return "UNKNOWNOperation"; // shouldnt reach here
}

std::shared_ptr<clbinOperation> makeBinaryOperation(BinaryOperator* binOper) {
    std::string opCode = binOper->getOpcodeStr().str();
    OperationType opType;
    if (opCode.compare("+")==0){
        opType = OperationType::add;
    }else if (opCode.compare("-")==0){
        opType = OperationType::subtract;
    }else if (opCode.compare("*")==0) {
        opType = OperationType::multiply;
    } else if (opCode.compare("==")==0) {
        opType = OperationType::equals;
    } else if (opCode.compare("!=")==0) {
        opType = OperationType::ne;
    } else if (opCode.find("=")!=std::string::npos) {
        // This is assigned in the initial stages of the processing. Later the assignment operator
        // will eventually either become a save to local variable or memoryStore
        opType = OperationType::assign;
    } else if (opCode.compare("/")==0) {
        opType = OperationType::divide;
    } else if (opCode.compare("<=")==0) {
        opType = OperationType::le;
    } else if (opCode.compare(">=")==0) {
        opType = OperationType::ge;
    } else if (opCode.compare("<")==0) {
        opType = OperationType::less;
    }else if (opCode.compare(">")==0) {
        opType = OperationType::greater;
    }

    return std::shared_ptr < clbinOperation
            > (new clbinOperation(
                    binOper, opType, opCode));
}

std::shared_ptr<clforOperation> makeForOperation(ForStmt* forStmt, int loopId) {
    return std::shared_ptr<clforOperation>(new clforOperation(forStmt, loopId));
}

std::shared_ptr<clcallOperation> makeCallOperation(CallExpr* callExpr, const OperationType& opType ) {
    return std::shared_ptr<clcallOperation>(new clcallOperation(callExpr, opType));
}

std::shared_ptr<clfunctionOperation> makeFunctionOperation(FunctionDecl* funcDecl) {
    return std::shared_ptr<clfunctionOperation>(new clfunctionOperation(funcDecl));
}

std::shared_ptr<clcompoundOperation> makeCompoundOperation(CompoundStmt* compoundStmt) {
    return std::shared_ptr<clcompoundOperation>(new clcompoundOperation(compoundStmt));
}

std::shared_ptr<clifOperation> makeIfOperation(IfStmt* ifStmt) {
    return std::shared_ptr<clifOperation>(new clifOperation(ifStmt));
}

std::shared_ptr<clunaryOperation> makeUnaryOperation(UnaryOperator* unaryOper) {
    std::string opCode = unaryOper->getOpcodeStr(unaryOper->getOpcode()).str();
    OperationType opType;
    if (opCode.compare("++")==0){
        opType = OperationType::add;
    }else if (opCode.compare("--")==0){
        opType = OperationType::subtract;
    }

    std::shared_ptr<clunaryOperation> temp =  std::shared_ptr <clunaryOperation> (new clunaryOperation(unaryOper, opType, opCode));
    temp->isPostOperator() = unaryOper->isPostfix();
    return temp;
}

std::shared_ptr<clsubsOperation> makeSubscriptOperation(ArraySubscriptExpr* subsOper, OperationType& opType) {
    return std::shared_ptr < clsubsOperation
            > (new clsubsOperation(
                   subsOper, opType));
}

std::shared_ptr<clvarDeclOperation> makeVarDeclOperation(VarDecl* varDecl, DeclStmt* varDeclStmt) {
    std::shared_ptr<clvarDeclOperation> temp =  std::shared_ptr < clvarDeclOperation
            > (new clvarDeclOperation(
                   varDecl, varDeclStmt));
    return temp;
}

std::shared_ptr<clvarRefOperation> makeVarRefOperation(DeclRefExpr* declRefExpr) {
    std::shared_ptr<clvarRefOperation> temp = std::shared_ptr<clvarRefOperation>(new clvarRefOperation(declRefExpr, declRefExpr->getDecl()->getNameAsString()));
    //temp->variableDeclaration() = declMap[declRefExpr->getDecl()];
    return temp;
}

std::shared_ptr<clintOperation> makeIntLiteralOperation(IntegerLiteral* intLiteral) {
    std::shared_ptr<clintOperation> temp = std::shared_ptr<clintOperation>(new clintOperation(intLiteral));
    return temp;
}

std::shared_ptr<clfloatOperation> makeFloatLiteralOperation(FloatingLiteral* floatLiteral) {
    std::shared_ptr<clfloatOperation> temp = std::shared_ptr<clfloatOperation>(new clfloatOperation(floatLiteral));
    return temp;
}

/*
   * Removes all the variables from one scope by searching for the key=scope.
   * Then remove the found entry from the map. This will basically remove (key,pair)
   * which is the (scopeNumber, vector of variables). Thus all the variables in
   * that scope will be removed
  */
  void clearScopeVariables(int scopeNumber, std::shared_ptr<ParseState> parseState) {
      ScopeTracker<operation>::ScopeIterator poppedScopeVariables = parseState->scopeTracker.scopeVariablesMap.find(scopeNumber); // Clear the variables of the popped scope
      //std::cout << "Before clearScopeVariables VariableChanges size = " << parseState->scopeTracker.variableChanges.size() << std::endl;
      spdlog::get("sopelogger")->debug("Before clearScopeVariables VariableChanges size = {}", parseState->scopeTracker.variableChanges.size());
      if(poppedScopeVariables != parseState->scopeTracker.scopeVariablesMap.end()) {
          // First also remove the variables that are defined in that scope
          // from the variableChanges map. This is because there might be redeclations
          // of the same variables later on and we dont want to be mislead
          std::vector<std::shared_ptr<operation>>::iterator deletedScopeVariableIt = poppedScopeVariables->second.begin();
          while(deletedScopeVariableIt != poppedScopeVariables->second.end()) {
              std::map<std::shared_ptr<operation>, std::set<int>>::iterator searchResult = parseState->scopeTracker.variableChanges.find(*deletedScopeVariableIt);
              if(searchResult != parseState->scopeTracker.variableChanges.end()) {
                  parseState->scopeTracker.variableChanges.erase(searchResult);
                  //std::cout << " Deleted a record from variableChanges container" << std::endl;
                  spdlog::get("sopelogger")->debug(" Deleted a record from variableChanges container");
              }
              deletedScopeVariableIt++;
          }
          parseState->scopeTracker.scopeVariablesMap.erase(poppedScopeVariables);
      }
      //std::cout << "After clearScopeVariables VariableChanges size = " << parseState->scopeTracker.variableChanges.size() << std::endl;
      spdlog::get("sopelogger")->debug("After clearScopeVariables VariableChanges size = ", parseState->scopeTracker.variableChanges.size());
  }

static void printOperation(std::shared_ptr<operation> oper, std::ostream& os) {
    if(binaryOperation* binOper = dyn_cast<binaryOperation>(oper.get())) {
        //std::cout << binOper->opCode() ;// << "\n";
        os << binOper->opCode() ;
        //spdlog::get("sopelogger")->debug("{}",binOper->opCode());
    }
    else if(/*memOperation* memOper = */dyn_cast<memOperation>(oper.get())) {
        //std::cout << "[@]";// << "\n";
        //spdlog::get("sopelogger")->debug("[@]");
        os << "[@]";
    }
    else if(varRefOperation* varOper = dyn_cast<varRefOperation>(oper.get())) {
        //std::cout << varOper->getMainStringLiteral();// << "\n";
        //spdlog::get("sopelogger")->debug("{}",varOper->getMainStringLiteral());
        os << varOper->getMainStringLiteral();
    }
    else if(loopOperation* lOper = dyn_cast<loopOperation>(oper.get())) {
        //std::cout << "LOOP: " << lOper->statements().size() << " statements, init = "
        //          << getOperationTypeForDebugging(lOper->init())
        //          << ", cond = " << getOperationTypeForDebugging(lOper->condition())
        //          << ", inc = " << getOperationTypeForDebugging(lOper->increment());
//        spdlog::get("sopelogger")->debug("LOOP: {} statements, init = {}, cond = {}, inc = {}", lOper->statements().size(),
//                                         getOperationTypeForDebugging(lOper->init()),
//                                         getOperationTypeForDebugging(lOper->condition()),
//                                         getOperationTypeForDebugging(lOper->increment()));
        os << "LOOP: " << lOper->statements().size() << " statements, init = "
                  << getOperationTypeForDebugging(lOper->init())
                  << ", cond = " << getOperationTypeForDebugging(lOper->condition())
                  << ", inc = " << getOperationTypeForDebugging(lOper->increment());

    }
    else if(/*clvarDeclOperation* varDecl = */dyn_cast<clvarDeclOperation>(oper.get())) {
        //std::cout << "=" ;
        //std::cout << oper->getMainStringLiteral();
        //spdlog::get("sopelogger")->debug("{}",oper->getMainStringLiteral());
        os << oper->getMainStringLiteral();
    }
    else if(clifOperation* ifOper = dyn_cast<clifOperation>(oper.get())) {
        //std::cout << "IF: cond = " << getOperationTypeForDebugging(ifOper->binaryCondition());
        //spdlog::get("sopelogger")->debug("IF: cond = {}", getOperationTypeForDebugging(ifOper->binaryCondition()));
        os << "IF: cond = " << getOperationTypeForDebugging(ifOper->binaryCondition());
    }
    else if(clcompoundOperation* compOper = dyn_cast<clcompoundOperation>(oper.get())) {
        //std::cout << "{ : " << compOper->statements().size();
        //spdlog::get("sopelogger")->debug("{ : {}", compOper->statements().size());
        os << "{ : " << compOper->statements().size();
    }
    else if(dyn_cast<clintOperation>(oper.get())) {
        //std::cout << oper->getMainStringLiteral();
        //spdlog::get("sopelogger")->debug("{}", oper->getMainStringLiteral());
        os << oper->getMainStringLiteral();
    }
    else if(dyn_cast<clfloatOperation>(oper.get())) {
        //std::cout << oper->getMainStringLiteral();
        //spdlog::get("sopelogger")->debug("{}", oper->getMainStringLiteral());
        os << oper->getMainStringLiteral();
    }
}

static void printFormatter(std::shared_ptr<operation> oper, std::ostream& os) {
    if(dyn_cast<funcOperation>(oper.get())) {
            //std::cout << std::endl;
            os << std::endl;
    }
    else if(dyn_cast<loopOperation>(oper.get())) {
            //std::cout << std::endl;
            os << std::endl;
    }
    else if(dyn_cast<compoundOperation>(oper.get())) {
        //std::cout << std::endl;
        os << std::endl;
    }
    else if(dyn_cast<ifOperation>(oper.get())) {
        //std::cout << std::endl;
        os << std::endl;
    }
    else {
        //std::cout << " ";
        os << " ";
    }
}


bool checkUnaryOperation(std::shared_ptr<operation> nextOperation) {
    if(dyn_cast<clvarDeclOperation>(nextOperation.get())) {
        std::cout << "CheckUnaryOperation = true" << std::endl;
        return true;
    }
    else {
        std::cout << "CheckUnaryOperation = false" << std::endl;
        return false;
    }
}
/*
bool predicateAlwaysTrue(std::shared_ptr<operation> toCheck) {
    return true;
}

bool predicateNoVarDecls(std::shared_ptr<operation> toCheck) {
    return !dyn_cast<varDeclOperation>(toCheck.get());
}
*/

#endif // UTILS_H

