#ifndef PASS2VISITOR
#define PASS2VISITOR

#include <string>
#include <vector>
#include <iostream>
#include "clang/AST/ASTConsumer.h"
#include "clang/AST/RecursiveASTVisitor.h"
#include "clang/Frontend/CompilerInstance.h"
#include "clang/Frontend/FrontendAction.h"
#include "clang/Tooling/Tooling.h"
#include "clang/Tooling/Refactoring.h"
#include "clang/Tooling/CommonOptionsParser.h"
#include "clang/Rewrite/Frontend/Rewriters.h"
#include "clang/Rewrite/Core/Rewriter.h"
#include "../ClangOperationWrappers.h"
#include "../utils/OperationHelpers.h"
#include "../Repository.h"
#include "../utils/Utils.h"
#include "Pass2Processor.h"
#include <stack>

#include "../defuse/DefUse.h"
#include <spdlog/spdlog.h>
using namespace clang::defuse;

template<typename OperationTypename>
class BuildDefUseVisitor
        : public RecursiveASTVisitor<BuildDefUseVisitor<OperationTypename>> {
public:
    explicit BuildDefUseVisitor(ASTContext *Context, DefUse** defUse, std::shared_ptr<Repository<OperationTypename>> repository, const std::string& fName, std::vector<std::string>& funcNames, bool injection)
        : functions(funcNames)/*, DU(defUse)*/ {
        std::cout << "Constructing BuildDefUseVisitor" << std::endl;
        /*std::cout << "Defuse at " << defUse << std::endl;
        std::cout << "DUs at " << DUs << std::endl;*/
        DUs = defUse;
        parseState = std::shared_ptr<ParseState>(new ParseState());
        parseState->Context = Context;
        parseState->repo = repository;
        parseState->fileName = fName;
        parseState->functionNames = funcNames;
        parseState->replacements = nullptr;
        parseState->inject = injection;
        parseState->function = nullptr;
        parseState->loop = nullptr;
        parseState->currentAssignmentNode = nullptr;
        parseState->assignmentLHS = nullptr;
        parseState->assignmentRHS = nullptr;
        parseState->assignmentOperChildren = nullptr;
        parseState->functionParentMap = nullptr;
        parseState->currentParent = nullptr;
        std::shared_ptr<Pass2Processor> clASTProcessor = std::shared_ptr<Pass2Processor>(new Pass2Processor(parseState, astProcessor));

        astProcessor = clASTProcessor;
        // We need to set the self pointer here. It does not work the way we do it in the constructor unless we pass
        // as reference to the astProcessor shared_ptr. If we just pass the shared_ptr by value, then the weak_ptr
        // in clangASTProcessor will be set to the initial address value of astProcessor shared_ptr which is nullptr.
        clASTProcessor->setSelfSharedPointer(astProcessor);

        std::cout << "Repository use count visitor = " << repository.use_count() << "\n";
    }

    virtual ~BuildDefUseVisitor() {
        //std::cout << "Destroying Call visitor" <<"\n";
        spdlog::get("sopelogger")->debug("Destroying Call visitor");
        functions.swap(parseState->functionNames);
        //if(DU)
        //    delete DU;
    }

    bool VisitCXXRecordDecl(CXXRecordDecl *Declaration) {
        if(parseState->Context->getSourceManager().isInMainFile(Declaration->getLocation()))
        {
            FullSourceLoc FullLocation = parseState->Context->getFullLoc(Declaration->getLocStart());
            if (FullLocation.isValid())
                std::cout << "Found declaration at "
                          << FullLocation.printToString(parseState->Context->getSourceManager()) << "\n ";
        }
        return true;
    }

    bool VisitCallExpr(CallExpr* callExpr) {
        if(parseState->function) {
            std::cout << "Found call expr = " << callExpr->getDirectCallee()->getNameAsString() << std::endl;
            //bool ret = astProcessor->processCallExpr(callExpr);
            //RecursiveASTVisitor<BuildDefUseVisitor>::TraverseDecl(callExpr->getDirectCallee());
        }
        return true;
    }

    bool VisitFunctionDecl(FunctionDecl* functionDecl) {
        if (!parseState->functionNames.empty() && std::find(parseState->functionNames.begin(), parseState->functionNames.end(), functionDecl->getNameAsString()) != parseState->functionNames.end()) {
            *DUs = DefUse::BuildDefUseChains(functionDecl, functionDecl->getBody(), parseState->Context, 0, 0, 0, true);
            DU = *DUs;
            parseState->function = functionDecl;
            /*std::cout << "In FuncDecl" << std::endl;
            std::cout << "DUs at " << DUs << std::endl;
            std::cout << "*DUs at " << *DUs << std::endl;
            std::cout << "DU at " << DU << std::endl;*/
        }
        else {
            // We may need to delete DU here instead of the destructor. Or We may need to keep a map of DU
            // One DU entry per function that we want to parse.
            parseState->function = nullptr;
        }
        return true;
    }

    bool VisitDeclRefExpr(DeclRefExpr* declRef) {
        if(parseState->function && !dyn_cast<FunctionDecl>(declRef->getDecl()) && !dyn_cast<EnumConstantDecl>(declRef->getDecl())) {
            std::cout << "Pass-2-Visitor found DeclRefExpr " << declRef->getNameInfo().getAsString()  << " at " << declRef->getLocation().printToString(parseState->Context->getSourceManager()) << std::endl;
            if(DU->isUse(declRef) ) {
                for(DefUse::defs_iterator I = DU->defs_begin(declRef), E= DU->defs_end(); I != E; I++) {
                    const DefUseNode* DUn = *I;
                    if(DUn->getKind() == DefUseNode::NodeKind::VarDecl) {
                        const VarDecl* D = DUn->getVarDecl();
                        std::cout << "DEF'D' by vardecl: " << D->getNameAsString() << " at " << D->getLocation().printToString(parseState->Context->getSourceManager()) << std::endl;
                    }
                    else if(DUn->getKind() == DefUseNode::NodeKind::VarRef) {
                        const DeclRefExpr* D = DUn->getVarRef();
                        std::cout << "DEF'D' by declref: " << D->getNameInfo().getAsString() << " at " << D->getLocation().printToString(parseState->Context->getSourceManager()) << std::endl;
                    }
                }
            }
            else if(DU->isDef(declRef)) {
                std::cout << "Var " << declRef->getNameInfo().getAsString() << " is DEF" << std::endl;
                for(DefUse::uses_iterator I = DU->uses_begin(declRef), E = DU->uses_end(); I != E; I++){
                    const DeclRefExpr* U = *I;
                    std::cout << "USED by : " << U->getNameInfo().getAsString() << " at " << U->getLocation().printToString(parseState->Context->getSourceManager()) << std::endl;
                }
            }
            else {
                std::cout << "DeclRefExpr is neither def not use. Something went wrong I believe."<<std::endl;
            }
        }
        return true;
    }

private:
    std::shared_ptr<ParseState> parseState;
    std::shared_ptr<Pass2Processor> astProcessor;
    std::vector<std::string>& functions;
    DefUse**DUs;
    DefUse* DU;
};

template<typename OperationTypename>
class BuildDefUseConsumer : public clang::ASTConsumer {
public:
    explicit BuildDefUseConsumer(ASTContext *Context, DefUse** defUse, std::shared_ptr<Repository<OperationTypename>> repo, const std::string& fileName, std::vector<std::string>& functionNames, bool inject)
        : Visitor(Context, defUse, repo, fileName, functionNames, inject)  {
        //std::cout << "Repository use count consumer= " << repo.use_count() << "\n";
    }

    virtual ~BuildDefUseConsumer() {}

    virtual void HandleTranslationUnit(clang::ASTContext &Context) {
        Visitor.TraverseDecl(Context.getTranslationUnitDecl());
    }
private:
    BuildDefUseVisitor<OperationTypename> Visitor;
};

template<typename OperationTypename>
class BuildDefUseAction : public clang::ASTFrontendAction {
public:
    BuildDefUseAction(DefUse** defUse, const std::string& fName, std::vector<std::string>& funcNames, std::shared_ptr<Repository<OperationTypename>> repo, bool injection) : DU(defUse), fileName(fName), functionNames(funcNames), repository(repo), inject(injection){
        //std::cout << "Repository use count action= " << repository.use_count() << "\n";
    }
    virtual ~BuildDefUseAction() {
        //std::cout << "destroying FindNamedClassAction\n";
    }

    virtual std::unique_ptr<clang::ASTConsumer> CreateASTConsumer(
            clang::CompilerInstance &Compiler, llvm::StringRef InFile) {
        return std::unique_ptr<clang::ASTConsumer>(
                    new BuildDefUseConsumer<OperationTypename>(&Compiler.getASTContext(), DU, repository, fileName, functionNames, inject));
    }
private:
    DefUse**DU;
    std::string fileName;
    std::vector<std::string>& functionNames;
    std::shared_ptr<Repository<OperationTypename>> repository;
    Replacements* replacements;
    bool inject;
};

template<typename OperationTypename>
class BuildDefUseActionFactory: public FrontendActionFactory {
public:
    BuildDefUseActionFactory(DefUse** defUse, std::string fName, std::vector<std::string>& funcNames, std::shared_ptr<Repository<OperationTypename>> repo, bool injection) : DU(defUse), fileName(fName), functionNames(funcNames), repository(repo), inject(injection){
        //std::cout << "Repository use count factory = " << repository.use_count() << "\n";
    }

    clang::FrontendAction* create() {
        //std::cout << "Repository use count factory::create()= " << repository.use_count() << "\n";
        return new BuildDefUseAction<OperationTypename>(DU, fileName, functionNames, repository, inject);
    }
public:
    DefUse**DU;
    std::string fileName;
    std::vector<std::string>& functionNames;
    std::shared_ptr<Repository<OperationTypename>> repository;
    Replacements* replacements;
    bool inject;
};

#endif // PASS2VISITOR

