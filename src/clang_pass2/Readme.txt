README
============
This pass will statically extract the dependencies between variables and calls and buidl a
dependence graph/chain.

main () {
 int k = read from console;
 double* hilb = createhilbert(k*k);
 int k3 = k*3;
 double* vander = createvandermonde(k3*k3);
 sgemm(hilb,vander);
 print(hilb);
}

In this code, we have:
1- createhilbert depends on variable k;
2- k3 depends on the value of k;
3- createvander depends on variable k3;
4- sgemm depends on variables hilb and vander. Thus sgemm can not run until vander returns a result;
5- print depends on hilbert.

The graph will look as follows:

k               - level 0
|_ hilb         - level 1
    |_ sgemm   - level 3: because sgemm also depends on vander and not just hilb
  |_ print      - level 2
|_ k3
  |_ vander
    |_ sgemm


To create this graph, we need to track variable declarations and variable references. Any time
there is a function or variable that references another variable, we add an edge between the
two. After that we topologically sort the graph. At that point we have the above graph.
