#ifndef INJECTOR
#define INJECTOR

#include "clang/Basic/SourceLocation.h"
#include "clang/Tooling/Core/Replacement.h"

using namespace clang;
using namespace clang::tooling;

enum InjectionType {
    START, // Inject the start sequence
    STOP, // inject the stop sequence
    OTHER, // this indicates that its only one injection. Not a start nor a stop. An example of this is a comment.
                //A timing capture can't be a OTHER because it needs two injections; one to start and one to stop
};

class InjectionGenerator {
public:
    virtual ~InjectionGenerator() {}
    virtual std::string generate(InjectionType type) = 0;
};


/* Generates code to capture performance metrics. This
 * is basically startCaptur and stopCapture function calls.
 * In addition to that, it also keeps track of the loop indeces
 * because it needs to know unique IDs for each Injection
 * i.e. a unique ID for each forLoop. This will be helpful to
 * associate each startCapture and stopCapture with a single loop.
 */
class CaptureGenerator: public InjectionGenerator {
public:
    //CaptureGenerator();
    std::string generate(InjectionType type);
private:
};

std::string CaptureGenerator::generate(InjectionType type) {
    if(type == InjectionType::START)
        return "// Will instrument START here -- Elie\n";
    else if(type == InjectionType::STOP)
        return "// Will instrument STOP here -- Elie";
    else
        return "// Will instrument here -- Elie";
}

/* Injector class resposible for injecting code at the right locations
 * in the original source files. The injector needs to know the filename
 * and location inside the file to be able to inject at the correct place.
 * The location that we are going to use is the FullSourceLoc clang object
 * that is available for all types of nodes in the AST.
 */
class Injector{
public:
    Injector(Replacements* replacements, InjectionGenerator& generator);
    void inject(FullSourceLoc location, InjectionType type, const std::string& preFormat, const std::string& postFormat );
private:
    Replacements* replaces;
    InjectionGenerator* injectionGenerator;
};


Injector::Injector(Replacements *replacements, InjectionGenerator &generator): replaces(replacements), injectionGenerator(&generator) {}

void Injector::inject(FullSourceLoc location, InjectionType type, const std::string &preFormat, const std::string &postFormat)  {
    Replacement rep(location.getManager(), location, 0, preFormat + injectionGenerator->generate(type) + postFormat);
    replaces->insert(rep);
}

#endif // INJECTOR

