#ifndef PARSESTATE
#define PARSESTATE

#include "utils/TypeDefinitions.h"
#include "utils/OperationHelpers.h"
#include "Repository.h"
#include "clang/Tooling/Tooling.h"
#include "clang/AST/ParentMap.h"
#include "clang/Tooling/Core/Replacement.h"
#include "proc/IASTProcessor.h"
#include <spdlog/spdlog.h>

template<typename T>
struct ScopeTracker {
    using ScopeIterator = typename std::map<int, std::vector<std::shared_ptr<T>>>::iterator;
    int numScopes;
    int numVariables;
    std::map<int, std::vector<std::shared_ptr<T>>> scopeVariablesMap;
    // shared_ptr has operator== which compares sp1.get() == sp2.get()
    // Therefore it can be used as a key in the map if we need to search the map
    // Be careful, the variableChanges map stores varDecls and not varRefs
    std::map<std::shared_ptr<T>, std::set<int>> variableChanges;
};

struct LoopInfoStruct {
    ForStmt* current;
    std::string modifiedText; // the modified version of the loop text that will be injected into the queryer
    std::string start;
    std::string end;
    std::string tripCount;  // trip count in a string format. ie. k/1 or k/2 when increment=k+=2, or log(base2)(k) when k*=2, or log(basen)(k) when inc is k*=n
    std::vector<std::shared_ptr<LoopInfoStruct>> innerLoops;
};

//template<typename OperationTypename>
struct ParseState {
    ~ParseState() {//std::cout << "destroying struct ParseState. Should only happen at the end of run.\n";
                   spdlog::get("sopelogger")->debug("destroying struct ParseState. Should only happen at the end of run");
                  }
    ASTContext *Context;
    // Add any member variables that need to be used by other Visit* function. This can help in
    // many cases like, know which function the loop is in. Or know which loop the expression is in. ETC.
    std::shared_ptr<Repository<operation>> repo;
    std::string fileName;
    std::vector<std::string> functionNames;
    clang::tooling::Replacements* replacements;
    bool inject;
    FunctionDecl *function;
    Stmt *loop; // current loop stmt. Can be wither ForStmt or WhileStmt
    bool isForLoopInit = false; //used to set if the binaryoperator is part of the loop init statement. we don't consider inits as a computation operation
    bool isForLoopCondition = false;
    bool isForLoopIncrement = false;
    bool isNewAssignmentStmt = false; // this means all binaryoperators that are visited after are children of the new
    bool isUnaryOperation = false;
    VarDecl* currentVarDeclInitNode;
    VarDecl* initLHS;
    Expr* initRHS;
    Stmt* currentAssignmentNode;
    Stmt* assignmentLHS;
    Stmt* assignmentRHS;
    ParentMap* assignmentOperChildren;
    ParentMap* functionParentMap;
    Pattern<operation> pattern;
    std::stack<Stmt*> clangOperationStack;
    //std::stack<std::shared_ptr<operation>> clangOperandWrapperStack;
    std::stack<std::shared_ptr<operation>> clangOperationWrapperStack;
    // processedOprationStack: keeps track of which operations have been processed. This is useful when popping off two operands
    // of a binary operation. These operands don't need to be declreferences. They can be two subscript operators for example.
    // If we don't specify that these two subscript operators have been processed, then we will infinitely wait for the operands
    // of each subscript operator to show up; although we have already extracted the two operands from the stack and processed the operation
    std::stack<bool> processedOperationStack;
    std::stack<Stmt*> operationParentStack;
    std::stack<std::shared_ptr<operation>> operationOnlyStack;
    Stmt* currentParent;
    int numScopes;
    int numVariables;
    ScopeTracker<operation> scopeTracker;
    std::map<Decl*, std::shared_ptr<operation>> declMap;
    std::stack<std::shared_ptr<IASTProcessor>> processorStack;
    // each function will have a a record in the map. This record will contain all the loops in that function and their structure
    std::map<FunctionDecl*, std::vector<std::shared_ptr<LoopInfoStruct>>> functionLoopsMap;
    int loopId; // this gives a unique id for each loop we parse
    // A vector or all the functions parsed by clangASTProcessor. These functions are
    // std::shared<operation> which can be used to traverse the function operation tree.
    // All processing after ClangASTProcessor will use this vector to further process the operation AST
    std::vector<std::shared_ptr<operation>> parsedFunctions;
};

#endif // PARSESTATE

