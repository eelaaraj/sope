#ifndef IFSTMTPROCESSOR_H
#define IFSTMTPROCESSOR_H

#include "clang/AST/RecursiveASTVisitor.h"
#include "../ClangOperationWrappers.h"
#include "../Comparator.h"
#include "../utils/OperationHelpers.h"
#include "IASTProcessor.h"
#include "../utils/Utils.h"
#include "../ParseState.h"
#include <spdlog/spdlog.h>

class IfStmtProcessor : public IASTProcessor
{
public:
    IfStmtProcessor(IfStmt* ifStmt, std::shared_ptr<ParseState> parseState);
    ~IfStmtProcessor() { //std::cout << "Destroying IfStmtProcessor at " << this << "\n";
                       spdlog::get("sopelogger")->debug("Destroying IfStmtProcessor at {}",(void*)this);
                       }
    void processNode(std::shared_ptr<IASTProcessor> parentWithCallback);
    void callBack(std::shared_ptr<operation> childCallerOperation);

private:
    IfStmt* fStmt;
    std::shared_ptr<ParseState> pState;
    std::shared_ptr<IASTProcessor> pWithCallback;
    std::shared_ptr<clifOperation> op;
    //int numOfStmts;
    //Stmt* lastStmtInBody;
    bool isIfCondition;
    bool isThenPart;
};


IfStmtProcessor::IfStmtProcessor(IfStmt* ifStmt, std::shared_ptr<ParseState> parseState) : fStmt(ifStmt), pState(parseState), isIfCondition(true), isThenPart(false)
{
    /*if(CompoundStmt* compStmt = clang::dyn_cast<CompoundStmt>(fStmt->getThen())) {
        numOfStmts = compStmt->size();
        lastStmtInBody = compStmt->body_back();
        //for(auto = )
    }
    else {
        numOfStmts = 1; // Could be 0 if the for loop is empty : for(...);
        lastStmtInBody = fStmt->getThen();
    }*/
}

void IfStmtProcessor::processNode(std::shared_ptr<IASTProcessor> parentWithCallback) {
    pWithCallback = parentWithCallback;

    op = makeIfOperation(fStmt);
    pState->numScopes++;
    //std::cout << "--------- Scope: " << pState->numScopes << std::endl;
    spdlog::get("sopelogger")->debug("--------- Scope: {}", pState->numScopes);
    FullSourceLoc FullLocation = pState->Context->getFullLoc(fStmt->getIfLoc());
    if (FullLocation.isValid()) {
        //std::cout << " Found IF inside function " << pState->function->getNameAsString() << " at "
        //          << FullLocation.printToString(pState->Context->getSourceManager()) << std::endl;
        spdlog::get("sopelogger")->debug(" Found IF inside function {} at {}", pState->function->getNameAsString(), FullLocation.printToString(pState->Context->getSourceManager()));
    }
}

void IfStmtProcessor::callBack(std::shared_ptr<operation> childCallerOperation) {
    //std::cout << "IfStmtProcessor callback called " << std::endl;
    spdlog::get("sopelogger")->debug("IfStmtProcessor callback called ");
    // Pop the child operation
    pState->processorStack.pop();

    if(isIfCondition) {
        isIfCondition = false;
        isThenPart = true;
        op->binaryCondition() = childCallerOperation;
    }
    else if(isThenPart){
        isThenPart = false;
        op->thenPart() = childCallerOperation;
        // Sometimes the if does not have an else. Thus we need to
        // do the callback here and not in the ELSE part
        if( !(fStmt->getElse()) ) {
            op->elsePart() = nullptr;
            std::cout << " If has no else. Will callback parent without waiting for more operations" << std::endl;
            spdlog::get("sopelogger")->debug("Destroying IfStmtProcessor at {}",(void*)this);
            pWithCallback->callBack(op);
        }
        //numOfStmts--;
    }
    else {
        op->elsePart() = childCallerOperation;
        pWithCallback->callBack(op);
    }
}
#endif // IFSTMTPROCESSOR_H
