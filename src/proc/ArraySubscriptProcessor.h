#ifndef ARRAYSUBSCRIPTPROCESSOR_H
#define ARRAYSUBSCRIPTPROCESSOR_H

#include "clang/AST/RecursiveASTVisitor.h"
#include "../ClangOperationWrappers.h"
#include "../Comparator.h"
#include "../utils/OperationHelpers.h"
#include "IASTProcessor.h"
#include "../utils/Utils.h"
#include "../ParseState.h"
#include <spdlog/spdlog.h>

class ArraySubscriptProcessor : public IASTProcessor
{
public:
    ArraySubscriptProcessor(ArraySubscriptExpr* subscriptExpr, std::shared_ptr<ParseState> parseState);
    ~ArraySubscriptProcessor() { //std::cout << "Destroying ArraySubscriptProcessor at " << this << "\n";
                                 spdlog::get("sopelogger")->debug("Destroying ArraySubscriptProcessor at {}", (void*)this);
                               }
    void processNode(std::shared_ptr<IASTProcessor> parentWithCallback);
    void callBack(std::shared_ptr<operation> childCallerOperation);
private:
    std::vector<std::shared_ptr<operation>> getVariablesChangedInScope(int scope);
    AccessType deduceAccessType(std::shared_ptr<operation> indexOperation);
private:
    ArraySubscriptExpr* subsExpr;
    std::shared_ptr<ParseState> pState;
    std::shared_ptr<IASTProcessor> pWithCallback;
    std::shared_ptr<clsubsOperation> op;
    int numOfOperands;
};

ArraySubscriptProcessor::ArraySubscriptProcessor(ArraySubscriptExpr* subscriptExpr, std::shared_ptr<ParseState> parseState) : subsExpr(subscriptExpr), pState(parseState), numOfOperands(0)
{

}

void ArraySubscriptProcessor::processNode(std::shared_ptr<IASTProcessor> parentWithCallback) {
    pWithCallback = parentWithCallback;

    Stmt* parent = pState->assignmentOperChildren->getParent(subsExpr);
    while (parent != pState->currentAssignmentNode) {
        if (parent == nullptr) // parent not found in the map. therefore not part of the current assignment operator
            return;
        parent = pState->assignmentOperChildren->getParent(parent);
    }
    //std::cout << "[]" << std::endl;
    spdlog::get("sopelogger")->debug("[]");
    OperationType opType;
    bool addToStack = false;
    if (pState->currentAssignmentNode) { // Only continue if we already encountered an assignment operator
        if (subsExpr == pState->assignmentLHS) {
            addToStack = true;
            opType = OperationType::memoryStore;
        } else if (subsExpr == pState->assignmentRHS) {
            addToStack = true;
            opType = OperationType::memoryLoad;
        } else // recursively check parents and ancestors in the parent map to find if operator is on LHS or RHS
        {
            Stmt* parent = pState->assignmentOperChildren->getParent(subsExpr);
            while (parent != nullptr && parent != pState->assignmentLHS && parent != pState->assignmentRHS) {
                parent = pState->assignmentOperChildren->getParent(parent);

            }
            if (parent == pState->assignmentLHS) {
                opType = OperationType::memoryStore;
                addToStack = true;
            }
            else if (parent == pState->assignmentRHS) {
                opType = OperationType::memoryLoad;
                addToStack = true;
            }
            else {
                addToStack = false;
                //std::cout << "Subs operator is not a child of any assignment operator and therefore should not be added on stack\n";
                spdlog::get("sopelogger")->debug("Subs operator is not a child of any assignment operator and therefore should not be added on stack");
            }
        }
    }
    // Only if the parent is a binary(assignment or compound) operation and
    // only if the subscript appears on the LHS, will it be a memoryStore
    op = makeSubscriptOperation(subsExpr, opType);
    //pattern.addToPattern(std::shared_ptr<memOperation>(new ClangSubscriptWrapper<OperationTypeCompare>(subsExpr)));
    //pushOperationOnStacks(subsExpr, std::shared_ptr<memOperation>(new ClangSubscriptWrapper<OperationTypeCompare>(subsExpr, opType)));

    /*
    if(addToStack)
        pushOperationOnStacks(subsExpr, makeSubscriptOperation(subsExpr, opType));
    */
}


std::vector<std::shared_ptr<operation>> ArraySubscriptProcessor::getVariablesChangedInScope(int scope) {
    std::vector<std::shared_ptr<operation>> returns;
    for(auto it = pState->scopeTracker.variableChanges.begin(); it!=pState->scopeTracker.variableChanges.end(); it++) {
        if(it->second.find(scope) != it->second.end())
            returns.push_back(it->first);
    }
    return returns;
}

AccessType ArraySubscriptProcessor::deduceAccessType(std::shared_ptr<operation> indexOperation) {
  // This mostly depends on the scope being observed. If its the lowest scope then the accesstype might be
  // different than if we are looking at the operation from a higher scope
    //std::cout << "--- Deducing Access type ... " << std::endl;
    spdlog::get("sopelogger")->debug("--- Deducing Access type --- ");
    // hasMultiplicationParent could be either multiplication or division or modulo
    // If any of those operations are encountered then any variable that is referenced by this
    // oeprator or any of its nested oeprators should be considered non-contiguous
    std::stack<bool> isMultOperation;
    std::stack<int> operandCount;
    std::vector<std::shared_ptr<operation>> contiguousVars;
    std::vector<std::shared_ptr<operation>> noncontiguousVars;
    //std::cout << "Current scope = " << pState->numScopes << std::endl;
    spdlog::get("sopelogger")->debug("Current scope = {}",pState->numScopes);

    std::ostringstream oss;
    std::function<void(std::shared_ptr<operation>)> findVariableAccessType =
            [&isMultOperation, &operandCount, &contiguousVars, &noncontiguousVars, &oss]
            (std::shared_ptr<operation> toPrint)
    {
        //std::cout << "Found mult = " << hasMultiplicationParent << std::endl;
        if(clbinOperation* binOper = dyn_cast<clbinOperation>(toPrint.get())) {
            if( binOper->opCode().find("*")!=std::string::npos || binOper->opCode().find("/")!=std::string::npos) {
                //std::cout << "ENCOUNERED MULT OR DIV operation " << std::endl;
                //spdlog::get("sopelogger")->debug("ENCOUNERED MULT OR DIV operation");
                oss << "ENCOUNERED MULT OR DIV operation " << std::endl;
                isMultOperation.push(true);
                operandCount.push(0);
            }
            else {
                //std::cout << "ENCOUNERED ADD OR SUB operation " << std::endl;
                //spdlog::get("sopelogger")->debug("ENCOUNERED ADD OR SUB operation");
                oss << "ENCOUNERED ADD OR SUB operation " << std::endl;
                if(isMultOperation.empty())
                    isMultOperation.push(false);
                else {// push whatever was on top of the stack. Basically propagate whatever operation was on already on the stack whenever we encounter a new addition operator
                    isMultOperation.push(isMultOperation.top());
                }
                operandCount.push(0);
            }
        }
        if(!isMultOperation.empty()) {
            if(clvarRefOperation* varRef = dyn_cast<clvarRefOperation>(toPrint.get())) {
                //std::cout << varRef->variableLiteral() << " - " << (isMultOperation.top() ? "non-contiguous" : "contiguous") << std::endl;
                //spdlog::get("sopelogger")->debug("{} - {}",varRef->variableLiteral(), (isMultOperation.top() ? "non-contiguous" : "contiguous"));
                oss << varRef->variableLiteral() << " - " << (isMultOperation.top() ? "non-contiguous" : "contiguous") << std::endl;
                if(isMultOperation.top())
                    noncontiguousVars.push_back(toPrint);
                else
                    contiguousVars.push_back(toPrint);
                (operandCount.top())++;
                while(operandCount.top() == 2) {
                    operandCount.pop();
                    isMultOperation.pop();
                    if(operandCount.empty())
                        break;
                    operandCount.top()++; // add the operand count of the parent operation to simulate pushing the processed operation as an operand
                }
            }
        }
    };

    // Elie: Created the bound formatter because printFormatter changes signatures from void(std::shared_ptr) to void(shared, ostream)
    // So binding the new signature to a function with the old signature to make it compile
    std::function<void(std::shared_ptr<operation>)> boundFormatter = std::bind(&printFormatter, std::placeholders::_1, std::ref(oss));
    indexOperation->traversePreOrder(findVariableAccessType, boundFormatter, [](std::shared_ptr<operation>){return true;}, indexOperation);
    //std::cout << oss.str();
    spdlog::get("sopelogger")->debug("{}",oss.str());
    // The traversal in the previous line will find which variables might
    // cause a memory operation to be contiguous and which ones are not
    // The following loop will go over these potential contiguous variables
    // and check if they have changed in the currentScope. This will mean
    // that we have contiguous or sequential access. In the case that they're not changed
    // in the current scope, then this means that the memory access will be constant
    // with respect ot that variable
    for(auto it = contiguousVars.begin(); it!=contiguousVars.end(); it++) {
        clvarRefOperation* varRef = dyn_cast<clvarRefOperation>(it->get());
        auto variableChangesIt = pState->scopeTracker.variableChanges.find(varRef->variableDeclaration());
        if(variableChangesIt != pState->scopeTracker.variableChanges.end()) {
            // Look if the variable was changed in the current scope (numScopes)
            auto foundVarChangeInScope = variableChangesIt->second.find(pState->numScopes);
            if(foundVarChangeInScope != variableChangesIt->second.end()) {
                //std::cout << "found that variable " << varRef->variableLiteral() << " changed in scope " << pState->numScopes << std::endl;
                spdlog::get("sopelogger")->debug("found that variable {} changed in scope {}",varRef->variableLiteral(), pState->numScopes);
            }
        }
    }
    // Find all the variables that have changed in the currentScope. These
    // will then be checked with the variables that were extracted from the
    // memory access index. If any of those variables intersect, then we can
    // deduce the type of memory access by checking if the variable was already
    // classified as contiguous or not. If not intersection occurs, then the
    // memory access is constant

    std::vector<std::shared_ptr<operation>> variablesChangedInCurrentScope = getVariablesChangedInScope(pState->numScopes);
    for_each(variablesChangedInCurrentScope.begin(), variablesChangedInCurrentScope.end(), [](std::shared_ptr<operation> obj) { //std::cout << obj->getMainStringLiteral() << std::endl;
                                                                                                                              spdlog::get("sopelogger")->debug("{}",obj->getMainStringLiteral());
                                                                                                                              });

    // If any of the "nonContiguous" variables (these basically are variables that appear in multiplication operations)
    // are changing in the current scope, then it means the memory access is noncontiguous. Else its either
    // contiguous (if the "contiguous" variables are changing in the current scope) or constant (if none of
    // the variables change in the current scope)
    std::function<std::vector<std::shared_ptr<operation>>
            (std::vector<std::shared_ptr<operation>>& , std::vector<std::shared_ptr<operation>>&)> findCommonVariables =
            [](std::vector<std::shared_ptr<operation>>& vecA,std::vector<std::shared_ptr<operation>>& vecB) {
        std::vector<std::shared_ptr<operation>> returns;
        for(std::vector<std::shared_ptr<operation>>::iterator itA = vecA.begin(); itA != vecA.end(); itA++){
            std::for_each(vecB.begin(), vecB.end(), [&itA, &returns] (std::shared_ptr<operation>& vecBObject) {
                //std::cout << "Printing both elements using the base pointer: " << (*itA)->getMainStringLiteral() << " | " << vecBObject->getMainStringLiteral() << std::endl;
                spdlog::get("sopelogger")->debug("Printing both elements using the base pointer: {} | {}",(*itA)->getMainStringLiteral() , vecBObject->getMainStringLiteral());
                // vecA should be the variableChanges vector (which contains varDecls) while
                // vecB should contain varRefs
                clvarDeclOperation* elementA = dyn_cast<clvarDeclOperation>((*itA).get());
                clvarRefOperation* elementB = dyn_cast<clvarRefOperation>(vecBObject.get());
                //std::cout << "Printing elemA: " << elementA->getMainStringLiteral() << std::endl;
                //std::cout << "Printing elemB: " << elementB->variableLiteral() << std::endl;
                spdlog::get("sopelogger")->debug("Printing elemA: {}" , elementA->getMainStringLiteral());
                spdlog::get("sopelogger")->debug("Printing elemB: {}" , elementB->variableLiteral());
                if(elementA && elementB && *itA == elementB->variableDeclaration())
                    returns.push_back(vecBObject);
        });
        }
        return returns;
    };

    std::vector<std::shared_ptr<operation>> commonWithNonContiguous = findCommonVariables(variablesChangedInCurrentScope, noncontiguousVars);
    std::vector<std::shared_ptr<operation>> commonWithContiguous = findCommonVariables(variablesChangedInCurrentScope, contiguousVars);
    AccessType aType;
    if(!commonWithNonContiguous.empty()) {
        aType = AccessType::strobed;
        //std::cout << "ACCESS TYPE STROBED" << std::endl;
        spdlog::get("sopelogger")->debug("ACCESS TYPE STROBED");
    }
    else if(!commonWithContiguous.empty()) {
        aType = AccessType::contiguous;
        //std::cout << "ACCESS TYPE CONTIGUOUS" << std::endl;
        spdlog::get("sopelogger")->debug("ACCESS TYPE CONTIGUOUS");
    }
    else {
        aType = AccessType::constant;
        //std::cout << "ACCESS TYPE CONSTANT" << std::endl;
        spdlog::get("sopelogger")->debug("ACCESS TYPE CONSTANT");
    }

    //std::cout << "\n--- Deduced Access type --- " << std::endl;
    spdlog::get("sopelogger")->debug("--- Deduced Access type --- ");

    return aType;
}


void ArraySubscriptProcessor::callBack(std::shared_ptr<operation> childCallerOperation){
    //std::cout << "ArraySubscriptProcessor callback called" << std::endl;
    spdlog::get("sopelogger")->debug("ArraySubscriptProcessor callback called");

    if(numOfOperands==0) {
        op->variable() = childCallerOperation.get()->getMainStringLiteral();
        op->lhsVariable() = childCallerOperation;
        // Pop the child operation
        pState->processorStack.pop();
    }
    else if (numOfOperands == 1) {
        op->index() = childCallerOperation.get()->getMainStringLiteral();
        op->rhsIndex() = childCallerOperation;
        op->getAccessType() = deduceAccessType(childCallerOperation);
        // Pop the child operation
        pState->processorStack.pop();
        // At this point we processed both operands and can inform the parent operations
        // the we are ready to be treated as an operand
        pWithCallback->callBack(op);
    }
    numOfOperands++;
}

#endif // ARRAYSUBSCRIPTPROCESSOR_H
