#ifndef FLOATLITERALPROCESSOR_H
#define FLOATLITERALPROCESSOR_H

#include "clang/AST/RecursiveASTVisitor.h"
#include "../ClangOperationWrappers.h"
#include "../Comparator.h"
#include "../utils/OperationHelpers.h"
#include "IASTProcessor.h"
#include "../utils/Utils.h"
#include "../ParseState.h"
#include <spdlog/spdlog.h>

class FloatLiteralProcessor : public IASTProcessor
{
public:
    FloatLiteralProcessor(FloatingLiteral* floatingLiteral, std::shared_ptr<ParseState> parseState);
    ~FloatLiteralProcessor() { //std::cout << "Destroying FloatingLiteralProcessor at " << this << "\n";
                               spdlog::get("sopelogger")->debug("Destroying FloatingLiteralProcessor at {}",(void*)this);
                             }
    void processNode(std::shared_ptr<IASTProcessor> parentWithCallback);
    void callBack(std::shared_ptr<operation> childCallerOperation);
private:
    FloatingLiteral* floatLiteral;
    std::shared_ptr<ParseState> pState;
    std::shared_ptr<IASTProcessor> pWithCallback;
    std::shared_ptr<clfloatOperation> op;
};

FloatLiteralProcessor::FloatLiteralProcessor(FloatingLiteral* floatingLiteral, std::shared_ptr<ParseState> parseState) : floatLiteral(floatingLiteral), pState(parseState)
{

}

void FloatLiteralProcessor::processNode(std::shared_ptr<IASTProcessor> parentWithCallback) {
    pWithCallback = parentWithCallback;

    op = makeFloatLiteralOperation(floatLiteral);
    pWithCallback->callBack(op);
/*
    if(pState->isForLoopInit || pState->isForLoopCondition) { // we need to build this expression or else it will mess with all the coming operations
        // IntegerLiteral is an operand to some operation whether its a binary operation or
        // even a varDecl operation (which is equivalent to an assignment binary operator)
        pushVariableOnStacks(intLiteral, intWrapped);
    }
*/
}

void FloatLiteralProcessor::callBack(std::shared_ptr<operation> childCallerOperation) {
    //std::cout << "IntLiteralProcessor callback called" << std::endl;
    spdlog::get("sopelogger")->debug("IntLiteralProcessor callback called");
}

#endif // FLOATLITERALPROCESSOR_H
