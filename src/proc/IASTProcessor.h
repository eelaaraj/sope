#ifndef IASTPROCESSOR_H
#define IASTPROCESSOR_H

#include "../utils/TypeDefinitions.h"
/*
 * IASTProcessor: Interface for all ast processors. Even the ClangASTProcessor implements this
 *                interface to be consistent with all other interfaces in such a way that it
 *                can be notified when the last node processor finishes.
 *                All classes implementing this interfaace have to implement processNode which
 *                should start the processing on the actual AST decl or stmt. This function
 *                takes its parent as an argument. This is because we want a mechanism to
 *                callback the parent when processNode finishes its execution. This is done
 *                usign the callBack function which should be implemetned by all classes of this
 *                interface.
 */
class IASTProcessor {
public:
    virtual ~IASTProcessor() { /*std::cout << "destroying IASTProcessor at "<< this << "\n";*/}
    virtual void processNode(std::shared_ptr<IASTProcessor> parentWithCallback) = 0;
    virtual void callBack(std::shared_ptr<operation> childCallerOperation) = 0;
};


#endif // IASTPROCESSOR_H

