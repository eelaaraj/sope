#ifndef COMPOUNDSTMTPROCESSOR_H
#define COMPOUNDSTMTPROCESSOR_H

#include "clang/AST/RecursiveASTVisitor.h"
#include "../ClangOperationWrappers.h"
#include "../Comparator.h"
#include "../utils/OperationHelpers.h"
#include "IASTProcessor.h"
#include "../utils/Utils.h"
#include "../ParseState.h"
#include <spdlog/spdlog.h>

class CompoundStmtProcessor : public IASTProcessor
{
public:
    CompoundStmtProcessor(CompoundStmt* compoundStmt, std::shared_ptr<ParseState> parseState);
    ~CompoundStmtProcessor() { //std::cout << "Destroying CompoundStmtProcessor at " << this << "\n";
                               spdlog::get("sopelogger")->debug("Destroying CompoundStmtProcessor at {}", (void*)this );
                             }
    void processNode(std::shared_ptr<IASTProcessor> parentWithCallback);
    void callBack(std::shared_ptr<operation> childCallerOperation);

private:
    CompoundStmt* compStmt;
    std::shared_ptr<ParseState> pState;
    std::shared_ptr<IASTProcessor> pWithCallback;
    std::shared_ptr<clcompoundOperation> op;
    int numOfStmts;
    Stmt* lastStmtInBody;
};


CompoundStmtProcessor::CompoundStmtProcessor(CompoundStmt* compoundStmt, std::shared_ptr<ParseState> parseState) : compStmt(compoundStmt), pState(parseState), numOfStmts(compStmt->size()), lastStmtInBody(compStmt->body_back())
{
}

void CompoundStmtProcessor::processNode(std::shared_ptr<IASTProcessor> parentWithCallback) {
    pWithCallback = parentWithCallback;

    op = makeCompoundOperation(compStmt);
    FullSourceLoc FullLocation = pState->Context->getFullLoc(compStmt->getLocStart());
    if (FullLocation.isValid()) {
        //std::cout << " Found Compound stmt inside function " << pState->function->getNameAsString() << " at "
        //          << FullLocation.printToString(pState->Context->getSourceManager()) << std::endl;
        spdlog::get("sopelogger")->debug(" Found Compound stmt inside function {} at {}",pState->function->getNameAsString(),FullLocation.printToString(pState->Context->getSourceManager()));
    }

}

void CompoundStmtProcessor::callBack(std::shared_ptr<operation> childCallerOperation) {
    //std::cout << "CompoundStmtProcessor callback called - # of stmts = " << numOfStmts << std::endl;
    spdlog::get("sopelogger")->debug("CompoundStmtProcessor callback called - # of stmts = {}", numOfStmts);

    op->addStatement(childCallerOperation);
    // Pop the child operation
    pState->processorStack.pop();
    // Call the parent
    // if(body is processed || (body size is 0 && (all init, cond and inc were processed)) => callback the parent
    if(childCallerOperation->getUnderlyingPtr() == lastStmtInBody) {
        // We should not decrement scopes here. We should do it in the for, if, and function
        // and not compound statements. This is because for loop will have a new scope
        // when it initializes a variable in its init or condition parts.
        pWithCallback->callBack(op);
    }
}

#endif // COMPOUNDSTMTPROCESSOR_H
