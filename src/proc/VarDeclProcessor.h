#ifndef VARDECLPROCESSOR_H
#define VARDECLPROCESSOR_H

#include "clang/AST/RecursiveASTVisitor.h"
#include "../ClangOperationWrappers.h"
#include "../Comparator.h"
#include "../utils/OperationHelpers.h"
#include "IASTProcessor.h"
#include "../utils/Utils.h"
#include "../ParseState.h"
#include <spdlog/spdlog.h>

class VarDeclProcessor : public IASTProcessor
{
public:
    VarDeclProcessor(VarDecl* varDeclaration, std::shared_ptr<ParseState> parseState);
    ~VarDeclProcessor() { //std::cout << "Destroying VarDeclProcessor at " << this << "\n";
                          spdlog::get("sopelogger")->debug("Destroying VarDeclProcessor at {}", (void*)this);
                        }
    void processNode(std::shared_ptr<IASTProcessor> parentWithCallback);
    void callBack(std::shared_ptr<operation> childCallerOperation);
private:
    VarDecl* varDecl;
    std::shared_ptr<ParseState> pState;
    std::shared_ptr<IASTProcessor> pWithCallback;
    std::shared_ptr<clvarDeclOperation> op;
};

VarDeclProcessor::VarDeclProcessor(VarDecl* varDeclaration, std::shared_ptr<ParseState> parseState) : varDecl(varDeclaration), pState(parseState)
{

}

void VarDeclProcessor::processNode(std::shared_ptr<IASTProcessor> parentWithCallback) {
    pWithCallback = parentWithCallback;

    ast_type_traits::DynTypedNode node = ast_type_traits::DynTypedNode::create<VarDecl>(*varDecl);
    ast_type_traits::DynTypedNode immediateParent = pState->Context->getParents(node).front();
    const DeclStmt* declStmt = immediateParent.get<DeclStmt>();

    op = makeVarDeclOperation(varDecl, const_cast<DeclStmt*>(declStmt));
    pState->scopeTracker.scopeVariablesMap[pState->numScopes].push_back( op);
    pState->declMap[varDecl] =  op;
    //std::cout << "--- Found in scope[" << pState->numScopes << "] VarDecl = " << varDecl->getDeclName().getAsString() <<"\n";
    spdlog::get("sopelogger")->debug("--- Found in scope[{}] VarDecl = {}", pState->numScopes, varDecl->getDeclName().getAsString());
    if(varDecl->hasInit()) {
        Expr* varDeclInit = varDecl->getInit();
        if(dyn_cast<BinaryOperator>(varDeclInit)) {
            //std::cout << "--- Init is = BinaryOperator\n";
            spdlog::get("sopelogger")->debug("--- Init is = BinaryOperator");
        }
        else if(dyn_cast<ImplicitCastExpr>(varDeclInit)){
            //std::cout << "--- Init is = ImplicitCastExpr\n";
            spdlog::get("sopelogger")->debug("--- Init is = ImplicitCastExpr");
        }
        else if(dyn_cast<IntegerLiteral>(varDeclInit)){
            //std::cout << "--- Init is = IntegerLiteral\n";
            spdlog::get("sopelogger")->debug("--- Init is = IntegerLiteral");
        }
        else if(dyn_cast<FloatingLiteral>(varDeclInit)){
            //std::cout << "--- Init is = FloatLiteral\n";
            spdlog::get("sopelogger")->debug("--- Init is = FloatLiteral");
        }
        else if(dyn_cast<CallExpr>(varDeclInit)){
            //std::cout << "--- Init is = some form of CallExpr\n";
            spdlog::get("sopelogger")->debug("--- Init is = some form of CallExpr");
        }
        else{
            //std::cout << "--- Init is = Unknown yet\n";
            spdlog::get("sopelogger")->debug("--- Init is = Unknown yet");
        }
    }
    else {
        //std::cout << "--- Init is = Uninitialized\n";
        spdlog::get("sopelogger")->debug("--- Init is = Uninitialized");
        // If its unitialized, we need to call callback ourselves
        op->init() = nullptr;
        op->initExpression() = "";
        pWithCallback->callBack(op);
    }

    /*
    if(pState->isForLoopInit || pState->isForLoopCondition) { // we need to build this expression or else it will mess with all the coming operations
        // We need to push a Stmt* on the stack. So we get the parent of varDecl which is an
        // adapter DeclStmt*
        if(varDecl->hasInit()) { // This is because some varDecls like function parameters have no inits
            std::cout << "The parent of varDecl is of class : " << immediateParent.get<Stmt>()->getStmtClassName() << " = " << declStmt << std::endl;
            std::cout << "VarDecl = " << varDecl << std::endl;
            pushOperationOnStacks(declStmt,  op, true);
        }
    }
    */
}

void VarDeclProcessor::callBack(std::shared_ptr<operation> childCallerOperation) {
    // VarDecl will have only one child calling it back.
    // Therefore now its legit to pop that last processor from the stack
    // and then call the parent of this VarDeclProcessor
    //std::cout << "VarDeclProcessor callback called" << std::endl;
    spdlog::get("sopelogger")->debug("VarDeclProcessor callback called");
    op->init() = childCallerOperation;
    op->initExpression() = childCallerOperation->getMainStringLiteral();

    // Pop the child operation
    pState->processorStack.pop();
    // Call the parent
    pWithCallback->callBack(op);
}

#endif // VARDECLPROCESSOR_H
