#ifndef DECLREFPROCESSOR_H
#define DECLREFPROCESSOR_H

#include "clang/AST/RecursiveASTVisitor.h"
#include "../ClangOperationWrappers.h"
#include "../Comparator.h"
#include "../utils/OperationHelpers.h"
#include "IASTProcessor.h"
#include "../utils/Utils.h"
#include "../ParseState.h"
#include <spdlog/spdlog.h>

class DeclRefProcessor : public IASTProcessor
{
public:
    DeclRefProcessor(DeclRefExpr* declRefExpr, std::shared_ptr<ParseState> parseState);
    ~DeclRefProcessor() { //std::cout << "Destroying DeclRefProcessor at " << this << "\n";
                          spdlog::get("sopelogger")->debug("Destroying DeclRefProcessor at {}", (void*)this);
                        }
    void processNode(std::shared_ptr<IASTProcessor> parentWithCallback);
    void callBack(std::shared_ptr<operation> childCallerOperation);
private:
    DeclRefExpr* declRef;
    std::shared_ptr<ParseState> pState;
    std::shared_ptr<IASTProcessor> pWithCallback;
    std::shared_ptr<clvarRefOperation> op;
};

DeclRefProcessor::DeclRefProcessor(DeclRefExpr* declRefExpr, std::shared_ptr<ParseState> parseState) : declRef(declRefExpr), pState(parseState)
{

}

void DeclRefProcessor::processNode(std::shared_ptr<IASTProcessor> parentWithCallback) {
    pWithCallback = parentWithCallback;

    // NOTE: changed this check from the initial check that was in the initial ASTSequence builder
    // if(pState->isForLoopInit || pState->isForLoopCondition || pState->isForLoopIncrement || pState->isUnaryOperation) {
    if(pState->function) {
        //pattern.addToPattern(std::shared_ptr<>)
        //std::cout << "DeclRefExpr: "
        //          << declRef->getDecl()->getNameAsString() << " | Global ID = " << declRef->getDecl() << std::endl;
        spdlog::get("sopelogger")->debug("DeclRefExpr: {} | Global ID = {}", declRef->getDecl()->getNameAsString(), (void*)declRef->getDecl());

        op = makeVarRefOperation(declRef);
        op->variableDeclaration() = pState->declMap[declRef->getDecl()]; // this line was moved from the makeVarRefOperation function because it does not have access to declMap[]
        std::shared_ptr<operation> upcasted = op;
        if(!dyn_cast<clvarRefOperation>(upcasted.get())) {
            //std::cout << "--------------------------------- couldn't cast operation to clvarRefOperation" << std::endl;
            spdlog::get("sopelogger")->debug("--------------------------------- couldn't cast operation to clvarRefOperation");
        }

        if(pState->isUnaryOperation ) {// reset it
            pState->isUnaryOperation = false;
            // indicate the usage of that variable in the usage map
            pState->scopeTracker.variableChanges[op->variableDeclaration()].insert(pState->numScopes);
            //std::cout << "Inserted var " << declRef->getDecl()->getNameAsString() << " in variableChanges at scope : " << pState->numScopes << std::endl;
            spdlog::get("sopelogger")->debug("Inserted var {} in variableChanges at scope : {}", declRef->getDecl()->getNameAsString(), pState->numScopes);
        }
        // In the case of DeclRefExpr, we can callback the parent directly here because
        // no more processing can occur after we have encountered a leaf declref
        pWithCallback->callBack(op);
    }
}

void DeclRefProcessor::callBack(std::shared_ptr<operation> childCallerOperation) {
// No one will actually callback a declrefProcessor
    //std::cout << "DeclRefProcessor callback called" << std::endl;
    spdlog::get("sopelogger")->debug("DeclRefProcessor callback called");
}

#endif // DECLREFPROCESSOR_H
