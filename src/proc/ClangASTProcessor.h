#ifndef CLANGASTPROCESSOR_H
#define CLANGASTPROCESSOR_H

#include <map>
#include "clang/AST/RecursiveASTVisitor.h"
#include "../utils/TypeDefinitions.h"
#include "../ClangOperationWrappers.h"
#include "../Comparator.h"
#include "../utils/OperationHelpers.h"
#include "../PatternPostProcessor.h"
#include "../PatternTreeCreator.h"
// Processor includes
#include "FunctionDeclProcessor.h"
#include "ForStmtProcessor.h"
#include "BinOpProcessor.h"
#include "UnaryOpProcessor.h"
#include "VarDeclProcessor.h"
#include "DeclRefProcessor.h"
#include "ArraySubscriptProcessor.h"
#include "IntLiteralProcessor.h"
#include "FloatLiteralProcessor.h"
#include "IfStmtProcessor.h"
#include "CompoundStmtProcessor.h"
#include "CallExprProcessor.h"
#include "IASTProcessor.h"
#include "../utils/Utils.h"
#include "../ParseState.h"
#include "../Injector.h"

#include "../serialize/JointJsonGenerator.h"
#include "../serialize/PatternTreeSerializer.h"
#include "clang/Analysis/CFG.h"
#include "../defuse/DefUse.h"
#include "../knowledge_base/MockUtils.h"
#include <spdlog/spdlog.h>

using namespace clang;
using namespace clang::tooling;
using namespace pattern::matching::wrapper;
using namespace pattern::matching;

/*
 * ClangASTProcessor: Is the class resposible for processing each type of Stmt in the visitor class.
 *                    When a certain decl or stmt is encountered, a corresponding function is called.
 *                    In that function, we have the flexibility of doing some pre-processing such as
 *                    maintaining stack, ....
 *                    After we are done with pre-processing, we can call the appropriate ASTProcessor
 *                    responsible of handling that stmt or decl.
 *                    After that we can do some post-processing such as hooking/wiring ASTProcessors
 *                    together so that they can report to their parents when they are done processing
 *                    their nodes.
 *
 */
template <typename PostProcessor>
class ClangASTProcessor : public IASTProcessor
{
public:
    //ClangASTProcessor(ASTContext *Context, std::shared_ptr<Repository<operation> > repository, const std::string& fName, const std::vector<std::string>& funcNames, Replacements* replace, bool injection);
    ClangASTProcessor(std::shared_ptr<ParseState> pState, const std::shared_ptr<IASTProcessor>& selfPointer);// : parseState(pState) { /*processorStack.push(this);*/}
    ~ClangASTProcessor() {
        //finalizeOperationProcessing();
        if(parseState->assignmentOperChildren)
            delete parseState->assignmentOperChildren;
        if(parseState->functionParentMap)
            delete parseState->functionParentMap;
        //std::cout << "destroying ClangASTProcessor at "<< this << "\n";
        spdlog::get("sopelogger")->debug("destroying ClangASTProcessor at {}",(void*) this);
    }

    bool processCallExpr(CallExpr* callExpr);
    bool processBinaryOperator(BinaryOperator* binaryOperator);
    bool processUnaryOperator(UnaryOperator* unaryOperator);
    bool processForStmt(ForStmt* forStmt); // increments scope
    bool processIfStmt(IfStmt* ifStmt); // increments scope
    bool processCompoundStmt(CompoundStmt* compoundStmt); // does not increment scope. For, if, and function do it already
    bool processFunctionDecl(FunctionDecl* functionDecl); // sets the scope to 1
    bool processRecordDecl();
    bool processArraySubscriptExpr(ArraySubscriptExpr* arraySubscriptExpr);
    bool processVariableDecl(VarDecl* varDecl); // assigns the new variable to the currentScope and adds its to the ScopeTracker
    bool processDeclRefExpr(DeclRefExpr* declRefExpr);
    bool processIntegerLiteral(IntegerLiteral* integerLiteral);
    bool processFloatingLiteral(FloatingLiteral* floatLiteral);
    void processNode(std::shared_ptr<IASTProcessor> parentWithCallback);
    void callBack(std::shared_ptr<operation> childCallerOperation);
    void setSelfSharedPointer(std::shared_ptr<IASTProcessor> selfPointer);
private:
    void finalizeOperationProcessing();
    std::vector<std::shared_ptr<operation>> popTillParent(Stmt*);
    void resetStateVariables();
    void clearScopeVariables(int scopeNumber);
    void popLowerScopes(Stmt* parent);
    std::vector<std::shared_ptr<operation>> getVariablesChangedInScope(int scope);
    AccessType deduceAccessType(std::shared_ptr<operation> indexOperation);
    void setupAndCheckAssignmentOper(BinaryOperator*& binOper);
    void setupAndCheckVarDeclInit(VarDecl* varDecl);
    void processIfNextIsUnaryOperation();
    void processUnaryOperation();
    void processBinaryOperation();
    Stmt* getMajorParent(Stmt* stmt);
    void pushVariableOnStacks(Expr* declExpr, std::shared_ptr<operation> wrappedDecl);
    void pushOperationOnStacks(const Stmt* oper, std::shared_ptr<operation> wrappedOperation, bool pushOnParentStack=false);
    void patternGenerator(std::shared_ptr<operation> oper);
    //clang::defuse::DefUse* runDefUseAnalysis(FunctionDecl* function, Stmt* functionBody, ASTContext* context, CFG* cfg, bool verbose);
    //void queryMatchingTrees(ITreeQuery *&treeQuery, ITreeQueryResult *&treeQueryResult, IMetricQuery *&metricQuery, IMetricQueryResult *&metricQueryResult);
    //void queryDefUse(clang::defuse::DefUse* du, CFG* cfg, std::shared_ptr<ParseState>, ITreeQueryResult* treeQueryResult);
private:
    int currentScope;
    //ScopeTracker<operation> scopeTracker;
    std::map<ForStmt*, std::shared_ptr<LoopInfoStruct>> loopMap;
private:
    std::shared_ptr<ParseState> parseState;
    // We pass in a reference of a shared pointer because we don't want to increment the
    // use_count() of the shared_ptr. If we pass in the shared_ptr by value, then the
    // use_count will be 2 instantaneouly (1 by the caller who created the ClangASTProcessor,
    // and 1 for the self pointer). This will in turn prevent the shared_ptr from destroying
    // the instance because the count will never reach 0. It will decrement once when the
    // caller class gets destroyed, but that's it. The clangASTprocessor will keep a self-pointer
    // to itself and will never get destroyed.
    std::weak_ptr<IASTProcessor> selfSharedPointer;
    // PostProcessor is what performs the SOPE functionalities after the ClangASTProcessor
    // finishes parsing the code, creating the AST, and creating our wrapper Operation objects
    // from the AST. The PostProcessor will either BENCHMARK the code we are parsing
    // OR it will LOOKUP a code similar to it.
    PostProcessor processingPolicy;
};
/*
bool ClangASTProcessor::processVariableDecl(VarDecl* varDecl) {
    std::shared_ptr<operation> varDeclWrapped = makeVarDeclOperation(varDecl, nullptr);
    parseState->scopeTracker.scopeVariablesMap[currentScope].push_back(varDeclWrapped);
    return true;
}
*/

//ClangASTProcessor::ClangASTProcessor(ASTContext *Context, std::shared_ptr<Repository<operation>> repository, const std::string& fName,
//                                     const std::vector<std::string>& funcNames, Replacements* replace, bool injection): selfSharedPointer(nullptr)
//{
//    parseState = std::shared_ptr<ParseState>(new ParseState());
//    parseState->Context = Context;
//    parseState->repo = repository;
//    parseState->fileName = fName;
//    parseState->functionNames = funcNames;
//    parseState->replacements = replace;
//    parseState->inject = injection;
    //processorStack.push(this);
//}

template<typename PostProcessor>
ClangASTProcessor<PostProcessor>::ClangASTProcessor(std::shared_ptr<ParseState> pState, const std::shared_ptr<IASTProcessor>& selfPointer) : parseState(pState), selfSharedPointer(selfPointer) { /*processorStack.push(this);*/}

template<typename PostProcessor>
void ClangASTProcessor<PostProcessor>::setSelfSharedPointer(std::shared_ptr<IASTProcessor> selfPointer) {
    selfSharedPointer = selfPointer;
}

// Used as a function pointer to add operation to the pattern data structure
template<typename PostProcessor>
void ClangASTProcessor<PostProcessor>::patternGenerator(std::shared_ptr<operation> oper) {
    if(!(dyn_cast<varRefOperation>(oper.get()))) {
        parseState->pattern.addToPattern(oper);
        //llvm::outs() << "\nAdded operation " << oper.get() << " to pattern" << " operation type: " << oper->getOpType() << " kind: " << oper->getKind();
        //llvm::outs() << "\nAccessing operation from pattern" << pattern.at(pattern.size()-1).get() << " operation type: " << pattern.at(pattern.size()-1)->getOpType() << " kind: " << pattern.at(pattern.size()-1)->getKind();
    }
}

// Used as a function pointer to add operation to the pattern data structure
void patternGenerator(std::shared_ptr<operation> oper, std::shared_ptr<ParseState> parseState) {
    if(!(dyn_cast<varRefOperation>(oper.get()))) {
        parseState->pattern.addToPattern(oper);
        //llvm::outs() << "\nAdded operation " << oper.get() << " to pattern" << " operation type: " << oper->getOpType() << " kind: " << oper->getKind();
        //llvm::outs() << "\nAccessing operation from pattern" << pattern.at(pattern.size()-1).get() << " operation type: " << pattern.at(pattern.size()-1)->getOpType() << " kind: " << pattern.at(pattern.size()-1)->getKind();
    }
}


template<typename PostProcessor>
void ClangASTProcessor<PostProcessor>::resetStateVariables() {
    // Shouldn't reset "function" variable because we check it in the else statement in the VisitFuncDecl
    parseState->function = nullptr;
    parseState->numScopes = 1; // The function scope is 1
    parseState->loopId = 0;
    parseState->loop = nullptr;
    if(parseState->functionParentMap) {
        //std::cout << "Before deleting parent map\n";
        spdlog::get("sopelogger")->debug("Before deleting parent map");
        delete parseState->functionParentMap;
        //std::cout << "After deleting parent map\n";
        spdlog::get("sopelogger")->debug("After deleting parent map");
        parseState->functionParentMap = nullptr;
    }
    parseState->currentParent = nullptr;
    parseState->isForLoopInit = true;
    parseState->isForLoopCondition = false;
    parseState->isForLoopIncrement = false;
    parseState->isNewAssignmentStmt = true;
    parseState->isUnaryOperation = false;
    parseState->currentAssignmentNode = nullptr;
    parseState->assignmentLHS = nullptr;
    parseState->assignmentRHS = nullptr;
    if (parseState->assignmentOperChildren) {
      delete parseState->assignmentOperChildren;
      parseState->assignmentOperChildren = nullptr;
    }
    parseState->pattern = Pattern<operation>();
    // These should be empty. If they're not, then there's something wrong usually.
    if(!parseState->clangOperationStack.empty() || !parseState->clangOperationWrapperStack.empty() || !parseState->processedOperationStack.empty() || !parseState->operationParentStack.empty() || !parseState->operationOnlyStack.empty()) {
        std::cerr << "One of the stacks was not empty upon reset" << std::endl;
    }
    while(!parseState->clangOperationStack.empty())
        parseState->clangOperationStack.pop();
    while(!parseState->clangOperationWrapperStack.empty())
        parseState->clangOperationWrapperStack.pop();
    while(!parseState->processedOperationStack.empty())
        parseState->processedOperationStack.pop();
    while(!parseState->operationParentStack.empty())
        parseState->operationParentStack.pop();
    while(!parseState->operationOnlyStack.empty())
        parseState->operationOnlyStack.pop();
}

/*
 * Removes all the variables from one scope by searching for the key=scope.
 * Then remove the found entry from the map. This will basically remove (key,pair)
 * which is the (scopeNumber, vector of variables). Thus all the variables in
 * that scope will be removed
*/
template<typename PostProcessor>
void ClangASTProcessor<PostProcessor>::clearScopeVariables(int scopeNumber) {
    ScopeTracker<operation>::ScopeIterator poppedScopeVariables =parseState->scopeTracker.scopeVariablesMap.find(scopeNumber); // Clear the variables of the popped scope
    std::cout << "Before clearScopeVariables VariableChanges size = " << parseState->scopeTracker.variableChanges.size() << std::endl;
    if(poppedScopeVariables != parseState->scopeTracker.scopeVariablesMap.end()) {
        // First also remove the variables that are defined in that scope
        // from the variableChanges map. This is because there might be redeclations
        // of the same variables later on and we dont want to be mislead
        std::vector<std::shared_ptr<operation>>::iterator deletedScopeVariableIt = poppedScopeVariables->second.begin();
        while(deletedScopeVariableIt != poppedScopeVariables->second.end()) {
            std::map<std::shared_ptr<operation>, std::set<int>>::iterator searchResult = parseState->scopeTracker.variableChanges.find(*deletedScopeVariableIt);
            if(searchResult != parseState->scopeTracker.variableChanges.end()) {
                parseState->scopeTracker.variableChanges.erase(searchResult);
                std::cout << " Deleted a record from variableChanges container" << std::endl;
            }
            deletedScopeVariableIt++;
        }
        parseState->scopeTracker.scopeVariablesMap.erase(poppedScopeVariables);
    }
    std::cout << "After clearScopeVariables VariableChanges size = " << parseState->scopeTracker.variableChanges.size() << std::endl;
}

template<typename PostProcessor>
void ClangASTProcessor<PostProcessor>::setupAndCheckAssignmentOper(BinaryOperator*& binOper) {
    // Check if the operator is an assignmentOperator. If it is then setup the parent map of its descendents
    // and keep track of its as being the current Top-Level operation (by keeping a pointer to it and its LHS and RHS
      if (binOper->isAssignmentOp() || binOper->isCompoundAssignmentOp()) {
          parseState->isNewAssignmentStmt = true;
          parseState->currentAssignmentNode = binOper;
          parseState->assignmentLHS = binOper->getLHS();
          parseState->assignmentRHS = binOper->getRHS();
          // Create a parent map for all descendents of the assignment operator
          // This will be used to check if the next binaryOperators are children of the LHS OR the RHS of the assignment
          if (parseState->assignmentOperChildren) {
              // delete the parent map because we will create a new one for the new assignment oeprator
              delete parseState->assignmentOperChildren;
          }
          // I should have been able to create a map on the stack but it was always null. So i had to create on heap.
          parseState->assignmentOperChildren = new ParentMap(binOper);
          parseState->assignmentOperChildren->addStmt(binOper); // I shouldnt need to do this but for some reason it only worked if i did
      } else {
          parseState->isNewAssignmentStmt = false;
      }
}
template<typename PostProcessor>
void ClangASTProcessor<PostProcessor>::setupAndCheckVarDeclInit(VarDecl* varDecl) {
    if(varDecl->hasInit()) {
        ast_type_traits::DynTypedNode node = ast_type_traits::DynTypedNode::create<VarDecl>(*varDecl);
        ast_type_traits::DynTypedNode immediateParent = parseState->Context->getParents(node).front();
        DeclStmt* declStmt = const_cast<DeclStmt*>(immediateParent.get<DeclStmt>());

        parseState->isNewAssignmentStmt = true;
        /*parseState->currentVarDeclInitNode = varDecl;
        parseState->initLHS = varDecl;
        parseState->initRHS= varDecl->getInit();*/
        parseState->currentAssignmentNode = declStmt;
        parseState->assignmentLHS = declStmt;
        parseState->assignmentRHS = varDecl->getInit();
        // Create a parent map for all descendents of the assignment operator
        // This will be used to check if the next binaryOperators are children of the LHS OR the RHS of the assignment
        if (parseState->assignmentOperChildren) {
            // delete the parent map because we will create a new one for the new assignment oeprator
            delete parseState->assignmentOperChildren;
        }
        // I should have been able to create a map on the stack but it was always null. So i had to create on heap.
        parseState->assignmentOperChildren = new ParentMap(declStmt);
        parseState->assignmentOperChildren->addStmt(declStmt); // I shouldnt need to do this but for some reason it only worked if i did
    }
    else {
         parseState->isNewAssignmentStmt = false;
    }
}

template<typename PostProcessor>
bool ClangASTProcessor<PostProcessor>::processFunctionDecl(FunctionDecl* functionDecl) {
    // resets all variables including "function" variable. We don't that variable anymore since we already finalized processing
    // in the previous check.
    resetStateVariables();
    assert(parseState);

    if (!parseState->functionNames.empty() && std::find(parseState->functionNames.begin(), parseState->functionNames.end(), functionDecl->getNameAsString()) != parseState->functionNames.end())
    {
        std::string empty ="";
        PrintingPolicy pp(parseState->Context->getLangOpts());
        llvm::raw_string_ostream rso(empty);
        for(Decl* d : functionDecl->decls()) {
            d->print(rso, pp);
            rso<<",";
        }
        rso<<"(";
        for(unsigned int i=0; i < functionDecl->getNumParams();i++) {
            functionDecl->getParamDecl(i)->print(rso,pp);
            if(i!=functionDecl->getNumParams()-1) {
                rso<<",";
            }
        }
        rso<<")";
        spdlog::get("sopelogger")->info("FUNCTION args: {}", rso.str());
        // Everytime a new function is found, it means all the state variables have to be reset.
        std::shared_ptr<IASTProcessor> functionProcessor = std::shared_ptr<IASTProcessor>(new FunctionDeclProcessor(functionDecl, parseState));
        parseState->processorStack.push(functionProcessor);
        // TODO: Not a safe way of passing shared_ptr to this. The shared_ptr
        // does not manage the native ptr. Its just a wrapper in this case.
        //functionProcessor->processNode(std::shared_ptr<IASTProcessor>(this));
        functionProcessor->processNode(selfSharedPointer.lock());
        // set the correspoding variables of the parseState
        parseState->function = functionDecl;
    }
    else {
        // This is not necessary anymore since we already reset the variables after finalizing the processing
        // at the beginning of this function.
        parseState->function = nullptr;
    }
    return true;
}

template<typename PostProcessor>
bool ClangASTProcessor<PostProcessor>::processCallExpr(CallExpr* callExpr) {
    if(parseState->function) {
        // The operationType::cxxMemberCall can be set if we are are processing CXXMembercall instead of callexpr
        std::shared_ptr<IASTProcessor> callExprProcessor = std::shared_ptr<IASTProcessor>(new CallExprProcessor(callExpr, OperationType::call, parseState));
        auto parentProcessor = parseState->processorStack.top();
        parseState->processorStack.push(callExprProcessor);
        callExprProcessor->processNode(parentProcessor);
    }
    return true;
}

/* This function takes the current parse state as input and extracts the
 * current scope and uses that scope to take a snapshot of all the variables
 * that are visible so far by that scope. The returned result will
 * contain the variables in the pevious parent scopes that have been
 * declared til now. Any declarations that might occur (in a parent scope
 * but after the current scope ends) will not be visible yet.
*/
/*
std::vector<std::string> takeScopeVariableSnapshot(ParseState& parseState) {
    std::vector<std::string> parentScopesVisibleVars;
    int currentScope = parseState.numScopes;
    std::map<int, std::vector<std::shared_ptr<T>>>::iterator beginIt =  parseState.scopeTracker.scopeVariablesMap.begin();
    while(beginIt != parseState.scopeTracker.scopeVariablesMap.end()) {
        int scope = beginIt->first;
        if(scope != currentScope) {
            auto scopeVars = parseState.scopeTracker.scopeVariablesMap[scope];
            for(auto var : scopeVars)
                parentScopesVisibleVars.push_back(var->variableLiteral);
        }
    }
    return parentScopesVisibleVars;
}
*/

/* Called by processForStmt. It Unravels the expr and builds a stirng expression of the loop.
 * It replaces all variables with their corresponding mapVariables that will be passed in when
 * querying datapoints from the knowledge base. Ex: for(int k=0; k<d; k++ {
 * becomes : for(iVars["k"] = 0; iVars["k"] < iVars["d"]; iVars["k"]++)
 */
void unravelExpr(Expr* expr, /*std::function<void()> callable */ std::ostringstream& oss) {
    if(BinaryOperator* binOp = dyn_cast<BinaryOperator>(expr)) {
        unravelExpr(binOp->getLHS(), oss);
        oss << binOp->getOpcodeStr().str();
        unravelExpr(binOp->getRHS(), oss) ;
    }
    else if(UnaryOperator* unaryOp = dyn_cast<UnaryOperator>(expr)) {
        oss << unaryOp->getOpcodeStr(unaryOp->getOpcode()).str();
        unravelExpr(unaryOp->getSubExpr(), oss) ;
    }
    else if(DeclRefExpr* declRef = dyn_cast<DeclRefExpr>(expr)) {
        std::string name = declRef->getNameInfo().getAsString();
        const Type* varType = declRef->getType().getTypePtr();
        if(!varType->isPointerType()) {
            if(varType->isIntegerType()) {
                oss << "intVars[\"";
            }
            else if(varType->isFloatingType()) {
                oss << "floatVars[\"";
            }
            else if(varType->isCharType()) {
                oss << "stringVars[\"";
            }
            oss << name << "\"]" ;
        }
        else {
            spdlog::get("sopelogger")->error("Loop expression could not be unraveled. One of the variables is a pointer type");
        }
    }
    else if(IntegerLiteral* intLiteral = dyn_cast<IntegerLiteral>(expr)) {
        SmallVector<char,3> str;
        intLiteral->getValue().toStringSigned(str, 10);
        StringRef stref(str.data(), str.size());
        oss << stref.str();
    }
    else if(FloatingLiteral* floatLiteral = dyn_cast<FloatingLiteral>(expr)) {
        SmallVector<char,3> str;
        floatLiteral->getValue().toString(str, 10);
        StringRef stref(str.data(), str.size());
        oss << stref.str();
    }
    else if(StringLiteral* stringLiteral = dyn_cast<StringLiteral>(expr)) {
        oss << stringLiteral->getString().str();
    }
    else if(ImplicitCastExpr* implicitCastExpr = dyn_cast<ImplicitCastExpr>(expr)) { // for(;k<d;k++), k and d in the condition are both casted to Rvalues first so we don't see a declrefExpr, but instead an implicitCastExpr
        unravelExpr(implicitCastExpr->getSubExpr(), oss);
    }
    else {
        spdlog::get("sopelogger")->error("Loop expression could not be unraveled. One of the expression types is not supported: {}", expr->getType().getAsString());
    }
}

void unravelDecl(VarDecl* varDecl, std::ostringstream& oss) {
    const Type* declType = varDecl->getType().getTypePtr();
    if(!declType->isPointerType() && varDecl->hasInit()) {
        if(declType->isIntegerType()) {
            oss << "intVars[\"";
        }
        else if(declType->isFloatingType()) {
            oss << "floatVars[\"";
        }
        else if(declType->isCharType()) {
            oss << "stringVars[\"";
        }
        oss << varDecl->getNameAsString() << "\"]" << "=";
        unravelExpr(varDecl->getInit(), oss);
    }
    else {
        spdlog::get("sopelogger")->error("Loop init does not have an initializer or is pointer type");
    }
}

/* Check is the LHS of the expr has the desired variable we want. This will be used
 * to check if the condition checks for the bounds of the current loop variable
 * such as for(p=0; p<k;..) rather than for(p=0;s<k;...). The former can help us deduce
 * the bounds while the second gives us no clue and may give us false clues even. It is also
 * used to check the increment. If p++ or p+=2 rather than s++;
 */
bool isLHSDesiredVar(Expr* expr, VarDecl* varDecl, std::string variable="") {
    if(DeclRefExpr* ref = dyn_cast<DeclRefExpr>(expr)) {
        if(ref->getDecl() == varDecl) {
            return true;
        }
    }
    else if(ImplicitCastExpr* implicitCast = dyn_cast<ImplicitCastExpr>(expr)) {
        return isLHSDesiredVar(implicitCast->getSubExpr(), varDecl, variable);
    }
    return false; // in all other cases
}

std::string getStartBound(VarDecl* loopVariable, Expr* expr) {
    std::ostringstream oss;
    if(BinaryOperator* binOp = dyn_cast<BinaryOperator>(expr)) {
        if(binOp->isComparisonOp() &&  isLHSDesiredVar( binOp->getLHS(), loopVariable)) {
            unravelExpr(binOp->getRHS(), oss) ;
            return oss.str();
        }
        else {
            spdlog::get("sopelogger")->error("Can not deduce trip count because end bound does not use the start loop variable");
        }
    }
    else {
        spdlog::get("sopelogger")->error("Can not deduce trip count because condition is not a binary operation");
    }
}

std::string getEndBound(VarDecl* loopVariable, Expr* expr) {
    std::ostringstream oss;
    if(BinaryOperator* binOp = dyn_cast<BinaryOperator>(expr)) {
        if(binOp->isComparisonOp() &&  isLHSDesiredVar( binOp->getLHS(), loopVariable)) {
            unravelExpr(binOp->getRHS(), oss) ;
            return oss.str();
        }
        else {
            spdlog::get("sopelogger")->error("Can not deduce trip count because end bound does not use the start loop variable");
        }
    }
    else {
        spdlog::get("sopelogger")->error("Can not deduce trip count because condition is not a binary operation");
    }
}

std::string getIncrement(VarDecl* loopVariable, Expr* expr, std::string start, std::string end) {
    std::ostringstream oss;
    std::string tripCount = "";
    if(BinaryOperator* binOp = dyn_cast<BinaryOperator>(expr)) {
        // TODO: won't handle the case k=k+2. For this we need to analyze the RHS and
        // check if its contains the loop variable k, the opcode (+) and the integer literal 2.
        // Then we can say, the loop trip count =  (end-start)/2 based on the above. It's HARD.
        if(binOp->isCompoundAssignmentOp() &&  isLHSDesiredVar( binOp->getLHS(), loopVariable)) {
            unravelExpr(binOp->getRHS(), oss) ;
            std::string inc = oss.str();
            if(binOp->isAdditiveOp()) {
                // the trip count will be divided by the increment: (end-start)/x
                tripCount = "std::abs( "+end+" - "+"("+start+") )" + "/" + "(" + inc + ")";
            }
            else if(binOp->isMultiplicativeOp()) {
                // the trip count will be the log(base x)(end-start)
                // logB(x) = loga(x) / loga(B) => tripcount = loge(end-start)/loge(x)
                tripCount = "log(std::abs(" + end + "-" + "("+start+")" + "))" + "/ " + "log("+inc+")";
            }
            //return tripCount;
        }
        else {
            spdlog::get("sopelogger")->error("Can not deduce trip count because increment does not use the start loop variable");
        }
    }
    else if(UnaryOperator* unaryOp = dyn_cast<UnaryOperator>(expr)) {
        if(unaryOp->isIncrementDecrementOp() && isLHSDesiredVar(unaryOp->getSubExpr(),loopVariable)) {
            // trip count is (start-end)
            tripCount = "std::abs(" + end + "-" + "("+ start + ")" + ")";
            //return tripCount;
        }
        else {
            spdlog::get("sopelogger")->error("Can not deduce trip count because unary operation is neither incmrent or decrement");        }
    }
    else {
        spdlog::get("sopelogger")->error("Can not deduce trip count because increment is neither binary nor unary operation");
    }
    return tripCount;
}

/* The ForStmt has init, condvar, cond, inc, and body stmts. What we need out of them
 * to figure out access patterns is the init, cond, and inc as a minimum. The INIT can
 * be either a DeclStmt of a VarDecl (which can be either a BinaryOperator (ex:
 * int i = j+1) or an ImplicitCastExpr of a DeclRefExpr (ex: int i = j). The INIT
 * can also be BinaryOperator (ex: i = j + 1 )
 *
 */
template<typename PostProcessor>
bool ClangASTProcessor<PostProcessor>::processForStmt(ForStmt* forStmt) {

    // if function is not null then its definitely one of the functions we are looking for
    // This has been taken care of by the visitfuncDecl above
    if (parseState->function)
    {
        // Get all the loops' parents who are loops. This builds a tree of loops that
        // will be used later to traverse all the hierarchy of loops and their children
        // when building a QUERYER to mimic the COI function.
        std::shared_ptr<LoopInfoStruct> li = std::make_shared<LoopInfoStruct>();
        li->current = forStmt;

        Stmt* parent = parseState->functionParentMap->getParent(forStmt);
        bool foundParent = false;
        while(parent!=nullptr) {
            if(ForStmt* parentFor = dyn_cast<ForStmt>(parent)) {
                if(loopMap.find(parentFor)!=loopMap.end()) {
                    loopMap[parentFor]->innerLoops.push_back(li);
                    foundParent = true;
                    break;
                }
                else {
                    spdlog::get("sopelogger")->error("Something went wrong when building loop tree in processForStmt");
                }
            }
            else {
                parent  = parseState->functionParentMap->getParent(parent);
            }
        }
        loopMap[forStmt] = li; // this adds the current loop to the ClangASTProc loop map to be used by later loops for lookup
        if(!foundParent) {
            parseState->functionLoopsMap[parseState->function].push_back(li); // adds the current loop as a level 0 loop of the current function
        }

        // print the modified loops
        std::ostringstream oss;
        oss << "for(";
        Stmt* init = forStmt->getInit();
        Expr* cond = forStmt->getCond();
        VarDecl* condVar = forStmt->getConditionVariable(); // this should be null. we need to restrict this
        Expr* inc = forStmt->getInc();
        VarDecl* loopInit = nullptr;
        if(condVar == nullptr) {
            DeclStmt* declStmt = dyn_cast<DeclStmt>(init);
            if(declStmt && declStmt->isSingleDecl()) {
                VarDecl* varDecl = dyn_cast<VarDecl>(declStmt->getSingleDecl());
                loopInit = varDecl;
                // transforms the loops init
                unravelDecl(varDecl, oss);
            }
            else {
                spdlog::get("sopelogger")->error("Loop has no init declstmt or has more than one init declaration.");
            }
            oss<<";";
            unravelExpr(cond,oss);
            oss<<";";
            unravelExpr(inc,oss);
            oss<<")";
            spdlog::get("sopelogger")->info("Modified loop : {}", oss.str());
            // Set the modified text in the LoopInfoStruct
            // get the start value of the init
            std::ostringstream startValueStream;
            unravelExpr(loopInit->getInit(), startValueStream);
            li->start = startValueStream.str();
            li->end = getEndBound(loopInit, cond);
            li->tripCount = getIncrement(loopInit, inc, li->start, li->end);
            li->modifiedText = oss.str();
            spdlog::get("sopelogger")->info("START : {}", li->start);
            spdlog::get("sopelogger")->info("END : {}", li->end);
            spdlog::get("sopelogger")->info("TRIP COUNT : {}", li->tripCount);
        }
        else {
            spdlog::get("sopelogger")->error("Loop has a condition variable. This case is not handled yet.");
        }

        // Everytime a new loop is found, it means all the loop state variables have to be reset.
        // This includes the loop variable, the binary operator variable and anything underneath.
        // The function variable is higher than the loop so we should NOT reset it.
        //ClangForLoopWrapper<OperationTypeCompare> forOperation(forStmt);
        //pattern.addToPattern(std::shared_ptr<operation>(new ClangForLoopWrapper<OperationTypeCompare>(forStmt)));
        //SourceRange forRange = forStmt->getSourceRange();

        // I have thought of getting the parent of forStmt and then going through its children until i get to the stmt that is
        // just before the forstmt. Then we need to check if that stmt is regular expr which ends with semicolon. Or it could just
        // be that the forStmt is the first stmt in a function. In that case we should get the body start ... A few cases like this
        // which we will need to handle
        if(parseState->inject) {
            CaptureGenerator generator;
            Injector injector(parseState->replacements, generator);
            injector.inject(parseState->Context->getFullLoc(forStmt->getLocStart()), InjectionType::START, "", "");

            //Replacement rep(parseState->Context->getSourceManager(), parseState->Context->getFullLoc(forStmt->getLocStart()), 0, "// Will instrument START here -- Elie\n");
            //parseState->replacements->insert(rep);

            // For some reason te getLocEnd returns different things. If the loop body is not braced, then it returns the position of
            // the last character before the semicolon. If the body is braced, then it returns to the position of the brace.
            // Thus we need to offset the LocEnd with 2.
            // For formatting reasons, we check getOffset(1) and determine the newLines based on that.
            SourceLocation semicolonLocationForNonBracedLoops = forStmt->getLocEnd().getLocWithOffset(1);
            const char* semiColonCandidate = parseState->Context->getSourceManager().getCharacterData(semicolonLocationForNonBracedLoops);
            StringRef replacementTextPrefix, replacementTextPostfix;
            if(*semiColonCandidate == ';') {
                replacementTextPrefix = "\n";
                replacementTextPostfix = "";
            }
            else {
                replacementTextPrefix = "";
                replacementTextPostfix = "\n";
            }
            //Replacement repEnd(parseState->Context->getSourceManager(), parseState->Context->getFullLoc(forStmt->getLocEnd().getLocWithOffset(2)), 0, replacementTextPrefix.str() + "// Will instrument STOP here -- Elie" + replacementTextPostfix.str());
            //parseState->replacements->insert(repEnd);

            injector.inject(parseState->Context->getFullLoc(forStmt->getLocEnd().getLocWithOffset(2)), InjectionType::STOP, replacementTextPrefix, replacementTextPostfix);
        }

        std::shared_ptr<IASTProcessor> forProcessor = std::shared_ptr<IASTProcessor>(new ForStmtProcessor(forStmt, parseState));
        auto parentProcessor = parseState->processorStack.top();
        parseState->processorStack.push(forProcessor);
        forProcessor->processNode(parentProcessor);
    }

    return true;
}

template<typename PostProcessor>
bool ClangASTProcessor<PostProcessor>::processIfStmt(IfStmt* ifStmt) {
    if(parseState->function) {
        std::shared_ptr<IASTProcessor> ifOpProcessor = std::shared_ptr<IASTProcessor>(new IfStmtProcessor(ifStmt, parseState));
        auto parentProcessor = parseState->processorStack.top();
        parseState->processorStack.push(ifOpProcessor);
        ifOpProcessor->processNode(parentProcessor);
    }
    return true;
}

template<typename PostProcessor>
bool ClangASTProcessor<PostProcessor>::processCompoundStmt(CompoundStmt* compoundStmt) {
    if(parseState->function) {
        //std::cout << "Encountered a compound stmt of size = " << compoundStmt->size() << std::endl;
        std::shared_ptr<IASTProcessor> compoundOpProcessor = std::shared_ptr<IASTProcessor>(new CompoundStmtProcessor(compoundStmt, parseState));
        auto parentProcessor = parseState->processorStack.top();
        parseState->processorStack.push(compoundOpProcessor);
        compoundOpProcessor->processNode(parentProcessor);
    }
    return true;
}

template<typename PostProcessor>
bool ClangASTProcessor<PostProcessor>::processBinaryOperator(BinaryOperator* binaryOperator) {
    // We need to figure out if this binary operator is part of (LHS or RHS) another binary operator.
    // This will help us build/break-down complex statements from/into smaller expressions
    if (parseState->function) { // TODO: Right now we only care for binary operations in a loop
        // 1- Check if operator is assignment and setup all corresponding state variables
        setupAndCheckAssignmentOper(binaryOperator);

        std::shared_ptr<IASTProcessor> binOpProcessor = std::shared_ptr<IASTProcessor>(new BinOpProcessor(binaryOperator, parseState));
        auto parentProcessor = parseState->processorStack.top();
        parseState->processorStack.push(binOpProcessor);
        binOpProcessor->processNode(parentProcessor);
    }
    return true;
}

template<typename PostProcessor>
bool ClangASTProcessor<PostProcessor>::processDeclRefExpr(DeclRefExpr* declRefExpr) {
    // isForLoopInit condition is checked because we dont yet parse the init part of the loop
    // Thus we also shouldn't push the variableReferences onto the stack or else, they will be
    // picked up (as two operands) by the previous operation in line.
    if (parseState->function) {

        std::shared_ptr<IASTProcessor> declRefProcessor = std::shared_ptr<IASTProcessor>(new DeclRefProcessor(declRefExpr, parseState));
        auto parentProcessor = parseState->processorStack.top();
        parseState->processorStack.push(declRefProcessor);
        declRefProcessor->processNode(parentProcessor);
    }
    /*
     * VERY IMPORTANT NOTE: Usually the last declRef will trigger the deletes. At this point there
     * is a chain of callbacks from this declrefProcessor till the functiondeclProcessor. Although
     * these processors are being popped at the beginning of each callback function in the corresposing
     * processors, they really don't get deleted (ref counts goes to 0) till this point. This is
     * because each processor still keeps a shard_ptr to its parent. The last declref will callBack()
     * its parent and then its parent and then its parent. When the funcDeclProcessor callback
     * is called, this chain starts to unravel back to the declrefprocessor to the statement that
     * is directly after the callback to the parent which is usually the exit. At that point, the
     * declref processor returns from the processNode function(which in this case called the callback
     * on the parent because declref can not have any children). When the
     * clangastprocessor::processDeclRefExpr returns, declRefProcessor will have its ref count = 0
     * and therefore will delete the declrefprocessor which will decrement the ref_count for its
     * parent processor, which will in turn delete the parent processor, and that parent will
     * trigger the delete of its parent and so on, until the functiondeclprocessor is deleted.
     */
    return true;
}

template<typename PostProcessor>
bool ClangASTProcessor<PostProcessor>::processVariableDecl(VarDecl* varDecl) {
    if(parseState->function) { // && varDecl->hasInit()) {
        // get the declStmt which is the parnt of the varDeclby looking up in the Context object
        setupAndCheckVarDeclInit(varDecl);

        std::shared_ptr<IASTProcessor> varDeclProcessor = std::shared_ptr<IASTProcessor>(new VarDeclProcessor(varDecl, parseState));
        auto parentProcessor = parseState->processorStack.top();
        parseState->processorStack.push(varDeclProcessor);
        varDeclProcessor->processNode(parentProcessor);
    }
    return true;
}

template<typename PostProcessor>
bool ClangASTProcessor<PostProcessor>::processArraySubscriptExpr(ArraySubscriptExpr* arraySubscriptExpr) {
    if(parseState->loop && parseState->assignmentOperChildren && parseState->currentAssignmentNode) {
        std::shared_ptr<IASTProcessor> arrSubsProcessor = std::shared_ptr<IASTProcessor>(new ArraySubscriptProcessor(arraySubscriptExpr, parseState));
        auto parentProcessor = parseState->processorStack.top();
        parseState->processorStack.push(arrSubsProcessor);
        arrSubsProcessor->processNode(parentProcessor);
    }

    return true;
}

template <typename PostProcessor>
bool ClangASTProcessor<PostProcessor>::processUnaryOperator(UnaryOperator* unaryOperator) {
    if(parseState->function) {
        std::shared_ptr<IASTProcessor> unaryProcessor = std::shared_ptr<IASTProcessor>(new UnaryOpProcessor(unaryOperator, parseState));
        auto parentProcessor = parseState->processorStack.top();
        parseState->processorStack.push(unaryProcessor);
        unaryProcessor->processNode(parentProcessor);
    }
    return true;
}

template<typename PostProcessor>
bool ClangASTProcessor<PostProcessor>::processIntegerLiteral(IntegerLiteral* integerLiteral) {
    if(parseState->function) {
        std::shared_ptr<IASTProcessor> intLiteralProcessor = std::shared_ptr<IASTProcessor>(new IntLiteralProcessor(integerLiteral, parseState));
        auto parentProcessor = parseState->processorStack.top();
        parseState->processorStack.push(intLiteralProcessor);
        intLiteralProcessor->processNode(parentProcessor);
    }
    return true;
}

template<typename PostProcessor>
bool ClangASTProcessor<PostProcessor>::processFloatingLiteral(FloatingLiteral* floatLiteral) {
    if(parseState->function) {
        std::shared_ptr<IASTProcessor> floatLiteralProcessor = std::shared_ptr<IASTProcessor>(new FloatLiteralProcessor(floatLiteral, parseState));
        auto parentProcessor = parseState->processorStack.top();
        parseState->processorStack.push(floatLiteralProcessor);
        floatLiteralProcessor->processNode(parentProcessor);
    }
    return true;
}

template<typename PostProcessor>
void ClangASTProcessor<PostProcessor>::processNode(std::shared_ptr<IASTProcessor> parentWithCallback) {

}

//#include "defuse/DefUse.h"
#include "../knowledge_base/MockUtils.h"
#include "clang/Analysis/CFGStmtMap.h"
#include "clang/Basic/SourceManager.h"
#include "../estimator/Estimator.h"
#include "../static_analyzer/StaticAnalyzer.h"
#include "../utils/ProcessingUtils.h"
using namespace clang::defuse;
/*
template<typename PostProcessor>
DefUse* ClangASTProcessor<PostProcessor>::runDefUseAnalysis(FunctionDecl* function, Stmt* functionBody, ASTContext* context, CFG* cfg, bool verbose) {
    // Getting DefUse at this point. We already have the AST and PatternTree here
    DefUse* du = DefUse::BuildDefUseChains(function, functionBody, context, 0, cfg, 0, verbose);
    // End of getting DefUse
    return du;
}


template<typename PostProcessor>
void ClangASTProcessor<PostProcessor>::queryDefUse(DefUse* du, CFG* cfg, std::shared_ptr<ParseState> parseState, ITreeQueryResult* treeQueryResult) {
    // Find the block that contains the matched subtree
    // We can use the already created parseState->functionParentMap and CFGStmtMap to get the blockId
    // of the result subtree.
    std::cout << "\n======================" << std::endl;
    std::cout << "Querying DefUse Chains" << std::endl;
    std::cout << "======================" << std::endl;
    ASTContext* context = parseState->Context;
    // Solution from : http://stackoverflow.com/questions/13760197/how-to-get-basic-block-id-of-a-statement-inside-visit-methods
    std::unique_ptr<CFGStmtMap> cfgMap = llvm::make_unique<CFGStmtMap>(*(CFGStmtMap::Build(cfg, parseState->functionParentMap)));
    std::vector<QueryMatch> matchSet;
    if(ASTTreeQueryResult* res = dyn_cast<ASTTreeQueryResult>(treeQueryResult)) {
        matchSet = res->matchSet;
    }
    else if (HashTreeQueryResult* res = dyn_cast<HashTreeQueryResult>(treeQueryResult)) {
        matchSet = res->matchSet;
    }
    //ASTTreeQueryResult::MatchSet matchSet = dyn_cast<ASTTreeQueryResult>(treeQueryResult)->matchSet;
    std::cout << "HERE matchset size = " << matchSet.size() << std::endl;
    for(QueryMatch& match : matchSet) {
        std::cout << "Matched node has pattern of size = " << match.coiNode->getPattern()->size() << std::endl;
        unsigned blockId = cfgMap->getBlock(match.coiNode->getPattern()->at(0)->getUnderlyingPtr())->getBlockID();
        //unsigned blockId = cfgMap->getBlock(match.resultTreeOperation->getUnderlyingPtr())->getBlockID();
        std::cout << "Matched subtree rooted at blockId = " <<  blockId << std::endl;
        // This returns the variables that are used (actually the declrefexpr that reach this block) in the block
        std::vector<DeclRefExpr const*> BlockDataRefs = du->queryRefBlockData(blockId);
        std::vector<VarDecl const*> BlockDataDecls = du->queryDeclBlockData(blockId);

        clang::BeforeThanCompare<SourceLocation> before = clang::BeforeThanCompare<SourceLocation>(context->getSourceManager());
        for(DeclRefExpr const* ref : BlockDataRefs) {
            bool isBefore = before(ref->getLocStart(), match.coiNode->getPattern()->at(0)->getUnderlyingPtr()->getLocStart());
            //bool isBefore = before(ref->getLocStart(), match.resultTreeOperation->getUnderlyingPtr()->getLocStart());
            std::cout << "Checking if DeclRefExpr " << ref->getNameInfo().getAsString() << " is defined before the current scope ? " << (isBefore?"yep":"nope") << std::endl;
            if(isBefore) {
                    std::cout << "Inject:_captureVar(" << ref->getDecl()->getNameAsString() << ") at line = "
                              << context->getSourceManager().getSpellingLineNumber(ref->getLocStart()) + 1 << std::endl;
            }
        }
        for(VarDecl const* decl : BlockDataDecls) {
            bool isBefore = before(decl->getLocStart(), match.coiNode->getPattern()->at(0)->getUnderlyingPtr()->getLocStart());
            //bool isBefore = before(decl->getLocStart(), match.resultTreeOperation->getUnderlyingPtr()->getLocStart());
            std::cout << "Checking if VarDecl " << decl->getNameAsString() << " is defined before the current scope ? " << (isBefore?"yep":"nope") << std::endl;
            if(isBefore) {
                    std::cout << "Inject:_captureVar(" << decl->getNameAsString() << ") at line = "
                              << context->getSourceManager().getSpellingLineNumber(decl->getLocStart()) + 1 << std::endl;
            }
        }
        std::cout << "Inject: _captureVar(ALL_LOOP_VARIABLES) at line = "
                  << match.coiNode->getPattern()->at(0)->getUnderlyingPtr()->getLocStart().printToString(context->getSourceManager()) << std::endl;
                  //<< match.resultTreeOperation->getUnderlyingPtr()->getLocStart().printToString(context->getSourceManager()) << std::endl;
        std::cout << "Capturing the key variables of the loop (basically iteration 0) can help us understand "
                     "more what's happening in the loop. After that capture, we need to exit the loop or the "
                     "whole application somehow __terminate__()." << std::endl;
    }
    // End-of finding the block
}
*/

#include "../json.hpp"
using json = nlohmann::json;


template<typename PostProcessor>
void ClangASTProcessor<PostProcessor>::callBack(std::shared_ptr<operation> childCallerOperation) {
    //createPattern();
    //createTree();

    //std::cout << "Processor stack size before function pop = " << parseState->processorStack.size() << "\n";
    spdlog::get("sopelogger")->debug("Processor stack size before function pop = {}", parseState->processorStack.size());
    parseState->processorStack.pop();
    //std::cout << "Processor stack size after function pop = " << parseState->processorStack.size() << "\n";
    spdlog::get("sopelogger")->debug("Processor stack size after function pop = {}", parseState->processorStack.size());

    //std::cout << "ClangASTProcessor called back" << std::endl;
    spdlog::get("sopelogger")->debug("ClangASTProcessor called back");
    if(clfunctionOperation* func = dyn_cast<clfunctionOperation>(childCallerOperation.get())) {
        // Create the pattern tree, print it, and add it to the tree repository

        // Creates the pattern tree and assigns levels to each node in the tree
        auto pTree = createPatternTree<operation>(func);

        // Pretty prints the pattern tree
        //bfTreeCreator.tree().traverseTreeDFST2BL2R<std::string, std::string>(printPatternOperations, coutFormatString, setup, cleanup);
        //parseState->repo->addPatternTree(bfTreeCreator.tree());
        //parseState->repo->addAbstractSyntaxTree(childCallerOperation);
        //pTree.traverseTreeDFST2BL2R<std::string, std::string>(printPatternOperations, coutFormatString, setup, cleanup);
        parseState->repo->addPatternTree(pTree);
        parseState->repo->addAbstractSyntaxTree(childCallerOperation);

        FileStorage storage("./db/index.json");
        MockHashKnowledgeBase kb(&storage);
        processingPolicy(&kb, parseState, pTree, childCallerOperation );
/*
        // This runId will be the unique id for this run. It will be used to create files with unique names,
        // run dynamic analyzer, pass it to the instrumented executable to associate the captured variables with
        // a specific run.
        int runId = ::generateRunId();
        std::string sRunId = std::to_string(runId);

        // Serializes the pattern tree into json. The first json is the raw json to store int the DB whereas
        // the second is the jointjs json for visualiztion purposes
        serializePatternTree<operation>(pTree, sRunId);

        // Pretty prints the AST tree
        prettyPrintTrees<operation>(childCallerOperation, pTree, parseState);

        // Post processes the pattern
        postProcessPattern(parseState);

        // the pattern should be processed by now
        parseState->repo->addPattern(parseState->pattern);
        std::cout << "Creating a tree out of pattern (size: "
                  << parseState->pattern.size() << " )" << std::endl;

        // This is manually creating a tree from the post-processed pattern
        // TODO: remove this. It's not needed anymore since we programatically
        // create the WHOLE pattern tree at the beginning of this function
        std::ostringstream oss;
        std::function<void(std::shared_ptr<IPatternNode>&, std::function<std::string(std::shared_ptr<operation>)>)>
                printPatternWithOpType = std::bind(&printPatternNode, std::placeholders::_1, printOpType, std::ref(oss));
        typedef std::shared_ptr<PatternNode<operation>> spn;
        typedef std::shared_ptr<Pattern<operation>> sp;

        sp rootPattern = sp(new Pattern<operation>(parseState->pattern));
        spn root = spn(new PatternNode<operation>(rootPattern, -1));
        PatternTree<operation> tree(root);
        IPatternTree* tempTree = &tree;
        std::cout << "Created the tree" << std::endl;
        tempTree->traverseTreeBFS<operation,std::string>(printPatternWithOpType);

        // These variables will be used in all the following:
        // runDefUse(), queryTree(), queryDefUse(), queryTreeMetric(), runEstimator()
        FunctionDecl* function = parseState->function;
        Stmt* functionBody = parseState->function->getBody();
        ASTContext* context = parseState->Context;
        const CFG::BuildOptions bo ;
        std::unique_ptr<CFG> ucfg = CFG::buildCFG(function, functionBody, context, bo);
        CFG* cfg = ucfg.get();
        // End of common variables

        // Run def-use analysis
        DefUse* du = runDefUseAnalysis(function, functionBody, context, cfg, false);

        // Run static analysis
        runStaticAnalysis(childCallerOperation, du);

        // Match trees by querying the knowledge base
        ITreeQuery* treeQuery;
        ITreeQueryResult* treeQueryResult;
        IMetricQuery* metricQuery;
        IMetricQueryResult* metricQueryResult;
        queryMatchingTrees(treeQuery, treeQueryResult, metricQuery, metricQueryResult, parseState);
        ::queryMatchingTreeFingerprints(treeQuery, treeQueryResult, metricQuery, metricQueryResult, parseState, runId);

        // Query the def-use data, to get the CFG blocks of the matched subtrees
        queryDefUse(du, cfg, parseState, treeQueryResult);

        // Run dynamic analyzer. The analyzer will take care of calling the injector
        // and then running the program to capture the runtime variables.
        auto captures = runDynamicAnalysis(runId);
//        queryDynamicAnalysis();
//        queryTreeMetrics();
//        runEstimator();

        auto matches = treeQueryResult->getMatches();
        auto metrics = metricQueryResult->getMetrics();
        spdlog::get("sopelogger")->info("Loading captures for function: {}", parseState->function->getNameAsString());
        auto functionCaptures = getFunctionCaptures(captures, parseState->function->getNameAsString());

        IStorage* storage = new FileStorage("./db/index.json");
        MockHashKnowledgeBase concreteMock = MockHashKnowledgeBase(storage);
        IKnowledgeBase* ibase = &concreteMock;
        NaiveEstimator estimator = NaiveEstimator(ibase, &matches, metrics, functionCaptures, parseState->functionLoopsMap[parseState->function]);
        estimator.estimate();

        delete metricQueryResult;
        delete metricQuery;
        delete treeQueryResult;
        delete treeQuery; // required because createMockQuery will allocate on heap
        delete du;*/
    }
}

#endif // CLANGASTPROCESSOR_H
