#ifndef UNARYOPPROCESSOR_H
#define UNARYOPPROCESSOR_H

#include "clang/AST/RecursiveASTVisitor.h"
#include "../ClangOperationWrappers.h"
#include "../Comparator.h"
#include "../utils/OperationHelpers.h"
#include "IASTProcessor.h"
#include "../utils/Utils.h"
#include "../ParseState.h"
#include <spdlog/spdlog.h>

class UnaryOpProcessor : public IASTProcessor
{
public:
    UnaryOpProcessor(UnaryOperator* unaryOperator, std::shared_ptr<ParseState> parseState);
    ~UnaryOpProcessor() { //std::cout << "Destroying UnaryOpProcessor at " << this << "\n";
                          spdlog::get("sopelogger")->debug("Destroying UnaryOpProcessor at {}",(void*) this);
                        }
    void processNode(std::shared_ptr<IASTProcessor> parentWithCallback);
    void callBack(std::shared_ptr<operation> childCallerOperation);
private:
    UnaryOperator* unaryOp;
    std::shared_ptr<ParseState> pState;
    std::shared_ptr<IASTProcessor> pWithCallback;
    std::shared_ptr<clunaryOperation> op;
private:
    template<typename T>
    T* getParentOfType (Stmt* oper);
};

UnaryOpProcessor::UnaryOpProcessor(UnaryOperator* unaryOperator, std::shared_ptr<ParseState> parseState) : unaryOp(unaryOperator), pState(parseState)
{

}

template<typename T>
T* UnaryOpProcessor::getParentOfType (Stmt* oper) {
    //bool foundParentOfRequestedType = false;
    Stmt* temp = oper;
    Stmt* parent = nullptr;
    do {
        parent = pState->functionParentMap->getParent(temp);
        if(parent) {
            if(T* parentPointer = dyn_cast<T>(parent)) {
                return parentPointer;
            }
        }
        else {
            break;
        }
        temp = parent;
    }
    while(parent!=nullptr); // && !foundParentOfRequestedType);

    return nullptr;
}

void UnaryOpProcessor::processNode(std::shared_ptr<IASTProcessor> parentWithCallback) {
    pWithCallback = parentWithCallback;

    if(ForStmt* forLoop = getParentOfType<ForStmt>(unaryOp)) {
        if(forLoop->getInc() == unaryOp) {
            pState->isForLoopIncrement = true;
            pState->isUnaryOperation = true;
            op = makeUnaryOperation(unaryOp);
            //std::cout << "-------------------------- Unary Operator " << unaryOp->getOpcodeStr(unaryOp->getOpcode()).str() << "found at " << unaryOp->getLocStart().printToString(pState->Context->getSourceManager()) << std::endl;
            spdlog::get("sopelogger")->debug("-------------------------- Unary Operator {} found at {}", unaryOp->getOpcodeStr(unaryOp->getOpcode()).str(), unaryOp->getLocStart().printToString(pState->Context->getSourceManager()) );
            //pushOperationOnStacks(unaryOp, unaryWrapped, true);
        }
    }
    else {
        // Probably a statement.  Could be inside the function or inside the loop
        // We should probably check that it ddoesnt't have a binary operation
        // as its parent. Ex: D[...] = S[++i*d+k] + ...
        // This example shows that i is being changed in index of subscript operation
        // and we should be careful that it might crash the binary operation
        // Elie: added the following condition to deal with cases such as x = -1. The - sign
        // is a unaryop but its not really a decrement or inc. Its just an arithmetic op and thus
        // it should not cause the next variable to be added to the scopeTracker.variablechanges
        // because no variable has changed.
        if(!unaryOp->isArithmeticOp())
            pState->isUnaryOperation = true;
        op = makeUnaryOperation(unaryOp);
        //std::cout << "-------------------------- Unary Operator outside loop init/cond/incr " << unaryOp->getOpcodeStr(unaryOp->getOpcode()).str() << "found at " << unaryOp->getLocStart().printToString(pState->Context->getSourceManager()) << std::endl;
        spdlog::get("sopelogger")->debug("-------------------------- Unary Operator outside loop init/cond/incr {} found at {}", unaryOp->getOpcodeStr(unaryOp->getOpcode()).str() , unaryOp->getLocStart().printToString(pState->Context->getSourceManager()));
        //pushOperationOnStacks(unaryOp, unaryWrapped, true);
    }
}

void UnaryOpProcessor::callBack(std::shared_ptr<operation> childCallerOperation){
    //std::cout << "UnaryOpProcessor callback called" << std::endl;
    spdlog::get("sopelogger")->debug("UnaryOpProcessor callback called");
    op->operand() = childCallerOperation;
    op->onlyOperandLiteral() = childCallerOperation->getMainStringLiteral();
    // Pop the child operation
    pState->processorStack.pop();
    // Call the parent
    pWithCallback->callBack(op);
}

#endif // UNARYOPPROCESSOR_H
