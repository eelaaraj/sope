#ifndef BINOPPROCESSOR_H
#define BINOPPROCESSOR_H

#include "clang/AST/RecursiveASTVisitor.h"
#include "../ClangOperationWrappers.h"
#include "../Comparator.h"
#include "../utils/OperationHelpers.h"
#include "IASTProcessor.h"
#include "../ParseState.h"
#include "../utils/Utils.h"
#include <spdlog/spdlog.h>

class BinOpProcessor : public IASTProcessor
{
public:
    BinOpProcessor(BinaryOperator* binaryOperator, std::shared_ptr<ParseState> parseState);
    ~BinOpProcessor() { //std::cout << "Destroying BinOpProcessor at " << this << "\n";
                        spdlog::get("sopelogger")->debug("Destroying BinOpProcessor at {}", (void*)this);
                      }
    void processNode(std::shared_ptr<IASTProcessor> parentWithCallback);
    void callBack(std::shared_ptr<operation> childCallerOperation);

private:
    BinaryOperator* binOp;
    std::shared_ptr<ParseState> pState;
    std::shared_ptr<IASTProcessor> pWithCallback;
    std::shared_ptr<clbinOperation> op;
    int numOfOperands;
};

BinOpProcessor::BinOpProcessor(BinaryOperator* binaryOperator, std::shared_ptr<ParseState> parseState) : binOp(binaryOperator), pState(parseState), numOfOperands(0)
{

}

void BinOpProcessor::processNode(std::shared_ptr<IASTProcessor> parentWithCallback) {
    pWithCallback = parentWithCallback;

    // 2- Check if the operator is part of a FOR or WHILE loop condition and ignore if so
    if(pState->loop) { // This condition is needed when the binOp is not part of a loop
                       // PState->loop will be null and will crash the dyn_cast below
        if (ForStmt *theLoop = dyn_cast<ForStmt>(pState->loop)) {
            const Expr *eCondition = theLoop->getCond();
            const Expr *eIncrement = theLoop->getInc();
            if (eCondition == binOp) {
                // Reset the variable indicating we are processing a loop init;
                // asuming there will be a condition. If not, we need to find a different way to reset isForLoopInit
                pState->isForLoopInit = false;
                pState->isForLoopCondition = true;
                pState->isForLoopIncrement = false;
            }
            else if(pState->isForLoopInit) {
                pState->isForLoopCondition = false;
                pState->isForLoopIncrement = false;
                //return true;
            }
            else if(eIncrement == binOp) { // happens in the case: for(... ; ... ; i+=1)
                pState->isForLoopInit = false;
                pState->isForLoopCondition = false;
                pState->isForLoopIncrement = true; // this will only work if the operator is a binary operator.
                // In the case the operator is unary like ++i, then isForLoopIncrement will still be false
                // therefore it has to be set somewhere in the visitUnaryOperator by looking up the parent
                // map of the operator and checking if the parent is a for loop and then checking downwards
                // if the forloop increment is actually the unary operator (because the unary operator
                // might be a statement of the loop and still have its parent as the loop. So its not
                // sufficient to check if the parent is a loop. It should also satisfy the check where
                // the loop's increment is the unary operator.
            }
        } else if (WhileStmt * theLoop = dyn_cast<WhileStmt>(pState->loop)) {
            const Expr *eCondition = theLoop->getCond();
            if (eCondition == binOp) {
                pState->isForLoopInit = false; // assuming there will be a condition
                pState->isForLoopCondition = true;
                pState->isForLoopIncrement = false;
            }
            else if(pState->isForLoopInit) {
                pState->isForLoopCondition = false;
                pState->isForLoopIncrement = false;
            }
        }
    }

    op = makeBinaryOperation(binOp);

    if(pState->isNewAssignmentStmt) {
        //std::cout << std::endl;
        spdlog::get("sopelogger")->debug("\n");
    }

   // std::cout << binOp->getOpcodeStr().str() << " ";
    spdlog::get("sopelogger")->debug("{}", binOp->getOpcodeStr().str());

}

void BinOpProcessor::callBack(std::shared_ptr<operation> childCallerOperation) {
    //std::cout << "BinOpProcessor callback called" << std::endl;
    spdlog::get("sopelogger")->debug("BinOpProcessor callback called");
    if(numOfOperands==0) {
        op->firstOperand() = childCallerOperation->getMainStringLiteral();
        op->lhs() = childCallerOperation;
        // Pop the child operation
        pState->processorStack.pop();
    }
    else if (numOfOperands == 1) {
        op->secondOperand() = childCallerOperation->getMainStringLiteral();
        op->rhs() = childCallerOperation;
        // Pop the child operation
        pState->processorStack.pop();
        // At this point we processed both operands and can inform the parent operations
        // the we are ready to be treated as an operand
        pWithCallback->callBack(op);
    }
    numOfOperands++;
}

#endif // BINOPPROCESSOR_H
