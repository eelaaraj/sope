#ifndef FORSTMTPROCESSOR_H
#define FORSTMTPROCESSOR_H

#include "clang/AST/RecursiveASTVisitor.h"
#include "../ClangOperationWrappers.h"
#include "../Comparator.h"
#include "../utils/OperationHelpers.h"
#include "IASTProcessor.h"
#include "../utils/Utils.h"
#include "../ParseState.h"
#include <spdlog/spdlog.h>

class ForStmtProcessor : public IASTProcessor
{
public:
    ForStmtProcessor(ForStmt* forStmt, std::shared_ptr<ParseState> parseState);
    ~ForStmtProcessor() { //std::cout << "Destroying ForStmtProcessor at " << this << "\n";
                        spdlog::get("sopelogger")->debug("Destroying ForStmtProcessor at {}", (void*)this);}
    void processNode(std::shared_ptr<IASTProcessor> parentWithCallback);
    void callBack(std::shared_ptr<operation> childCallerOperation);

private:
    ForStmt* fStmt;
    std::shared_ptr<ParseState> pState;
    std::shared_ptr<IASTProcessor> pWithCallback;
    std::shared_ptr<clforOperation> op;
    int numOfStmts;
    Stmt* lastStmtInBody;
};


ForStmtProcessor::ForStmtProcessor(ForStmt* forStmt, std::shared_ptr<ParseState> parseState) : fStmt(forStmt), pState(parseState), numOfStmts(0)
{
    if(CompoundStmt* compStmt = clang::dyn_cast<CompoundStmt>(fStmt->getBody())) {
        numOfStmts = compStmt->size();
        lastStmtInBody = compStmt->body_back();
        //for(auto = )
    }
    else {
        numOfStmts = 1; // Could be 0 if the for loop is empty : for(...);
        lastStmtInBody = fStmt->getBody();
    }
}

void ForStmtProcessor::processNode(std::shared_ptr<IASTProcessor> parentWithCallback) {
    pWithCallback = parentWithCallback;

    op = makeForOperation(fStmt, pState->loopId);
    //pushOperationOnStacks(fStmt, op, true);
    pState->numScopes++;
    pState->loopId++;
    //std::cout << "--------- Scope: " << pState->numScopes << std::endl;
    spdlog::get("sopelogger")->debug("--------- Scope: {}",pState->numScopes);
    pState->loop = fStmt;
    pState->isForLoopInit = true;
    FullSourceLoc FullLocation = pState->Context->getFullLoc(fStmt->getForLoc());
    if (FullLocation.isValid()) {
        //std::cout << " Found FOR loop inside function " << pState->function->getNameAsString() << " at "
        //          << FullLocation.printToString(pState->Context->getSourceManager()) << std::endl;
        spdlog::get("sopelogger")->debug(" Found FOR loop inside function {} at ", pState->function->getNameAsString(), FullLocation.printToString(pState->Context->getSourceManager()));
    }
}

void ForStmtProcessor::callBack(std::shared_ptr<operation> childCallerOperation) {
    //std::cout << "ForStmtProcessor callback called - # of stmts = " << numOfStmts << std::endl;
    spdlog::get("sopelogger")->debug("ForStmtProcessor callback called - # of stmts = {}", numOfStmts);

    // Pop the child operation
    pState->processorStack.pop();

    if(pState->isForLoopInit) {
        pState->isForLoopInit = false;
        op->init() = childCallerOperation;
    }
    else if(pState->isForLoopCondition) {
        pState->isForLoopCondition = false;
        op->condition() = childCallerOperation;
    }
    else if(pState->isForLoopIncrement) {
        pState->isForLoopIncrement = false;
        op->increment() = childCallerOperation;
    }
    else {
        //op->addStatement(childCallerOperation);
        // A workaround to avoid incorporating CompoundStmt in loops and functions
        if(clcompoundOperation* compOper = dyn_cast<clcompoundOperation>(childCallerOperation.get())) {
            op->statements() = compOper->statements();
        }
        else {
            op->addStatement(childCallerOperation);
        }
        // TODO: We might have a problem here, if the loop had no body !!! The
        // parent will never be called.

        // By this time we have set the body and now we can call the parent
        // We have to decrement the scope here. Its better than having to
        // dyn_cast the childoperation and checkign if it was a for, while, if-else,
        // or even a function. Thus every operation takes care of its scope.
        clearScopeVariables(pState->numScopes, pState);
        pState->numScopes--;
        pWithCallback->callBack(op);
    }
}

#endif // FORSTMTPROCESSOR_H
