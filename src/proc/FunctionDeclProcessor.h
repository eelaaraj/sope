#ifndef FUNCTIONDECLPROCESSOR_H
#define FUNCTIONDECLPROCESSOR_H

#include "clang/AST/RecursiveASTVisitor.h"
#include "../ClangOperationWrappers.h"
#include "../Comparator.h"
#include "../utils/OperationHelpers.h"
#include "IASTProcessor.h"
#include "../utils/Utils.h"
#include "../ParseState.h"
#include <spdlog/spdlog.h>

using namespace clang;

class FunctionDeclProcessor: public IASTProcessor
{
public:
    FunctionDeclProcessor(FunctionDecl* functionDecl, std::shared_ptr<ParseState> parseState);
    ~FunctionDeclProcessor() { //std::cout << "Destroying FuncDeclProcesor at: " << this << "\n";
                             spdlog::get("sopelogger")->debug("Destroying FuncDeclProcesor at: {}" , (void*) this);
                             }
    void processNode(std::shared_ptr<IASTProcessor> parentWithCallback);
    void callBack(std::shared_ptr<operation> childCallerOperation);
private:
    FunctionDecl* fDecl;
    std::shared_ptr<ParseState> pState;
    std::shared_ptr<IASTProcessor> pWithCallback;
    // This is a workaround for now because we don't have a functionDecl operation wrapper
    // If we did, we could have easily had an operation instead (just like a clforWrapper) and
    // add the statements to that operation
    std::vector<std::shared_ptr<operation>> functionStmts;
    int numOfStmts;
    std::shared_ptr<clfunctionOperation> op;
    Stmt* lastStmtInBody;
};

FunctionDeclProcessor::FunctionDeclProcessor(FunctionDecl* functionDecl, std::shared_ptr<ParseState> parseState): IASTProcessor(), fDecl(functionDecl), pState(parseState)
{
    if(CompoundStmt* compStmt = dyn_cast<CompoundStmt>(fDecl->getBody())) {
        numOfStmts = compStmt->size();
        lastStmtInBody = compStmt->body_back();
    }
    else {
        numOfStmts = 1; // Could be 0 if the function is empty : functionName(...);
        lastStmtInBody = fDecl->getBody();
    }
}

void FunctionDeclProcessor::processNode(std::shared_ptr<IASTProcessor> parentWithCallback) {
    this->pWithCallback = parentWithCallback;

    pState->function = fDecl;
    pState->loop = nullptr;
    if(pState->functionParentMap)
        delete pState->functionParentMap;
    pState->functionParentMap = new ParentMap(fDecl->getBody());
    pState->functionParentMap->addStmt(fDecl->getBody());
    FullSourceLoc FullLocation = pState->Context->getFullLoc(fDecl->getLocStart());
    pState->currentParent = fDecl->getBody();
    //std::cout << "fuction body: " << pState->currentParent << std::endl;
    spdlog::get("sopelogger")->debug("function body: ", (void*) pState->currentParent);

    if (FullLocation.isValid()) {
        //std::cout << "Found the requested func declaration at "
        //          << FullLocation.printToString(pState->Context->getSourceManager()) << std::endl;
        spdlog::get("sopelogger")->debug("Found the requested func declaration at {}",FullLocation.printToString(pState->Context->getSourceManager()));
    }

    //std::cout << "Function name = " << fDecl->getNameAsString() << std::endl;
    spdlog::get("sopelogger")->debug("Function name = {}", fDecl->getNameAsString());
    op = makeFunctionOperation(fDecl);
    //parentWithCallback->callBack();
}

void FunctionDeclProcessor::callBack(std::shared_ptr<operation> childCallerOperation) {
    // If we know the number of statements in a function, then we can check that
    // and see if all the processors corresponding to those statements have
    // called back. Then we can finish processing
    //std::cout << "FunctionDeclProcessor callback called" << std::endl;
    spdlog::get("sopelogger")->debug("FunctionDeclProcessor callback called");

    //std::cout << "childCallerOperation->getUnderlyingPtr() = " << childCallerOperation->getUnderlyingPtr() << std::endl;
    //std::cout << "lastStmtInBody = " << lastStmtInBody << std::endl;

    //op->addStatement(childCallerOperation);

    // Ignore paramVarDecl or else they will be mistaken for the body of the function and mess
    // everything up from patterntree creation to everything else
    if(dyn_cast<clvarDeclOperation>(childCallerOperation.get())) {
        pState->processorStack.pop();
        return;
    }
    // A workaround to avoid incorporating CompoundStmt in loops and functions
    if(clcompoundOperation* compOper = dyn_cast<clcompoundOperation>(childCallerOperation.get())) {
        op->statements() = compOper->statements();
    }
    else {
        op->addStatement(childCallerOperation);
    }
    // Pop the child operation
    pState->processorStack.pop();

    // Directly call parent. This is because by now we already set the body of the
    // function which is either a compound stmt or one statement
    clearScopeVariables(pState->numScopes, pState);
    pState->numScopes--;
    pWithCallback->callBack(op);
}

#endif // FUNCTIONDECLPROCESSOR_H
