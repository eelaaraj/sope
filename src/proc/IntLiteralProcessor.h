#ifndef INTLITERALPROCESSOR_H
#define INTLITERALPROCESSOR_H

#include "clang/AST/RecursiveASTVisitor.h"
#include "../ClangOperationWrappers.h"
#include "../Comparator.h"
#include "../utils/OperationHelpers.h"
#include "IASTProcessor.h"
#include "../utils/Utils.h"
#include "../ParseState.h"
#include <spdlog/spdlog.h>

class IntLiteralProcessor : public IASTProcessor
{
public:
    IntLiteralProcessor(IntegerLiteral* integerLiteral, std::shared_ptr<ParseState> parseState);
    ~IntLiteralProcessor() { //std::cout << "Destroying IntLiteralProcessor at " << this << "\n";
                             spdlog::get("sopelogger")->debug("Destroying IntLiteralProcessor at {}",(void*) this);
                           }
    void processNode(std::shared_ptr<IASTProcessor> parentWithCallback);
    void callBack(std::shared_ptr<operation> childCallerOperation);
private:
    IntegerLiteral* intLiteral;
    std::shared_ptr<ParseState> pState;
    std::shared_ptr<IASTProcessor> pWithCallback;
    std::shared_ptr<clintOperation> op;
};

IntLiteralProcessor::IntLiteralProcessor(IntegerLiteral* integerLiteral, std::shared_ptr<ParseState> parseState) : intLiteral(integerLiteral), pState(parseState)
{

}

void IntLiteralProcessor::processNode(std::shared_ptr<IASTProcessor> parentWithCallback) {
    pWithCallback = parentWithCallback;

    op = makeIntLiteralOperation(intLiteral);
    pWithCallback->callBack(op);
/*
    if(pState->isForLoopInit || pState->isForLoopCondition) { // we need to build this expression or else it will mess with all the coming operations
        // IntegerLiteral is an operand to some operation whether its a binary operation or
        // even a varDecl operation (which is equivalent to an assignment binary operator)
        pushVariableOnStacks(intLiteral, intWrapped);
    }
*/
}

void IntLiteralProcessor::callBack(std::shared_ptr<operation> childCallerOperation) {
    //std::cout << "IntLiteralProcessor callback called" << std::endl;
    spdlog::get("sopelogger")->debug("IntLiteralProcessor callback called");
}

#endif // INTLITERALPROCESSOR_H
