#ifndef CALLEXPRPROCESSOR_H
#define CALLEXPRPROCESSOR_H

#include "clang/AST/RecursiveASTVisitor.h"
#include "../ClangOperationWrappers.h"
#include "../Comparator.h"
#include "../utils/OperationHelpers.h"
#include "IASTProcessor.h"
#include "../utils/Utils.h"
#include "../ParseState.h"
#include <spdlog/spdlog.h>

using namespace clang;

class CallExprProcessor : public IASTProcessor
{
public:
    CallExprProcessor(CallExpr* expr, const OperationType& operationType, std::shared_ptr<ParseState> parseState);
    ~CallExprProcessor() { //std::cout << "Destroying CallExprProcessor at: " << this << "\n";
                           spdlog::get("sopelogger")->debug("Destroying CallExprProcessor at: {}", (void*)this );
                         }
    void processNode(std::shared_ptr<IASTProcessor> parentWithCallback);
    void callBack(std::shared_ptr<operation> childCallerOperation);
private:
    CallExpr* callExpr;
    OperationType opType; // This is used so we can use this processor to create callexpr, cxxmembercall, and cxxoperatorcall
    std::shared_ptr<ParseState> pState;
    std::shared_ptr<IASTProcessor> pWithCallback;
    std::shared_ptr<clcallOperation> op;
    int numArgs;
    //const Stmt* firstArg; // Can't use the address of the last argument. Check the constructor comments below.
};

CallExprProcessor::CallExprProcessor(CallExpr *expr, const OperationType& operationType, std::shared_ptr<ParseState> parseState)
    :IASTProcessor(), callExpr(expr), opType(operationType), pState(parseState), numArgs(callExpr->getNumArgs() + 1)  {
    // We can't use the address of the last argument to check if we processed all the arguments
    // This is because the arguments are usually declReferences and two declREf to the same
    // variable have different addresses!! Thus we should only use the number of arguments
    // although its not the safest thing to do.
    // In the case of call expression, the numArgs is callExpr->numargs +1 because the
    // the function name will be passed as a DeclRef to FunctionToPointerDecay which
    // transforms the function to a pointer. In case the call is a CXXMember call then
    // the numArgs = callExpr->numArgs.
    if(operationType == OperationType::cxx_member_call)
        numArgs = callExpr->getNumArgs();
    /*if(callExpr->getNumArgs() > 0) {
        lastArg = callExpr->getArg(callExpr->getNumArgs()-1);
    }*/
}

void CallExprProcessor::processNode(std::shared_ptr<IASTProcessor> parentWithCallback) {
    this->pWithCallback = parentWithCallback;

    op = makeCallOperation(callExpr, opType);
}

void CallExprProcessor::callBack(std::shared_ptr<operation> childCallerOperation) {
    //std::cout << "CallExprProcessor callback called" << std::endl;
    spdlog::get("sopelogger")->debug("CallExprProcessor callback called");
    // Add the operation as an argument to the function call and then pop the childprocessor
    std::cout << "Call arg = " << childCallerOperation->getMainStringLiteral() << std::endl;
    op->addArg(childCallerOperation);
    pState->processorStack.pop();
    numArgs--;
    if(numArgs == 0) {
        pWithCallback->callBack(op);
    }
}

#endif // CALLEXPRPROCESSOR_H
