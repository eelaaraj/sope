#ifndef PATTERNTREECONSUMER
#define PATTERNTREECONSUMER

#include "Operations.h"

using namespace pattern::matching;

template<typename T>
class IPatternTreeConsumer {
public:
    virtual ~IPatternTreeConsumer() {}
    virtual void process(const PatternTree<T>& tree) const = 0;
    virtual PatternTree<T>& processedTree() = 0;
    virtual void reportChanges() = 0;
};

// Transformer Trees will change the tree.
template<typename T>
class IPatternTreeTransformer: public IPatternTreeConsumer<T> {
public:
    virtual ~IPatternTreeTransformer() {}
    virtual void process(const PatternTree<T>& tree) const = 0;
    virtual PatternTree<T>& processedTree() = 0;
    virtual void reportChanges() = 0;
};

/*
class APatternTreeTransformer: public IPatternTreeConsumer {
public:
private:
};
*/

template<typename T>
class IPatternTreeAnalysis: public IPatternTreeConsumer<T> {
    virtual ~IPatternTreeAnalysis() {}
    virtual void process(const PatternTree<T>& tree) const = 0;
    virtual PatternTree<T>& processedTree() = 0;
    virtual void reportChanges() = 0;
};

// Analysis consumers should not change the tree. They are just analyzers
// not transformers
template<typename T>
class APatternTreeAnalysis: public IPatternTreeAnalysis<T> {
public:
    APatternTreeAnalysis() : _tree(nullptr) {}//: _tree(tree) {}
    virtual ~APatternTreeAnalysis() {}
    virtual void process(const PatternTree<T>& tree) const = 0;
    virtual PatternTree<T>& processedTree() { return _tree;}
private:
    PatternTree<T>* _tree;
};

template<typename T>
class DummyAnalysis : public APatternTreeAnalysis<T> {
public:
    DummyAnalysis() : APatternTreeAnalysis<T>() {}
    void process( PatternTree<T>& tree) override { this->_tree = tree; }
    void reportChanges() { std::cout << "Nothing to report from Dummy analyzer" << std::endl; }
};

#endif // PATTERNTREECONSUMER

