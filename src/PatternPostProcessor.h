/*
 * PatternPostProcessor.h
 *
 *  Created on: Aug 11, 2015
 *      Author: heisenberg
 */

#ifndef PATTERNPOSTPROCESSOR_H_
#define PATTERNPOSTPROCESSOR_H_

using namespace pattern::matching;

namespace pattern {
namespace processing {

template<typename T>
class PatternPostProcessor {
public:
    PatternPostProcessor() {}
    virtual ~PatternPostProcessor(){}

	virtual void process() = 0;
};

template<typename T>
class AssignmentReducer: public PatternPostProcessor<T> {
public:
	AssignmentReducer(Pattern<T>& pattern);
    ~AssignmentReducer() {}

	void process();
private:
	Pattern<T>& _pattern;
};

template<typename T>
AssignmentReducer<T>::AssignmentReducer(Pattern<T>& pattern) :
		_pattern(pattern) {
}

template<typename T>
void AssignmentReducer<T>::process() {
	typename Pattern<T>::iteratorType it = _pattern.begin();
	// Implementing Erase-Remove IDIOM. Couldn't use erase(remove_if(start, end, predicate), end) because
	// my predicate needed the current element adn the next element which I couldn't figure out how to do
	// using unary predicates nor binary predicates.
	typename Pattern<T>::iteratorType result = it;
	while(it!= _pattern.end()-1) {
		OperationType currentOper = (*it)->getOpType();
		OperationType nextOper = (*++it)->getOpType();
		//std::cout << "current = " << currentOper << " | next = " << nextOper << std::endl;
		if(!(currentOper == OperationType::memoryStore && nextOper == OperationType::assign)) { // This becomes our predicate
			// add it to the element that should stay in the pattern
			*result = *(--it);
			++result;
			++it;
		}
	}
	*result = *(it); // basically we are adding the last element in the vector which we didnt process in the array
	++result;
	 // We shouldnt have a problem erasing from the vector because shared_ptrs to the operations still exist in their parent operation
	// So the reference count should stay greater than zero and these deleted operations will not be destroyed.
	_pattern.erase(result, _pattern.end());
}



} /* namespace processing */
} /* namespace pattern */

#endif /* PATTERNPOSTPROCESSOR_H_ */
