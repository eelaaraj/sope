/*
 * test2.cpp
 *
 *  Created on: Jul 13, 2015
 *      Author: heisenberg
 */
#include <iostream>
/*#include <iomanip>
#include <cmath>
#include <random>
#include <chrono>
#include <vector>
#include <map>
#include <algorithm>
#include <string>

using namespace std;*/

/*std::map<std::string, std::map<int, std::vector<std::chrono::duration<double>>>> timedUnitInnerTimes;
std::map<int, std::vector<std::chrono::duration<double>>> randomAccessInnerTimes;
std::map<int, std::vector<std::chrono::duration<double>>> croutInnerTimes;
std::map<int, std::vector<std::chrono::duration<double>>> dolittleInnerTimes;*/
// Crout uses unit diagonals for the upper triangle
void Crout(int d,double*S,double*D, float*D2){
    for(int k=0;k<d;++k){
        for(int i=k;i<d;++i){
            //++i;
            double sum=0.;
            for(int p=0;p<k;++p)sum+=D[i*d+p]*D[p*d+k];
            D[i*d+k]=S[i*d+k]-sum; // not dividing by diagonals
        }
        //int j;
        for(int j=k+1;j<d;++j){
            double sum=0.;
            for(int p=0;p<k;++p)sum+=D[k*d+p]*D[p*d+j];
            //if(D[k*d+k] < 0.01 && D[k*d+k] > -0.01) cout << " crout problem: " << D[k*d+k] << endl;
            D[k*d+j]=(S[k*d+j]-sum)/D[k*d+k];
        }
    }
}

int main() {
    Crout(100, nullptr, nullptr, nullptr);
    return 0;
}


