/*
 * test3.cpp
 *
 *  Created on: Jul 13, 2015
 *      Author: heisenberg
 */
#include &lt;iostream&gt;

/* Test loop
 * Test loop increment in binary op format
 * Test variable declaration
 * Test binary op
 * Test decl references
 */
void test1(int a, int b, int c) {
    for(int i=0; i&lt;a; i++) {
        int x = a+b;
        x = b+c;
    }
}

/*
 * In addition to test1,
 * Test nested loop
 * Test var decl without init
 */
void test2(int a, int b, int c) {
    for(int i=0; i&lt;a; i=i+1) {
        int x;
        x = a+b;
        for(int j=0; j&lt;b; j++) {
            x = b+c;
        }
        x = a+c;
    }
}

/*
 * In addition to test2,
 * Test if condition with no else
 */
void test3(int a, int b, int c) {
    for(int i=0; i&lt;a; i=i+1) {
        int x;
        x = a+b;
        if(i==5 &amp;&amp; x==3)
            for(int j=0; j&lt;b; j++) {
                x += 1;
            }
        else x+=5;
        x = a+c;
    }
}


/*
 * In addition to test3,
 * Test if-else condition
 * Test nested ifs
 */
void test4(int a, int b, int c) {
    for(int i=0; i&lt;a; i=i+1) {
        int x;
        x = a+b;
        if(i==5)
            for(int j=0; j&lt;b; j++) {
                x += 1;
            }
        else
            x+= 2;

        x = a+c;
    }
}

/*
 * In addition to test2,
 * Test if condition with else (which has one binary operation)
 */
void test5(int a, int b, int c) {
    for(int i=0; i&lt;a; i=i+1) {
        int x;
        x = a+b;
        if(i==5 &amp;&amp; x==3)
            for(int j=0; j&lt;b; j++) {
                x += 1;
            }
        else
            if(i==10)
                for(int j=1; j&lt;b; j++) {
                    x += 2;
                }
        x = a+c;
    }
}

// Crout uses unit diagonals for the upper triangle
void Crout(int d,double*S,double*D){
    for(int k=0;k&lt;d;++k){
        for(int i=k;i&lt;d;++i){
            double sum=0.;
            for(int p=0;p&lt;k;++p)sum+=D[i*d+p]*D[p*d+k];
            D[i*d+k]=S[i*d+k]-sum; // not dividing by diagonals
        }
        for(int j=k+1;j&lt;d;++j){
            double sum=0.;
            for(int p=0;p&lt;k;++p)sum+=D[k*d+p]*D[p*d+j];
            D[k*d+j]=(S[k*d+j]-sum)/D[k*d+k];
        }
    }
}

// Crout uses unit diagonals for the upper triangle
void Crout2(int d,double*S,double*D){
    for(int k=0;k&lt;d;++k){
        for(int i=k;i&lt;d;++i){
            double sum=0.;
            for(int p=0;p&lt;k;++p)sum+=D[i*d+p]*D[p*d+k];
            D[i*d+k]=S[i*d+k]-sum; // not dividing by diagonals
        }
        for(int j=k+1;j&lt;d;++j){
            double sum=0.;
            for(int p=0;p&lt;k;++p)sum+=D[k*d+p]*D[p*d+j];
            D[k*d+j]=(S[k*d+j]-sum)/D[k*d+k];
        }
    }
    for(int k=0;k&lt;d;++k){
        for(int i=k;i&lt;d;++i){
            double sum=0.;
            for(int p=0;p&lt;k;++p)sum+=D[i*d+p]*D[p*d+k];
            D[i*d+k]=S[i*d+k]-sum; // not dividing by diagonals
        }
        for(int j=k+1;j&lt;d;++j){
            double sum=0.;
            for(int p=0;p&lt;k;++p)sum+=D[k*d+p]*D[p*d+j];
            D[k*d+j]=(S[k*d+j]-sum)/D[k*d+k];
        }
    }
}

int main() {
    return 0;
}


