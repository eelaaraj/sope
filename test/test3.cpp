/*
 * test3.cpp
 *
 *  Created on: Jul 13, 2015
 *      Author: heisenberg
 */
//#include <iostream>

/* Test loop
 * Test loop increment in binary op format
 * Test variable declaration
 * Test binary op
 * Test decl references
 */
void test1(int a, int b, int c) {
    for(int i=0; i<a; i++) {
        int x = a+b;
        x = b+c;
    }
}

/*
 * In addition to test1,
 * Test nested loop
 * Test var decl without init
 */
void test2(int a, int b, int c) {
    for(int i=0; i<a; i=i+1) {
        int x;
        x = a+b;
        for(int j=0; j<b; j++) {
            x = b+c;
        }
        x = a+c;
    }
}

/*
 * In addition to test2,
 * Test if condition with no else
 */
void test3(int a, int b, int c) {
    for(int i=0; i<a; i=i+1) {
        int x;
        x = a+b;
        if(i==5 && x==3)
            for(int j=0; j<b; j++) {
                x += 1;
            }
        //else x+=5;
        x = a+c;
    }
}


/*
 * In addition to test3,
 * Test if-else condition
 * Test nested ifs
 */
void test4(int a, int b, int c) {
    for(int i=0; i<a; i=i+1) {
        int x;
        x = a+b;
        if(i==5)
            for(int j=0; j<b; j++) {
                x += 1;
            }
        else
            x+= 2;

        x = a+c;
    }
}

/*
 * In addition to test2,
 * Test if condition with else (which has one binary operation)
 */
void test5(int a, int b, int c) {
    // Will instrument START here -- Elie
for(int i=0; i<a; i=i+1) {
        int x;
        x = a+b;
        if(i==5 && x==3)
            // Will instrument START here -- Elie
for(int j=0; j<b; j++) {
                x += 1;
            }
// Will instrument STOP here -- Elie
        else
            if(i==10)
                // Will instrument START here -- Elie
for(int j=1; j<b; j++) {
                    x += 2;
                }
// Will instrument STOP here -- Elie
        x = a+c;
    }
// Will instrument STOP here -- Elie
}

// Crout uses unit diagonals for the upper triangle
void Crout2(int d,double*S,double*D){
    for(int k=0;k<d;++k){
        for(int i=k;i<d;++i){
            double sum=0.;
            for(int p=0;p<k;++p)sum+=D[i*d+p]*D[p*d+k];
            D[i*d+k]=S[i*d+k]-sum; // not dividing by diagonals
        }
        for(int j=k+1;j<d;++j){
            double sum=0.;
            for(int p=0;p<k;++p)sum+=D[k*d+p]*D[p*d+j];
            D[k*d+j]=(S[k*d+j]-sum)/D[k*d+k];
        }
    }
    for(int k=0;k<d;++k){
        for(int i=k;i<d;++i){
            double sum=0.;
            for(int p=0;p<k;++p)sum+=D[i*d+p]*D[p*d+k];
            D[i*d+k]=S[i*d+k]-sum; // not dividing by diagonals
        }
        for(int j=k+1;j<d;++j){
            double sum=0.;
            for(int p=0;p<k;++p)sum+=D[k*d+p]*D[p*d+j];
            D[k*d+j]=(S[k*d+j]-sum)/D[k*d+k];
        }
    }
}

void solveCrout(int d,double*LU,double*b,double*x){
   double y[d];
   for(int i=0;i<d;++i){
      double sum=0.;
      for(int k=0;k<i;++k)sum+=LU[i*d+k]*y[k];
      y[i]=(b[i]-sum)/LU[i*d+i];
   }
   for(int i=d-1;i>=0;--i){
      double sum=0.;
      for(int k=i+1;k<d;++k)sum+=LU[i*d+k]*x[k];
      x[i]=(y[i]-sum); // not dividing by diagonals
   }
}

// Crout uses unit diagonals for the upper triangle
void Crout(int d,double*S,double*D){
    //_capture_(d);
    for(int k=0;k<d;++k){
        //_capture_(k);
        for(int i=k;i<d;++i){
            double sum=0.;
            for(int p=0;p<k;++p)sum+=D[i*d+p]*D[p*d+k];
            D[i*d+k]=S[i*d+k]-sum; // not dividing by diagonals
        }
        for(int j=k+1;j<d;++j){
            double sum=0.;
            for(int p=0;p<k;++p)sum+=D[k*d+p]*D[p*d+j];
            D[k*d+j]=(S[k*d+j]-sum)/D[k*d+k];
        }
    }
}
class X {
   public:
    int xMemberFunc(int a, double b){
        int x = 0;
        int z = x+3;
        return z;
    }
};
// Doolittle uses unit diagonals for the lower triangle
void Doolittle(int d,double*S,double*D){
    //X x;
    //int f = x.xMemberFunc(1, 2.);
    Crout(d,S,D);
   for(int k=0;k<d;++k){
      for(int j=k;j<d;++j){
         double sum=0.;
         for(int p=0;p<k;++p)sum+=D[k*d+p]*D[p*d+j];
         D[k*d+j]=(S[k*d+j]-sum); // not dividing by diagonals
      }
      for(int i=k+1;i<d;++i){
         double sum=0.;
         for(int p=0;p<k;++p)sum+=D[i*d+p]*D[p*d+k];
         D[i*d+k]=(S[i*d+k]-sum)/D[k*d+k];
      }
   }
}

void solveDoolittle(int d,double*LU,double*b,double*x){
   double y[d];
   for(int i=0;i<d;++i){
      double sum=0.;
      for(int k=0;k<i;++k)sum+=LU[i*d+k]*y[k];
      y[i]=(b[i]-sum); // not dividing by diagonals
   }
   for(int i=d-1;i>=0;--i){
      double sum=0.;
      for(int k=i+1;k<d;++k)sum+=LU[i*d+k]*x[k];
      x[i]=(y[i]-sum)/LU[i*d+i];
   }
}

/* This following test with the 4 functions below, will allow us to
 * test the DEF-USE technique. The call to a function that takes a
 * pointer as an argument (such as function2 in mainFunction) will
 * result in a DEFinition for that argument. Any use of that variable
 * thereon will be a USE of that Definition and not a previous definition
 */
#include <string>
double function1(int a, double b) {
 return a + b ;
}

double function3(double a, const double* const b) {
 return a + *b ;
}

double function2(double a, double* b) {
 *b = *b *2;
 return a + *b ;
}

int mainFunction(int argc, const char** argv) {
    if(argc<3)
        return 1;
    int a = std::stoi(argv[1]);
    double b = std::stod(argv[2]);
    double* pb = &b;

    double f = function3(a, &b);
    double c = function1(a, b);

    double d = function1(a, *pb);

    double e = function2(c, pb);

    printf("%p\n", pb);
    printf("%p\n", pb);
    printf("%f\n", *pb);
    printf("%f\n", c);
    printf("%f\n", d);
    return 0;
}

int main() {
    return 0;
}



