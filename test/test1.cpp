/*
 * test1.cpp
 *
 *  Created on: Jul 13, 2015
 *      Author: heisenberg
 */

#include <iostream>
#include <iomanip>
#include <string>

using namespace std;
// These functions print matrices and vectors in a nice format

void coutVector(int d,double*v){
    cout<<'\n';
    for(int j=0;j<d;++j)
        cout<<setw(14)<<v[j];
    cout<<'\n';
}

void coutMatrix(int d,double*m){
    cout<<'\n';
    for(int i=0;i<d;++i) {
        coutVector(d,&m[i*d]);
        for(int j=0;j<d;++j)
            cout<<setw(14)<<m[i*d+j];
        cout<<'\n';
    }
}


