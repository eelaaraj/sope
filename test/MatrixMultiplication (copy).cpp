/*
 * MatrixMultiplication.cpp
 *
 *  Created on: Jun 4, 2015
 *      Author: heisenberg
 */

#include <iostream>
#include <iomanip>
#include <cmath>
#include <random>
#include <chrono>
#include <vector>
#include <map>
#include <algorithm>
#include <string>

struct Sum {
	Sum() { sum = std::chrono::duration<double>::zero(); }
	void operator()(std::chrono::duration<double> elapsed) { sum+=elapsed;}
	std::chrono::duration<double> sum;
};

void multiply(double* A, double* B, double* C, int aRows, int aCols, int bRows, int bCols, int cRows, int cCols) {
	for (int i = 0; i < cRows; i++) {
		for (int j = 0; j < cCols; j++) {
			int cCurrent = i * cCols + j;
			//if(matSizeInc > 110)
			//   std::cout << "cCurrent = " << cCurrent << std::endl;
			for (int k = 0; k < aCols; k++) {
				//int aIndex = i*aCols + k;
				//int bIndex = k*cCols + j;
				/*if(matSizeInc>110) {
				 //std::cout << "HERE" << std::endl;
				 std::cout << "k = " << k << " | " ;
				 std::cout << aIndex << " | " << bIndex << " | ";
				 std::cout << aSize << " | " << bSize << std::endl;
				 }*/
				//if(dummy < 10000)
				C[cCurrent] += A[i * aCols + k] * B[k * cCols + j];
				// dummy++;
			}
		}
	}
}


int main(int argc, char* argv[]){
   // the following checks are published at:
   // http://math.fullerton.edu/mathews/n2003/CholeskyMod.html

   // We want to solve the system Ax=b

  /* double A[25]={2.,1.,1.,3.,2.,
                 1.,2.,2.,1.,1.,
                 1.,2.,9.,1.,5.,
                 3.,1.,1.,7.,1.,
                 2.,1.,5.,1.,8.};*/

   int iterations = 1500;
   int increment = 5; // Increment the size of the matrix by 5 each time
   int maxMatrixSize = 5000;

   for(int matSizeInc = 0 ; matSizeInc < maxMatrixSize ; matSizeInc+=increment) {
   /*int aRows = std::stoi(argv[1], nullptr, 10);
   int aCols = std::stoi(argv[2], nullptr, 10);
   int bRows = std::stoi(argv[3], nullptr, 10);
   int bCols = std::stoi(argv[4], nullptr, 10);*/
   int aRows = 1;
   int aCols = 1 + matSizeInc;
   int bRows = 1 + matSizeInc;
   int bCols = 1;

   int aSize = aRows * aCols;
   int bSize = bRows * bCols;
   double* A = new double[aSize];
   double* B= new double[bSize];
   int cSize = aRows * bCols;
   double* C = new double[cSize];
   int& cRows = aRows;
   int& cCols = bCols;

   std::vector<std::chrono::duration<double>> total;

   for(int iter=0; iter <iterations; iter++) {

	   for(int i=0; i<aSize; i++) {
		   A[i] = rand() % 10;
	   }
	   for(int i=0 ; i < bSize; i++) {
		   B[i] = rand() % 10;
	   }

	   int dummy = 0;
	   std::chrono::time_point<std::chrono::system_clock> start, end;
	   start = std::chrono::system_clock::now();
	   // The actual multiplication
	   // Can be done maybe slightly more efficiently if we c(0,0), then c(1,0) because we will load columns of B in memory
	   // which will be better because then loading each row of A will use locality of reference and load all the column or most
	   // of it. While if we do c(0,0), c(0,1) then we should load one row of A and then load each column from B which does not
	   // make use of locailty of reference.
	   /*if(matSizeInc >110) {
		   std::cout << "matsizeInc = " << matSizeInc << std::endl;
		   std::cout << "aRows = " << aRows << " | "<< "aCols = " << aCols << "| " << "bCols = " << bCols<< std::endl;
		   std::cout << "cRows = " << cRows << " | "<< "cCols = " << cCols << std::endl;
	   }*/

	   for(int i=0; i<cRows; i++) {
		   for(int j=0; j<cCols; j++) {
			   int cCurrent = i*cCols + j;
			   //if(matSizeInc > 110)
				//   std::cout << "cCurrent = " << cCurrent << std::endl;
			   for(int k=0; k < aCols; k++) {
				   //int aIndex = i*aCols + k;
				   //int bIndex = k*cCols + j;
				   /*if(matSizeInc>110) {
					   //std::cout << "HERE" << std::endl;
					   std::cout << "k = " << k << " | " ;
					   std::cout << aIndex << " | " << bIndex << " | ";
					   std::cout << aSize << " | " << bSize << std::endl;
				   }*/
				   //if(dummy < 10000)
					   C[cCurrent] += A[i*aCols + k] * B[k*cCols + j];
				  // dummy++;
			   }
		   }
	   }
	   end = std::chrono::system_clock::now();
	   std::chrono::duration<double> elapsed = end-start;
	   total.push_back(elapsed);

   }

   Sum multSum = for_each(total.begin(), total.end(), Sum());

   double runTimeAverage = multSum.sum.count()/iterations;
   //std::cout << "Iterations = " << iterations << std::endl;
   //std::cout << "--------------------------------------" << std::endl;
   //std::cout << "A[" << aRows << "," << aCols << "] * B[" << bRows << "," << bCols << "]" << std::endl;
   //std::cout << "Multiplication run-time average: " << runTimeAverage << std::endl;
   std::cout << matSizeInc+1 << " ; " << runTimeAverage << std::endl;

   delete[] A;
   delete[] B;
   }
}



