//#include <gsl/gsl_math.h>
//#include <gsl/gsl_cblas.h>
#include "gsl/gsl_math.h"
#include "gsl/gsl_cblas.h"
#include "cblas.h"

// sgemm includes
#include "error_cblas_l3.h"
#include "error_cblas_l2.h"

#include <iostream>

class IWrapper;

extern "C" {
IWrapper* _metric_capture_long_double_timer_setup_(const std::string& fileName, const std::string& functionName, int loopCount);
void _metric_capture_start_(IWrapper* capturer, int loopId);
void _metric_capture_stop_(IWrapper* capturer, int loopId);
void _metric_capture_cleanup_(IWrapper* capturer);
}

void
cblas_sdsdot (const int N, const float alpha, const float *X, const int incX,
              const float *Y, const int incY)
{
#define INIT_VAL  alpha
#define ACC_TYPE  double
#define BASE float
    {
      ACC_TYPE r = INIT_VAL;
      INDEX i;
      //INDEX ix = OFFSET(N, incX);
      //INDEX iy = OFFSET(N, incY);
      INDEX ix; //= OFFSET(N, incX);
      INDEX iy; //= OFFSET(N, incY);

      for (i = 0; i < N; i++) {
        r += X[ix] * Y[iy];
        ix += incX;
        iy += incY;
      }

      //return r;
    }
#undef ACC_TYPE
#undef BASE
#undef INIT_VAL
}

void cblas_sgemv (const enum CBLAS_ORDER order, const enum CBLAS_TRANSPOSE TransA,
             const int M, const int N, const float alpha, const float *A,
             const int lda, const float *X, const int incX, const float beta,
             float *Y, const int incY)
{
    #define BASE float
    {
      INDEX i, j;
      INDEX lenX, lenY;

      const int Trans = (TransA != CblasConjTrans) ? TransA : CblasTrans;

      CHECK_ARGS12(GEMV,order,TransA,M,N,alpha,A,lda,X,incX,beta,Y,incY);

      if (M == 0 || N == 0)
        return;

      if (alpha == 0.0 && beta == 1.0)
        return;

      if (Trans == CblasNoTrans) {
        lenX = N;
        lenY = M;
      } else {
        lenX = M;
        lenY = N;
      }

      /* form  y := beta*y */
      if (beta == 0.0) {
        INDEX iy = OFFSET(lenY, incY);
        for (i = 0; i < lenY; i++) {
          Y[iy] = 0.0;
          iy += incY;
        }
      } else if (beta != 1.0) {
        INDEX iy = OFFSET(lenY, incY);
        for (i = 0; i < lenY; i++) {
          Y[iy] *= beta;
          iy += incY;
        }
      }

      if (alpha == 0.0)
        return;

      if ((order == CblasRowMajor && Trans == CblasNoTrans)
          || (order == CblasColMajor && Trans == CblasTrans)) {
        /* form  y := alpha*A*x + y */
        INDEX iy = OFFSET(lenY, incY);
        for (i = 0; i < lenY; i++) {
          BASE temp = 0.0;
          INDEX ix = OFFSET(lenX, incX);
          for (j = 0; j < lenX; j++) {
            temp += X[ix] * A[lda * i + j];
            ix += incX;
          }
          Y[iy] += alpha * temp;
          iy += incY;
        }
      } else if ((order == CblasRowMajor && Trans == CblasTrans)
                 || (order == CblasColMajor && Trans == CblasNoTrans)) {
        /* form  y := alpha*A'*x + y */
        INDEX ix = OFFSET(lenX, incX);
        for (j = 0; j < lenX; j++) {
          const BASE temp = alpha * X[ix];
          if (temp != 0.0) {
            INDEX iy = OFFSET(lenY, incY);
            for (i = 0; i < lenY; i++) {
              Y[iy] += temp * A[lda * j + i];
              iy += incY;
            }
          }
          ix += incX;
        }
      } else {
        BLAS_ERROR("unrecognized operation");
      }
    }
#undef BASE
}

void
cblas_sgemm (const enum CBLAS_ORDER Order, const enum CBLAS_TRANSPOSE TransA,
             const enum CBLAS_TRANSPOSE TransB, const int M, const int N,
             const int K, const float alpha, const float *A, const int lda,
             const float *B, const int ldb, const float beta, float *C,
             const int ldc)
{
#define BASE float
    {
      INDEX i, j, k;
      INDEX n1, n2;
      INDEX ldf, ldg;
      int TransF, TransG;
      const BASE *F, *G;

      CHECK_ARGS14(GEMM,Order,TransA,TransB,M,N,K,alpha,A,lda,B,ldb,beta,C,ldc);

      if (alpha == 0.0 && beta == 1.0)
        return;

      if (Order == CblasRowMajor) {
        n1 = M;
        n2 = N;
        F = A;
        ldf = lda;
        TransF = (TransA == CblasConjTrans) ? CblasTrans : TransA;
        G = B;
        ldg = ldb;
        TransG = (TransB == CblasConjTrans) ? CblasTrans : TransB;
      } else {
        n1 = N;
        n2 = M;
        F = B;
        ldf = ldb;
        TransF = (TransB == CblasConjTrans) ? CblasTrans : TransB;
        G = A;
        ldg = lda;
        TransG = (TransA == CblasConjTrans) ? CblasTrans : TransA;
      }

      /* form  y := beta*y */
      if (beta == 0.0) {
        for (i = 0; i < n1; i++) {
          for (j = 0; j < n2; j++) {
            C[ldc * i + j] = 0.0;
          }
        }
      } else if (beta != 1.0) {
        for (i = 0; i < n1; i++) {
          for (j = 0; j < n2; j++) {
            C[ldc * i + j] *= beta;
          }
        }
      }

      if (alpha == 0.0)
        return;

      if (TransF == CblasNoTrans && TransG == CblasNoTrans) {
          IWrapper* wrapper = _metric_capture_long_double_timer_setup_("TestSentinelMatching/test_blass.cpp","cblas_sgemm", 3);
        /* form  C := alpha*A*B + C */
          _metric_capture_start_(wrapper,0);
        for (k = 0; k < K; k++) {
          //_metric_capture_start_(wrapper,1);
          for (i = 0; i < n1; i++) {
            const BASE temp = alpha * F[ldf * i + k];
             //_metric_capture_start_(wrapper,2);
            if (temp != 0.0) {
              for (j = 0; j < n2; j++) {
                C[ldc * i + j] += temp * G[ldg * k + j];
              }
            }
            //_metric_capture_stop_(wrapper, 2);
          }
          //_metric_capture_stop_(wrapper, 1);
        }
        _metric_capture_stop_(wrapper, 0);
        _metric_capture_cleanup_(wrapper);

      } else if (TransF == CblasNoTrans && TransG == CblasTrans) {

        /* form  C := alpha*A*B' + C */

        for (i = 0; i < n1; i++) {
          for (j = 0; j < n2; j++) {
            BASE temp = 0.0;
            for (k = 0; k < K; k++) {
              temp += F[ldf * i + k] * G[ldg * j + k];
            }
            C[ldc * i + j] += alpha * temp;
          }
        }

      } else if (TransF == CblasTrans && TransG == CblasNoTrans) {

        for (k = 0; k < K; k++) {
          for (i = 0; i < n1; i++) {
            const BASE temp = alpha * F[ldf * k + i];
            if (temp != 0.0) {
              for (j = 0; j < n2; j++) {
                C[ldc * i + j] += temp * G[ldg * k + j];
              }
            }
          }
        }

      } else if (TransF == CblasTrans && TransG == CblasTrans) {

        for (i = 0; i < n1; i++) {
          for (j = 0; j < n2; j++) {
            BASE temp = 0.0;
            for (k = 0; k < K; k++) {
              temp += F[ldf * k + i] * G[ldg * j + k];
            }
            C[ldc * i + j] += alpha * temp;
          }
        }

      } else {
        BLAS_ERROR("unrecognized operation");
      }
    }
#undef BASE
}

void
cblas_sgemm1 (const enum CBLAS_ORDER Order, const enum CBLAS_TRANSPOSE TransA,
             const enum CBLAS_TRANSPOSE TransB, const int M, const int N,
             const int K, const float alpha, const float *A, const int lda,
             const float *B, const int ldb, const float beta, float *C,
             const int ldc)
{
#define BASE float
    {
        INDEX i, j, k;
        INDEX n1, n2;
        INDEX ldf, ldg;
        // Elie: added this because my parser does not parse function parameters and
        // therefore can't add them to the scopetracker.variablesChanges. Thus the
        // entry for ldc_paramDecl will be null in the map and will crash the arraySubOperation
        // when it tries to deduce the access type of ldc_paramDecl
        //INDEX ldc = ldc_paramVarDecl;
        int TransF, TransG;
        const BASE *F, *G;

//        if (alpha == 0.0 && beta == 1.0)
//          return;

        if (Order == CblasRowMajor) {
          n1 = M;
          n2 = N;
          F = A;
          ldf = lda;
          //TransF = (TransA == CblasConjTrans) ? CblasTrans : TransA;
          G = B;
          ldg = ldb;
          //TransG = (TransB == CblasConjTrans) ? CblasTrans : TransB;
        } else {
            n1 = N;
            n2 = M;
            F = B;
            ldf = ldb;
            //TransF = (TransB == CblasConjTrans) ? CblasTrans : TransB;
            G = A;
            ldg = lda;
            //TransG = (TransA == CblasConjTrans) ? CblasTrans : TransA;
          }
        /* form  y := beta*y */
        if (beta == 0.0) {
          for (i = 0; i < n1; i++) {
            for (j = 0; j < n2; j++) {
              C[ldc * i + j] = 0.0;
            }
          }
        } else if (beta != 1.0) {
          for (i = 0; i < n1; i++) {
            for (j = 0; j < n2; j++) {
              C[ldc * i + j] *= beta;
            }
          }
        }

//        if (alpha == 0.0)
//          return;

        if (TransF == CblasNoTrans && TransG == CblasNoTrans) {

          /* form  C := alpha*A*B + C */

          for (k = 0; k < K; k++) {
            for (i = 0; i < n1; i++) {
              const BASE temp = alpha * F[ldf * i + k];
              if (temp != 0.0) {
                for (j = 0; j < n2; j++) {
                  C[ldc * i + j] += temp * G[ldg * k + j];
                }
              }
            }
          }

        } else if (TransF == CblasNoTrans && TransG == CblasTrans) {

          /* form  C := alpha*A*B' + C */

          for (i = 0; i < n1; i++) {
            for (j = 0; j < n2; j++) {
              BASE temp = 0.0;
              for (k = 0; k < K; k++) {
                temp += F[ldf * i + k] * G[ldg * j + k];
              }
              C[ldc * i + j] += alpha * temp;
            }
          }

        } else if (TransF == CblasTrans && TransG == CblasNoTrans) {

          for (k = 0; k < K; k++) {
            for (i = 0; i < n1; i++) {
              const BASE temp = alpha * F[ldf * k + i];
              if (temp != 0.0) {
                for (j = 0; j < n2; j++) {
                  C[ldc * i + j] += temp * G[ldg * k + j];
                }
              }
            }
          }

        } else if (TransF == CblasTrans && TransG == CblasTrans) {

          for (i = 0; i < n1; i++) {
            for (j = 0; j < n2; j++) {
              BASE temp = 0.0;
              for (k = 0; k < K; k++) {
                temp += F[ldf * k + i] * G[ldg * j + k];
              }
              C[ldc * i + j] += alpha * temp;
            }
          }

        } //else {
          //BLAS_ERROR("unrecognized operation");
        //}
    }
#undef BASE
}

void
cblas_bkp_sgemm (const enum CBLAS_ORDER Order, const enum CBLAS_TRANSPOSE TransA,
             const enum CBLAS_TRANSPOSE TransB, const int M, const int N,
             const int K, const float alpha, const float *A, const int lda,
             const float *B, const int ldb, const float beta, float *C,
             const int ldc_paramVarDecl)
{
#define BASE float
    {
      INDEX i, j, k;
      INDEX n1, n2;
      INDEX ldf, ldg;
      // Elie: added this because my parser does not parse function parameters and
      // therefore can't add them to the scopetracker.variablesChanges. Thus the
      // entry for ldc_paramDecl will be null in the map and will crash the arraySubOperation
      // when it tries to deduce the access type of ldc_paramDecl
      INDEX ldc = ldc_paramVarDecl;
      int TransF, TransG;
      const BASE *F, *G;

      //CHECK_ARGS14(GEMM,Order,TransA,TransB,M,N,K,alpha,A,lda,B,ldb,beta,C,ldc);

      if (alpha == 0.0 && beta == 1.0)
        return;

      if (Order == CblasRowMajor) {
        n1 = M;
        n2 = N;
        F = A;
        ldf = lda;
        TransF = (TransA == CblasConjTrans) ? CblasTrans : TransA;
        G = B;
        ldg = ldb;
        TransG = (TransB == CblasConjTrans) ? CblasTrans : TransB;
      } else {
        n1 = N;
        n2 = M;
        F = B;
        ldf = ldb;
        TransF = (TransB == CblasConjTrans) ? CblasTrans : TransB;
        G = A;
        ldg = lda;
        TransG = (TransA == CblasConjTrans) ? CblasTrans : TransA;
      }

      /* form  y := beta*y */
      if (beta == 0.0) {
        for (i = 0; i < n1; i++) {
          for (j = 0; j < n2; j++) {
            C[ldc * i + j] = 0.0;
          }
        }
      } else if (beta != 1.0) {
        for (i = 0; i < n1; i++) {
          for (j = 0; j < n2; j++) {
            C[ldc * i + j] *= beta;
          }
        }
      }

      if (alpha == 0.0)
        return;

      if (TransF == CblasNoTrans && TransG == CblasNoTrans) {

        /* form  C := alpha*A*B + C */

        for (k = 0; k < K; k++) {
          for (i = 0; i < n1; i++) {
            const BASE temp = alpha * F[ldf * i + k];
            if (temp != 0.0) {
              for (j = 0; j < n2; j++) {
                C[ldc * i + j] += temp * G[ldg * k + j];
              }
            }
          }
        }

      } else if (TransF == CblasNoTrans && TransG == CblasTrans) {

        /* form  C := alpha*A*B' + C */

        for (i = 0; i < n1; i++) {
          for (j = 0; j < n2; j++) {
            BASE temp = 0.0;
            for (k = 0; k < K; k++) {
              temp += F[ldf * i + k] * G[ldg * j + k];
            }
            C[ldc * i + j] += alpha * temp;
          }
        }

      } else if (TransF == CblasTrans && TransG == CblasNoTrans) {

        for (k = 0; k < K; k++) {
          for (i = 0; i < n1; i++) {
            const BASE temp = alpha * F[ldf * k + i];
            if (temp != 0.0) {
              for (j = 0; j < n2; j++) {
                C[ldc * i + j] += temp * G[ldg * k + j];
              }
            }
          }
        }

      } else if (TransF == CblasTrans && TransG == CblasTrans) {

        for (i = 0; i < n1; i++) {
          for (j = 0; j < n2; j++) {
            BASE temp = 0.0;
            for (k = 0; k < K; k++) {
              temp += F[ldf * k + i] * G[ldg * j + k];
            }
            C[ldc * i + j] += alpha * temp;
          }
        }

      } else {
        //BLAS_ERROR("unrecognized operation");
      }
    }
#undef BASE
}

#include <iostream>
void printArray(float* arr, int rows, int cols) {
    for(int i=0; i<rows; i++) {
        for(int j=0; j<cols; j++)
            std::cout << arr[cols*i + j] << "  ";
        std::cout << "\n";
    }
}

#include <cmath>

float* create_hilbert_matrix_float(unsigned long size)
{
  unsigned long i, j;
  float* hilb = new float[size * size];
  for(i=0; i<size; i++) {
    for(j=0; j<size; j++) {
      hilb[i*size+j] = 1.0/(i+j+1.0);
    }
  }
  return hilb;
}

float* create_random_vector_float(unsigned long size)
{
  srand(time(NULL));
  unsigned long i, j;
  float* randVec = new float[size];
  for(i=0; i<size; i++) {
      randVec[i] = 1.0/((rand()%size)+1.0);
  }
  return randVec;
}

float * create_vandermonde_matrix_float(unsigned long size)
{
  unsigned long i, j;
  float* vander = new float[size * size];
  for(i=0; i<size; i++) {
    for(j=0; j<size; j++) {
      vander[i *size + j] =  pow(i + 1.0, size - j - 1.0);
    }
  }
  return vander;
}

int main (int argc, char* argv[]) {

// Below Just a reminder
//    enum CBLAS_ORDER {CblasRowMajor=101, CblasColMajor=102};
//    enum CBLAS_TRANSPOSE {CblasNoTrans=111, CblasTrans=112, CblasConjTrans=113};
//    enum CBLAS_UPLO {CblasUpper=121, CblasLower=122};
//    enum CBLAS_DIAG {CblasNonUnit=131, CblasUnit=132};
//    enum CBLAS_SIDE {CblasLeft=141, CblasRight=142};

    int startSGEMV = 1;
    int endSGEMV = 30;
    int startSGEMM = 1;
    int endSGEMM = 10;
    if(argc>4) {
        startSGEMV = std::stoi(argv[1]);
        endSGEMV = std::stoi(argv[2]);
        startSGEMM = std::stoi(argv[3]);
        endSGEMM = std::stoi(argv[4]);
    }
    // Running sgemv
    {
        //int order = 101;
        //int trans = 111;
        CBLAS_ORDER order = CBLAS_ORDER::CblasRowMajor;
        CBLAS_TRANSPOSE trans = CBLAS_TRANSPOSE::CblasNoTrans;
        int M = 1;
        int N = 1;
        int lda = 1;
        float alpha = 1.0f;
        float beta = -0.3f;
        float A[] = { -0.805f };
        float X[] = { -0.965f };
        int incX = -1;
        float Y[] = { 0.537f };
        int incY = -1;
        //float y_expected[] = { 0.615725f };
        cblas_sgemv(order, trans, M, N, alpha, A, lda, X, incX, beta, Y, incY);
        printArray(Y, 1 ,1);
    }
    {
        CBLAS_ORDER order = CBLAS_ORDER::CblasRowMajor;
        CBLAS_TRANSPOSE trans = CBLAS_TRANSPOSE::CblasNoTrans;
        int M = 3;
        int N = 2;
        int lda = 2;
        float alpha = 1.0f;
        float beta =  0.0f;
        float A[] = { 0.5f, 0.7f, 0.3f, -0.2f, -0.1f, 0.6f };
        float X[] = { -0.25f, 0.41f };
        int incX = 1;
        float Y[] = { 0.0f, 0.0f, 0.0f };
        int incY = 1;
        //float y_expected[] = { 0.162f, -0.157f, 0.271f };
        cblas_sgemv(order, trans, M, N, alpha, A, lda, X, incX, beta, Y, incY);
        printArray(Y, 3, 1);
    }

    // Using matrix multiplication sgemm on the same example as matrix-vector multiplication sgemv.
    {
        CBLAS_ORDER order = CBLAS_ORDER::CblasRowMajor;
        CBLAS_TRANSPOSE transA = CBLAS_TRANSPOSE::CblasNoTrans;
        CBLAS_TRANSPOSE transB = CBLAS_TRANSPOSE::CblasNoTrans;
        int M = 3;
        int N = 1;
        int K = 2;
        float alpha = 1.0f;
        float beta = 0.0f;
        float A[] = { 0.5f, 0.7f, 0.3f, -0.2f, -0.1f, 0.6f };
        float X[] = { -0.25f, 0.41f };
        int lda = 2;
        int ldb = 1;
        float Y[] = { 0.0f, 0.0f, 0.0f };
        int ldc = 1;
        //float y_expected[] = { 0.162f, -0.157f, 0.271f };
        cblas_sgemm(order, transA, transB, M, N, K, alpha, A, lda, X, ldb, beta, Y, ldc);
        printArray(Y, 3, 1);
    }


    // Running sgemm

    //const double flteps = 1e-4, dbleps = 1e-6;
    {
        CBLAS_ORDER order = CBLAS_ORDER::CblasRowMajor;
        CBLAS_TRANSPOSE transA = CBLAS_TRANSPOSE::CblasNoTrans;
        CBLAS_TRANSPOSE transB = CBLAS_TRANSPOSE::CblasNoTrans;
        int M = 1;
        int N = 2;
        int K = 4;
        float alpha = 1.0f;
        float beta = 0.0f;
        float A[] = { 0.199f, 0.237f, 0.456f, 0.377f };
        int lda = 4;
        float B[] = { 0.842f, -0.734f, 0.323f, -0.957f, -0.303f, -0.873f, -0.871f, -0.819f };
        int ldb = 2;
        float C[] = { 0.498f, -0.925f };
        int ldc = 2;
        //float C_expected[] = { -0.222426f, -1.07973f };
        cblas_sgemm(order, transA, transB, M, N, K, alpha, A, lda, B, ldb, beta, C, ldc);
        printArray(C, 1,2);
    }
    std::cout << "\n--- SGEMV benchmarks ---\n";
    std::cout << "-------------------------\n";
    // SGEMV
    for(int i=startSGEMV; i<=endSGEMV; i++) {
        int matrixSize = i*100;
        float *A = create_hilbert_matrix_float(matrixSize);
        float *X = create_random_vector_float(matrixSize);
        float *Y = new float[matrixSize];
        CBLAS_ORDER order = CBLAS_ORDER::CblasRowMajor;
        CBLAS_TRANSPOSE trans = CBLAS_TRANSPOSE::CblasNoTrans;
        int M = matrixSize;
        int N = matrixSize;
        int lda = matrixSize;
        float alpha = 1.0f;
        float beta =  0.0f;
        int incX = 1;
        int incY = 1;
        std::cout << matrixSize << " x 1 | ";
        IWrapper* wrapper = _metric_capture_long_double_timer_setup_("TestSentinelMatching/test_blass.cpp","main/cblas_sgemv", 1);
        _metric_capture_start_(wrapper,0);
        cblas_sgemv(order, trans, M, N, alpha, A, lda, X, incX, beta, Y, incY);
        _metric_capture_stop_(wrapper,0);
        _metric_capture_cleanup_(wrapper);
        delete [] A;
        delete [] X;
        delete [] Y;
    }
    std::cout << "\n--- SGEMM benchmarks ---\n";
    std::cout << "-------------------------\n";
    // SGEMM
    for(int i=startSGEMM; i<=endSGEMM; i++) {
        int matrixSize = i*100;
        // Another timer to check how much hilbert and vandermonde matrix creation takes.
        //IWrapper* matric_create_wrapper = _metric_capture_long_double_timer_setup_("TestSentinelMatching/test_blass.cpp","create_matrices", 2);
        //_metric_capture_start_(matric_create_wrapper,0);

        float *A = create_hilbert_matrix_float(matrixSize);

        //_metric_capture_stop_(matric_create_wrapper, 0);
        //_metric_capture_start_(matric_create_wrapper,1);

        float *B = create_vandermonde_matrix_float(matrixSize);

        //_metric_capture_stop_(matric_create_wrapper, 1);
        //_metric_capture_cleanup_(matric_create_wrapper);

        for(int reps=0; reps<100; reps++) {
            float *C = new float[matrixSize*matrixSize];
            CBLAS_ORDER order = CBLAS_ORDER::CblasRowMajor;
            CBLAS_TRANSPOSE transA = CBLAS_TRANSPOSE::CblasNoTrans;
            CBLAS_TRANSPOSE transB = CBLAS_TRANSPOSE::CblasNoTrans;
            int M = matrixSize;
            int N = matrixSize;
            int K = matrixSize;
            float alpha = 1.0f;
            float beta = 0.0f;
            int lda = matrixSize;
            int ldb = matrixSize;
            int ldc = matrixSize;

            std::cout << matrixSize << " x " << matrixSize << "\n";
            // Timer to time SGEMM
            //IWrapper* wrapper = _metric_capture_long_double_timer_setup_("TestSentinelMatching/test_blass.cpp","main/cblas_sgemm", 1);
            //_metric_capture_start_(wrapper,0);
            cblas_sgemm(order, transA, transB, M, N, K, alpha, A, lda, B, ldb, beta, C, ldc);
            //_metric_capture_stop_(wrapper,0);
            //_metric_capture_cleanup_(wrapper);
            delete [] C;
        }
        delete [] A;
        delete [] B;        
    }

    return 0;
}

